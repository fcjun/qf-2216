const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin"); // html 处理插件

module.exports = {
  mode: "development", //"development",//production
  entry: "./src/main.js",
  output: {
    //出口
    path: path.resolve(__dirname, "dist"),
    filename: "index_[hash].js",
    // hash 是一个变量 将源代码产生一个hash字符串内容不变 hash串不变
    // name: 多入口的key值
    clean: true, // 将之前的打包文件清除
  },
  //处理特殊模块
  module: {
    rules: [
      // 处理css css-loader style-loader loader的顺序是从后往前
      {
        test: /\.css$/,
        exclude: /(node_modules|bower_components)/, // 取出某些目录不检测
        use: ["style-loader", "css-loader"],
      },
      // 处理less
      {
        test: /\.less$/,
        exclude: /(node_modules|bower_components)/, // 取出某些目录不检测
        use: ["style-loader", "css-loader", "less-loader"],
      },
      // 处理图片资源  webpack4 file-loader path-loader  webpack5直接可以使用
      {
        exclude: /(node_modules|bower_components)/, // 取出某些目录不检测
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: "asset/inline",
      },
      // 通过插件解析jsx
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/, // 取出某些目录不检测
        use: {
          loader: "babel-loader", //使用loader 解析js文件
          options: {
            // presets: ["@babel/preset-env", "@babel/preset-react"],
            presets: ["@babel/preset-env"],
            plugins: ["@babel/plugin-transform-react-jsx"],
          },
        },
      },
    ],
  },
  // webpack插件
  plugins: [
    // 打包的时候讲html从模版读取插入打包后的js文件，放到出口目录下
    new HtmlWebpackPlugin({
      title: "My App",
      filename: "index.html",
      template: "./public/index.html", //模版文件路径
    }),
  ],
  // 本地服务器配置
  devServer: {
    static: "./dist", // 本地服务器访问的目录
    port: 2333, // 端口号
    proxy: {}, // 代理
  },
};
// index_8f66a8b08b03b95c3a12.js
// index_8f66a8b08b03b95c3a12.js
// index_076de6cb258395e508a7.js
// 浏览器缓存策略 协商缓存 强缓存
