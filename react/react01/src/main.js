import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

// 将HelloMessage 组件渲染到 挂载到id为app的元素伤
ReactDOM.render(<App />, document.getElementById("app"));
