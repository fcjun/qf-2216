import React from "react";
import StateDemo from "./Demo/StateDemo.jsx";
import SetStateDemo from "./Demo/SetStateDemo.jsx";
import AttrBindDemo from "./Demo/AttrBindDemo.jsx";
import  VifDemo from "./Demo/VIfDemo.jsx"
import VforDemo from "./Demo/VforDemo.jsx"
class App extends React.Component {
  render() {
    return (
      <div>
        {/* <StateDemo></StateDemo> */}
        {/* <SetStateDemo></SetStateDemo> */}
        {/* <AttrBindDemo></AttrBindDemo> */}
        {/* <VifDemo></VifDemo> */}
        <VforDemo></VforDemo>
      </div>
    );
  }
  // jsx中只允许有一个根元素
}

export default App;

/*
react 中的组件
1. 声明类 类名就是组件名
2. 在类里写一个render函数函数jsx （vue template）
3. export defautl 抛出组件
4. 把组件名当成标签名使用

*/
