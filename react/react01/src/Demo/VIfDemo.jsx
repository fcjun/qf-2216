import React, { Component } from "react";
import "./test.less";
class VifDemo extends Component {
  state = {
    show: true,
    name:123,
  };
  toggle = () => {
    this.setState({ show: !this.state.show });
  };
  renderHehe = () => {
    if(this.state.show) {
      return (<div className="red">||</div>)
    }
    return
  }
  render() {
    return (
      <div>
        <h1>条件渲染</h1>
        {/* {this.state.show || <div className="red">||</div>}
        {this.state.show && <div className="blue">&&</div>} */}
        {/* {this.state.show ? <div className="blue">&&</div> : false} */}
        {/* 渲染函数 */}
        { this.renderHehe() } 
        <button onClick={this.toggle}>toggle</button>
      </div>
    );
  }
}
export default VifDemo;
/*
react 没有v-if 
表达式 ：通过 ||  &&  三元表达式实现
渲染函数： 在函数内部根据判断return不同的东西
*/
