import React, { Component } from "react";
class StateDemo extends Component {
  state = {
    name: "韩梅梅",
    num: 1,
    bool: true,
    arr: [1,2,3,[4,5,[6,7]]],
    obj: {us:123,ps:456},
    unde: undefined,
    nul: null
  };
  render() {
    console.log(this);
    return (
      <div>
        <h1>1.State demo</h1>
        {this.state.name} 
        <hr />
        {this.state.num}
        <hr />
        {this.state.bool ? "真": "假"}
        <hr />
        {this.state.arr}
        <hr />
        {this.state.obj.us}
        <hr />
        {this.state.undefined || "heh"}
        <hr />
        {this.state.nul}
      </div>
    );
  }
}
export default StateDemo;
/*
基础 string  num  直接使用
真假 undefined boolean null 在页面不显示 需要用到表达式
特殊  arr  不管多少级 平铺展开
     obj 不能直接在视图上使用

*/ 