import React, { Component } from "react";
import "./test.less";
class VforDemo extends Component {
  state = {
    arrs: ["咒怨",'山村老师',"午夜凶铃","贞子大战伽椰子"],
    // arrs: ["<li>咒怨<li>",'<li>山村老师</li>',"午夜凶铃","贞子大战伽椰子"]
    // arrs: [<li>咒怨<li>,<li>山村老师</li>,"午夜凶铃","贞子大战伽椰子"]
    obj:{us:123,ps:456}
  };
 
  render() {
    return (
      <div>
        <h1>列表渲染</h1>
        {this.state.arrs.map((item,index) => {
          return <li key={index}>{item}</li>
        })}
        <hr />
        {Object.keys(this.state.obj).map((item,index) => {
          return <li key={index}>{item}:{this.state.obj[item]}</li>
        })}
      </div>
    );
  }
}
export default VforDemo;
/*
react 中的v-for 主要依赖于 数组能被平铺

1. 实现 9*9乘法表
2. 隔行变色
3. 鼠标滑过有特效
4. 做一个下拉菜单 控制各行变色的颜色

红 绿 

红绿 蓝紫 黄粉

*/
