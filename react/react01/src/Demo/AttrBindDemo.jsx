import React, { Component } from "react";
import "./test.less";
class AttrBindDemo extends Component {
  state = {
    colorToggle:true,
    imgs:[
      "https://img0.baidu.com/it/u=1705694933,4002952892&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=281",
      "https://img0.baidu.com/it/u=1472391233,99561733&fm=253&fmt=auto&app=138&f=JPEG?w=889&h=500"
    ],
  };
  toggle = () => {
    this.setState({colorToggle: !this.state.colorToggle})
  }
  render() {
    return (
      <div>
        <h1>属性绑定</h1>
        <div className={this.state.colorToggle?"red":"blue"}></div>
        <button onClick={this.toggle}>toggle</button>
        <img src={this.state.colorToggle?this.state.imgs[0]: this.state.imgs[1]} alt="" />
      </div>
    );
  }
}
export default AttrBindDemo;
/*
1. react class 是关键字 类名 使用className 
2. 属性绑定 属性名外面放{ 变量 或者 表达式}

*/ 