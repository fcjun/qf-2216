import React, { Component } from "react";

class SetStateDemo extends Component {
  state = {
    name: "韩梅梅",
  };
  changeName = () => {
    console.log("点到我了");
    // this.setState({ name: 123 })
    // this.state.name = 123;
    // this.setState({});
    // 合并setSate
    // this.setState({name:1});
    // this.setState({name:2});
    // this.setState({name:3});
    // this.setState({ name:"韩梅梅"});
    // setTimeout(() => {
    //   this.setState({ name: 1 });
    //   this.setState({ name: 2 });
    //   this.setState({ name: 3 });
    //   this.setState({ name: "韩梅梅" });
    // }, 1000);
    this.setState({name:123},() => {
      //数据修改完成后的回调
      console.log(2, this.state.name)
    })
    console.log(1, this.state.name)
  };
  render() {
    console.log("render");
    const { name } = this.state;
    return (
      <div>
        <h1>setStateDemo</h1>
        {name}
        <button onClick={this.changeName}>改名</button>
      </div>
    );
  }
}

/*
react 中没有响应式 修改数据要通过 setState 实现
setState 触发render 函数执行 更新视图
setState 合并执行机制
17. 同步中的多次setState 会合并触发一次render  
    异步中的多次setState 每一个都会触发render 
18. 版本不管同步异步都合并

setState 是同步还是异步 ？
setState 是一个异步函数
*/
export default SetStateDemo;
