# 1.创建项目

```
npx create-react-app my-app --template typescript
```

# 2.改造目录结构

```
src
	api
	components
	layout
	store
	router
	utils
	views
	App.tsx
	index.tsx
```

#### 打开文件发现jsx代码处都有问题，修改 tsconfig.json

```shell
 - jsx: 'react-jsx'
 + jsx: 'react'
```



# 3.安装一些必须的模块

## 状态管理器

```
cnpm i redux react-redux redux-saga immutable redux-immutable -S
cnpm i @types/redux-immutable -D
```

```
cnpm i redux react-redux redux-thunk immutable redux-immutable -S
cnpm i @types/redux-immutable -D
```

```
cnpm i redux react-redux redux-saga -S
```

```
cnpm i redux react-redux redux-thunk -S
```

```
cnpm i mobx mobx-react -S
```

本项目选择第一个

## 路由

2021年11月4日 发布了 react-router-dom的v6.0.0版本：https://reactrouter.com/

本项目采用 V5版本：https://v5.reactrouter.com/web/guides/quick-start

```
cnpm i react-router-dom@5 -S
cnpm i @types/react-router-dom@5 -D
```

## 数据验证

> 思考，有没有必要安装 prop-types ?

```
cnpm i prop-types -S
```

## 数据请求

```
cnpm i axios -S
```

> 以前版本中 cnpm i @types/axios -S

## ui库

```
cnpm i antd -S
```

```
// src/App.css
@import '~antd/dist/antd.css';
```



# 4.创建主布局文件

> src/layout/main/Index.tsx 座位后台管理系统的主页面布局(包含左侧的菜单栏，顶部，底部等)

https://ant.design/components/layout-cn/#components-layout-demo-custom-trigger

```tsx
// src/layout/main/Index.ts
import React from 'react'
import { Layout, Menu } from 'antd';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
} from '@ant-design/icons';

const { Header, Sider, Content } = Layout;

class Index extends React.Component {
  state = {
    collapsed: false,
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
    return (
      <Layout id="components-layout-demo-custom-trigger">
        <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
          <div className="logo" />
          <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
            <Menu.Item key="1" icon={<UserOutlined />}>
              nav 1
            </Menu.Item>
            <Menu.Item key="2" icon={<VideoCameraOutlined />}>
              nav 2
            </Menu.Item>
            <Menu.Item key="3" icon={<UploadOutlined />}>
              nav 3
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Header className="site-layout-background" style={{ padding: 0 }}>
            {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
              className: 'trigger',
              onClick: this.toggle,
            })}
          </Header>
          <Content
            className="site-layout-background"
            style={{
              margin: '24px 16px',
              padding: 24,
              minHeight: 280,
            }}
          >
            Content
          </Content>
        </Layout>
      </Layout>
    );
  }
}

export default Index
```

```tsx
// src/App.tsx
import React from 'react'
import Index from './layout/main/Index'

import './App.css'
const App = () => {
  return (
    <>
      <Index />
    </>
  )
}

export default App
```

```css
// src/App.css
@import '~antd/dist/antd.css';

#root, #components-layout-demo-custom-trigger {
  height: 100%;
}

#components-layout-demo-custom-trigger .trigger {
  padding: 0 24px;
  font-size: 18px;
  line-height: 64px;
  cursor: pointer;
  transition: color 0.3s;
}

#components-layout-demo-custom-trigger .trigger:hover {
  color: #1890ff;
}

#components-layout-demo-custom-trigger .logo {
  height: 32px;
  margin: 16px;
  background: rgba(255, 255, 255, 0.3);
}

.site-layout .site-layout-background {
  background: #fff;
}
```

# 5.拆分主界面

```tsx
// src/layout/main/components/SideBar.tsx
import React, { FC, useState } from 'react';
import { Layout, Menu } from 'antd';
import {
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
} from '@ant-design/icons';

const { Sider } = Layout;
type Props = {};

const SideBar: FC<Props> = (props) => {
  const [ collapsed, setCollapsed ] = useState(false)
  return (
    <Sider trigger={null} collapsible collapsed={collapsed}>
      <div className="logo" />
      <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
        <Menu.Item key="1" icon={<UserOutlined />}>
          nav 1
        </Menu.Item>
        <Menu.Item key="2" icon={<VideoCameraOutlined />}>
          nav 2
        </Menu.Item>
        <Menu.Item key="3" icon={<UploadOutlined />}>
          nav 3
        </Menu.Item>
      </Menu>
    </Sider>
  )
}

export default SideBar;
```

```tsx
// src/layout/main/components/Appheader.tsx
import React, { FC, useState } from 'react';
import { Layout } from 'antd';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
} from '@ant-design/icons';

const { Header } = Layout;
type Props = {};

const AppHeader: FC<Props> = (props) => {
  const [ collapsed, setCollapsed ] = useState(false)

  const toggle = () => {
    setCollapsed(!collapsed)
  }
  return (
    <Header className="site-layout-background" style={{ padding: 0 }}>
      {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
        className: 'trigger',
        onClick: toggle,
      })}
    </Header>
  )
}

export default AppHeader;

```

```tsx
// src/layout/main/components/AppMain

import React, { FC } from 'react';
import { Layout } from 'antd';

const { Content } = Layout;
type Props = {};

const AppMain: FC<Props> = (props) => {
  return (
    <Content
      className="site-layout-background"
      style={{
        margin: '24px 16px',
        padding: 24,
        minHeight: 280,
      }}
    >
      Content
    </Content>
  )
}

export default AppMain;
```

```tsx
// src/layout/main/components/index.tsx

export { default as SideBar } from './SideBar'
export { default as AppMain } from './AppMain'
export { default as AppHeader } from './AppHeader'

```

```js
// src/layout/main/Index.tsx
import React from 'react'
import { Layout } from 'antd';
import { SideBar, AppHeader, AppMain } from './components'
class Index extends React.Component {
  render() {
    return (
      <Layout id="components-layout-demo-custom-trigger">
        <SideBar/>
        <Layout className="site-layout">
          <AppHeader />
          <AppMain />
        </Layout>
      </Layout>
    );
  }
}

export default Index
```

# 6.状态管理器配置-saga

菜单的收缩封装到一个叫做 app 的模块

使用不可变的数据结构 immutable,如果初始值为对象，使用 Map，如果初始值为数组，使用 List

## 创建reducer的app模块

```ts
// src/store/modules/app.ts
import { Map } from 'immutable'
const initialState = Map({
  collapsed: localStorage.getItem('collapsed') === 'true'
})

export default (state = initialState, { type }: { type: string; payload?: any }) => {
  switch (type) {

    case 'CHANGE_COLLAPSED':
      // { ...state, collapsed: !state.collapsed }
      localStorage.setItem('collapsed', String(!state.get('collapsed')))
      return state.set('collapsed', !state.get('collapsed'))

    default:
      return state
  }
};

```

## 创建sagas中app模块

Redux-saga 结合es6中的 generator 函数

```ts
// src/store/sagas/app.ts
import { put } from 'redux-saga/effects'
// call 方法用来触发异步的请求
// put 相当于以前的dispatch,触发的是reducer中的 aciton.type

export function * changeCollapsedAction () {
  yield put({
    type: 'CHANGE_COLLAPSED'
  })
}




```

## 创建mySaga分配器

```ts
// src/store/mySaga.ts
import { takeLatest } from 'redux-saga/effects'

import { changeCollapsedAction } from './sagas/app'

function * mySaga () {
  yield takeLatest('REQUEST_CHANGE_COLLAPSED', changeCollapsedAction) // 如果有人触发key，立马执行value的函数
}

export default mySaga
```

## 创建状态管理器store

```ts
// src/store/index.ts
import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
// import thunk from 'redux-thunk'

import { combineReducers } from 'redux-immutable' // 组合reducer 一定要使用这个

import mySaga from './mySaga' // 必须存在

import app from './modules/app'

const reducer = combineReducers({
  app
})

const sagaMiddleware = createSagaMiddleware() // 生成saga中间件

const store = createStore(reducer, applyMiddleware(sagaMiddleware))
// const store = createStore(reducer, applyMiddleware(thunk))

// 一定要在生成store之后运行
sagaMiddleware.run(mySaga)

export default store

```

## 入口文件引入状态状态管理器

```tsx
// src/index.tsx
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';
import store from './store'

ReactDOM.render(
  <React.StrictMode>
    <Provider store = { store }>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

```

## 左侧菜单栏调用状态

高阶组件

```tsx
// src/layout/main/components/SideBar.tsx
import React, { FC } from 'react';
import { Layout, Menu } from 'antd';
import {
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
} from '@ant-design/icons';

import { connect } from 'react-redux'

const { Sider } = Layout;

type Props = {
  collapsed: boolean
};

const SideBar: FC<Props> = ({ collapsed }) => {
  // const [ collapsed, setCollapsed ] = useState(false)
  return (
    <Sider trigger={null} collapsible collapsed={collapsed}>
      <div className="logo" />
      <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
        <Menu.Item key="1" icon={<UserOutlined />}>
          nav 1
        </Menu.Item>
        <Menu.Item key="2" icon={<VideoCameraOutlined />}>
          nav 2
        </Menu.Item>
        <Menu.Item key="3" icon={<UploadOutlined />}>
          nav 3
        </Menu.Item>
      </Menu>
    </Sider>
  )
}

export default connect(
  (state: any) => {
    console.log(state)
    return {
      collapsed: state.getIn(['app', 'collapsed'])
    }
  }
)(SideBar);
```

## 头部组件调用以及更改状态

```tsx
// src/layout/main/components/AppHeader.tsx
import React, { FC } from 'react';
import { Layout } from 'antd';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
} from '@ant-design/icons';
import { connect } from 'react-redux'
const { Header } = Layout;
type Props = {
  collapsed: boolean;
  toggleCollapsed: Function
};

const AppHeader: FC<Props> = ({ collapsed, toggleCollapsed }) => {

  const toggle = () => {
    toggleCollapsed()
  }
  return (
    <Header className="site-layout-background" style={{ padding: 0 }}>
      {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
        className: 'trigger',
        onClick: toggle,
      })}
    </Header>
  )
}
// connect(mapStateToProps, mapDispatchToProps)()
export default connect(
  (state: any) => {
    return {
      collapsed: state.getIn(['app', 'collapsed'])
    }
  },
  (dispatch) => {
    return {
      toggleCollapsed () {
        dispatch({
          type: 'REQUEST_CHANGE_COLLAPSED'
        })
      }
    }
  }
)(AppHeader);
```

# 7.状态管理配置-thunk

## 创建reducer的app模块

```ts
// src/store/modules/app.ts
import { Map } from 'immutable'
const initialState = Map({
  collapsed: localStorage.getItem('collapsed') === 'true'
})

export default (state = initialState, { type }: { type: string; payload?: any }) => {
  switch (type) {

    case 'CHANGE_COLLAPSED':
      // { ...state, collapsed: !state.collapsed }
      localStorage.setItem('collapsed', String(!state.get('collapsed')))
      return state.set('collapsed', !state.get('collapsed'))

    default:
      return state
  }
};

```

## 创建状态管理器

```ts
// src/store/index.ts
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

import { combineReducers } from 'redux-immutable'

import app from './modules/app'

const reducer = combineReducers({
  app
})

const store = createStore(reducer, applyMiddleware(thunk))

export default store

```

## 入口文件引入状态状态管理器

```tsx
// src/index.tsx
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';
import store from './store'

ReactDOM.render(
  <React.StrictMode>
    <Provider store = { store }>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

```

## 左侧菜单栏调用状态

高阶组件

```tsx
// src/layout/main/components/SideBar.tsx
import React, { FC } from 'react';
import { Layout, Menu } from 'antd';
import {
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
} from '@ant-design/icons';

import { connect } from 'react-redux'

const { Sider } = Layout;

type Props = {
  collapsed: boolean
};

const SideBar: FC<Props> = ({ collapsed }) => {
  // const [ collapsed, setCollapsed ] = useState(false)
  return (
    <Sider trigger={null} collapsible collapsed={collapsed}>
      <div className="logo" />
      <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
        <Menu.Item key="1" icon={<UserOutlined />}>
          nav 1
        </Menu.Item>
        <Menu.Item key="2" icon={<VideoCameraOutlined />}>
          nav 2
        </Menu.Item>
        <Menu.Item key="3" icon={<UploadOutlined />}>
          nav 3
        </Menu.Item>
      </Menu>
    </Sider>
  )
}

export default connect(
  (state: any) => {
    console.log(state)
    return {
      collapsed: state.getIn(['app', 'collapsed'])
    }
  }
)(SideBar);
```

## 创建app模块的异步行为

```ts
// src/store/actions/app.ts
const actions = {
  changeCollapsedAction () { 
    return (dispatch: any) =>{
      // 异步操作
      dispatch({ // 触发reducer
        type: 'CHANGE_COLLAPSED'
      })
    }
  }
}
export default actions
```

## 头部组件调用以及更改状态

```tsx
// src/layout/main/components/AppHeader.tsx
import React, { FC } from 'react';
import { Layout } from 'antd';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
} from '@ant-design/icons';
import { connect } from 'react-redux'
import action from './../../../store/actions/app'

const { Header } = Layout;
type Props = {
  collapsed: boolean;
  toggleCollapsed: Function
};

const AppHeader: FC<Props> = ({ collapsed, toggleCollapsed }) => {

  const toggle = () => {
    toggleCollapsed()
  }
  return (
    <Header className="site-layout-background" style={{ padding: 0 }}>
      {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
        className: 'trigger',
        onClick: toggle,
      })}
    </Header>
  )
}
// connect(mapStateToProps, mapDispatchToProps)()
export default connect(
  (state: any) => {
    return {
      collapsed: state.getIn(['app', 'collapsed'])
    }
  },
  (dispatch: any) => {
    return {
      toggleCollapsed () {
        // 加不加（） 取决于你在定义acitons时的写法
        // 如果你是函数返回一个函数（且返回的函数有默认参数dispatch），需要加
        // 如果函数直接写业务逻辑，且默认参数为dispatch,不要加
        dispatch(action.changeCollapsedAction())
      }
    }
  }
)(AppHeader);
```

# 8.左侧菜单栏

## 1.设计左侧菜单栏的数据

```ts
// src/router/menus.ts
import {
  HomeOutlined
} from '@ant-design/icons';
const menus = [
  {
    key: '0-0', // 树形控件时需要
    title: '系统首页',
    path: '/',
    icon: HomeOutlined
  },
  {
    key: '0-1',
    title: '轮播图管理',
    path: '/banner',
    icon: HomeOutlined,
    children: [
      {
        key: '0-1-0',
        title: '轮播图列表',
        path: '/banner/list',
        icon: HomeOutlined
      },
      {
        key: '0-1-1',
        title: '添加轮播图',
        path: '/banner/add',
        icon: HomeOutlined,
        hidden: true
      }
    ]
  },
  {
    key: '0-2',
    title: '产品管理',
    path: '/pro',
    icon: HomeOutlined,
    children: [
      {
        key: '0-2-0',
        title: '产品列表',
        path: '/pro/list',
        icon: HomeOutlined
      },
      {
        key: '0-2-1',
        title: '秒杀列表',
        path: '/pro/seckill',
        icon: HomeOutlined
      },
      {
        key: '0-2-2',
        title: '推荐列表',
        path: '/pro/recommend',
        icon: HomeOutlined
      },
      {
        key: '0-2-3',
        title: '筛选列表',
        path: '/pro/search',
        icon: HomeOutlined
      },
    ]
  },
  {
    key: '0-3',
    title: '账户管理',
    path: '/user',
    icon: HomeOutlined,
    children: [
      {
        key: '0-3-0',
        title: '用户管理',
        path: '/user/list',
        icon: HomeOutlined
      },
      {
        key: '0-3-1',
        title: '管理员管理',
        path: '/user/admin',
        icon: HomeOutlined
      }
    ]
  },
  {
    key: '0-4',
    title: '设置',
    path: '/setting',
    icon: HomeOutlined,
    hidden: true
  }
]

export default menus
```

## 2.渲染左侧菜单栏

> 带有hidden属性不渲染在左侧菜单栏

```tsx
// src/layout/main/components/SideBar.tsx
import React, { FC } from 'react';
import { Layout, Menu } from 'antd';
import { connect } from 'react-redux'

import menus, { IRoute } from './../../../router/menus'

const { Sider } = Layout;
const { SubMenu } = Menu
type Props = {
  collapsed: boolean
};

const SideBar: FC<Props> = ({ collapsed }) => {
  // const [ collapsed, setCollapsed ] = useState(false)

  const renderMenu = (menus: IRoute[]) => {
    return menus.map(item => {
      if (item.children) {
        return (
          <SubMenu key={item.path} icon={<item.icon />} title={ item.title }>
            {
              renderMenu(item.children)
            }
          </SubMenu>
        )
      } else {
        return (
          item.hidden ? null : <Menu.Item key={item.path} icon={<item.icon />}>
            { item.title }
          </Menu.Item>
        )
      }
    })
  }
  return (
    <Sider trigger={null} collapsible collapsed={collapsed}>
      <div className="logo" />
      <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>

        {
          renderMenu(menus)
        }
        {/* <Menu.Item key="1" icon={<UserOutlined />}>
          nav 1
        </Menu.Item>
        <Menu.Item key="2" icon={<VideoCameraOutlined />}>
          nav 2
        </Menu.Item>
        <Menu.Item key="3" icon={<UploadOutlined />}>
          nav 3
        </Menu.Item> */}
      </Menu>
    </Sider>
  )
}

export default connect(
  (state: any) => {
    console.log(state)
    return {
      collapsed: state.getIn(['app', 'collapsed'])
    }
  }
)(SideBar);
```

> 如果左侧菜单栏数据过于庞大，每个管理项里又有很多项，--- 需要只展开一个菜单项
>
> https://ant-design.gitee.io/components/menu-cn/#components-menu-demo-sider-current

```tsx
import React, { FC } from 'react';
import { Layout, Menu } from 'antd';
import { connect } from 'react-redux'

import menus, { IRoute } from './../../../router/menus'

const { Sider } = Layout;
const { SubMenu } = Menu
type Props = {
  collapsed: boolean
};


const rootSubmenuKeys: string[] = []
menus.forEach(item => {
  if (item.children) {
    rootSubmenuKeys.push(item.path)
  }
})

const SideBar: FC<Props> = ({ collapsed }) => {
  const [openKeys, setOpenKeys] = React.useState(['']);

  const onOpenChange = (keys: string[]) => {
    const latestOpenKey = keys.find(key => openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey as string) === -1) {
      setOpenKeys(keys);
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
    }
  };

  const renderMenu = (menus: IRoute[]) => {
    return menus.map(item => {
      if (item.children) {
        return (
          <SubMenu key={item.path} icon={<item.icon />} title={ item.title }>
            {
              renderMenu(item.children)
            }
          </SubMenu>
        )
      } else {
        return (
          item.hidden ? null : <Menu.Item key={item.path} icon={<item.icon />}>
            { item.title }
          </Menu.Item>
        )
      }
    })
  }
  return (
    <Sider trigger={null} collapsible collapsed={collapsed}>
      <div className="logo" />
      <Menu theme="dark" mode="inline" openKeys={openKeys} onOpenChange={onOpenChange}>

        {
          renderMenu(menus)
        }
        {/* <Menu.Item key="1" icon={<UserOutlined />}>
          nav 1
        </Menu.Item>
        <Menu.Item key="2" icon={<VideoCameraOutlined />}>
          nav 2
        </Menu.Item>
        <Menu.Item key="3" icon={<UploadOutlined />}>
          nav 3
        </Menu.Item> */}
      </Menu>
    </Sider>
  )
}

export default connect(
  (state: any) => {
    console.log(state)
    return {
      collapsed: state.getIn(['app', 'collapsed'])
    }
  }
)(SideBar);
```

# 9.定义路由

## 1.创建对应的页面

```
views
	home
		Index.tsx  系统首页
	banner
		Index.tsx	 轮播图列表
		Add.tsx    添加轮播图
	pro
		Index.tsx  产品列表
		Seckill.tsx  秒杀列表
		Recommend.tsx  推荐列表
		Search.tsx  筛选列表
	user
		Index.tsx  用户列表
		Admin.tsx  管理员列表
	setting
		Index.tsx 设置
	error
		NotMatch.tsx  404
```

```tsx
import React, { FC } from 'react';

type Props = {};

const Index: FC<Props> = (props) => {
  return (
    <div>
      系统首页
    </div>
  )
}

export default Index;
```

## 2.定义router/menus.ts

```ts
// src/router/menus.ts
import React from 'react'
import {
  HomeOutlined
} from '@ant-design/icons';
export interface IRoute {
  key: string
  title: string
  path: string
  icon: any
  component?: React.LazyExoticComponent<React.FC<any>>
  hidden?: boolean
  children?: Array<IRoute>
}
const menus: IRoute[] = [
  {
    key: '0-0', // 树形控件时需要
    title: '系统首页',
    path: '/',
    icon: HomeOutlined,
    component: React.lazy(() => import('./../views/home/Index'))
  },
  {
    key: '0-1',
    title: '轮播图管理',
    path: '/banner',
    icon: HomeOutlined,
    children: [
      {
        key: '0-1-0',
        title: '轮播图列表',
        path: '/banner/list',
        icon: HomeOutlined,
        component: React.lazy(() => import('./../views/banner/Index'))
      },
      {
        key: '0-1-1',
        title: '添加轮播图',
        path: '/banner/add',
        icon: HomeOutlined,
        hidden: true,
        component: React.lazy(() => import('./../views/banner/Add'))
      }
    ]
  },
  {
    key: '0-2',
    title: '产品管理',
    path: '/pro',
    icon: HomeOutlined,
    children: [
      {
        key: '0-2-0',
        title: '产品列表',
        path: '/pro/list',
        icon: HomeOutlined,
        component: React.lazy(() => import('./../views/pro/Index'))
      },
      {
        key: '0-2-1',
        title: '秒杀列表',
        path: '/pro/seckill',
        icon: HomeOutlined,
        component: React.lazy(() => import('./../views/pro/Seckill'))
      },
      {
        key: '0-2-2',
        title: '推荐列表',
        path: '/pro/recommend',
        icon: HomeOutlined,
        component: React.lazy(() => import('./../views/pro/Recommend'))
      },
      {
        key: '0-2-3',
        title: '筛选列表',
        path: '/pro/search',
        icon: HomeOutlined,
        component: React.lazy(() => import('./../views/pro/Search'))
      },
    ]
  },
  {
    key: '0-3',
    title: '账户管理',
    path: '/user',
    icon: HomeOutlined,
    children: [
      {
        key: '0-3-0',
        title: '用户管理',
        path: '/user/list',
        icon: HomeOutlined,
        component: React.lazy(() => import('./../views/user/Index'))
      },
      {
        key: '0-3-1',
        title: '管理员管理',
        path: '/user/admin',
        icon: HomeOutlined,
        component: React.lazy(() => import('./../views/user/Admin'))
      }
    ]
  },
  {
    key: '0-4',
    title: '设置',
    path: '/setting',
    icon: HomeOutlined,
    hidden: true,
    component: React.lazy(() => import('./../views/setting/Index'))
  }
]

export default menus
```

## 3.定义路由

```tsx
// src/App.tsx
import React from 'react'
import { HashRouter as Router, Switch, Route } from 'react-router-dom'
import Index from './layout/main/Index'

import './App.css'
const App = () => {
  return (
    <Router>
      <Switch>
        <Route path="/" component = { Index } />
      </Switch>
    </Router>
  )
}

export default App
```

```tsx
// src/layout/main/components/AppMain.tsx
import React, { FC } from 'react';
import { Layout, Spin } from 'antd';
import { Route, Switch } from 'react-router-dom';
import menus, { IRoute } from './../../../router/menus'
const { Content } = Layout;
type Props = {};

const AppMain: FC<Props> = (props) => {
  const renderRoute = (menus: IRoute[]): any => {
    return menus.map(item => {
      if (item.children) {
        return renderRoute(item.children)
      } else {
        return (
          <Route 
            path={item.path} 
            exact 
            key ={item.path} 
            component={ item.component }
          />
        )
      }
    })
  }
  return (
    <Content
      className="site-layout-background"
      style={{
        margin: '24px 16px',
        padding: 24,
        minHeight: 280,
      }}
    >
      <React.Suspense fallback = { <Spin />}>
        <Switch>
          {
            renderRoute(menus)
          }
        </Switch>
      </React.Suspense>
    </Content>
  )
}

export default AppMain;
```

> 需要实现404 以及当用户输入 /banner，需要指向 /banner/list

```ts
import React from 'react'
import {
  HomeOutlined
} from '@ant-design/icons';
export interface IRoute {
  key: string
  title: string
  path: string
  icon: any
  redirect?: any
  component?: React.LazyExoticComponent<React.FC<any>>
  hidden?: boolean
  children?: Array<IRoute>
}
const menus: IRoute[] = [
  {
    key: '0-0', // 树形控件时需要
    title: '系统首页',
    path: '/',
    icon: HomeOutlined,
    component: React.lazy(() => import('./../views/home/Index'))
  },
  {
    key: '0-1',
    title: '轮播图管理',
    path: '/banner',
    icon: HomeOutlined,
    redirect: '/banner/list',
    children: [
      {
        key: '0-1-0',
        title: '轮播图列表',
        path: '/banner/list',
        icon: HomeOutlined,
        component: React.lazy(() => import('./../views/banner/Index'))
      },
      {
        key: '0-1-1',
        title: '添加轮播图',
        path: '/banner/add',
        icon: HomeOutlined,
        hidden: true,
        component: React.lazy(() => import('./../views/banner/Add'))
      }
    ]
  },
  {
    key: '0-2',
    title: '产品管理',
    path: '/pro',
    redirect: '/pro/list',
    icon: HomeOutlined,
    children: [
      {
        key: '0-2-0',
        title: '产品列表',
        path: '/pro/list',
        icon: HomeOutlined,
        component: React.lazy(() => import('./../views/pro/Index'))
      },
      {
        key: '0-2-1',
        title: '秒杀列表',
        path: '/pro/seckill',
        icon: HomeOutlined,
        component: React.lazy(() => import('./../views/pro/Seckill'))
      },
      {
        key: '0-2-2',
        title: '推荐列表',
        path: '/pro/recommend',
        icon: HomeOutlined,
        component: React.lazy(() => import('./../views/pro/Recommend'))
      },
      {
        key: '0-2-3',
        title: '筛选列表',
        path: '/pro/search',
        icon: HomeOutlined,
        component: React.lazy(() => import('./../views/pro/Search'))
      },
    ]
  },
  {
    key: '0-3',
    title: '账户管理',
    path: '/user',
    redirect: '/user/list',
    icon: HomeOutlined,
    children: [
      {
        key: '0-3-0',
        title: '用户管理',
        path: '/user/list',
        icon: HomeOutlined,
        component: React.lazy(() => import('./../views/user/Index'))
      },
      {
        key: '0-3-1',
        title: '管理员管理',
        path: '/user/admin',
        icon: HomeOutlined,
        component: React.lazy(() => import('./../views/user/Admin'))
      }
    ]
  },
  {
    key: '0-4',
    title: '设置',
    path: '/setting',
    icon: HomeOutlined,
    hidden: true,
    component: React.lazy(() => import('./../views/setting/Index'))
  }
]

export default menus
```

```tsx
import React, { FC } from 'react';
import { Layout, Spin } from 'antd';
import { Route, Switch, Redirect } from 'react-router-dom';
import menus, { IRoute } from './../../../router/menus'
import NotMatch from './../../../views/error/NotMatch'
const { Content } = Layout;
type Props = {};

const AppMain: FC<Props> = (props) => {
  const renderRoute = (menus: IRoute[]): any => {
    return menus.map(item => {
      if (item.children) {
        return renderRoute(item.children)
      } else {
        return (
          <Route 
            path={item.path} 
            exact 
            key ={item.path} 
            component={ item.component }
          />
        )
      }
    })
  }
  const renderRedirect = (menus: IRoute[]) => {
    return menus.map(item => {
      if (item.children) {
        return (
          <Redirect 
            path={ item.path }
            key = { item.path }
            exact
            to = { item.redirect }
          />
        )
      } else {
        return null
      }
    })
  }
  return (
    <Content
      className="site-layout-background"
      style={{
        margin: '24px 16px',
        padding: 24,
        minHeight: 280,
      }}
    >
      <React.Suspense fallback = { <Spin />}>
        <Switch>
          {
            renderRoute(menus)
          }
          {
            renderRedirect(menus)
          }
          <Route path="*" component = { NotMatch }/>
        </Switch>
      </React.Suspense>
    </Content>
  )
}

export default AppMain;
```

# 10 点击左侧菜单切换路由

```tsx
// src/layout/main/components/SideBar.tsx
import React, { FC, useCallback } from 'react';
import { Layout, Menu } from 'antd';
import { connect } from 'react-redux'

import menus, { IRoute } from './../../../router/menus'
import { useHistory } from 'react-router-dom';

const { Sider } = Layout;
const { SubMenu } = Menu
type Props = {
  collapsed: boolean
};


const rootSubmenuKeys: string[] = []
menus.forEach(item => {
  if (item.children) {
    rootSubmenuKeys.push(item.path)
  }
})

const SideBar: FC<Props> = ({ collapsed }) => {
  const [openKeys, setOpenKeys] = React.useState(['']);

  const onOpenChange = (keys: string[]) => {
    const latestOpenKey = keys.find(key => openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey as string) === -1) {
      setOpenKeys(keys);
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
    }
  };

  const renderMenu = (menus: IRoute[]) => {
    return menus.map(item => {
      if (item.children) {
        return (
          <SubMenu key={item.path} icon={<item.icon />} title={ item.title }>
            {
              renderMenu(item.children)
            }
          </SubMenu>
        )
      } else {
        return (
          item.hidden ? null : <Menu.Item key={item.path} icon={<item.icon />}>
            { item.title }
          </Menu.Item>
        )
      }
    })
  }

  // ++++++++++++++++++++++++++++
  const history = useHistory()
  const changeUrl = useCallback(({ key }: {key: string}) => {
    history.push(key)
  }, [])
  return (
    <Sider trigger={null} collapsible collapsed={collapsed}>
      <div className="logo" />
      <Menu 
        theme="dark" 
        mode="inline" 
        openKeys={openKeys} 
        onOpenChange={onOpenChange}
        onClick = { changeUrl } {/* ++++++ 8/}
      >

        {
          renderMenu(menus)
        }
        {/* <Menu.Item key="1" icon={<UserOutlined />}>
          nav 1
        </Menu.Item>
        <Menu.Item key="2" icon={<VideoCameraOutlined />}>
          nav 2
        </Menu.Item>
        <Menu.Item key="3" icon={<UploadOutlined />}>
          nav 3
        </Menu.Item> */}
      </Menu>
    </Sider>
  )
}

export default connect(
  (state: any) => {
    console.log(state)
    return {
      collapsed: state.getIn(['app', 'collapsed'])
    }
  }
)(SideBar);
```

> 当页面刷新时，需要保证当前二级路由是展开的，且当前路由是被选中的状态

```tsx
import React, { FC, useCallback } from 'react';
import { Layout, Menu } from 'antd';
import { connect } from 'react-redux'

import menus, { IRoute } from './../../../router/menus'
import { useHistory, useLocation } from 'react-router-dom';

const { Sider } = Layout;
const { SubMenu } = Menu
type Props = {
  collapsed: boolean
};


const rootSubmenuKeys: string[] = []
menus.forEach(item => {
  if (item.children) {
    rootSubmenuKeys.push(item.path)
  }
})

const SideBar: FC<Props> = ({ collapsed }) => {

  const { pathname } = useLocation() // +++++++++++++

  const [openKeys, setOpenKeys] = React.useState(['/' + pathname.split('/')[1] ]);// +++++++++++++
  const [selectedKeys, setSelectedKeys] = React.useState([pathname]);// +++++++++++++

  const onOpenChange = (keys: string[]) => {
    const latestOpenKey = keys.find(key => openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey as string) === -1) {
      setOpenKeys(keys);
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
    }
  };

  const renderMenu = (menus: IRoute[]) => {
    return menus.map(item => {
      if (item.children) {
        return (
          <SubMenu key={item.path} icon={<item.icon />} title={ item.title }>
            {
              renderMenu(item.children)
            }
          </SubMenu>
        )
      } else {
        return (
          item.hidden ? null : <Menu.Item key={item.path} icon={<item.icon />}>
            { item.title }
          </Menu.Item>
        )
      }
    })
  }

  const history = useHistory()
  const changeUrl = useCallback(({ key }: {key: string}) => {
    history.push(key)
    setSelectedKeys([key]) // +++++++++++++
  }, [])
  return (
    <Sider trigger={null} collapsible collapsed={collapsed}>
      <div className="logo" />
      <Menu 
        theme="dark" 
        mode="inline" 
        openKeys={openKeys} 
        selectedKeys = { selectedKeys }
        onOpenChange={onOpenChange}
        onClick = { changeUrl }
      >

        {
          renderMenu(menus)
        }
        {/* <Menu.Item key="1" icon={<UserOutlined />}>
          nav 1
        </Menu.Item>
        <Menu.Item key="2" icon={<VideoCameraOutlined />}>
          nav 2
        </Menu.Item>
        <Menu.Item key="3" icon={<UploadOutlined />}>
          nav 3
        </Menu.Item> */}
      </Menu>
    </Sider>
  )
}

export default connect(
  (state: any) => {
    console.log(state)
    return {
      collapsed: state.getIn(['app', 'collapsed'])
    }
  }
)(SideBar);
```

# 11.数据请求的封装

```ts
// src/uitls/request.ts
import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios'
// 自定义axios
const ins: AxiosInstance = axios.create({
  baseURL: 'http://121.89.205.189/admin',
  timeout: 6000
})

// 自定义拦截器
// 请求拦截器
ins.interceptors.request.use((config: AxiosRequestConfig) => {
  config.headers ? config.headers.token = localStorage.getItem('token') || '' : config.headers = {}
  return config
}, error => Promise.reject(error))

// interface MyAxiosResponse<T, D> extends AxiosResponse{
//   code?: string
// }
// 响应拦截器
ins.interceptors.response.use((response: AxiosResponse<any, any>) => {
  if (response.data.code === '10119') {
    // 未登录时跳转到登录页面
    window.location.href = "/#/login"
  }
  return response.data
}, error => Promise.reject(error))

// 自定义各种数据请求 axios({})
export default function request (config: AxiosRequestConfig<any>): Promise<AxiosResponse<any, any>> {
  const { url = '', method = 'GET', data = {}, headers = {} } = config
  switch (method.toUpperCase()) {
    case 'GET':
      return ins.get(url, { params: data })
    case 'POST':
      // 表单提交  application/x-www-form-url-encoded
      if (headers['content-type'] === 'application/x-www-form-url-encoded') {
        // 转参数 URLSearchParams/第三方库qs
        const p = new URLSearchParams()
        for (const key in data) {
          p.append(key, data[key])
        }
        return ins.post(url, p, { headers })
      }
      // 文件提交  multipart/form-data
      if (headers['content-type'] === 'multipart/form-data') {
        const p = new FormData()
        for (const key in data) {
          p.append(key, data[key])
        }
        return ins.post(url, p, { headers })
      }
      // 默认 application/json
      return ins.post(url, data)
    case 'PUT': // 修改数据 --- 所有的数据的更新
      return ins.put(url, data)
    case 'DELETE': // 删除数据
      return ins.delete(url, { data })
    case 'PATCH': // 更新局部资源
      return ins.patch(url, data)
    default:
      return ins(config)
  }
}


```

```ts
// src/api/user.ts
import request from '../utils/request'

// 登录
export function loginFn (data: { adminname: string; password: string }) {
  return request({
    url: '/admin/login',
    method: 'POST',
    data
  })
}

```

# 12 构建登录页面以及路由

```tsx
// src/views/login/Index.tsx
import React, { FC } from 'react';

type Props = {};

const Index: FC<Props> = (props) => {
  return (
    <div>
      登录
    </div>
  )
}

export default Index;
```

```tsx
// src/App.tsx
import React from 'react'
import { HashRouter as Router, Switch, Route } from 'react-router-dom'
import Index from './layout/main/Index'
import Login from './views/login/Index'

import './App.css'
const App = () => {
  return (
    <Router>
      <Switch>
        <Route path="/login" component = { Login } />
        <Route path="/" component = { Index } />
      </Switch>
    </Router>
  )
}

export default App
```

https://ant-design.gitee.io/components/form-cn/#components-form-demo-normal-login

```tsx
import React, { FC } from 'react';
import { Form, Input, Button } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import './style.css'
type Props = {};

const Index: FC<Props> = (props) => {
  const onFinish = (values: any) => {
    console.log('Received values of form: ', values);
  };

  return (
    <div className = "login_box">
      <Form
        name="normal_login"
        className="login-form"
        onFinish={onFinish}
      >
        <Form.Item
          name="adminname"
          rules={[{ required: true, message: '请输入管理员账户!' }]}
        >
          <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="管理员账户" />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[{ required: true, message: '请输入正确的密码!' }]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="密码"
          />
        </Form.Item>
        
        <Form.Item>
          <Button type="primary" htmlType="submit" className="login-form-button">
            登 录
          </Button>
        </Form.Item>
      </Form>

    </div>
  );
}

export default Index;
```

```css
// src/views/login/style.css
.login_box {
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background: url(https://cas.1000phone.net/cas/images/login/bg.png) center no-repeat;
  background-size: cover;
  overflow: hidden;
  position: relative;
}
.login-form {
  width: 420px;
  background-color: #fff;
  
}
.ant-form {
  padding: 30px 24px !important;
}
.login-form-forgot {
  float: right;
}
.ant-col-rtl .login-form-forgot {
  float: left;
}
.login-form-button {
  width: 100%;
}
```

> 使用状态管理器，异步操作可以在组件，也可以在状态管理器

# 13.使用状态管理器管理登录信息

```ts
// src/store/modules/user.ts
import { Map } from 'immutable'
const initialState = Map({
  adminname: localStorage.getItem('adminname') || '',
  token: localStorage.getItem('token') || '',
  role: Number(localStorage.getItem('role')),
  isLogin: localStorage.getItem('isLogin') === 'true'
})

export default (state = initialState, { type, payload }: { type: string; payload?: number | string | boolean | undefined }) => {
  switch (type) {

    case 'CHANGE_ADMIN_NAME':
      localStorage.setItem('adminname', payload as string)
      return state.set('adminname', payload as string)
    case 'CHANGE_TOKEN':
      localStorage.setItem('token', payload as string)
      return state.set('token', payload as string)
    case 'CHANGE_ROLE':
      localStorage.setItem('role', payload as string)
      return state.set('role', payload as number)
    case 'CHANGE_LOGIN_STATE':
      localStorage.setItem('isLogin', payload as string)
      return state.set('isLogin', payload as boolean)

    default:
      return state
  }
};

```

```ts
// src/store/sagas/user.ts
import { put, call } from 'redux-saga/effects'
import { message } from 'antd'
import { loginFn } from './../../api/user'
export interface ResponseGenerator {
  code?: any,
  data?: any,
  message?: string
}
export function * loginAction (action: { type: string, payload: { adminname: string, password: string}}) {
  // console.log(action.payload)
  const res: ResponseGenerator = yield call(loginFn, action.payload)
  console.log(res)
  if (res.code === '10005') {
    // 没有账号
    message.error('没有账号')
  } else if (res.code === '10003') {
    // 密码错误
    message.error('密码错误')
  } else {
    // ok
    message.success('登录成功')
    yield put({
      type: 'CHANGE_ADMIN_NAME', payload: res.data.adminname
    })
    yield put({
      type: 'CHANGE_TOKEN', payload: res.data.token
    })
    yield put({
      type: 'CHANGE_ROLE', payload: res.data.role
    })
    yield put({
      type: 'CHANGE_LOGIN_STATE', payload: true
    })
    window.location.href = "/"
  }
  
}


```

```ts
// src/store/mySaga.ts
import { takeLatest } from 'redux-saga/effects'

import { changeCollapsedAction } from './sagas/app'
import { loginAction } from './sagas/user'

function * mySaga () {
  yield takeLatest('REQUEST_CHANGE_COLLAPSED', changeCollapsedAction)
  yield takeLatest('REQUEST_LOGIN_ACTION', loginAction)
}

export default mySaga
```

```ts
// src/store/index.ts
import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
// import thunk from 'redux-thunk'

import { combineReducers } from 'redux-immutable'

import mySaga from './mySaga' // 必须存在

import app from './modules/app'
import user from './modules/user' // +++++++++++

const reducer = combineReducers({
  app, user
})

const sagaMiddleware = createSagaMiddleware() // 生成saga中间件

const store = createStore(reducer, applyMiddleware(sagaMiddleware))
// const store = createStore(reducer, applyMiddleware(thunk))

// 一定要在生成store之后运行
sagaMiddleware.run(mySaga)

export default store

```



```tsx
// src/views/login/Index.tsx
import React, { FC } from 'react';
import { Form, Input, Button } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { connect } from 'react-redux' 
import './style.css'
type Props = {
  loginFn: Function
};

const Index: FC<Props> = (props) => {
  const onFinish = (values: any) => {
    console.log('Received values of form: ', values);
    props.loginFn(values)
  };

  return (
    <div className = "login_box">
      <Form
        name="normal_login"
        className="login-form"
        onFinish={onFinish}
      >
        <Form.Item
          name="adminname"
          rules={[{ required: true, message: '请输入管理员账户!' }]}
        >
          <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="管理员账户" />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[{ required: true, message: '请输入正确的密码!' }]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="密码"
          />
        </Form.Item>
        
        <Form.Item>
          <Button type="primary" htmlType="submit" className="login-form-button">
            登 录
          </Button>
        </Form.Item>
      </Form>

    </div>
  );
}

export default connect(
  () => ({}),
  (dispatch) => {
    return {
      loginFn (values: {adminname: string;password:string}) {
        dispatch({
          type: 'REQUEST_LOGIN_ACTION',
          payload: values
        })
      }
    }
  }
)(Index);
```

# 14.登录验证

## 1.前端校验

```tsx
// src/App.tsx
import React from 'react'
import { HashRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import Index from './layout/main/Index'
import Login from './views/login/Index'

import './App.css'
const App = (props: { isLogin: boolean}) => {
  console.log(props.isLogin)
  return (
    <Router>
      <Switch>
        { // 已登录，输入登录路由跳转到系统首页
          props.isLogin ? <Redirect path="/login" exact to="/" /> : 
          <Route path="/login" component = { Login } /> 
        }
        { // 未登录，输入路由，跳转到登录页面
          props.isLogin ? <Route path="/" component = { Index } /> :
            <Redirect to="/login" />
        }
        
      </Switch>
    </Router>
  )
}

export default connect(
  (state: any) => {
    return {
      isLogin: state.getIn(['user', 'isLogin'])
    }
  }
)(App)
```



## 2.token校验

> 封装axios时已经实现 --- 响应拦截器

# 15.登录完成跳转页面

之前的写法是在sagas中通过 `window.location.herg=""`强制跳转

现在的做法就是监听状态管理器中的数据的改变

```tsx
import React, { FC, useEffect } from 'react';
import { Form, Input, Button } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { connect } from 'react-redux' 
import './style.css'
type Props = {
  isLogin: boolean
  loginFn: Function,
  history: {
    push: Function
  }
};

const Index: FC<Props> = (props) => {
  const onFinish = (values: any) => {
    console.log('Received values of form: ', values);
    props.loginFn(values)
  };
  // 无法监听登录已完成，但是可以监听状态管理器中的 状态的变化
  useEffect(() => { // +++++++++++
    props.isLogin && props.history.push('/')
  }, [props.isLogin])

  return (
    <div className = "login_box">
      <Form
        name="normal_login"
        className="login-form"
        onFinish={onFinish}
      >
        <Form.Item
          name="adminname"
          rules={[{ required: true, message: '请输入管理员账户!' }]}
        >
          <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="管理员账户" />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[{ required: true, message: '请输入正确的密码!' }]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="密码"
          />
        </Form.Item>
        
        <Form.Item>
          <Button type="primary" htmlType="submit" className="login-form-button">
            登 录
          </Button>
        </Form.Item>
      </Form>

    </div>
  );
}

export default connect(
  (state: any) => ({ isLogin: state.getIn(['user', 'isLogin'])}), // +++++++++++
  (dispatch) => {
    return {
      loginFn (values: {adminname: string;password:string}) {
        dispatch({
          type: 'REQUEST_LOGIN_ACTION',
          payload: values
        })
      }
    }
  }
)(Index);
```

> 如果使用的thunk去操作异步，可以在异步阶段通过return new Promise（）去解决问题

# 16.退出登录后还在原来的页面

```tsx
// src/layout/main/components/AppHeader.tsx
import React, { FC, useCallback, useEffect } from 'react';
import { Layout, Button } from 'antd';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
} from '@ant-design/icons';
import { useHistory, useLocation } from 'react-router-dom'
import { connect } from 'react-redux'
const { Header } = Layout;
type Props = {
  collapsed: boolean;
  toggleCollapsed: Function,
  logoutFn: Function,
  isLogin: boolean
};

const AppHeader: FC<Props> = ({ collapsed, toggleCollapsed, logoutFn, isLogin }) => {
  const history = useHistory()// ++++++++++++++
  const { pathname } = useLocation()// ++++++++++++++
  const toggle = () => {
    toggleCollapsed()
  }
  const logout = useCallback(() => { // ++++++++++++++
    localStorage.clear()
    logoutFn()
  }, [])
  useEffect(() => {
    !isLogin && history.push('/login?redirect=' + pathname)
  }, [isLogin])
  return (
    <Header className="site-layout-background" style={{ padding: 0 }}>
      {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
        className: 'trigger',
        onClick: toggle,
      })}
      <Button onClick = { logout }>退出</Button>
    </Header>
  )
}
// connect(mapStateToProps, mapDispatchToProps)()
export default connect(
  (state: any) => {
    return {
      collapsed: state.getIn(['app', 'collapsed']),
      isLogin: state.getIn(['user', 'isLogin']) // ++++++++++++++
    }
  },
  (dispatch) => {
    return {
      toggleCollapsed () {
        dispatch({
          type: 'REQUEST_CHANGE_COLLAPSED'
        })
      },
      logoutFn () {
        dispatch({
          type: 'REQUEST_LOGOUT_ACTION'
        })
      }
    }
  }
)(AppHeader);
```

```tsx
// src/views/login/Index.tsx
import React, { FC, useEffect } from 'react';
import { Form, Input, Button } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { connect } from 'react-redux' 
import { useLocation } from 'react-router-dom'
import './style.css'
type Props = {
  isLogin: boolean
  loginFn: Function,
  history: {
    push: Function
  }
};

const Index: FC<Props> = (props) => {
  const { search } = useLocation() // +++++++++++++++++++
  const onFinish = (values: any) => {
    console.log('Received values of form: ', values);
    props.loginFn(values)
  };
  // 无法监听登录已完成，但是可以监听状态管理器中的 状态的变化
  useEffect(() => {
    const url = search.split('?redirect=')[1] // +++++++++++++++++++
    props.isLogin && props.history.push(url)// +++++++++++++++++++
  }, [props.isLogin])

  return (
    <div className = "login_box">
      <Form
        name="normal_login"
        className="login-form"
        onFinish={onFinish}
      >
        <Form.Item
          name="adminname"
          rules={[{ required: true, message: '请输入管理员账户!' }]}
        >
          <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="管理员账户" />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[{ required: true, message: '请输入正确的密码!' }]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="密码"
          />
        </Form.Item>
        
        <Form.Item>
          <Button type="primary" htmlType="submit" className="login-form-button">
            登 录
          </Button>
        </Form.Item>
      </Form>

    </div>
  );
}

export default connect(
  (state: any) => ({ isLogin: state.getIn(['user', 'isLogin'])}),
  (dispatch) => {
    return {
      loginFn (values: {adminname: string;password:string}) {
        dispatch({
          type: 'REQUEST_LOGIN_ACTION',
          payload: values
        })
      }
    }
  }
)(Index);
```

> 当通过前端校验代码实现以后 ----报出错误，因为Router路由器设置的位置不对
>
> ```tsx
> // src/App.tsx
> import React from 'react'
> import { HashRouter as Router, Switch, Route, Redirect, useLocation } from 'react-router-dom'
> import { connect } from 'react-redux'
> import Index from './layout/main/Index'
> import Login from './views/login/Index'
> 
> import './App.css'
> const App = (props: { isLogin: boolean}) => {
>   const { search } = useLocation()
>   const url = search === '' ? '/login' : '/login' + search
>   console.log(props.isLogin)
>   return (
>      <Router>
>       <Switch>
>         {
>           props.isLogin ? <Redirect path={ url } exact to="/" /> : 
>           <Route path="/login" component = { Login } /> 
>         }
>         {
>           props.isLogin ? <Route path="/" component = { Index } /> :
>             <Redirect to="/login" />
>         }
>         {/* <Route path="/login" component = { Login } /> 
>         <Route path="/" component = { Index } /> */}
>       </Switch>
>      </Router>
>   )
> }
> 
> export default connect(
>   (state: any) => {
>     return {
>       isLogin: state.getIn(['user', 'isLogin'])
>     }
>   }
> )(App)
> ```
>
> ```tsx
> // src/index.tsx
> import React from 'react';
> import ReactDOM from 'react-dom';
> import App from './App';
> import reportWebVitals from './reportWebVitals';
> import { HashRouter as Router} from 'react-router-dom' // ++++++++
> import { Provider } from 'react-redux';
> import store from './store'
> 
> ReactDOM.render(
>   // <React.StrictMode>
>     <Provider store = { store }>
>       <Router> // ++++++++
>         <App />
>       </Router>// ++++++++
>     </Provider>,
>   // </React.StrictMode>,
>   document.getElementById('root')
> );
> 
> // If you want to start measuring performance in your app, pass a function
> // to log results (for example: reportWebVitals(console.log))
> // or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
> reportWebVitals();
> 
> ```
>
> ```tsx
> // src/App.tsx
> import React from 'react'
> import { HashRouter as Router, Switch, Route, Redirect, useLocation } from 'react-router-dom'
> import { connect } from 'react-redux'
> import Index from './layout/main/Index'
> import Login from './views/login/Index'
> 
> import './App.css'
> const App = (props: { isLogin: boolean}) => {
>   const { search } = useLocation()
>   const url = search === '' ? '/login' : '/login' + search
>   console.log(props.isLogin)
>   return (
>     // <Router>
>       <Switch>
>         {
>           props.isLogin ? <Redirect path={ url } exact to="/" /> : 
>           <Route path="/login" component = { Login } /> 
>         }
>         {
>           props.isLogin ? <Route path="/" component = { Index } /> :
>             <Redirect to="/login" />
>         }
>         {/* <Route path="/login" component = { Login } /> 
>         <Route path="/" component = { Index } /> */}
>       </Switch>
>     // </Router>
>   )
> }
> 
> export default connect(
>   (state: any) => {
>     return {
>       isLogin: state.getIn(['user', 'isLogin'])
>     }
>   }
> )(App)
> ```
>
> 

# 17.面包屑导航

```tsx
// src/layout/main/components/AppBreadcrumb.tsx
import React, { Component, useState, useEffect } from 'react';
import routes from './../../../router/menus'
import { Link, useLocation } from 'react-router-dom'
// 1.得到数据字典
const breadcrumbNameMap = {}
function getData (routes) {
  routes.forEach(item => {
    if (item.children) {
      getData(item.children)
    }
    breadcrumbNameMap[item.path] = item.title
  })
}
getData(routes)
console.log(breadcrumbNameMap)

const AppBreadcrumb = () => {
  const { pathname } = useLocation()
  const [title, setTitle] = useState('')
  const [subTitle, setSubTitle] = useState('')
  useEffect(() => {
    setTitle('/' + pathname.split('/')[1])
    setSubTitle(pathname)
  }, [pathname])

  return (
    <div style={{
      margin: '10px 16px'
    }}> 
      系统管理 / 
      <Link to="/"> 系统首页 </Link> /
      {
        title === subTitle 
          ? title === '/' 
            ? null 
            : <Link to={ title }> { breadcrumbNameMap[title] } </Link>  
          : <>
            <Link to={ title }> { breadcrumbNameMap[title] } </Link> /
            <Link to={ subTitle }> { breadcrumbNameMap[subTitle] } </Link>
          </>
      }
    </div>
  );
}

export default AppBreadcrumb

```

```
export { default as SideBar } from './SideBar'
export { default as AppMain } from './AppMain'
export { default as AppHeader } from './AppHeader'
export { default as AppBreadcrumb } from './AppBreadcrumb'

```

```tsx
// src/layout/main/Index.tsx
import React from 'react'
import { Layout } from 'antd';
import { SideBar, AppHeader, AppMain, AppBreadcrumb } from './components'
class Index extends React.Component {
  render() {
    console.log('999', this.props)
    return (
      <Layout id="components-layout-demo-custom-trigger">
        <SideBar/>
        <Layout className="site-layout">
          <AppHeader />
          <AppBreadcrumb />
          <AppMain />
        </Layout>
      </Layout>
    );
  }
}

export default Index

```

# 18. 管理员管理

## 1.设计接口

```ts
// src/api/admin.ts
import request from './../utils/request'

// 修改管理员接口
export function updateAdmin (params: { adminname: string; password: string; role: number; checkedKeys: string[] }) {
  return request({
    url: '/admin/update',
    method: 'POST',
    data: params
  })
}

// 删除管理员接口
export function deleteAdmin (params: { adminid: string}) {
  return request({
    url: '/admin/delete',
    method: 'POST',
    data: params
  })
}

// 添加管理员接口
export function addAdmin (params: { adminname: string; password: string; role: number; checkedKeys: string[] }) {
  return request({
    url: '/admin/add',
    method: 'POST',
    data: params
  })
}


// 获取管理员列表接口
export function getAdminList () {
  return request({
    url: '/admin/list'
  })
}


// 获取管理员详情接口
export function getAdminDetail (params: { adminname: string }) {
  return request({
    url: '/admin/detail',
    data: params
  })
}
```

## 2.展示管理员列表

```tsx
// src/views/user/Admin.tsx
import React, { FC, useState, useEffect } from 'react';
import { Space, Table, Button, Tag } from 'antd'
import { getAdminList } from './../../api/admin'
type Props = {};

interface IAdmin {
  adminid: string
  adminname: string
  password: string
  role: number
  checkedKeys: Array<string>
}

const Index: FC<Props> = (props) => {

  const columns = [
    {
      title: '序号',
      render: (text: null, record: IAdmin, index: number) => <span> { index + 1} </span>
    },
    {
      title: '账户',
      dataIndex: 'adminname'
    },
    {
      title: '角色',
      dataIndex: 'role',
      render: (text: number) => {
        return (
          <>
            { text === 2 ?  <Tag color="green">超级管理员</Tag> : <Tag>管理员</Tag>}
          </>
        )
      }
    },
    {
      title: '操作',
      render: () => {
        return (
          <Space>
            <Button type="ghost">编辑</Button>
            <Button danger>删除</Button>
          </Space>
        )
      }
    }
  ]

  const [adminList, setAdminList] = useState([])
  useEffect(() => {
    getAdminList().then(res => setAdminList(res.data))
  }, [])
  return (
    <Space direction="vertical" style= {{ width: '100%' }}>
      <Button type="primary">添加管理员</Button>
      <Table dataSource = { adminList } columns = { columns } rowKey="adminid"></Table>
    </Space>
  )
}

export default Index;
```

**优化数据表格**（分页器优化 - 跟随屏幕的宽高的变化二变化，序号-分页之后需要要连贯）

```tsx
import React, { FC, useState, useEffect } from 'react';
import { Space, Table, Button, Tag } from 'antd'
import { getAdminList } from './../../api/admin'
type Props = {};

interface IAdmin {
  adminid: string
  adminname: string
  password: string
  role: number
  checkedKeys: Array<string>
}

const Index: FC<Props> = (props) => {
  const [ height, setHeight ] = useState(0)
  useEffect(() => {
    setHeight(window.innerHeight)
  }, [])

  // 分页
  const [ current, setCurrent ] = useState(1)
  const [ pageSize, setPageSize ] = useState(10)
  const changePageSize = (current: number, size: number) => {
    setPageSize(size)
    setCurrent(current)
  }
  const changePage = (current: number) => {
    setCurrent(current)
  }

  const columns = [
    {
      title: '序号',
      render: (text: null, record: IAdmin, index: number) => <span> { (current - 1) * pageSize + index + 1} </span>
    },
    {
      title: '账户',
      dataIndex: 'adminname'
    },
    {
      title: '角色',
      dataIndex: 'role',
      sorter: (a: IAdmin, b: IAdmin) => a.role - b.role,
      render: (text: number) => {
        return (
          <>
            { text === 2 ?  <Tag color="green">超级管理员</Tag> : <Tag>管理员</Tag>}
          </>
        )
      }
    },
    {
      title: '操作',
      render: () => {
        return (
          <Space>
            <Button type="ghost">编辑</Button>
            <Button danger>删除</Button>
          </Space>
        )
      }
    }
  ]

  const [adminList, setAdminList] = useState([])
  useEffect(() => {
    getAdminList().then(res => setAdminList(res.data))
  }, [])
  return (
    <Space direction="vertical" style= {{ width: '100%' }}>
      <Button type="primary">添加管理员</Button>
      <Table 
        dataSource = { adminList } 
        columns = { columns } 
        rowKey="adminid"
        pagination ={{
          current,
          pageSize,
          pageSizeOptions: [ '2', '5', '10', '20'],
          showSizeChanger: true,
          onShowSizeChange: changePageSize,
          onChange: changePage,
          showQuickJumper: true,
          showTotal: (total: number) => <span>共有{total}条数据</span>,
          position: ['bottomRight']
        }}
        scroll = {{ y: height - 330 }}
        ></Table>
    </Space>
  )
}

export default Index;
```

**自定义hooks**

```tsx
// src/views/user/hooks/useColumns.tsx
import React from 'react'
import { Space, Button, Tag } from 'antd'
import { IAdmin } from '../Admin'
function useColumns (current: number, pageSize: number) {
  const columns = [
    {
      title: '序号',
      render: (text: null, record: IAdmin, index: number) => <span> { (current - 1) * pageSize + index + 1} </span>
    },
    {
      title: '账户',
      dataIndex: 'adminname'
    },
    {
      title: '角色',
      dataIndex: 'role',
      sorter: (a: IAdmin, b: IAdmin) => a.role - b.role,
      render: (text: number) => {
        return (
          <>
            { text === 2 ?  <Tag color="green">超级管理员</Tag> : <Tag>管理员</Tag>}
          </>
        )
      }
    },
    {
      title: '操作',
      render: () => {
        return (
          <Space>
            <Button type="ghost">编辑</Button>
            <Button danger>删除</Button>
          </Space>
        )
      }
    }
  ]

  return columns
}

export default useColumns
```

```tsx
// src/views/user/hooks/usePagination.tsx
import { useState } from 'react'
function usePagination () {
  const [ current, setCurrent ] = useState(1)
  const [ pageSize, setPageSize ] = useState(10)
  const changePageSize = (current: number, size: number) => {
    setPageSize(size)
    setCurrent(current)
  }
  const changePage = (current: number) => {
    setCurrent(current)
  }

  return {
    current,
    pageSize,
    changePageSize,
    changePage
  }
}

export default usePagination
```

```ts
// src/views/user/hooks/index.ts
export { default as useColumns } from './useColumns'
export { default as usePagination } from './usePagination'
```

```tsx
// src/views/user/Admin.tsx
import React, { FC, useState, useEffect } from 'react';
import { Space, Table, Button } from 'antd'
import { getAdminList } from './../../api/admin'
import { useColumns, usePagination } from './hooks'
type Props = {};

export interface IAdmin {
  adminid: string
  adminname: string
  password: string
  role: number
  checkedKeys: Array<string>
}

const Index: FC<Props> = (props) => {
  const [ height, setHeight ] = useState(0)
  useEffect(() => {
    setHeight(window.innerHeight)
  }, [])

  // 分页
  const { current, pageSize, changePageSize, changePage } = usePagination()

  const columns = useColumns(current, pageSize)

  const [adminList, setAdminList] = useState([])
  useEffect(() => {
    getAdminList().then(res => setAdminList(res.data))
  }, [])
  return (
    <Space direction="vertical" style= {{ width: '100%' }}>
      <Button type="primary">添加管理员</Button>
      <Table 
        dataSource = { adminList } 
        columns = { columns } 
        rowKey="adminid"
        pagination ={{
          current,
          pageSize,
          pageSizeOptions: [ '2', '5', '10', '20'],
          showSizeChanger: true,
          onShowSizeChange: changePageSize,
          onChange: changePage,
          showQuickJumper: true,
          showTotal: (total: number) => <span>共有{total}条数据</span>,
          position: ['bottomRight']
        }}
        scroll = {{ y: height - 330 }}
        ></Table>
    </Space>
  )
}

export default Index;
```



## 3.添加管理员

``````
// src/views/user/hooks/useDrawer.tsx
import { useState, useCallback } from "react"

function useDrawer () {
  const [ visible, setVisible] = useState(false)
  const showDrawer = useCallback(() => {
    setVisible(true)
  }, [])
  const closeDrawer = useCallback(() => {
    setVisible(false)
  }, [])
  return {
    visible,
    showDrawer,
    closeDrawer
  }
}

export default useDrawer
```

```ts
// src/views/user/hooks/setAdminname.tsx
import { useState, useCallback } from "react";

export default function () {
  const [ adminname, setAdminname] = useState('')
  const [ password, setPassword] = useState('')
  const [ role, setRole] = useState(1)
  const [ checkedKeys, setCheckedKeys] = useState(['0-0'])

  const changeAdminName = useCallback((e) => {
    setAdminname(e.target.value)
  }, [])
  const changePassword = useCallback((e) => {
    setPassword(e.target.value)
  }, [])
  const changeRole = useCallback((value) => {
    setRole(value)
  }, [])
  const changeCheckedKeys = useCallback((checkedKeys) => {
    setCheckedKeys(checkedKeys)
  }, [])
  const reset = useCallback(() => {
    setAdminname('')
    setPassword('')
    setRole(1)
    setCheckedKeys(['0-0'])
  }, [])
  return {
    adminname,
    password, 
    role,
    checkedKeys,
    changeAdminName,
    changePassword,
    changeRole,
    changeCheckedKeys,
    reset
  }
}
```

```ts
export { default as useColumns } from './useColumns'
export { default as usePagination } from './usePagination'

export { default as useDrawer } from './useDrawer'
export { default as useAddAdmin } from './useAddAdmin'

```



```js
src/views/user/Admin.tsx
import React, { FC, useState, useEffect } from 'react';
import { Space, Table, Button, Drawer, Form, Input, Select, Tree } from 'antd'
import { getAdminList, addAdmin } from './../../api/admin'
import { useColumns, usePagination, useDrawer, useAddAdmin } from './hooks'
import menus from './../../router/menus'
type Props = {};

export interface IAdmin {
  adminid: string
  adminname: string
  password: string
  role: number
  checkedKeys: Array<string>
}

const Index: FC<Props> = (props) => {
  // 设计高度
  const [ height, setHeight ] = useState(0)
  useEffect(() => {
    setHeight(window.innerHeight)
  }, [])

  // 分页
  const { current, pageSize, changePageSize, changePage } = usePagination()
  // 表格设计
  const columns = useColumns(current, pageSize)

  // 列表数据
  const [adminList, setAdminList] = useState([])
  useEffect(() => {
    getAdminList().then(res => setAdminList(res.data))
  }, [])

  // 抽屉效果
  const { visible, showDrawer, closeDrawer } = useDrawer()

  // 添加管理员表单
  const { 
    adminname,
    password, 
    role,
    checkedKeys,
    changeAdminName,
    changePassword,
    changeRole,
    changeCheckedKeys,
    reset } = useAddAdmin()
  return (
    <Space direction="vertical" style= {{ width: '100%' }}>
      <Button type="primary" onClick = { showDrawer } >添加管理员</Button>
      <Table 
        dataSource = { adminList } 
        columns = { columns } 
        rowKey="adminid"
        pagination ={{
          current,
          pageSize,
          pageSizeOptions: [ '2', '5', '10', '20'],
          showSizeChanger: true,
          onShowSizeChange: changePageSize,
          onChange: changePage,
          showQuickJumper: true,
          showTotal: (total: number) => <span>共有{total}条数据</span>,
          position: ['bottomRight']
        }}
        scroll = {{ y: height - 330 }}
        ></Table>
    
      <Drawer 
        title="添加管理员" 
        placement="right" 
        width="500"
        onClose={closeDrawer} visible={visible}
      >
        <Form>
          <Form.Item>
            <Input placeholder = "管理员账户" value={ adminname } onChange = { changeAdminName }></Input>
          </Form.Item>
          <Form.Item>
            <Input type="password" placeholder = "密码" value={ password } onChange = { changePassword }></Input>
          </Form.Item>
          <Form.Item>
            <Select defaultValue = { 1 } value= { role} onChange= { changeRole }>
              <Select.Option value = { 1 }>管理员</Select.Option>
              <Select.Option value = { 2 }>超级管理员</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item>
            <Tree 
              checkable 
              onCheck = { changeCheckedKeys }
              checkedKeys = { checkedKeys }
              treeData = { menus }
              />

          </Form.Item>
          <Form.Item>
            <Button type="primary"
              onClick = { () => {
                const data = {
                  adminname,
                  password, 
                  role,
                  checkedKeys,
                }
                console.log(data)
                addAdmin(data).then(res => {
                  reset()
                  getAdminList().then(res => setAdminList(res.data))
                  closeDrawer()
                })
              }}
            >添 加</Button>
          </Form.Item>
        </Form>
      </Drawer>
    </Space>
  )
}

export default Index;
```

## 4.删除管理员

```tsx
// src/views/user/hooks/useColumns.tsx
import React, { useState } from 'react'
import { Space, Button, Tag, Popconfirm } from 'antd'
import { IAdmin } from '../Admin'
import { deleteAdmin } from './../../../api/admin'
function useColumns (current: number, pageSize: number) {
  const [flag, setFlag] = useState(false)
  console.log('666999')
  const deleteAdminFn = (adminid: string) => {
    deleteAdmin({ adminid }).then(() => {
      setFlag(!flag) // ++++++++++++++++++++++++
    })
  }
  const columns = [
    {
      title: '序号',
      render: (text: null, record: IAdmin, index: number) => <span> { (current - 1) * pageSize + index + 1} </span>
    },
    {
      title: '账户',
      dataIndex: 'adminname'
    },
    {
      title: '角色',
      dataIndex: 'role',
      sorter: (a: IAdmin, b: IAdmin) => a.role - b.role,
      render: (text: number) => {
        return (
          <>
            { text === 2 ?  <Tag color="green">超级管理员</Tag> : <Tag>管理员</Tag>}
          </>
        )
      }
    },
    {
      title: '操作',
      render: (text: null, record: IAdmin) => {
        return (
          <Space>
            <Button type="ghost">编辑</Button>
            <Popconfirm
              title="确定删除吗？"
              onConfirm = { () => deleteAdminFn(record.adminid) }
              onCancel = {() => {}}
            >
              <Button danger>删除</Button>
            </Popconfirm>
          </Space>
        )
      }
    }
  ]

  return {
    columns,
    flag
  }
}

export default useColumns
```

```tsx
// src/views/user/Admin.tsx
import React, { FC, useState, useEffect } from 'react';
import { Space, Table, Button, Drawer, Form, Input, Select, Tree } from 'antd'
import { getAdminList, addAdmin } from './../../api/admin'
import { useColumns, usePagination, useDrawer, useAddAdmin } from './hooks'
import menus from './../../router/menus'
type Props = {};

export interface IAdmin {
  adminid: string
  adminname: string
  password: string
  role: number
  checkedKeys: Array<string>
}

const Index: FC<Props> = (props) => {


  // 设计高度
  const [ height, setHeight ] = useState(0)
  useEffect(() => {
    setHeight(window.innerHeight)
  }, [])

  // 分页
  const { current, pageSize, changePageSize, changePage } = usePagination()
  // 表格设计
  const { columns, flag } = useColumns(current, pageSize)

  // 列表数据
  const [adminList, setAdminList] = useState([])
  useEffect(() => {
    getAdminList().then(res => {
      setAdminList(res.data)
    })
  }, [])

  // 抽屉效果
  const { visible, showDrawer, closeDrawer } = useDrawer()

  // 删除数据
  useEffect(() => {
    getAdminList().then(res => {
      setAdminList(res.data)
    })
  }, [flag])
  // 添加管理员表单
  const { 
    adminname,
    password, 
    role,
    checkedKeys,
    changeAdminName,
    changePassword,
    changeRole,
    changeCheckedKeys,
    reset } = useAddAdmin()
  return (
    <Space direction="vertical" style= {{ width: '100%' }}>
      <Button type="primary" onClick = { showDrawer } >添加管理员</Button>
      <Table 
        dataSource = { adminList } 
        columns = { columns } 
        rowKey="adminid"
        pagination ={{
          current,
          pageSize,
          pageSizeOptions: [ '2', '5', '10', '20'],
          showSizeChanger: true,
          onShowSizeChange: changePageSize,
          onChange: changePage,
          showQuickJumper: true,
          showTotal: (total: number) => <span>共有{total}条数据</span>,
          position: ['bottomRight']
        }}
        scroll = {{ y: height - 330 }}
        ></Table>
    
      <Drawer 
        title="添加管理员" 
        placement="right" 
        width="500"
        onClose={closeDrawer} visible={visible}
      >
        <Form>
          <Form.Item>
            <Input placeholder = "管理员账户" value={ adminname } onChange = { changeAdminName }></Input>
          </Form.Item>
          <Form.Item>
            <Input type="password" placeholder = "密码" value={ password } onChange = { changePassword }></Input>
          </Form.Item>
          <Form.Item>
            <Select defaultValue = { 1 } value= { role} onChange= { changeRole }>
              <Select.Option value = { 1 }>管理员</Select.Option>
              <Select.Option value = { 2 }>超级管理员</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item>
            <Tree 
              checkable 
              onCheck = { changeCheckedKeys }
              checkedKeys = { checkedKeys }
              treeData = { menus }
              />

          </Form.Item>
          <Form.Item>
            <Button type="primary"
              onClick = { () => {
                const data = {
                  adminname,
                  password, 
                  role,
                  checkedKeys,
                }
                console.log(data)
                addAdmin(data).then(res => {
                  reset()
                  getAdminList().then(res => setAdminList(res.data))
                  closeDrawer()
                })
              }}
            >添 加</Button>
          </Form.Item>
        </Form>
      </Drawer>
    </Space>
  )
}

export default Index;
```





# 19.菜单栏权限

## 1.获取管理员信息

```ts
// src/layout/main/components/hooks/tool.ts

// ['0-0', '0-1-1', '0-2', '0-2-2'] ===>  ['0-0', '0-1', '0-1-1', '0-2', '0-2-2']

import { IRoute } from "../../../../router/menus"
// author gp25 宽哥
export const formatArr = (arr: string[]) => {
    const result = arr.reduce((_arr, item): any => [..._arr, ...sliceStr(item)], [])
    return [...new Set(result)]
}
const sliceStr = (str: string) => {
    if (str.split('-').length > 2) {
        return str.split('-').reduce((_arr, item): any => {
            if (!Array.isArray(_arr)) return [`${_arr}-${item}`]
            return [`${_arr[0]}-${item}`, ..._arr]
        })
    }
    return [str]
}
// 截取字符串  数组合并   数组去重
export function getCheckedKeys (checkedKeys: string[]) {
  const arr: string[] = []
  checkedKeys.forEach(item => {
    arr.push(item.slice(0, 3))
  })
  const result = Array.from(new Set([...checkedKeys, ...arr])).sort()
  return result
}

// 获取左侧的菜单栏数据
export function getMenu (menus: IRoute[],checkedKeys: string[]) {
  let arr: IRoute[] = []
  checkedKeys.forEach(item => {
    menus.forEach(value => {
      if(value.key === item) {
        // arr.push({...value})
        arr.push(Object.assign({}, value))
      }
    })
  })
  // 搞定第二层级
  arr.forEach(item => {
    if(item.children) {
      let newArr = getMenu(item.children,checkedKeys)
      item.children = newArr
    }
  })
  return arr
}


// 判断pathname是不是在menus中
// export function checkPathName (menus: IRoute[], pathname: string) {
//   // console.log(menus)
//   const flag =  menus.some(item => {
//     if (item.children) {
//       return checkPathName(item.children, pathname)
//     } else {
//       return item.path === pathname
//     }
//   })
//   // console.log(flag)
//   return flag
// }

export function checkPathName (routes: IRoute[], pathname: string, arr: string[]) {
  // const arr: string[] = []
  routes.forEach(item => {
    if (item.children) {
      checkPathName(item.children, pathname, arr)
    }
    arr.push(item.path)
  })
  console.log('9999', arr)
  return arr.indexOf(pathname) !== -1
}
```



```ts
// src/layout/main/components/hooks/useMenus.ts
import { IRoute } from "../../../../router/menus";
import React, { useState, useEffect } from 'react'
import {
  HomeOutlined
} from '@ant-design/icons';
import { getCheckedKeys, getMenu } from './tool'
import { getAdminDetail } from "../../../../api/admin";
export default function (menus: IRoute[], adminname: string) {
  const [checkedKeys, setCheckedKeys] = useState([])
  console.log('adminname', adminname)
  useEffect(() => {
    getAdminDetail({ adminname }).then(res => {
      console.log('detail', res)
      setCheckedKeys(res.data[0].checkedKeys)
    })
  }, [])
  // 根据checkedKeys 计算的得到 菜单栏数据 ['0-0', '0-1-0', '0-2-2', '0-2-3' , '0-3']
  // ==> ['0-0', '0-1', '0-1-0', '0-2', '0-2-2','0-2-3', '0-3']

  const newCheckedkeys = getCheckedKeys(checkedKeys)

  const newMenu: IRoute[] = getMenu(menus, newCheckedkeys)
  // 本接口如果账号是admin 没有设置过 checkedKeys,所以返回原数据
  return adminname === 'admin' ? menus : newMenu
}

```

```tsx
// src/layout/main/components/SideBar.tsx
import React, { FC, useCallback, useEffect } from 'react';
import { Layout, Menu } from 'antd';
import { connect } from 'react-redux'

import menus, { IRoute } from './../../../router/menus'
import { useHistory, useLocation } from 'react-router-dom';
import useMenus from './hooks/useMenus' // ++++++++++++++++++++++++++
const { Sider } = Layout;
const { SubMenu } = Menu
type Props = {
  collapsed: boolean,
  adminname: string
};


const rootSubmenuKeys: string[] = []
menus.forEach(item => {
  if (item.children) {
    rootSubmenuKeys.push(item.path)
  }
})

const SideBar: FC<Props> = ({ collapsed, adminname }) => {
  // 获取当前管理员的左侧的菜单栏数据
  // ++++++++++++++++++++++++++
  const newMenu = useMenus(menus, adminname)

  const { pathname } = useLocation()

  const [openKeys, setOpenKeys] = React.useState(['/' + pathname.split('/')[1] ]);
  const [selectedKeys, setSelectedKeys] = React.useState([pathname]);

  const onOpenChange = (keys: string[]) => {
    const latestOpenKey = keys.find(key => openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey as string) === -1) {
      setOpenKeys(keys);
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
    }
  };

  const renderMenu = (menus: IRoute[]) => {
    return menus.map(item => {
      if (item.children) {
        return (
          <SubMenu key={item.path} icon={<item.icon />} title={ item.title }>
            {
              renderMenu(item.children)
            }
          </SubMenu>
        )
      } else {
        return (
          item.hidden ? null : <Menu.Item key={item.path} icon={<item.icon />}>
            { item.title }
          </Menu.Item>
        )
      }
    })
  }

  const history = useHistory()
  const changeUrl = useCallback(({ key }: {key: string}) => {
    history.push(key)
    setSelectedKeys([key])
  }, [])


  useEffect(() => {
    setSelectedKeys([pathname])
  }, [pathname])
  return (
    <Sider trigger={null} collapsible collapsed={collapsed}>
      <div className="logo" />
      <Menu 
        theme="dark" 
        mode="inline" 
        openKeys={openKeys} 
        selectedKeys = { selectedKeys }
        onOpenChange={onOpenChange}
        onClick = { changeUrl }
      >

        { // ++++++++++++++++++++++++++
          renderMenu(newMenu)
        }
        {/* <Menu.Item key="1" icon={<UserOutlined />}>
          nav 1
        </Menu.Item>
        <Menu.Item key="2" icon={<VideoCameraOutlined />}>
          nav 2
        </Menu.Item>
        <Menu.Item key="3" icon={<UploadOutlined />}>
          nav 3
        </Menu.Item> */}
      </Menu>
    </Sider>
  )
}

export default connect(
  (state: any) => {
    console.log(state)
    return {
      collapsed: state.getIn(['app', 'collapsed']),
      adminname: state.getIn(['user', 'adminname'])
    }
  }
)(SideBar);

```

# 20 页面权限

访问的路由在menus，显示无权限，不在显示404

```tsx
// src/layout/main/components/AppMain.tsx
import React, { FC } from 'react';
import { Layout, Spin } from 'antd';
import { Route, Switch, Redirect, useLocation } from 'react-router-dom';
import menus, { IRoute } from './../../../router/menus'
import NotMatch from './../../../views/error/NotMatch'
import useMenus from './hooks/useMenus'
import { connect } from 'react-redux' 
import { checkPathName } from './hooks/tool'
const { Content } = Layout;
type Props = {
  adminname: string
};

const AppMain: FC<Props> = (props) => {
  // 获取当前管理员的左侧的菜单栏数据
  const newMenu = useMenus(menus, props.adminname) // ++++++++
  const { pathname } = useLocation() // ++++++++

  const renderRoute = (menus: IRoute[]): any => {
    return menus.map(item => {
      if (item.children) {
        return renderRoute(item.children)
      } else {
        return (
          <Route 
            path={item.path} 
            exact 
            key ={item.path} 
            component={ item.component }
          />
        )
      }
    })
  }
  const renderRedirect = (menus: IRoute[]) => {
    return menus.map(item => {
      if (item.children) {
        return (
          <Redirect 
            path={ item.path }
            key = { item.path }
            exact
            to = { item.redirect }
          />
        )
      } else {
        return null
      }
    })
  }
  return (
    <Content
      className="site-layout-background"
      style={{
        margin: '24px 16px',
        padding: 24,
        minHeight: 280,
      }}
    >
      <React.Suspense fallback = { <Spin />}>
        <Switch>
          { // ++++++++
            renderRoute(newMenu)
          }
          {// ++++++++
            renderRedirect(newMenu)
          }
          <Route path="*" >
            {// ++++++++
              checkPathName(newMenu, pathname) ? <span>无权限</span> : <NotMatch />
            }
          </Route>
        </Switch>
      </React.Suspense>
    </Content>
  )
}

export default connect((state:any) => ({ adminname: state.getIn(['user', 'adminname'])}))(AppMain);
```

# 21 按钮权限

```tsx
// src/views/banner/Index.tsx
import React, { FC } from 'react';
import { Button, message } from 'antd'
import { connect } from 'react-redux'
import menus from './../../router/menus'
import { checkPathName } from './../../layout/main/components/hooks/tool'
import  useMenus from './../../layout/main/components/hooks/useMenus'
import { useLocation } from 'react-router-dom';
type Props = {};

const Index: FC<Props> = (props: any) => {
  const newMenu = useMenus(menus, props.adminname)
  const { pathname } = useLocation()
  console.log('6666', checkPathName(newMenu, pathname, []) )
  return (
    <div>
      <Button onClick = {() => {
        checkPathName(newMenu, pathname, []) 
          ? props.history.push('/banner/add') 
          : message.error('请联系管理员增加您权限')
        
      }}>添加轮播图</Button>
      轮播图列表
    </div>
  )
}

export default connect((state: any) => state.getIn(['user', 'adminname']))(Index);
```


 

``````

# 22.数据可视化

方案：

echarts: https://echarts.apache.org/zh/index.html

> ts中使用 echarts ： https://echarts.apache.org/handbook/zh/basics/import#%E5%9C%A8-typescript-%E4%B8%AD%E6%8C%89%E9%9C%80%E5%BC%95%E5%85%A5

highCharts:https://www.highcharts.com.cn/

Antv: https://antv.gitee.io/zh/

	g2:https://antv-g2.gitee.io/zh/
	
	g2plot:https://g2plot.antv.vision/zh/
	
	react中使用g2:https://charts.ant.design/zh-CN

D3:视频地址：链接: https://pan.baidu.com/s/1SVS36TjtcR27Rqj_HURDZA  密码: p9ur



拓展： webgl / three.js

## 1.echarts

添加页面以及配置路由

```ts
{
    key: '0-5',
    title: '数据可视化',
    path: '/data',
    icon: HomeOutlined,
    children: [
      {
        key: '0-5-0',
        title: 'echarts',
        path: '/data/ehcarts',
        icon: HomeOutlined,
        component: React.lazy(() => import('./../views/data/Echart'))
      }
    ]
  }
```

```tsx
// src/views/data/Echart.tsx
import React, { FC, useState, useEffect } from 'react';
import ReactECharts from 'echarts-for-react'
import { getDataFn } from './../../api/data'
type Props = {};

const Index: FC<Props> = (props) => {
  const [data, setData] = useState([50, 30, 224, 218, 135, 147, 260])
  const [x, setX] = useState(['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'])
  const changeData = () => {
    const arr = []
    for (var i = 0; i < 7; i++) {
      arr.push(Math.random() * 100 + 100)
    }
    setData(arr)
  }

  useEffect(() => {
    getDataFn().then(res => {
      console.log('echarts', res.data)
      const cate: string[] = []
      const value: number[] = []
      // res.data.forEach((item: { x: string; val: number}) => {
      //   cate.push(item.x)
      //   value.push(item.val)
      // })
      for (var i = 0; i < Math.floor(Math.random() * 4 + 3); i++) {
        cate.push(res.data[i].x)
        value.push(res.data[i].val)
      }
      setData(value)
      setX(cate)
    })
  }, [])
  const getLineOption = () => {
    return {
      title: {
        text: '折线图',
        textAlign: 'center',
        left: '50%'
      },
      legend: {
        type: 'plain',
        show: true,
        data: ["星期", "日期"],
        right: 0
      },
      grid: {
        show: true
      },
      tooltip: {
        show: true
      },
      xAxis: {
        scale: true,
        type: 'category',
        data: x
      },
      dataZoom: [
        {
          type: 'inside',
          start: 50,
          end: 100
        },
        {
          show: true,
          type: 'slider',
          top: '90%',
          start: 50,
          end: 100
        }
      ],
      yAxis: {
        scale: true,
        type: 'value'
      },
      series: [
        {
          name: "星期",
          data: data,
          type: 'line'
        },
        {
          name: "日期",
          data: data,
          type: 'bar'
        }
      ]
    };
  }

  const getBarOption = () => {
    return {
      title: {
        text: '折线图',
        textAlign: 'center',
        left: '50%'
      },
      xAxis: {
        type: 'category',
        data: x
      },
      yAxis: {
        type: 'value'
      },
      series: [
        {
          data: data,
          type: 'bar'
        }
      ]
    };
  }

  const getOption = () => {
    return {
      xAxis: {
        type: 'category',
        boundaryGap: false
      },
      yAxis: {
        type: 'value',
        boundaryGap: [0, '30%']
      },
      visualMap: {
        type: 'piecewise',
        show: false,
        dimension: 0,
        seriesIndex: 0,
        pieces: [
          {
            gt: 1,
            lt: 3,
            color: 'rgba(0, 0, 180, 0.4)'
          },
          {
            gt: 5,
            lt: 7,
            color: 'rgba(0, 0, 180, 0.4)'
          }
        ]
      },
      series: [
        {
          type: 'line',
          smooth: 0.6,
          symbol: 'none',
          lineStyle: {
            color: '#5470C6',
            width: 5
          },
          markLine: {
            symbol: ['none', 'none'],
            label: { show: false },
            data: [{ xAxis: 1 }, { xAxis: 3 }, { xAxis: 5 }, { xAxis: 7 }]
          },
          areaStyle: {},
          data: [
            ['2019-10-10', 200],
            ['2019-10-11', 560],
            ['2019-10-12', 750],
            ['2019-10-13', 580],
            ['2019-10-14', 250],
            ['2019-10-15', 300],
            ['2019-10-16', 450],
            ['2019-10-17', 300],
            ['2019-10-18', 100]
          ]
        }
      ]
    }
  }

  const getKOption = () => {
    return {
      xAxis: {
        data: ['2017-10-24', '2017-10-25', '2017-10-26', '2017-10-27']
      },
      yAxis: {},
      series: [
        {
          barMaxWidth: 10, 
          type: 'candlestick',
          data: [
            [20, 34, 10, 38],
            [40, 35, 30, 50],
            [31, 38, 33, 44],
            [38, 15, 5, 42]
          ]
        }
      ]
    };
  }
  return (
    <div>
      <h1>数据可视化：echarts</h1>
      <button onClick={ changeData }>改变数据</button>
      
      <ReactECharts
        option={ getLineOption() }
      />

      <ReactECharts
        option={ getBarOption() }
      />

      <ReactECharts
        option={ getOption() }
      />

      <ReactECharts
        option={ getKOption() }
      />
    </div>
  )
}

export default Index;
```

## 2.antv - g2

```ts
{
    key: '0-5',
    title: '数据可视化',
    path: '/data',
    icon: HomeOutlined,
    children: [
      {
        key: '0-5-0',
        title: 'echarts',
        path: '/data/ehcarts',
        icon: HomeOutlined,
        component: React.lazy(() => import('./../views/data/Echart'))
      },
      {
        key: '0-5-1',
        title: 'g2',
        path: '/data/g2',
        icon: HomeOutlined,
        component: React.lazy(() => import('./../views/data/G2'))
      }
    ]
  }
```

```tsx
// src/views/data/G2.tsx
import React, { FC, useLayoutEffect } from 'react';
import { Chart } from '@antv/g2'
type Props = {};

const Index: FC<Props> = (props) => {
  const data = [
    { genre: 'Sports', sold: 275 },
    { genre: 'Strategy', sold: 115 },
    { genre: 'Action', sold: 120 },
    { genre: 'Shooter', sold: 350 },
    { genre: 'Other', sold: 150 },
  ];
  
  useLayoutEffect(() => {

    // Step 1: 创建 Chart 对象
    const chart = new Chart({
      container: 'c1', // 指定图表容器 ID
      width: 600, // 指定图表宽度
      height: 300, // 指定图表高度
    });
    
    // Step 2: 载入数据源
    chart.data(data);
    
    // Step 3: 创建图形语法，绘制柱状图
    chart.interval().position('genre*sold');
    
    // Step 4: 渲染图表
    chart.render();
  })
  return (
    <div>
      <h1>数据可视化：g2</h1>
      <div id="c1"></div>
    </div>
  )
}

export default Index;
```

# 23.编辑器

## 1.富文本编辑器

```
cnpm i braft-editor -S
```

```tsx
// src/views/form/sditor
import React, { FC, useState } from 'react';
import { Input } from 'antd'
import BraftEditor from 'braft-editor'
import 'braft-editor/dist/index.css'
type Props = {};

const Index: FC<Props> = (props) => {
  const [ title, setTitle ] = useState('')
  const [editorState, setEditorState] = useState(BraftEditor.createEditorState('<p>Hello <b>World!</b></p>'),)
  const [outHtml, setOutHtml] = useState('')
  const handleChange = (editorState: any) => {
    console.log('editorState', editorState)
    setEditorState(editorState)
    setOutHtml(editorState.toHTML())
  }
  return (
    <div>
      富文本编辑器
      <Input placeholder="标题" value= { title } onChange= { (e) => {
        setTitle(e.target.value)
      } }/>
      <BraftEditor
        value={editorState}
        onChange={handleChange}
      />

      <h3>输出结果如下： { outHtml }</h3>
      <h4>预览效果：</h4>
      <div dangerouslySetInnerHTML={{ __html: outHtml }}></div>
    </div>
  )
}

export default Index;
```



## 2.markDown编辑器

```ts
{
    key: '0-6',
    title: '编辑器',
    path: '/form',
    icon: HomeOutlined,
    children: [
      {
        key: '0-6-0',
        title: '富文本编辑器',
        path: '/form/editor',
        icon: HomeOutlined,
        component: React.lazy(() => import('./../views/form/Editor'))
      },
      {
        key: '0-6-1',
        title: 'markDown编辑器',
        path: '/form/md',
        icon: HomeOutlined,
        component: React.lazy(() => import('./../views/form/Md'))
      }
    ]
  },
```



阅读器：https://www.npmjs.com/package/react-markdown

编辑器：https://www.npmjs.com/package/react-markdown-editor-lite

```jsx
// src/views/form/Editor.tsx
import React, { FC, useState } from 'react';
import { Input } from 'antd'
import BraftEditor from 'braft-editor'
import 'braft-editor/dist/index.css'
type Props = {};

const Index: FC<Props> = (props) => {
  const [ title, setTitle ] = useState('')
  const [editorState, setEditorState] = useState(BraftEditor.createEditorState('<p>Hello <b>World!</b></p>'),)
  const [outHtml, setOutHtml] = useState('')
  const handleChange = (editorState: any) => {
    console.log('editorState', editorState)
    setEditorState(editorState)
    setOutHtml(editorState.toHTML())
  }
  return (
    <div>
      富文本编辑器
      <Input placeholder="标题" value= { title } onChange= { (e) => {
        setTitle(e.target.value)
      } }/>
      <BraftEditor
        value={editorState}
        onChange={handleChange}
      />

      <h3>输出结果如下： { outHtml }</h3>
      <h4>预览效果：</h4>
      <div dangerouslySetInnerHTML={{ __html: outHtml }}></div>
    </div>
  )
}

export default Index;

```

```tsx
// src/views/form/ Md.tsx
import React, { FC } from 'react';
import ReactMarkdown from 'react-markdown'
import MdEditor from 'react-markdown-editor-lite';
// import style manually
import 'react-markdown-editor-lite/lib/index.css';
type Props = {};

const Index: FC<Props> = (props) => {
  
  const handleEditorChange = ( { html, text }: { html: any; text: any}) => {
    console.log('handleEditorChange', html, text);
  }
  return (
    <div>
      markDown编辑器
      <MdEditor style={{ height: '500px' }} renderHTML={text => {
        return <ReactMarkdown>{ text }</ReactMarkdown>
      }} onChange={handleEditorChange} />
    </div>
  )
}

export default Index;
```



# 24.导入以及导出

````tsx
{
    key: '0-7',
    title: '导入导出',
    path: '/excel',
    icon: HomeOutlined,
    children: [
      {
        key: '0-7-0',
        title: '导出数据',
        path: '/excel/export',
        icon: HomeOutlined,
        component: React.lazy(() => import('./../views/excel/Export'))
      },
      {
        key: '0-7-1',
        title: '导入数据',
        path: '/excel/import',
        icon: HomeOutlined,
        component: React.lazy(() => import('./../views/excel/Import'))
      }
    ]
  },
````



## 1.导出

> 如果只是纯HTML元素的table表格的数据导出
>
> ```
> cnpm i react-html-table-to-excel -S
> ```
>
> ```
> // src/test.d.ts
> declare module 'react-html-table-to-excel';
> ```
>
> > 因为本模块没有ts支持的声明文件
>
> ```tsx
> import React, { FC} from 'react';
> import { Space} from 'antd'
> import ReactHTMLTableToExcel from 'react-html-table-to-excel';
> type Props = {};
> 
> const Index: FC<Props> = (props) => {
> 
> 
>   return (
>     <Space direction="vertical" style= {{ width: '100%' }}>
>       <ReactHTMLTableToExcel
>         id="test-table-xls-button"
>         className="download-table-xls-button"
>         table="table-to-xls"
>         filename="tablexls"
>         sheet="tablexls"
>         buttonText="导出数据"
>         > </ReactHTMLTableToExcel>
>         <table id="table-to-xls">
>           <thead>
>             <tr>
>               <th>序号</th>
>               <th>姓名</th>
>             </tr>
>           </thead>
>           <tbody>
>             <tr>
>               <td>1</td>
>               <td>吴勋勋</td>
>             </tr>
>           </tbody>
>         </table>
> 
>     </Space>
>   )
> }
> 
> export default Index;
> ```

> cnpm i js-export-excel -S

```tsx
import React, { FC, useState, useEffect } from 'react';
import { Space, Table, Button, Drawer, Form, Input, Select, Tree } from 'antd'
import { getAdminList, addAdmin } from './../../api/admin'
import { useColumns, usePagination, useDrawer, useAddAdmin } from './hooks'
import menus from './../../router/menus'
import ExportJsonExcel from 'js-export-excel'
type Props = {};

export interface IAdmin {
  adminid: string
  adminname: string
  password: string
  role: number
  checkedKeys: Array<string>
}

const Index: FC<Props> = (props) => {


  // 设计高度
  const [ height, setHeight ] = useState(0)
  useEffect(() => {
    setHeight(window.innerHeight)
  }, [])

  // 分页
  const { current, pageSize, changePageSize, changePage } = usePagination()
  // 表格设计
  const { columns, flag } = useColumns(current, pageSize)

  // 列表数据
  const [adminList, setAdminList] = useState([])
  useEffect(() => {
    getAdminList().then(res => {
      setAdminList(res.data)
    })
  }, [])

  // 抽屉效果
  const { visible, showDrawer, closeDrawer } = useDrawer()

  // 删除数据
  useEffect(() => {
    getAdminList().then(res => {
      setAdminList(res.data)
    })
  }, [flag])
  // 添加管理员表单
  const { 
    adminname,
    password, 
    role,
    checkedKeys,
    changeAdminName,
    changePassword,
    changeRole,
    changeCheckedKeys,
    reset } = useAddAdmin()

  
  // 导出数据
  const exportExcel = () => {
    let dataTable = [];  //excel文件中的数据内容
    let option: {
      fileName?: string;
      datas?: any[]
    } = {};  //option代表的就是excel文件
      
    dataTable  = adminList;  //从props中获取数据源
    option.fileName = '下载文档';  //excel文件名称
    option.datas = [
        {
            sheetData: adminList,  //excel文件中的数据源
            sheetName: '安全组',  //excel文件中sheet页名称
            sheetFilter: ['adminname', 'password', 'role', 'checkedKeys'],  //excel文件中需显示的列数据
            sheetHeader: ['账户', '密码', '角色', '权限'],  //excel文件中每列的表头名称
        }
    ]
    let toExcel = new ExportJsonExcel(option);  //生成excel文件
    toExcel.saveExcel();  //下载excel文件
  }
  return (
    <Space direction="vertical" style= {{ width: '100%' }}>
      <Space>
        <Button type="primary" onClick = { showDrawer } >添加管理员</Button>
        <Button type="primary" onClick = { exportExcel } >导出数据</Button>
      </Space>
      <Table 
        dataSource = { adminList } 
        columns = { columns } 
        rowKey="adminid"
        pagination ={{
          current,
          pageSize,
          pageSizeOptions: [ '2', '5', '10', '20'],
          showSizeChanger: true,
          onShowSizeChange: changePageSize,
          onChange: changePage,
          showQuickJumper: true,
          showTotal: (total: number) => <span>共有{total}条数据</span>,
          position: ['bottomRight']
        }}
        scroll = {{ y: height - 330 }}
        ></Table>
    
      <Drawer 
        title="添加管理员" 
        placement="right" 
        width="500"
        onClose={closeDrawer} visible={visible}
      >
        <Form>
          <Form.Item>
            <Input placeholder = "管理员账户" value={ adminname } onChange = { changeAdminName }></Input>
          </Form.Item>
          <Form.Item>
            <Input type="password" placeholder = "密码" value={ password } onChange = { changePassword }></Input>
          </Form.Item>
          <Form.Item>
            <Select defaultValue = { 1 } value= { role} onChange= { changeRole }>
              <Select.Option value = { 1 }>管理员</Select.Option>
              <Select.Option value = { 2 }>超级管理员</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item>
            <Tree 
              checkable 
              onCheck = { changeCheckedKeys }
              checkedKeys = { checkedKeys }
              treeData = { menus }
              />

          </Form.Item>
          <Form.Item>
            <Button type="primary"
              onClick = { () => {
                const data = {
                  adminname,
                  password, 
                  role,
                  checkedKeys,
                }
                // console.log(data)
                addAdmin(data).then(res => {
                  reset()
                  getAdminList().then(res => setAdminList(res.data))
                  closeDrawer()
                })
              }}
            >添 加</Button>
          </Form.Item>
        </Form>
      </Drawer>
    </Space>
  )
}

export default Index;
```

> 以上方案为纯前端的导出，实际上还有其余的导出方法，比如通过接口实现，前端可以通过a的href属性实现

## 2.导入

```
cnpm install xlsx
```

```tsx
// src/views/excel/Import.tsx
import React, { FC, useRef, useState } from 'react';
import { Table } from 'antd'
import * as XLSX from 'xlsx'
interface IUser {
  index: string
  name: string
  password: string
  age: string
}
type Props = {};
const Index: FC<Props> = (props) => {
  const fileRef = useRef<any>()
  const [ list, setList ] = useState<Array<IUser>>([])
  const upLoadFile = () => {
    const file = fileRef.current.files[0]
    const reader = new FileReader()
    reader.readAsBinaryString(file)
    reader.onload = function () {
      // console.log(this.result)
      const workbook = XLSX.read(this.result, { type: 'binary' });
      const t = workbook.Sheets['Sheet1']
      // console.log(t)
      const r: IUser[] = XLSX.utils.sheet_to_json(t)
      // console.log(r)
      setList(r)
      // 同步上传到服务器
    }

  }

  const columns = [
    {
      title: '序号',
      dataIndex: 'index'
    },
    {
      title: '姓名',
      dataIndex: 'name'
    },
    {
      title: '密码',
      dataIndex: 'password'
    },
    {
      title: '年龄',
      dataIndex: 'age'
    }
  ]
  return (
    <div>
      <input type="file" ref={fileRef} onChange = { upLoadFile }/>
      <Table dataSource={ list } columns = { columns } rowKey = "index"></Table>
    </div>
  )
}

export default Index;
```

> 如果在nodejs环境中,通过接口实现
>
> ```js
> const xlsx = require('node-xlsx').default;
> // 导入excel表格的数据
> router.get('/uploadPro', (req, res, next) => {
>   const originData = xlsx.parse(`${__dirname}/pro.xlsx`);
>   const firstData = originData[0].data
>   const arr = []
>   for (var i = 0; i < firstData.length; i++) { 
>     if (i !== 0) {
>       arr.push({
>         proid: 'pro_'+ uuid.v4(),
>         category: firstData[i][0],
>         brand: firstData[i][1],
>         proname: firstData[i][2],
>         banners: firstData[i][3],
>         originprice: firstData[i][4],
>         sales: firstData[i][5],
>         stock: firstData[i][6],
>         desc: firstData[i][7],
>         issale: firstData[i][8],
>         isrecommend: firstData[i][9],
>         discount: firstData[i][10],
>         isseckill: firstData[i][11],
>         img1: firstData[i][12],
>         img2: firstData[i][13],
>         img3: firstData[i][14],
>         img4: firstData[i][15],
>       })
>     }
>   }
>   // 拿到 arr 的数据，先清空 产品的表格数据，然后再插入
>   mysql.delete(Product, {}, 1).then(() => { // 不要忘记写1，因为1 代表的是删除多条数据
>     // 所有的数据已删除完成
>     // 插入数据
>     mysql.insert(Product, arr).then(() => {
>       // 重定向到 商品的管理的页面路由
>       res.send('导入数据成功') // 相当于浏览器自动跳转到了 /pro 的路由
>     })
>   })
> })
> ```
>
> 

# 25.地图

https://huiyan.baidu.com/github/react-bmapgl/#/%E5%BF%AB%E9%80%9F%E5%85%A5%E9%97%A8

```tsx
// src/views/map/Inex.tsx
import React, { FC } from 'react';
import {Map, Marker, NavigationControl, ZoomControl, InfoWindow} from 'react-bmapgl';
type Props = {};

const Index: FC<Props> = (props) => {
  return (
    <div>
      百度地图
      <Map center={{lng: 116.402544, lat: 39.928216}} zoom="11">
        <NavigationControl map=""> </NavigationControl>
        <ZoomControl map=""></ZoomControl>
        <Marker map="" icon="" position={{lng: 116.402544, lat: 39.928216}} ></Marker>
        
        <InfoWindow map="" position={{lng: 116.402544, lat: 39.928216}} text="内容" title="标题"/>
      </Map>
    </div>
  )
}

export default Index;
```

```ts
{
    key: '0-8', 
    title: '百度地图',
    path: '/map/b',
    icon: HomeOutlined,
    component: React.lazy(() => import('./../views/map/Index'))
  },
```

```ts
{
    key: '0-9', 
    title: '高德地图',
    path: '/map/a',
    icon: HomeOutlined,
    component: React.lazy(() => import('./../views/map/AMap'))
  },
```

```tsx
//src/views/map/AMap.tsx
import React, { FC, useLayoutEffect } from 'react';

type Props = {};

const Index: FC<Props> = (props) => {
  useLayoutEffect(() => {
    var map = new window.AMap.Map('container', {
      zoom:11,//级别
      center: [116.397428, 39.90923],//中心点坐标
      viewMode:'3D'//使用3D视图
    });
  })
  return (
    <div>
      高德地图
      <div id="container" style={{ width: 1000, height: 500}}></div> 
    </div>
  )
}

export default Index;
```

```ts
// src/test.d.ts
declare module 'react-html-table-to-excel';
declare module 'js-export-excel';

declare interface Window {
  AMap: any
}
```

```html
// public/index.html
<script type="text/javascript" src="https://webapi.amap.com/maps?v=1.4.15&key=8c3ecb960bdcd6664c8a060c3dbc961b"></script> 
    <script>
      window.AMap = AMap
    </script>
```



