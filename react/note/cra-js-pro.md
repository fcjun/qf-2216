# 一.react后台管理系统

## 1.创建项目

```node
npx create-react-app haigou-admin

```

##  2.配置装饰器(但不是必须)

安装模块

```
cnpm i @babel/plugin-proposal-decorators customize-cra react-app-rewired -D
yarn add @babel/plugin-proposal-decorators customize-cra react-app-rewired -D
```

根目录下创建config-overrides.js

```js
// 参考配置连接：https://www.npmjs.com/package/customize-cra
const { override, addDecoratorsLegacy } = require("customize-cra")

module.exports = override(
  addDecoratorsLegacy() // 配置装饰器模式
)
```

修改package.json运行命令

```json
...,
"scripts": {
	"start": "react-app-rewired start", // update
	"build": "react-app-rewired build", // update
	"test": "react-app-rewired test",  // update
	"eject": "react-scripts eject"
},
...
```

## 3.配置UI库

https://ant.design/index-cn 查看文档

https://ant.design/docs/react/use-with-create-react-app-cn 查看具体的使用文档

```
cnpm i antd -S 
yarn add antd -S
```

删除src文件内容(保留reportWebVitals.js, setupTests.js,logo.svg,index.js,index.css)

index.css

```css
@import '~antd/dist/antd.css';

```

index.js

```js
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

```

测试UI库 App.jsx

```js
import React from 'react'
import { Button } from 'antd'
const App = () => {
  return (
    <>
      <Button  type="primary">hello</Button>
    </>
  )
}

export default App
```

## 4.修改目录结构

```
src
	api
	components
	layout
	router
	store
	utils
	views
```



## 5.搭建项目的主结构

查看Layout 组件，修改App.jsx

```js
import React from 'react';
import { Layout, Menu } from 'antd';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
} from '@ant-design/icons';

const { Header, Sider, Content } = Layout;

class App extends React.Component {
  state = {
    collapsed: false,
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
    return (
      <Layout id="components-layout-demo-custom-trigger">
        <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
          <div className="logo" />
          <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
            <Menu.Item key="1" icon={<UserOutlined />}>
              nav 1
            </Menu.Item>
            <Menu.Item key="2" icon={<VideoCameraOutlined />}>
              nav 2
            </Menu.Item>
            <Menu.Item key="3" icon={<UploadOutlined />}>
              nav 3
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Header className="site-layout-background" style={{ padding: 0 }}>
            {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
              className: 'trigger',
              onClick: this.toggle,
            })}
          </Header>
          <Content
            className="site-layout-background"
            style={{
              margin: '24px 16px',
              padding: 24,
              minHeight: 280,
            }}
          >
            Content
          </Content>
        </Layout>
      </Layout>
    );
  }
}

export default App
```

添加logo

```js
<Sider trigger={null} collapsible collapsed={this.state.collapsed}>
  <div className="logo" >
    <img src={ logo } style={{width: '32px', height: '32px', margin: '0 10px 0 0'}} alt=""/>
      { this.state.collapsed ? null : <span className="logoname">HAIGOU_ADMIN</span>  }
  </div>
  <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
    <Menu.Item key="1" icon={<UserOutlined />}>
           nav 1
  	</Menu.Item>
  	<Menu.Item key="2" icon={<VideoCameraOutlined />}>
    	nav 2
  	</Menu.Item>
  	<Menu.Item key="3" icon={<UploadOutlined />}>
    nav 3
  	</Menu.Item>
  </Menu>
</Sider>
        
```

相关样式 index.css

```css
@import '~antd/dist/antd.css';

#components-layout-demo-custom-trigger .trigger {
  padding: 0 24px;
  font-size: 18px;
  line-height: 64px;
  cursor: pointer;
  transition: color 0.3s;
}

#components-layout-demo-custom-trigger .trigger:hover {
  color: #1890ff;
}

#components-layout-demo-custom-trigger .logo {
  height: 32px;
  margin: 16px;
  background: rgba(255, 255, 255, 0.3);
  overflow: hidden; /* ++++++ */
}
#components-layout-demo-custom-trigger .logo .logoname{ /* ++++++ */
  color: #fff;
  font-weight: bold;
}

.site-layout .site-layout-background {
  background: #fff;
}

#root, #components-layout-demo-custom-trigger { /* ++++++ */
  height: 100%;
} 
```

移植App.jsx代码至 layout/main/Index.jsx,**`注意logo的路径`**

```jsx
import React from 'react';
import { Layout, Menu } from 'antd';
import logo from '../../logo.svg'
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
} from '@ant-design/icons';

const { Header, Sider, Content } = Layout;

class App extends React.Component {
  state = {
    collapsed: false,
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
    return (
      <Layout id="components-layout-demo-custom-trigger">
        <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
          <div className="logo" >
              <img src={ logo } style={{width: '32px', height: '32px', margin: '0 10px 0 0'}} alt=""/>
              { this.state.collapsed ? null : <span className="logoname">HAIGOU_ADMIN</span>  }
            </div>
          <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
            <Menu.Item key="1" icon={<UserOutlined />}>
              nav 1
            </Menu.Item>
            <Menu.Item key="2" icon={<VideoCameraOutlined />}>
              nav 2
            </Menu.Item>
            <Menu.Item key="3" icon={<UploadOutlined />}>
              nav 3
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Header className="site-layout-background" style={{ padding: 0 }}>
            {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
              className: 'trigger',
              onClick: this.toggle,
            })}
          </Header>
          <Content
            className="site-layout-background"
            style={{
              margin: '24px 16px',
              padding: 24,
              minHeight: 280,
            }}
          >
            Content
          </Content>
        </Layout>
      </Layout>
    );
  }
}

export default App
```



安装路由模块(react-router-dom刚更新为V6)

```
yarn add react-router-dom@5 -S
```

修改App.jsx

```js
import { HashRouter as Router, Switch, Route } from 'react-router-dom'
import Index from './layout/main/Index.jsx'
const App = () => {
  return (
    <Router >
      <Switch>
        <Route path="/" component = { Index } />
      </Switch>
    </Router>
  )
}

export default App
```

## 6.拆分组件

```jsx
// src/layout/main/components/SideBar.jsx
import React, { Component } from 'react';
import { Layout, Menu } from 'antd';
import logo from '../../../logo.svg';
import {
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
} from '@ant-design/icons';
const { Sider } = Layout;
export default class SideBar extends Component {
  state = {
    collapsed: false,
  };
  render() {
    return (
      <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
        <div className="logo" >
            <img src={ logo } style={{width: '32px', height: '32px', margin: '0 10px 0 0'}} alt=""/>
            { this.state.collapsed ? null : <span className="logoname">HAIGOU_ADMIN</span>  }
          </div>
        <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
          <Menu.Item key="1" icon={<UserOutlined />}>
            nav 1
          </Menu.Item>
          <Menu.Item key="2" icon={<VideoCameraOutlined />}>
            nav 2
          </Menu.Item>
          <Menu.Item key="3" icon={<UploadOutlined />}>
            nav 3
          </Menu.Item>
        </Menu>
      </Sider>
    );
  }
}

```

```jsx
// src/layout/main/components/AppHeader.jsx
import React, { Component } from 'react'
import { Layout } from 'antd';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined
} from '@ant-design/icons';
const { Header } = Layout;
export default class AppHeader extends Component {
  state = {
    collapsed: false,
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };
  render() {
    return (
      <Header className="site-layout-background" style={{ padding: 0 }}>
        { React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
          className: 'trigger',
          onClick: this.toggle,
        }) }
      </Header>
    )
  }
}

```

```jsx
// src/layout/main/components/AppMain.jsx
import React, { Component } from 'react'
import { Layout } from 'antd'import SideBar from "./SIdeBar"

export { default as SideBar } from './SideBar.jsx';
export { default as AppMain } from './AppMain.jsx';
export { default as AppHeader } from './AppHeader.jsx';
const { Content } = Layout
export default class AppMain extends Component {
  render() {
    return (
      <Content
        className="site-layout-background"
        style={{
          margin: '24px 16px',
          padding: 24,
          minHeight: 280,
        }}
      >
        Content
      </Content>
    )
  }
}

```

```jsx
// src/layout/main/components/index.js
export { default as SideBar } from './SideBar.jsx';
export { default as AppMain } from './AppMain.jsx';
export { default as AppHeader } from './AppHeader.jsx';
```

```jsx
// src/layout/main/Index.jsx
import React from 'react';
import { Layout } from 'antd';

import { SideBar, AppHeader, AppMain } from './components'

class App extends React.Component {

  render() {
    return (
      <Layout id="components-layout-demo-custom-trigger">
        <SideBar />
        <Layout className="site-layout">
          <AppHeader></AppHeader>
          <AppMain></AppMain>
        </Layout>
      </Layout>
    );
  }
  
}

export default App
```



## 7 .保留用户的使用习惯

> 如果左侧的菜单栏用户选择缩回去，刷新页面之后还应该是缩回去的，反之亦然

```
cnpm i redux react-redux redux-thunk immutable redux-immutable -S
```

```js
// src/store/modules/app.js
import { Map } from 'immutable'

const app = (state = Map({
  collapsed: localStorage.getItem('collapsed') === 'true'
}), action) => {
  switch (action.type) {
    case 'CHANGE_COLLAPSED':
      localStorage.setItem('collapsed', !state.get('collapsed'))
      return state.set('collapsed', !state.get('collapsed'))
    default:
      return state
  }
}

export default app
```

```js
// src/store/index.js
import { createStore, applyMiddleware } from 'redux'
import { combineReducers } from 'redux-immutable'
import thunk from 'redux-thunk'

import app from './modules/app'

const reducer = combineReducers({
  app
})

const store = createStore(reducer, applyMiddleware(thunk))

export default store
```

```jsx
// src/index.js
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux'
import store from './store'

ReactDOM.render(
  <React.StrictMode>
    <Provider store = { store }>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

```

```jsx
// src/layout/main/components/SideBar.jsx
import React, { Component } from 'react';
import { Layout, Menu } from 'antd';
import logo from '../../../logo.svg';

import { connect } from 'react-redux'
import {
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
} from '@ant-design/icons';
const { Sider } = Layout;

@connect((state) => {
  return {
    collapsed: state.getIn(['app', 'collapsed'])
  }
})
class SideBar extends Component {
  render() {
    return (
      <Sider trigger={null} collapsible collapsed={this.props.collapsed}>
        <div className="logo" >
            <img src={ logo } style={{width: '32px', height: '32px', margin: '0 10px 0 0'}} alt=""/>
            { this.props.collapsed ? null : <span className="logoname">HAIGOU_ADMIN</span>  }
          </div>
        <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
          <Menu.Item key="1" icon={<UserOutlined />}>
            nav 1
          </Menu.Item>
          <Menu.Item key="2" icon={<VideoCameraOutlined />}>
            nav 2
          </Menu.Item>
          <Menu.Item key="3" icon={<UploadOutlined />}>
            nav 3
          </Menu.Item>
        </Menu>
      </Sider>
    );
  }
}
export default SideBar // 装饰器写法
// export default connect((state) => { // 高阶组件的写法
//   return {
//     collapsed: state.getIn(['app', 'collapsed'])
//   }
// })(SideBar)
```

```jsx
// src/layout/main/components/AppHeader.jsx
import React, { Component } from 'react'
import { Layout } from 'antd';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined
} from '@ant-design/icons';
import { connect } from 'react-redux';
const { Header } = Layout;
@connect(state => {
  return {
    collapsed: state.getIn(['app', 'collapsed'])
  }
}, (dispatch) => {
  return {
    changeCollapsed () {
      dispatch({
        type: 'CHANGE_COLLAPSED'
      })
    }
  }
})
class AppHeader extends Component {

  toggle = () => {
    this.props.changeCollapsed()
  };
  render() {
    return (
      <Header className="site-layout-background" style={{ padding: 0 }}>
        { React.createElement(this.props.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
          className: 'trigger',
          onClick: this.toggle,
        }) }
      </Header>
    )
  }
}
export default AppHeader
```

> 点击切换图标，刷新浏览器查看效果



## 8左侧菜单



生成左侧菜单的基本配置信息 router/menus.js

```js
import {
  HomeOutlined,
  PictureOutlined,
  MenuOutlined
} from '@ant-design/icons';
const routes = [

  {
    key: '0-0',
    path: '/',
    title: '系统首页',
    icon: <HomeOutlined />
  },
  {
    key: '0-1',
    path: '/banner',
    title: '轮播图管理',
    icon: <PictureOutlined />,
    children: [
      {
        key: '0-1-0',
        path: '/banner/list',
        title: '轮播图列表',
        icon: <MenuOutlined />
      },
      {
        key: '0-1-1',
        path: '/banner/add',
        title: '添加轮播图',
        icon: <MenuOutlined />
      }
    ]
  },
  {
    key: '0-2',
    path: '/pro',
    title: '产品管理',
    icon: <PictureOutlined />,
    children: [
      {
        key: '0-2-0',
        path: '/pro/list',
        title: '产品列表',
        icon: <MenuOutlined />
      },
      {
        key: '0-2-1',
        path: '/pro/seckill',
        title: '秒杀列表',
        icon: <MenuOutlined />
      },
      {
        key: '0-2-2',
        path: '/pro/recommend',
        title: '推荐列表',
        icon: <MenuOutlined />
      },
      {
        key: '0-2-3',
        path: '/pro/search',
        title: '筛选列表',
        icon: <MenuOutlined />
      }
    ]
  },
  {
    key: '0-3',
    path: '/user',
    title: '账户管理',
    icon: <PictureOutlined />,
    children: [
      {
        key: '0-3-0',
        path: '/user/list',
        title: '用户列表',
        icon: <MenuOutlined />
      },
      {
        key: '0-3-1',
        path: '/user/admin',
        title: '管理员列表',
        icon: <MenuOutlined />
      }
    ]
  },
]

export default routes
```



配置左侧菜单栏( SideBar.jsx)

```js
import React, { Component } from 'react';
import { Layout, Menu } from 'antd';
import logo from '../../../logo.svg';

import { connect } from 'react-redux'
import routes from './../../../router/menus'

const { Sider } = Layout;
const { SubMenu } = Menu;

// 1.只展开二级菜单的选项 --- 注意值为数据中的path属性
const rootSubmenuKeys = []
routes.forEach(item => {
  if (item.children) {
    rootSubmenuKeys.push(item.path)
  }
})

@connect((state) => {
  return {
    collapsed: state.getIn(['app', 'collapsed'])
  }
})
class SideBar extends Component {
  // 2.展开的二级菜单的数据
  state = {
    openKeys: []
  }
  // 2.实现只展开其中的一个二级菜单
  onOpenChange = keys => {
    const latestOpenKey = keys.find(key => this.state.openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      this.setState({
        openKeys: keys
      })
    } else {
      this.setState({
        openKeys: latestOpenKey ? [latestOpenKey] : []
      })
    }
  }
  // 3.渲染左侧菜单栏数据，注意return的书写
  renderSideBar = (routes) => {
    return routes.map(item => {
      if (item.children) {
        return <SubMenu key={ item.path } icon={ item.icon } title={ item.title }>
          {
            this.renderSideBar(item.children)
          }
          </SubMenu>
      } else {
        return (
          <Menu.Item key={ item.path } icon={ item.icon }>
            { item.title }
          </Menu.Item>
        )
      }
    })
  }

  render() {
    return (
      <Sider trigger={null} collapsible collapsed={this.props.collapsed}>
        <div className="logo" >
            <img src={ logo } style={{width: '32px', height: '32px', margin: '0 10px 0 0'}} alt=""/>
            { this.props.collapsed ? null : <span className="logoname">HAIGOU_ADMIN</span>  }
          </div>
        <Menu theme="dark" mode="inline" openKeys={this.state.openKeys} onOpenChange={this.onOpenChange} defaultSelectedKeys={['1']}>
          {/* <Menu.Item key="1" icon={<UserOutlined />}>
            nav 1
          </Menu.Item> */}
          {
            // 3.调用递归函数渲染左侧的菜单栏
            this.renderSideBar(routes)
          }
        </Menu>
      </Sider>
    );
  }
}
export default SideBar
// export default connect((state) => {
//   return {
//     collapsed: state.getIn(['app', 'collapsed'])
//   }
// })(SideBar)
```



## 9.创建页面对应的组件

```jsx
// src/views/home/Index.jsx
const Com = () => {
  return (
    <div>系统首页</div>
  )
}
export default Com

// src/views/banner/Index.jsx
const Com = () => {
  return (
    <div>轮播图列表</div>
  )
}
export default Com
// src/views/banner/Add.jsx
const Com = () => {
  return (
    <div>添加轮播图</div>
  )
}
export default Com
// src/views/pro/Index.jsx
const Com = () => {
  return (
    <div>产品列表</div>
  )
}
export default Com
// src/views/pro/Seckill.jsx
const Com = () => {
  return (
    <div>秒杀列表</div>
  )
}
export default Com
// src/views/pro/Recommend.jsx
const Com = () => {
  return (
    <div>推荐列表</div>
  )
}
export default Com
// src/views/pro/Search.jsx
const Com = () => {
  return (
    <div>筛选列表</div>
  )
}
export default Com
// src/views/user/Index.jsx
const Com = () => {
  return (
    <div>用户列表</div>
  )
}
export default Com
// src/views/user/Admin.jsx
const Com = () => {
  return (
    <div>管理员列表</div>
  )
}
export default Com

// src/views/error/404.jsx
const Com = () => {
  return (
    <div>404</div>
  )
}
export default Com

// src/views/setting/Index
const Com = () => {
  return (
    <div>设置</div>
  )
}
export default Com
```



## 10.配置路由



使用 路由的 懒加载 - lazy + Suspense

```jsx
// src/router/menus.js
import { lazy } from 'react'
import {
  HomeOutlined,
  PictureOutlined,
  MenuOutlined
} from '@ant-design/icons';
const routes = [

  {
    key: '0-0',
    path: '/',
    title: '系统首页',
    icon: <HomeOutlined />,
    component: lazy(() => import('./../views/home/Index.jsx'))
  },
  {
    key: '0-1',
    path: '/banner',
    title: '轮播图管理',
    icon: <PictureOutlined />,
    children: [
      {
        key: '0-1-0',
        path: '/banner/list',
        title: '轮播图列表',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/banner/Index.jsx'))
      },
      {
        key: '0-1-1',
        path: '/banner/add',
        title: '添加轮播图',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/banner/Add.jsx'))
      }
    ]
  },
  {
    key: '0-2',
    path: '/pro',
    title: '产品管理',
    icon: <PictureOutlined />,
    children: [
      {
        key: '0-2-0',
        path: '/pro/list',
        title: '产品列表',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/pro/Index.jsx'))
      },
      {
        key: '0-2-1',
        path: '/pro/seckill',
        title: '秒杀列表',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/pro/Seckill.jsx'))
      },
      {
        key: '0-2-2',
        path: '/pro/recommend',
        title: '推荐列表',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/pro/Recommend.jsx'))
      },
      {
        key: '0-2-3',
        path: '/pro/search',
        title: '筛选列表',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/pro/Search.jsx'))
      }
    ]
  },
  {
    key: '0-3',
    path: '/user',
    title: '账户管理',
    icon: <PictureOutlined />,
    children: [
      {
        key: '0-3-0',
        path: '/user/list',
        title: '用户列表',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/user/Index.jsx'))
      },
      {
        key: '0-3-1',
        path: '/user/admin',
        title: '管理员列表',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/user/Admin.jsx'))
      }
    ]
  },
]

export default routes
```

页面配置路由 layout/main/components/AppMain.jsx

```jsx
import React, { Component, Suspense, lazy } from 'react'
import { Layout, Spin } from 'antd'
import { Switch, Route } from 'react-router-dom'
import routes from './../../../router/menus'
import NotMatch from './../../../views/error/404.jsx'
const { Content } = Layout
export default class AppMain extends Component {
  renderRoute = (routes) => {
    return routes.map(item => {
      if (item.children) {
        return this.renderRoute(item.children)
      } else {
        return (
          <Route key = { item.path } exact path = { item.path } component = { item.component } />
        )
      }
    })
  }
  render() {
    return (
      <Content
        className="site-layout-background"
        style={{
          margin: '24px 16px',
          padding: 24,
          minHeight: 280,
        }}
      >
        {/* Content */}
        <Suspense fallback = {  <Spin size="large"/> }>
          <Switch>
            {
              this.renderRoute(routes)
            }
            <Route path="*" component = { NotMatch } />
          </Switch>
        </Suspense>
      </Content>
    )
  }
}

```

**疑问：如果有的页面不需要在左侧菜单栏出现，如何处理**

先配置路由选项

```js
import { lazy } from 'react'
import {
  HomeOutlined,
  PictureOutlined,
  MenuOutlined
} from '@ant-design/icons';
const routes = [

  {
    key: '0-0',
    path: '/',
    title: '系统首页',
    icon: <HomeOutlined />,
    component: lazy(() => import('./../views/home/Index.jsx'))
  },
  {
    key: '0-1',
    path: '/banner',
    title: '轮播图管理',
    icon: <PictureOutlined />,
    children: [
      {
        key: '0-1-0',
        path: '/banner/list',
        title: '轮播图列表',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/banner/Index.jsx'))
      },
      {
        key: '0-1-1',
        path: '/banner/add',
        title: '添加轮播图',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/banner/Add.jsx')),
        hidden: true  // +++++++++
      }
    ]
  },
  {
    key: '0-2',
    path: '/pro',
    title: '产品管理',
    icon: <PictureOutlined />,
    children: [
      {
        key: '0-2-0',
        path: '/pro/list',
        title: '产品列表',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/pro/Index.jsx'))
      },
      {
        key: '0-2-1',
        path: '/pro/seckill',
        title: '秒杀列表',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/pro/Seckill.jsx'))
      },
      {
        key: '0-2-2',
        path: '/pro/recommend',
        title: '推荐列表',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/pro/Recommend.jsx'))
      },
      {
        key: '0-2-3',
        path: '/pro/search',
        title: '筛选列表',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/pro/Search.jsx'))
      }
    ]
  },
  {
    key: '0-3',
    path: '/user',
    title: '账户管理',
    icon: <PictureOutlined />,
    children: [
      {
        key: '0-3-0',
        path: '/user/list',
        title: '用户列表',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/user/Index.jsx'))
      },
      {
        key: '0-3-1',
        path: '/user/admin',
        title: '管理员列表',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/user/Admin.jsx'))
      }
    ]
  },
  { // +++++++++
    key: '0-4',
    path: '/setting',
    title: '设置',
    icon: <HomeOutlined />,
    component: lazy(() => import('./../views/setting/Index.jsx')),
    hidden: true
  },
]

export default routes
```

SideBar.jsx

```jsx
import React, { Component } from 'react';
import { Layout, Menu } from 'antd';
import logo from '../../../logo.svg';

import { connect } from 'react-redux'
import routes from './../../../router/menus'

const { Sider } = Layout;
const { SubMenu } = Menu;

// 1.只展开二级菜单的选项 --- 注意值为数据中的path属性
const rootSubmenuKeys = []
routes.forEach(item => {
  if (item.children) {
    rootSubmenuKeys.push(item.path)
  }
})

@connect((state) => {
  return {
    collapsed: state.getIn(['app', 'collapsed'])
  }
})
class SideBar extends Component {
  // 2.展开的二级菜单的数据
  state = {
    openKeys: []
  }
  // 2.实现只展开其中的一个二级菜单
  onOpenChange = keys => {
    const latestOpenKey = keys.find(key => this.state.openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      this.setState({
        openKeys: keys
      })
    } else {
      this.setState({
        openKeys: latestOpenKey ? [latestOpenKey] : []
      })
    }
  }
  // 3.渲染左侧菜单栏数据，注意return的书写
  renderSideBar = (routes) => {
    return routes.map(item => {
      if (item.children) {
        return <SubMenu key={ item.path } icon={ item.icon } title={ item.title }>
          {
            this.renderSideBar(item.children)
          }
          </SubMenu>
      } else {
        return (
          item.hidden ? null : <Menu.Item key={ item.path } icon={ item.icon }>
          { item.title }
        </Menu.Item>
        )
      }
    })
  }

  render() {
    return (
      <Sider trigger={null} collapsible collapsed={this.props.collapsed}>
        <div className="logo" >
            <img src={ logo } style={{width: '32px', height: '32px', margin: '0 10px 0 0'}} alt=""/>
            { this.props.collapsed ? null : <span className="logoname">HAIGOU_ADMIN</span>  }
          </div>
        <Menu theme="dark" mode="inline" openKeys={this.state.openKeys} onOpenChange={this.onOpenChange} defaultSelectedKeys={['1']}>
          {/* <Menu.Item key="1" icon={<UserOutlined />}>
            nav 1
          </Menu.Item> */}
          {
            // 3.调用递归函数渲染左侧的菜单栏
            this.renderSideBar(routes)
          }
        </Menu>
      </Sider>
    );
  }
}
export default SideBar
// export default connect((state) => {
//   return {
//     collapsed: state.getIn(['app', 'collapsed'])
//   }
// })(SideBar)

export default SideMenu
```

## 11.点击菜单栏跳转页面

```jsx
import React, { Component } from 'react';
import { Layout, Menu } from 'antd';
import { withRouter } from 'react-router-dom'
import logo from '../../../logo.svg';

import { connect } from 'react-redux'
import routes from './../../../router/menus'

const { Sider } = Layout;
const { SubMenu } = Menu;

// 1.只展开二级菜单的选项 --- 注意值为数据中的path属性
const rootSubmenuKeys = []
routes.forEach(item => {
  if (item.children) {
    rootSubmenuKeys.push(item.path)
  }
})

@connect((state) => {
  return {
    collapsed: state.getIn(['app', 'collapsed'])
  }
})
@withRouter  // +++++++++
class SideBar extends Component {
  // 2.展开的二级菜单的数据
  state = {
    openKeys: []
  }
  // 2.实现只展开其中的一个二级菜单
  onOpenChange = keys => {
    const latestOpenKey = keys.find(key => this.state.openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      this.setState({
        openKeys: keys
      })
    } else {
      this.setState({
        openKeys: latestOpenKey ? [latestOpenKey] : []
      })
    }
  }
  // 3.渲染左侧菜单栏数据，注意return的书写
  renderSideBar = (routes) => {
    return routes.map(item => {
      if (item.children) {
        return <SubMenu key={ item.path } icon={ item.icon } title={ item.title }>
          {
            this.renderSideBar(item.children)
          }
          </SubMenu>
      } else {
        return (
          item.hidden ? null : <Menu.Item key={ item.path } icon={ item.icon }>
          { item.title }
        </Menu.Item>
        )
      }
    })
  }
  changeUrl = ({ key }) => {
    // console.log(key)
    // console.log(this.props)
    this.props.history.push(key)
  }
  render() {
    return (
      <Sider trigger={null} collapsible collapsed={this.props.collapsed}>
        <div className="logo" >
            <img src={ logo } style={{width: '32px', height: '32px', margin: '0 10px 0 0'}} alt=""/>
            { this.props.collapsed ? null : <span className="logoname">HAIGOU_ADMIN</span>  }
          </div>
        <Menu onClick = { this.changeUrl } theme="dark" mode="inline" openKeys={this.state.openKeys} onOpenChange={this.onOpenChange} defaultSelectedKeys={['1']}>
          {/* <Menu.Item key="1" icon={<UserOutlined />}>
            nav 1
          </Menu.Item> */}
          {
            // 3.调用递归函数渲染左侧的菜单栏
            this.renderSideBar(routes)
          }
        </Menu>
      </Sider>
    );
  }
}
export default SideBar
// export default connect((state) => {
//   return {
//     collapsed: state.getIn(['app', 'collapsed'])
//   }
// })(SideBar)
```

**疑问：点击轮播图列表，刷新以后。选中的状态消失**

```jsx
import React, { Component } from 'react';
import { Layout, Menu } from 'antd';
import { withRouter } from 'react-router-dom'
import logo from '../../../logo.svg';

import { connect } from 'react-redux'
import routes from './../../../router/menus'

const { Sider } = Layout;
const { SubMenu } = Menu;

// 1.只展开二级菜单的选项 --- 注意值为数据中的path属性
const rootSubmenuKeys = []
routes.forEach(item => {
  if (item.children) {
    rootSubmenuKeys.push(item.path)
  }
})

@connect((state) => {
  return {
    collapsed: state.getIn(['app', 'collapsed'])
  }
})
@withRouter
class SideBar extends Component {
  // 2.展开的二级菜单的数据
  state = {
    openKeys: [],
    selectedKeys: [] // ++++++++++++++++
  }

  componentDidMount () {// ++++++++++++++++
    console.log(this.props.location.pathname)
    const pathname = this.props.location.pathname
    this.setState({
      openKeys: ['/' + pathname.split('/')[1]],
      selectedKeys: [pathname]
    })
  }

  // 2.实现只展开其中的一个二级菜单
  onOpenChange = keys => {
    const latestOpenKey = keys.find(key => this.state.openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      this.setState({
        openKeys: keys
      })
    } else {
      this.setState({
        openKeys: latestOpenKey ? [latestOpenKey] : []
      })
    }
  }
  // 3.渲染左侧菜单栏数据，注意return的书写
  renderSideBar = (routes) => {
    return routes.map(item => {
      if (item.children) {
        return <SubMenu key={ item.path } icon={ item.icon } title={ item.title }>
          {
            this.renderSideBar(item.children)
          }
          </SubMenu>
      } else {
        return (
          item.hidden ? null : <Menu.Item key={ item.path } icon={ item.icon }>
          { item.title }
        </Menu.Item>
        )
      }
    })
  }
  changeUrl = ({ key }) => {
    // console.log(key)
    // console.log(this.props)
    this.props.history.push(key)
    this.setState({ // ++++++++++++++++
      selectedKeys: [key]
    })
  }
  render() {
    return (
      <Sider trigger={null} collapsible collapsed={this.props.collapsed}>
        <div className="logo" >
            <img src={ logo } style={{width: '32px', height: '32px', margin: '0 10px 0 0'}} alt=""/>
            { this.props.collapsed ? null : <span className="logoname">HAIGOU_ADMIN</span>  }
          </div>
        <Menu 
          onClick = { this.changeUrl } 
          theme="dark"
          mode="inline" 
          openKeys={this.state.openKeys} 
          onOpenChange={this.onOpenChange} 
          selectedKeys={ this.state.selectedKeys }>  {/ * ++++++++++++++++ * /}
          {/* <Menu.Item key="1" icon={<UserOutlined />}>
            nav 1
          </Menu.Item> */}
          {
            // 3.调用递归函数渲染左侧的菜单栏
            this.renderSideBar(routes)
          }
        </Menu>
      </Sider>
    );
  }
}
export default SideBar
// export default connect((state) => {
//   return {
//     collapsed: state.getIn(['app', 'collapsed'])
//   }
// })(SideBar)
```

## 12.设计头部

main/AppHeader.jsx

```jsx
import React, { Component } from 'react'
import { Layout, Avatar, Menu, Dropdown } from 'antd';
import { Link } from 'react-router-dom'
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined
} from '@ant-design/icons';
import { connect } from 'react-redux';
const { Header } = Layout;
@connect(state => {
  return {
    collapsed: state.getIn(['app', 'collapsed'])
  }
}, (dispatch) => {
  return {
    changeCollapsed () {
      dispatch({
        type: 'CHANGE_COLLAPSED'
      })
    }
  }
})
class AppHeader extends Component {

  toggle = () => {
    this.props.changeCollapsed()
  };
  menu = (
    <Menu style={{ width: 100 }}>
      <Menu.Item >
        <Link to="/setting">设置</Link>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item >退出</Menu.Item>
    </Menu>
  )
  render() {
    return (
      <Header className="site-layout-background" style={{ padding: 0 }}>
        { React.createElement(this.props.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
          className: 'trigger',
          onClick: this.toggle,
        }) }
        <div className="userSet">
          欢迎您:admin &nbsp;&nbsp;&nbsp;&nbsp;
          <Dropdown overlay={this.menu} trigger={['click']}>
            <Avatar size="small" icon={<UserOutlined />} />
          </Dropdown>
        </div>
      </Header>
    )
  }
}
export default AppHeader
```

```css
 / * index.css */
...
.site-layout .site-layout-background {
  background: #fff;
  display: flex;
}

.site-layout-background .userSet {
  flex: 1;
  text-align: right;
  margin-right: 24px;
}
```



## 13.面包屑导航

选择参考 带路由的模式

```jsx
// AppMain.jsx
<Content
        className="site-layout-background"
        style={{
          margin: '0 16px 24px 16px', // +++++++++++++++++++
          padding: 24,
          minHeight: 280,
        }}
      >
  ......
</Content>
```

```jsx
// AppBreadcrumb.jsx
import React, { Component } from 'react';

export default class AppBreadcrumb extends Component {
  render() {
    return (
      <div style={{
        margin: '10px 16px'
      }}> AppBreadcrumb </div>
    );
  }
}

```

```js
// layout/main/components/index.js
export { default as SideBar } from './SideBar.jsx';
export { default as AppMain } from './AppMain.jsx';
export { default as AppHeader } from './AppHeader.jsx';
export { default as AppBreadcrumb } from './AppBreadcrumb.jsx';
```

```jsx
// src/layout/main/Index.jsx
import React from 'react';
import { Layout } from 'antd';

import { SideBar, AppHeader, AppMain, AppBreadcrumb } from './components'

class App extends React.Component {

  render() {
    return (
      <Layout id="components-layout-demo-custom-trigger">
        <SideBar />
        <Layout className="site-layout">
          <AppHeader></AppHeader>
          <AppBreadcrumb ></AppBreadcrumb>
          <AppMain></AppMain>
        </Layout>
      </Layout>
    );
  }
  
}

export default App
```

```jsx
// src/layout/main/components/AppBreadcrumb.jsx
import React, { Component } from 'react';
import routes from './../../../router/menus'
import { withRouter, Link } from 'react-router-dom'
// 1.得到数据字典
const breadcrumbNameMap = {}
function getData (routes) {
  routes.forEach(item => {
    if (item.children) {
      getData(item.children)
    }
    breadcrumbNameMap[item.path] = item.title
  })
}
getData(routes)
console.log(breadcrumbNameMap)
class AppBreadcrumb extends Component {
  state = { // 展示标题
    title: '/banner',
    subTitle: '/banner/add'
  }
  componentDidMount (){ // 初次渲染
    // console.log(1)
    const pathname = this.props.location.pathname
    this.setState({
      title: '/' + pathname.split('/')[1],
      subTitle: pathname
    })
  }
  componentDidUpdate (prevprops) { // 监听路由的变化
    // console.log(2, prevprops)
    // console.log(2, this.props)
    if (prevprops.location.pathname !== this.props.location.pathname) {
      const pathname = this.props.location.pathname
      this.setState({
        title: '/' + pathname.split('/')[1],
        subTitle: pathname
      })
    }
  }
  render() {
    return (
      <div style={{
        margin: '10px 16px'
      }}> 
        系统管理 / 
        <Link to="/"> 系统首页 </Link> /
       { this.state.title === '/'? null : (<><Link to={ this.state.title }> { breadcrumbNameMap[this.state.title] } </Link> /</>) } 
{        this.state.title === '/' ? null :  <Link to={ this.state.subTitle}> { breadcrumbNameMap[this.state.subTitle] } </Link> 
}      </div>
    );
  }
}

export default withRouter(AppBreadcrumb)

```

> 优化上面的面包屑

```jsx
import React, { Component } from 'react';
import routes from './../../../router/menus'
import { withRouter, Link } from 'react-router-dom'
// 1.得到数据字典
const breadcrumbNameMap = {}
function getData (routes) {
  routes.forEach(item => {
    if (item.children) {
      getData(item.children)
    }
    breadcrumbNameMap[item.path] = item.title
  })
}
getData(routes)
console.log(breadcrumbNameMap)
class AppBreadcrumb extends Component {
  state = {
    title: '/banner',
    subTitle: '/banner/add'
  }
  componentDidMount (){
    // console.log(1)
    const pathname = this.props.location.pathname
    this.setState({
      title: '/' + pathname.split('/')[1],
      subTitle: pathname
    })
  }
  componentDidUpdate (prevprops) {
    // console.log(2, prevprops)
    // console.log(2, this.props)
    if (prevprops.location.pathname !== this.props.location.pathname) {
      const pathname = this.props.location.pathname
      this.setState({
        title: '/' + pathname.split('/')[1],
        subTitle: pathname
      })
    }
  }
  render() {
    return (
      <div style={{
        margin: '10px 16px'
      }}> 
        系统管理 / 
        <Link to="/"> 系统首页 </Link> /
        {/* { this.state.title === '/'? null : (<><Link to={ this.state.title }> { breadcrumbNameMap[this.state.title] } </Link> /</>) } 
        { this.state.title === '/' ? null :  <Link to={ this.state.subTitle}> { breadcrumbNameMap[this.state.subTitle] } </Link> }       */}
        {/* <Link to={ this.state.title }> { breadcrumbNameMap[this.state.title] } </Link> /
        <Link to={ this.state.subTitle }> { breadcrumbNameMap[this.state.subTitle] } </Link> */}

        {
          this.state.title === this.state.subTitle 
            ? this.state.title === '/' 
              ? null 
              : <Link to={ this.state.title }> { breadcrumbNameMap[this.state.title] } </Link>  
            : <>
              <Link to={ this.state.title }> { breadcrumbNameMap[this.state.title] } </Link> /
              <Link to={ this.state.subTitle }> { breadcrumbNameMap[this.state.subTitle] } </Link>
            </>
        }
      </div>
    );
  }
}

export default withRouter(AppBreadcrumb)

```



## 14.路由重定向

```js
// src/router/menus.js
import { lazy } from 'react'
import {
  HomeOutlined,
  PictureOutlined,
  MenuOutlined
} from '@ant-design/icons';
const routes = [

  {
    key: '0-0',
    path: '/',
    title: '系统首页',
    icon: <HomeOutlined />,
    component: lazy(() => import('./../views/home/Index.jsx'))
  },
  {
    key: '0-1',
    path: '/banner',
    title: '轮播图管理',
    icon: <PictureOutlined />,
    redirect: '/banner/list',  // +++++++++
    children: [
      {
        key: '0-1-0',
        path: '/banner/list',
        title: '轮播图列表',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/banner/Index.jsx'))
      },
      {
        key: '0-1-1',
        path: '/banner/add',
        title: '添加轮播图',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/banner/Add.jsx')),
        hidden: true
      }
    ]
  },
  {
    key: '0-2',
    path: '/pro',
    title: '产品管理',
    icon: <PictureOutlined />,
    redirect: '/pro/list',  // +++++++++
    children: [
      {
        key: '0-2-0',
        path: '/pro/list',
        title: '产品列表',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/pro/Index.jsx'))
      },
      {
        key: '0-2-1',
        path: '/pro/seckill',
        title: '秒杀列表',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/pro/Seckill.jsx'))
      },
      {
        key: '0-2-2',
        path: '/pro/recommend',
        title: '推荐列表',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/pro/Recommend.jsx'))
      },
      {
        key: '0-2-3',
        path: '/pro/search',
        title: '筛选列表',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/pro/Search.jsx'))
      }
    ]
  },
  {
    key: '0-3',
    path: '/user',
    title: '账户管理',
    icon: <PictureOutlined />,
    redirect: '/user/list', // +++++++++
    children: [
      {
        key: '0-3-0',
        path: '/user/list',
        title: '用户列表',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/user/Index.jsx'))
      },
      {
        key: '0-3-1',
        path: '/user/admin',
        title: '管理员列表',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/user/Admin.jsx'))
      }
    ]
  },
  {
    key: '0-4',
    path: '/setting',
    title: '设置',
    icon: <HomeOutlined />,
    component: lazy(() => import('./../views/setting/Index.jsx')),
    hidden: true
  },
]

export default routes
```



```jsx
// src/layout/main/components/AppMain.jsx
import React, { Component, Suspense } from 'react'
import { Layout, Spin } from 'antd'
import { Switch, Route, Redirect } from 'react-router-dom'
import routes from './../../../router/menus'
import NotMatch from './../../../views/error/404.jsx'
const { Content } = Layout
export default class AppMain extends Component {
  renderRoute = (routes) => {
    return routes.map(item => {
      if (item.children) {
        return this.renderRoute(item.children)
      } else {
        return (
          <Route key = { item.path } exact path = { item.path } component = { item.component } />
        )
      }
    })
  }
  redirectRoute = (routes) => {//+++++++++++++++++++++++
    // 先处理routes数据，因为并不是所有的路由都有重定向
    const redirectRoutes = routes.filter(item => item.redirect)
    return redirectRoutes.map(item => {
      return <Redirect key={ item.path } path={ item.path } exact to={item.redirect} />
    })
    
  }
  render() {
    return (
      <Content
        className="site-layout-background"
        style={{
          margin: '0 16px 24px 16px',
          padding: 24,
          minHeight: 280,
        }}
      >
        {/* Content */}
        <Suspense fallback = {  <Spin size="large"/> }>
          <Switch>
            {
              this.renderRoute(routes)
            }
            { //+++++++++++++++++++++++
              this.redirectRoute(routes)
            }
            <Route path="*" component = { NotMatch } />
          </Switch>
        </Suspense>
      </Content>
    )
  }
}

```

> 此时发现点击面包屑的以及菜单，可以跳转路由了，但是左侧菜单栏的选中状态并没有发生任何的改变



调整左侧菜单的选中样式

```jsx
// src/layout/main/components/SideBar.jsx
import React, { Component } from 'react';
import { Layout, Menu } from 'antd';
import { withRouter } from 'react-router-dom'
import logo from '../../../logo.svg';

import { connect } from 'react-redux'
import routes from './../../../router/menus'

const { Sider } = Layout;
const { SubMenu } = Menu;

// 1.只展开二级菜单的选项 --- 注意值为数据中的path属性
const rootSubmenuKeys = []
routes.forEach(item => {
  if (item.children) {
    rootSubmenuKeys.push(item.path)
  }
})

@connect((state) => {
  return {
    collapsed: state.getIn(['app', 'collapsed'])
  }
})
@withRouter
class SideBar extends Component {
  // 2.展开的二级菜单的数据
  state = {
    openKeys: [],
    selectedKeys: []
  }

  componentDidMount () {
    console.log(this.props.location.pathname)
    const pathname = this.props.location.pathname
    this.setState({
      openKeys: ['/' + pathname.split('/')[1]],
      selectedKeys: [pathname]
    })
  }

  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  componentDidUpdate (prevProps) { // 点击面包屑时监听路由的变化，设计选中状态
    if (prevProps.location.pathname !== this.props.location.pathname) {
      const pathname = this.props.location.pathname
      this.setState({
        selectedKeys: [pathname]
      })
    }
  }

  // 2.实现只展开其中的一个二级菜单
  onOpenChange = keys => {
    const latestOpenKey = keys.find(key => this.state.openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      this.setState({
        openKeys: keys
      })
    } else {
      this.setState({
        openKeys: latestOpenKey ? [latestOpenKey] : []
      })
    }
  }
  // 3.渲染左侧菜单栏数据，注意return的书写
  renderSideBar = (routes) => {
    return routes.map(item => {
      if (item.children) {
        return <SubMenu key={ item.path } icon={ item.icon } title={ item.title }>
          {
            this.renderSideBar(item.children)
          }
          </SubMenu>
      } else {
        return (
          item.hidden ? null : <Menu.Item key={ item.path } icon={ item.icon }>
          { item.title }
        </Menu.Item>
        )
      }
    })
  }
  changeUrl = ({ key }) => {
    // console.log(key)
    // console.log(this.props)
    this.props.history.push(key)
    this.setState({
      selectedKeys: [key]
    })
  }
  render() {
    return (
      <Sider trigger={null} collapsible collapsed={this.props.collapsed}>
        <div className="logo" >
            <img src={ logo } style={{width: '32px', height: '32px', margin: '0 10px 0 0'}} alt=""/>
            { this.props.collapsed ? null : <span className="logoname">HAIGOU_ADMIN</span>  }
          </div>
        <Menu 
          onClick = { this.changeUrl } 
          theme="dark"
          mode="inline" 
          openKeys={this.state.openKeys} 
          onOpenChange={this.onOpenChange} 
          selectedKeys={ this.state.selectedKeys }>
          {/* <Menu.Item key="1" icon={<UserOutlined />}>
            nav 1
          </Menu.Item> */}
          {
            // 3.调用递归函数渲染左侧的菜单栏
            this.renderSideBar(routes)
          }
        </Menu>
      </Sider>
    );
  }
}
export default SideBar
// export default connect((state) => {
//   return {
//     collapsed: state.getIn(['app', 'collapsed'])
//   }
// })(SideBar)
```

## 15.实现登录相关

> 所有的后台管理系统的项目，都必须是用户在登录的情况下操作

### 1.设计登录的表单以及路由

```jsx
// src/views/login/Index.jsx
import React, { Component } from 'react';

export default class Login extends Component {
  render() {
    return (
      <div> 登录 </div>
    );
  }
}

```

```jsx
// src/App.jsx
import { HashRouter as Router, Switch, Route } from 'react-router-dom'
import Index from './layout/main/Index.jsx'
import Login from './views/login/Index.jsx'
const App = () => {
  return (
    <Router >
      <Switch>
        <Route path="/login" component = { Login } />
        <Route path="/" component = { Index } />
      </Switch>
    </Router>
  )
}

export default App
```

### 2.设计登录组件

https://ant.design/components/form-cn/#components-form-demo-normal-login

```jsx
// src/views/login/Index.jsx
import React from 'react';
import { Form, Input, Button } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
const Login = () => {
  const onFinish = (values) => {
    console.log('Received values of form: ', values);
  };

  return (
    <div className="loginBox">
      <Form
        name="normal_login"
        className="login-form"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
      >
        <Form.Item
          name="adminname"
          rules={[
            {
              required: true,
              message: '请输入管理员账户!',
            },
          ]}
        >
          <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="管理员账户" />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: '请输入正确的密码!',
            },
          ]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="密码"
          />
        </Form.Item>
        

        <Form.Item>
          <Button type="primary" htmlType="submit" className="login-form-button">
            登 录
          </Button>
         
        </Form.Item>
      </Form>
    </div>
  );
};
export default Login

```

```css
// src/index.css
@import '~antd/dist/antd.css';

#components-layout-demo-custom-trigger {
  margin: 0 16px 24px;
}
#components-layout-demo-custom-trigger .trigger {
  padding: 0 24px;
  font-size: 18px;
  line-height: 64px;
  cursor: pointer;
  transition: color 0.3s;
}

#components-layout-demo-custom-trigger .trigger:hover {
  color: #1890ff;
}

#components-layout-demo-custom-trigger .logo {
  height: 32px;
  margin: 16px;
  background: rgba(255, 255, 255, 0.3);
  overflow: hidden;
}
#components-layout-demo-custom-trigger .logo .logoname{
  color: #fff;
  font-weight: bold;
}

.site-layout .site-layout-background {
  background: #fff;
  display: flex;
}

.site-layout-background .userSet {
  flex: 1;
  text-align: right;
  margin-right: 24px;
}
#root, #components-layout-demo-custom-trigger {
  height: 100%;
} 

/* 登录表单 */
.loginBox {
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background: url(https://cas.1000phone.net/cas/images/login/bg.png) center no-repeat;
  background-size: cover;
  overflow: hidden;
  position: relative;
}
.login-form {
  width: 420px;
  padding: 30px 20px;
  background-color: #fff;
}
 .login-form-forgot {
  float: right;
}
.ant-col-rtl .login-form-forgot {
  float: left;
}
.login-form-button {
  width: 100%;
}
```

### 3.封装数据请求

```
cnpm i axios -S
```

```js
//src/utils/request.js
import axios from 'axios'
// 自定义axios
const ins = axios.create({
  baseURL: 'http://121.89.205.189/admin',
  timeout: 6000
})

// 自定义拦截器
// 请求拦截器
ins.interceptors.request.use((config) => {
  config.headers.token = localStorage.getItem('token') || ''
  return config
}, error => Promise.reject(error))

// 响应拦截器
ins.interceptors.response.use((response) => {
  return response.data
}, error => Promise.reject(error))

// 自定义各种数据请求 axios({})
export default function request (config) {
  const { url = '', method = 'GET', data = {}, headers = {} } = config
  switch (method.toUpperCase()) {
    case 'GET':
      return ins.get(url, { params: data })
    case 'POST':
      // 表单提交  application/x-www-form-url-encoded
      if (headers['content-type'] === 'application/x-www-form-url-encoded') {
        // 转参数 URLSearchParams/第三方库qs
        const p = new URLSearchParams()
        for (const key in data) {
          p.append(key, data[key])
        }
        return ins.post(url, p, { headers })
      }
      // 文件提交  multipart/form-data
      if (headers['content-type'] === 'multipart/form-data') {
        const p = new FormData()
        for (const key in data) {
          p.append(key, data[key])
        }
        return ins.post(url, p, { headers })
      }
      // 默认 application/json
      return ins.post(url, data)
    case 'PUT': // 修改数据 --- 所有的数据的更新
      return ins.put(url, data)
    case 'DELETE': // 删除数据
      return ins.delete(url, { data })
    case 'PATCH': // 更新局部资源
      return ins.patch(url, data)
    default:
      return ins(config)
  }
}

```

```js
// src/api/user.js
import request from './../utils/request'

// 登录接口
export function loginFn (params) {
  return request({
    url: '/admin/login',
    method: 'POST',
    data: params
  })
}
```

### 4.设计状态管理

```js
// src/store/modules/user.js
import { Map } from 'immutable'

const user = (state = Map({
  isLogin: localStorage.getItem('isLogin') === 'true',
  token: localStorage.getItem('token') || '',
  adminname: localStorage.getItem('adminname') || '',
  role: localStorage.getItem('role') * 1 || 1
}), action) => {
  switch (action.type) {
    case 'CHANGE_LOGIN_STATE':
      return state.set('isLogin', action.payload)
    case 'CHANGE_TOKEN':
      return state.set('token', action.payload)
    case 'CHANGE_ADMIN_NAME':
      return state.set('adminname', action.payload)
    case 'CHANGE_ROLE':
      return state.set('role', action.payload)
    default:
      return state
  }
}

export default user
```

```js
// src/store/index.js
import { createStore, applyMiddleware } from 'redux'
import { combineReducers } from 'redux-immutable'
import thunk from 'redux-thunk'

import app from './modules/app'
import user from './modules/user'

const reducer = combineReducers({
  app, user
})

const store = createStore(reducer, applyMiddleware(thunk))

export default store
```

> 假设这一次异步操作让在组件

### 5.登录结合状态管理

```jsx
// src/store/modules/user.js
import { Map } from 'immutable'

const user = (state = Map({
  isLogin: localStorage.getItem('isLogin') === 'true',
  token: localStorage.getItem('token') || '',
  adminname: localStorage.getItem('adminname') || '',
  role: localStorage.getItem('role') * 1 || 1
}), action) => {
  switch (action.type) {
    case 'CHANGE_LOGIN_STATE':
      return state.set('isLogin', action.payload)
    case 'CHANGE_TOKEN':
      return state.set('token', action.payload)
    case 'CHANGE_ADMIN_NAME':
      return state.set('adminname', action.payload)
    case 'CHANGE_ROLE':
      return state.set('role', action.payload)
    default:
      return state
  }
}

export default user
```

```js
// src/store/index.js
import { createStore, applyMiddleware } from 'redux'
import { combineReducers } from 'redux-immutable'
import thunk from 'redux-thunk'

import app from './modules/app'
import user from './modules/user'

const reducer = combineReducers({
  app, user
})

const store = createStore(reducer, applyMiddleware(thunk))

export default store
```

```js
// src/views/login/Index.jsx
import React from 'react';
import { Form, Input, Button, message } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { connect } from 'react-redux'  
import { loginFn } from './../../api/user'
const Login = (props) => {
  const onFinish = (values) => {
    console.log('Received values of form: ', values);
    props.loginAction(values)
  };

  return (
    <div className="loginBox">
      <Form
        name="normal_login"
        className="login-form"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
      >
        <Form.Item
          name="adminname"
          rules={[
            {
              required: true,
              message: '请输入管理员账户!',
            },
          ]}
        >
          <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="管理员账户" />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: '请输入正确的密码!',
            },
          ]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="密码"
          />
        </Form.Item>
        

        <Form.Item>
          <Button type="primary" htmlType="submit" className="login-form-button">
            登 录
          </Button>
         
        </Form.Item>
      </Form>
    </div>
  );
};
export default connect(
  () => ({}),
  (dispatch) => {
    return {
      loginAction (params) {
        loginFn(params).then(res => {
          console.log('111111', res)
          if (res.code === '10003') {
            message.error('密码错误')
          } else if (res.code === '10005') {
            message.error('该管理员账户不存在')
          } else {
            message.success('登录成功')
            // 保存状态到本地
            localStorage.setItem('token', res.data.token)
            localStorage.setItem('adminname', res.data.adminname)
            localStorage.setItem('role', res.data.role)
            localStorage.setItem('isLogin', true)
            // 更新状态管理器
            dispatch({ type: 'CHANGE_ADMIN_NAME', payload: res.data.adminname})
            dispatch({ type: 'CHANGE_TOKEN', payload: res.data.token})
            dispatch({ type: 'CHANGE_ROLE', payload: res.data.role})
            dispatch({ type: 'CHANGE_LOGIN_STATE', payload: 'true'})
            // 登录成功之后跳转到系统首页
            window.location.href = '/'
          }
        })
      }
    }
  }
)(Login)
```

### 6.验证登录状态

#### 1.前端校验

```jsx
//src/App.jsx
import { HashRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import Index from './layout/main/Index.jsx'
import Login from './views/login/Index.jsx'
import { connect } from 'react-redux'
const App = ({ isLogin }) => {
  return (
    <Router >
      <Switch>
        <Route path="/login" component = { Login } />
        {
          isLogin 
            ? <Route path="/" component = { Index } /> 
            : <Redirect to="/login" />
        }
        
      </Switch>
    </Router>
  )
}

export default connect(
  state => {
    return {
      isLogin: state.getIn(['user', 'isLogin'])
    }
  }
)(App)
```

#### 2.token验证

```js
// src/utils/request.js
import axios from 'axios'
// 自定义axios
const ins = axios.create({
  baseURL: 'http://121.89.205.189/admin',
  timeout: 6000
})

// 自定义拦截器
// 请求拦截器
ins.interceptors.request.use((config) => {
  config.headers.token = localStorage.getItem('token') || ''
  return config
}, error => Promise.reject(error))

// 响应拦截器
ins.interceptors.response.use((response) => {
  if (response.data.code === '10119') { // +++++++++++++++++++++
    window.location.href = "/#/login"
  }
  return response.data
}, error => Promise.reject(error))

// 自定义各种数据请求 axios({})
export default function request (config) {
  const { url = '', method = '', data = {}, headers = {} } = config
  switch (method.toUpperCase()) {
    case 'GET':
      return ins.get(url, { params: data })
    case 'POST':
      // 表单提交  application/x-www-form-url-encoded
      if (headers['content-type'] === 'application/x-www-form-url-encoded') {
        // 转参数 URLSearchParams/第三方库qs
        const p = new URLSearchParams()
        for (const key in data) {
          p.append(key, data[key])
        }
        return ins.post(url, p, { headers })
      }
      // 文件提交  multipart/form-data
      if (headers['content-type'] === 'multipart/form-data') {
        const p = new FormData()
        for (const key in data) {
          p.append(key, data[key])
        }
        return ins.post(url, p, { headers })
      }
      // 默认 application/json
      return ins.post(url, data)
    case 'PUT': // 修改数据 --- 所有的数据的更新
      return ins.put(url, data)
    case 'DELETE': // 删除数据
      return ins.delete(url, { data })
    case 'PATCH': // 更新局部资源
      return ins.patch(url, data)
    default:
      return ins(config)
  }
}

```

## 16.轮播图管理

### 1.前端页面构建

```js
// src/api/banner.js
import request from './../utils/request'

// 获取轮播图数据
export function getBannerList () {
  return request({
    url: '/banner/list'
  })
}
```



```jsx
// src/views/banner/Index.jsx
import { Table, Image, Button } from 'antd'
import { useState, useEffect } from 'react'
import { getBannerList } from './../../api/banner'
const Com = () => {
  const columns = [
    {
      title: '序号',
      render (text, record, index) {
        return <span>{ index + 1 }</span>
      }
    },
    {
      title: '图片',
      dataIndex: 'img',
      render (text, record, index) {
        return <Image src={ text } width={200} />
      }
    },
    {
      title: '链接',
      dataIndex: 'link'
    },
    {
      title: '提示',
      dataIndex: 'alt'
    },
    {
      title: '操作',
      render (text, record, index) {
        return <Button danger>删除</Button>
      }
    }
  ]
  const [bannerList, setBannerList] = useState([])

  useEffect(() => {
    getBannerList().then(res => {
      console.log(res)
      setBannerList(res.data)
    })
  }, [])

  return (
    <Table dataSource={bannerList} columns={columns} rowKey = "bannerid"/>
  )
}
export default Com

```



### 2.添加轮播图的页面

```jsx
// views/banner/Add.jsx
const Com = () => {
  return (
    <div>添加轮播图</div>
  )
}
export default Com

```

点击切换至添加页面

```jsx
// views/banner/Index.jsx
import { Table, Image, Button, Space } from 'antd'
import { useState, useEffect } from 'react'
import { getBannerList } from './../../api/banner'
const Com = (props) => {
  const columns = [
    {
      title: '序号',
      render (text, record, index) {
        return <span>{ index + 1 }</span>
      }
    },
    {
      title: '图片',
      dataIndex: 'img',
      render (text, record, index) {
        return <Image src={ text } width={200} />
      }
    },
    {
      title: '链接',
      dataIndex: 'link'
    },
    {
      title: '提示',
      dataIndex: 'alt'
    },
    {
      title: '操作',
      render (text, record, index) {
        return <Button danger>删除</Button>
      }
    }
  ]
  const [bannerList, setBannerList] = useState([])

  useEffect(() => {
    getBannerList().then(res => {
      console.log(res)
      setBannerList(res.data)
    })
  }, [])

  return (
    <Space direction="vertical" style={{ width: '100%'}}>
      <Button 
        type="primary" 
        style={{ marginBottom: '10px'}}
        onClick = { () => {
          // console.log(props)
          props.history.push('/banner/add')
        }}
        >
        添加轮播图
      </Button>
      <Table dataSource={bannerList} columns={columns} rowKey = "bannerid"/>
    </Space>
  )
}
export default Com
```

### 3.布局添加页面

```jsx
// src/views/banner/Add.jsx
import { useRef } from "react"

const Com = () => {
  const fileRef = useRef()
  const imgRef = useRef()
  const linkRef = useRef()
  const altRef = useRef()
  const imgPutRef = useRef()

  const getFile = () => {
    const file = fileRef.current.files[0]
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = function () {
      imgRef.current.src = this.result
      imgPutRef.current.value = this.result
    }
  }

  const submitBanner = () =>{
    const data = {
      link: linkRef.current.value,
      alt: altRef.current.value,
      img: imgPutRef.current.value
    }
    console.log(data)
  }
  return (
    <div>
      <input type="text" ref={ linkRef } name="link" placeholder="链接"/> <br/>
      <input type="text" ref={ altRef } name="alt" placeholder="描述"/><br/>
      <input type="file" ref={ fileRef } onChange = { getFile }/><br/>
      <input type="text" ref={ imgPutRef } name="img"/><br/>
      <img ref= {imgRef}  src="" alt=""/><br/>
      <button onClick = { submitBanner }>提交</button>
    </div>
  )
}
export default Com
```

### 4.调用相关的接口

```js
// src/api/banner.js
import request from './../utils/request'

// 获取轮播图数据
export function getBannerList () {
  return request({
    url: '/banner/list'
  })
}

// 添加轮播图数据
export function addBanner (params) {
  return request({
    url: '/banner/add',
    method: 'POST',
    data: params
  })
}
```

```jsx
import { useRef } from "react"
import { addBanner } from './../../api/banner'
const Com = ({ history }) => {
  const fileRef = useRef()
  const imgRef = useRef()
  const linkRef = useRef()
  const altRef = useRef()
  const imgPutRef = useRef()

  const getFile = () => {
    const file = fileRef.current.files[0]
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = function () {
      imgRef.current.src = this.result
      imgPutRef.current.value = this.result
    }
  }

  const submitBanner = () =>{
    const data = {
      link: linkRef.current.value,
      alt: altRef.current.value,
      img: imgPutRef.current.value
    }
    console.log(data)
    addBanner(data).then(res => {
      // history.push('/banner')
      history.goBack()
    })
  }
  return (
    <div>
      <input type="text" ref={ linkRef } name="link" placeholder="链接"/> <br/>
      <input type="text" ref={ altRef } name="alt" placeholder="描述"/><br/>
      <input type="file" ref={ fileRef } onChange = { getFile }/><br/>
      <input type="text" ref={ imgPutRef } name="img"/><br/>
      <img ref= {imgRef}  src="" alt=""/><br/>
      <button onClick = { submitBanner }>提交</button>
    </div>
  )
}
export default Com
```

### 5.数据删除

```js
import request from './../utils/request'

// 获取轮播图数据
export function getBannerList () {
  return request({
    url: '/banner/list'
  })
}

// 添加轮播图数据
export function addBanner (params) {
  return request({
    url: '/banner/add',
    method: 'POST',
    data: params
  })
}

// 删除轮播图数据
export function deleteBanner (params) {
  console.log('999', params)
  return request({
    url: '/banner/delete',
    // method: 'GET',
    data: params
  })
}
```

Views/banner/Index.jsx

```jsx
import { Table, Image, Button, Space, Popconfirm } from 'antd'
import { useState, useEffect } from 'react'
import { getBannerList, deleteBanner } from './../../api/banner'
const Com = (props) => {
  const columns = [
    {
      title: '序号',
      render (text, record, index) {
        return <span>{ index + 1 }</span>
      }
    },
    {
      title: '图片',
      dataIndex: 'img',
      render (text, record, index) {
        return <Image src={ text } width={200} />
      }
    },
    {
      title: '链接',
      dataIndex: 'link'
    },
    {
      title: '提示',
      dataIndex: 'alt'
    },
    {
      title: '操作',
      render (text, record, index) {
        return (
          <Popconfirm
          title="确定删除吗?"
          onConfirm={ () => {
            deleteBanner({ bannerid: record.bannerid }).then(res => {
              console.log('删除成功')
              getBannerList().then(res => {
                // console.log(res)
                setBannerList(res.data)
              })
            })
          }}
          onCancel={() => {}}
          okText="确定"
          cancelText="取消"
          >
            <Button danger>删除</Button>
          </Popconfirm>
        )
      }
    }
  ]
  const [bannerList, setBannerList] = useState([])

  useEffect(() => {
    getBannerList().then(res => {
      console.log(res)
      setBannerList(res.data)
    })
  }, [])

  return (
    <Space direction="vertical" style={{ width: '100%'}}>
      <Button 
        type="primary" 
        style={{ marginBottom: '10px'}}
        onClick = { () => {
          // console.log(props)
          props.history.push('/banner/add')
        }}
        >
        添加轮播图
      </Button>
      <Table dataSource={bannerList} columns={columns} rowKey = "bannerid"/>
    </Space>
  )
}
export default Com
```

##  17.产品管理

### 1封装相关接口

```js
// src/api/pro.js
import request from './../utils/request'

// 获取产品列表数据
export function getProList (params) {
  return request({
    url: '/pro/list',
    data: params
  })
}

// 获取产品分类数据
export function getCategoryList () {
  return request({
    url: '/pro/getCategory'
  })
}

// 获取产品分类数据 category search
export function getSearchList (params) {
  return request({
    url: '/pro/searchPro',
    method: 'POST',
    data: params
  })
}

// 修改商品是否 售卖 推荐 秒杀
// proid  type  flag
// type issale isseckill isrecommend
// flag true flase
export function updateFlag (params) {
  return request({
    url: '/pro/updateFlag',
    method: 'POST',
    data: params
  })
}

// 获取秒杀数据
// type isseckill isrecommend
// flag  1   0
export function getSeckillData () {
  return request({
    url: '/pro/showdata',
    method: 'POST',
    data: {
      type: 'isseckill',
      flag: 1
    }
  })
}



// 获取推荐数据
// type  isrecommend
// flag  1   0
export function getRecommendData (params) {
  return request({
    url: '/pro/showdata',
    method: 'POST',
    data: {
      type: 'isrecommend',
      flag: 1
    }
  })
}

// 获取产品详情
export function getProDetail (params) {
  return request({
    url: '/pro/showdata',
    data: params
  })
}



```



### 2.设计产品列表

```js
// src/views/pro/Index.js
import { Table, Space, Image, Button } from 'antd'
import { useState } from 'react'
import { getProList } from '../../api/pro'
import { useEffect } from 'react'
const Com = () => {
  const columns = [
    {
      title: '序号',
      render: (text, record, index) => (<span>{ index + 1 }</span>)
    },
    {
      title: '图片',
      dataIndex: 'img1',
      render: (text, record, index) => (<Image src = { text } style={{ width: 80, height: 80 }} />)
    },
    {
      title: '名称',
      dataIndex: 'proname',
    },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    {
      title: '分类',
      dataIndex: 'category',
    },
    {
      title: '原价',
      dataIndex: 'originprice',
    },
    {
      title: '折扣',
      dataIndex: 'discount',
    },
    {
      title: '销量',
      dataIndex: 'sales',
    },
    {
      title: '是否售卖',
      dataIndex: 'issale',
    },
    {
      title: '是否秒杀',
      dataIndex: 'isseckill',
    },
    {
      title: '是否推荐',
      dataIndex: 'isrecommend',
    },
    {
      title: '操作',
      render: (text, record, index) => (
        <Space>
          <Button>编辑</Button> 
          ｜
          <Button>删除</Button>
        </Space>
      )
    },
  ]
  const [ proList, setProList ] = useState([])

  useEffect(() => {
    getProList().then(res => {
      setProList(res.data)
    })
  }, [])
  return (
    <Space direction="vertical" style={{ width: '100%' }}>
      <Table dataSource = { proList } columns = { columns } rowKey = "proid" ></Table>
    </Space>
  )
}
export default Com
```

### 3.固定头和列

```js
import { Table, Space, Image, Button } from 'antd'
import { useState } from 'react'
import { getProList } from '../../api/pro'
import { useEffect } from 'react'
const Com = () => {
  const [ height, setHeight ] = useState(0)
  useEffect(() => {
    setHeight(window.innerHeight)
  }, [])

  const columns = [
    {
      align: 'center',
      title: '序号',
      width: 60,
      fixed: 'left',
      render: (text, record, index) => (<span>{ index + 1 }</span>)
    },
    {
      align: 'center',
      title: '图片',
      width: 120,
      fixed: 'left',
      dataIndex: 'img1',
      render: (text, record, index) => (<Image src = { text } style={{ width: 80, height: 80 }} />)
    },
    {
      align: 'center',
      title: '名称',
      width: 200,
      fixed: 'left',
      dataIndex: 'proname',
    },
    {
      align: 'center',
      title: '品牌',
      width: 110,
      dataIndex: 'brand',
    },
    {
      align: 'center',
      title: '分类',
      width: 110,
      dataIndex: 'category',
    },
    {
      align: 'center',
      title: '原价',
      width: 101,
      dataIndex: 'originprice',
    },
    {
      align: 'center',
      title: '折扣',
      width: 100,
      dataIndex: 'discount',
    },
    {
      align: 'center',
      title: '销量',
      width: 100,
      dataIndex: 'sales',
    },
    {
      align: 'center',
      title: '是否售卖',
      width: 60,
      fixed: 'right',
      dataIndex: 'issale',
    },
    {
      align: 'center',
      title: '是否秒杀',
      width: 60,
      fixed: 'right',
      dataIndex: 'isseckill',
    },
    {
      align: 'center',
      title: '是否推荐',
      width: 60,
      fixed: 'right',
      dataIndex: 'isrecommend',
    },
    {
      align: 'center',
      title: '操作',
      width: 320,
      fixed: 'right',
      render: (text, record, index) => (
        <Space>
          <Button>编辑</Button> 
          ｜
          <Button>删除</Button>
        </Space>
      )
    },
  ]
  const [ proList, setProList ] = useState([])

  useEffect(() => {
    getProList().then(res => {
      setProList(res.data)
    })
  }, [])
  return (
    <Space direction="vertical" style={{ width: '100%' }}>
      <Table 
        dataSource = { proList } 
        columns = { columns } 
        rowKey = "proid" 
        scroll={{ x: 1500, y: height - 300 }}
      ></Table>
    </Space>
  )
}
export default Com
```

### 4.设定合理的序号

```js
import { Table, Space, Image, Button } from 'antd'
import { useState } from 'react'
import { getProList } from '../../api/pro'
import { useEffect } from 'react'
const Com = () => {
  const [ height, setHeight ] = useState(0)
  useEffect(() => {
    setHeight(window.innerHeight)
  }, [])

  const [ current, setCurrent ] = useState(1)
  const [ pageSize, setPageSize ] = useState(10)

  const columns = [
    {
      align: 'center',
      title: '序号',
      width: 60,
      fixed: 'left',
      render: (text, record, index) => (<span>{ (current-1) * pageSize + index + 1 }</span>)
    },
    {
      align: 'center',
      title: '图片',
      width: 120,
      fixed: 'left',
      dataIndex: 'img1',
      render: (text, record, index) => (<Image src = { text } style={{ width: 80, height: 80 }} />)
    },
    {
      align: 'center',
      title: '名称',
      width: 200,
      fixed: 'left',
      dataIndex: 'proname',
    },
    {
      align: 'center',
      title: '品牌',
      width: 110,
      dataIndex: 'brand',
    },
    {
      align: 'center',
      title: '分类',
      width: 110,
      dataIndex: 'category',
    },
    {
      align: 'center',
      title: '原价',
      width: 101,
      dataIndex: 'originprice',
    },
    {
      align: 'center',
      title: '折扣',
      width: 100,
      dataIndex: 'discount',
    },
    {
      align: 'center',
      title: '销量',
      width: 100,
      dataIndex: 'sales',
    },
    {
      align: 'center',
      title: '是否售卖',
      width: 60,
      fixed: 'right',
      dataIndex: 'issale',
    },
    {
      align: 'center',
      title: '是否秒杀',
      width: 60,
      fixed: 'right',
      dataIndex: 'isseckill',
    },
    {
      align: 'center',
      title: '是否推荐',
      width: 60,
      fixed: 'right',
      dataIndex: 'isrecommend',
    },
    {
      align: 'center',
      title: '操作',
      width: 320,
      fixed: 'right',
      render: (text, record, index) => (
        <Space>
          <Button>编辑</Button> 
          ｜
          <Button>删除</Button>
        </Space>
      )
    },
  ]
  const [ proList, setProList ] = useState([])

  useEffect(() => {
    getProList().then(res => {
      setProList(res.data)
    })
  }, [])


  return (
    <Space direction="vertical" style={{ width: '100%' }}>
      <Table 
        dataSource = { proList } 
        columns = { columns } 
        rowKey = "proid" 
        scroll={{ x: 1500, y: height - 300 }}
        pagination = { {
          position: ['bottomRight'],
          current,
          pageSize,
          onChange: (page, pageSize) => {
            setCurrent(page)
            setPageSize(pageSize)
          },
          pageSizeOptions: ['5', '10', '15', '20'],
          showQuickJumper: true,
          showTotal: (total) => <span>共有{total}条数据</span>
        } }
      ></Table>
    </Space>
  )
}
export default Com
```

### 5.排序和筛选

```jsx
import { Table, Space, Image, Button } from 'antd'
import { useState } from 'react'
import { getProList, getCategoryList } from '../../api/pro'
import { useEffect } from 'react'
const Com = () => {
  // 分类数据
  const [ cateList, setCateList ] = useState([])
  useEffect(() => {
    getCategoryList().then(res => {
      console.log(res)
      const arr = []
      res.data.forEach(item => {
        arr.push({
          text: item,
          value: item
        })
      })
      setCateList(arr)
    })
  }, [])

  // 固定头和列
  const [ height, setHeight ] = useState(0)
  useEffect(() => {
    setHeight(window.innerHeight)
  }, [])

  // 分页器
  const [ current, setCurrent ] = useState(1)
  const [ pageSize, setPageSize ] = useState(10)

  const columns = [
    {
      align: 'center',
      title: '序号',
      width: 60,
      fixed: 'left',
      render: (text, record, index) => (<span>{ (current-1) * pageSize + index + 1 }</span>)
    },
    {
      align: 'center',
      title: '图片',
      width: 120,
      fixed: 'left',
      dataIndex: 'img1',
      render: (text, record, index) => (<Image src = { text } style={{ width: 80, height: 80 }} />)
    },
    {
      align: 'center',
      title: '名称',
      width: 200,
      fixed: 'left',
      dataIndex: 'proname',
    },
    {
      align: 'center',
      title: '品牌',
      width: 110,
      dataIndex: 'brand',
    },
    {
      align: 'center',
      title: '分类',
      width: 110,
      filters: cateList,
      onFilter: (value, record) => record.category.indexOf(value) === 0, // 筛选核心
      dataIndex: 'category',
    },
    {
      align: 'center',
      title: '原价',
      sorter: (a, b) => a.originprice - b.originprice,
      width: 101,
      dataIndex: 'originprice',
    },
    {
      align: 'center',
      title: '折扣',
      sorter: (a, b) => a.discount - b.discount,
      width: 100,
      dataIndex: 'discount',
    },
    {
      align: 'center',
      title: '销量',
      sorter: (a, b) => a.sales - b.sales, // 排序的核心
      width: 100,
      dataIndex: 'sales',
    },
    {
      align: 'center',
      title: '是否售卖',
      width: 60,
      fixed: 'right',
      dataIndex: 'issale',
    },
    {
      align: 'center',
      title: '是否秒杀',
      width: 60,
      fixed: 'right',
      dataIndex: 'isseckill',
    },
    {
      align: 'center',
      title: '是否推荐',
      width: 60,
      fixed: 'right',
      dataIndex: 'isrecommend',
    },
    {
      align: 'center',
      title: '操作',
      width: 320,
      fixed: 'right',
      render: (text, record, index) => (
        <Space>
          <Button>编辑</Button> 
          ｜
          <Button>删除</Button>
        </Space>
      )
    },
  ]
  const [ proList, setProList ] = useState([])

  useEffect(() => {
    getProList().then(res => {
      setProList(res.data)
    })
  }, [])


  return (
    <Space direction="vertical" style={{ width: '100%' }}>
      <Table 
        dataSource = { proList } 
        columns = { columns } 
        rowKey = "proid" 
        scroll={{ x: 1500, y: height - 300 }}
        pagination = { {
          position: ['bottomRight'],
          current,
          pageSize,
          onChange: (page, pageSize) => {
            setCurrent(page)
            setPageSize(pageSize)
          },
          pageSizeOptions: ['5', '10', '15', '20'],
          showQuickJumper: true,
          showTotal: (total) => <span>共有{total}条数据</span>
        } }
      ></Table>
    </Space>
  )
}
export default Com
```

### 6.滑动开关

```jsx
import { Table, Space, Image, Button, Switch } from 'antd'
import { useState } from 'react'
import { getProList, getCategoryList, updateFlag } from '../../api/pro'
import { useEffect } from 'react'
const Com = () => {
  // 分类数据
  const [ cateList, setCateList ] = useState([])
  useEffect(() => {
    getCategoryList().then(res => {
      console.log(res)
      const arr = []
      res.data.forEach(item => {
        arr.push({
          text: item,
          value: item
        })
      })
      setCateList(arr)
    })
  }, [])

  // 固定头和列
  const [ height, setHeight ] = useState(0)
  useEffect(() => {
    setHeight(window.innerHeight)
  }, [])

  // 分页器
  const [ current, setCurrent ] = useState(1)
  const [ pageSize, setPageSize ] = useState(10)

  const columns = [
    {
      align: 'center',
      title: '序号',
      width: 60,
      fixed: 'left',
      render: (text, record, index) => (<span>{ (current-1) * pageSize + index + 1 }</span>)
    },
    {
      align: 'center',
      title: '图片',
      width: 120,
      fixed: 'left',
      dataIndex: 'img1',
      render: (text, record, index) => (<Image src = { text } style={{ width: 80, height: 80 }} />)
    },
    {
      align: 'center',
      title: '名称',
      width: 200,
      fixed: 'left',
      dataIndex: 'proname',
    },
    {
      align: 'center',
      title: '品牌',
      width: 110,
      dataIndex: 'brand',
    },
    {
      align: 'center',
      title: '分类',
      width: 110,
      filters: cateList,
      onFilter: (value, record) => record.category.indexOf(value) === 0,
      dataIndex: 'category',
    },
    {
      align: 'center',
      title: '原价',
      sorter: (a, b) => a.originprice - b.originprice,
      width: 101,
      dataIndex: 'originprice',
    },
    {
      align: 'center',
      title: '折扣',
      sorter: (a, b) => a.discount - b.discount,
      width: 100,
      dataIndex: 'discount',
    },
    {
      align: 'center',
      title: '销量',
      sorter: (a, b) => a.sales - b.sales,
      width: 100,
      dataIndex: 'sales',
    },
    {
      align: 'center',
      title: '是否售卖',
      width: 60,
      fixed: 'right',
      dataIndex: 'issale',
      render: (text, record, index) => { // +++++
        return (
          <Switch checked = { text === 1 } onChange = { (checked) => {
            updateFlag({
              proid: record.proid,
              type: 'issale',
              flag: checked
            }).then(() => {
              getProList().then(res => {
                setProList(res.data)
              })
            })
          }}/>
        )
      }
    },
    {
      align: 'center',
      title: '是否秒杀',
      width: 60,
      fixed: 'right',
      dataIndex: 'isseckill',
      render: (text, record, index) => { // +++++
        return (
          <Switch checked = { text === 1 } onChange = { (checked) => {
            updateFlag({
              proid: record.proid,
              type: 'isseckill',
              flag: checked
            }).then(() => {
              getProList().then(res => {
                setProList(res.data)
              })
            })
          }}/>
        )
      }
    },
    {
      align: 'center',
      title: '是否推荐',
      width: 60,
      fixed: 'right',
      dataIndex: 'isrecommend',
      render: (text, record, index) => { // +++++
        return (
          <Switch checked = { text === 1 } onChange = { (checked) => {
            updateFlag({
              proid: record.proid,
              type: 'isrecommend',
              flag: checked
            }).then(() => {
              getProList().then(res => {
                setProList(res.data)
              })
            })
          }}/>
        )
      }
    },
    {
      align: 'center',
      title: '操作',
      width: 320,
      fixed: 'right',
      render: (text, record, index) => (
        <Space>
          <Button>编辑</Button> 
          ｜
          <Button>删除</Button>
        </Space>
      )
    },
  ]
  const [ proList, setProList ] = useState([])

  useEffect(() => {
    getProList().then(res => {
      setProList(res.data)
    })
  }, [])


  return (
    <Space direction="vertical" style={{ width: '100%' }}>
      <Table 
        dataSource = { proList } 
        columns = { columns } 
        rowKey = "proid" 
        scroll={{ x: 1500, y: height - 300 }}
        pagination = { {
          position: ['bottomRight'],
          current,
          pageSize,
          onChange: (page, pageSize) => {
            setCurrent(page)
            setPageSize(pageSize)
          },
          pageSizeOptions: ['5', '10', '15', '20'],
          showQuickJumper: true,
          showTotal: (total) => <span>共有{total}条数据</span>
        } }
      ></Table>
    </Space>
  )
}
export default Com
```

### 7.秒杀列表

```jsx
import { Table, Space, Image, Button, Switch } from 'antd'
import { useState } from 'react'
import { getSeckillData, getCategoryList, updateFlag } from '../../api/pro'
import { useEffect } from 'react'
const Com = () => {
  // 分类数据
  const [ cateList, setCateList ] = useState([])
  useEffect(() => {
    getCategoryList().then(res => {
      console.log(res)
      const arr = []
      res.data.forEach(item => {
        arr.push({
          text: item,
          value: item
        })
      })
      setCateList(arr)
    })
  }, [])

  // 固定头和列
  const [ height, setHeight ] = useState(0)
  useEffect(() => {
    setHeight(window.innerHeight)
  }, [])

  // 分页器
  const [ current, setCurrent ] = useState(1)
  const [ pageSize, setPageSize ] = useState(10)

  const columns = [
    {
      align: 'center',
      title: '序号',
      width: 60,
      fixed: 'left',
      render: (text, record, index) => (<span>{ (current-1) * pageSize + index + 1 }</span>)
    },
    {
      align: 'center',
      title: '图片',
      width: 120,
      fixed: 'left',
      dataIndex: 'img1',
      render: (text, record, index) => (<Image src = { text } style={{ width: 80, height: 80 }} />)
    },
    {
      align: 'center',
      title: '名称',
      width: 200,
      fixed: 'left',
      dataIndex: 'proname',
    },
    {
      align: 'center',
      title: '品牌',
      width: 110,
      dataIndex: 'brand',
    },
    {
      align: 'center',
      title: '分类',
      width: 110,
      filters: cateList,
      onFilter: (value, record) => record.category.indexOf(value) === 0,
      dataIndex: 'category',
    },
    {
      align: 'center',
      title: '原价',
      sorter: (a, b) => a.originprice - b.originprice,
      width: 101,
      dataIndex: 'originprice',
    },
    {
      align: 'center',
      title: '折扣',
      sorter: (a, b) => a.discount - b.discount,
      width: 100,
      dataIndex: 'discount',
    },
    {
      align: 'center',
      title: '销量',
      sorter: (a, b) => a.sales - b.sales,
      width: 100,
      dataIndex: 'sales',
    },
    {
      align: 'center',
      title: '是否秒杀',
      width: 60,
      fixed: 'right',
      dataIndex: 'isseckill',
      render: (text, record, index) => {
        return (
          <Switch checked = { text === 1 } onChange = { (checked) => {
            updateFlag({
              proid: record.proid,
              type: 'isseckill',
              flag: checked
            }).then(() => {
              getSeckillData().then(res => {
                setProList(res.data)
              })
            })
          }}/>
        )
      }
    },
    
    {
      align: 'center',
      title: '操作',
      width: 320,
      fixed: 'right',
      render: (text, record, index) => (
        <Space>
          <Button>编辑</Button> 
          ｜
          <Button>删除</Button>
        </Space>
      )
    },
  ]
  const [ proList, setProList ] = useState([])

  useEffect(() => {
    getSeckillData().then(res => {
      setProList(res.data)
    })
  }, [])


  return (
    <Space direction="vertical" style={{ width: '100%' }}>
      <Table 
        dataSource = { proList } 
        columns = { columns } 
        rowKey = "proid" 
        scroll={{ x: 1500, y: height - 300 }}
        pagination = { {
          position: ['bottomRight'],
          current,
          pageSize,
          onChange: (page, pageSize) => {
            setCurrent(page)
            setPageSize(pageSize)
          },
          pageSizeOptions: ['5', '10', '15', '20'],
          showQuickJumper: true,
          showTotal: (total) => <span>共有{total}条数据</span>
        } }
      ></Table>
    </Space>
  )
}
export default Com
```

### 8.推荐列表

```jsx
import { Table, Space, Image, Button, Switch } from 'antd'
import { useState } from 'react'
import { getRecommendData, getCategoryList, updateFlag } from '../../api/pro'
import { useEffect } from 'react'
const Com = () => {
  // 分类数据
  const [ cateList, setCateList ] = useState([])
  useEffect(() => {
    getCategoryList().then(res => {
      console.log(res)
      const arr = []
      res.data.forEach(item => {
        arr.push({
          text: item,
          value: item
        })
      })
      setCateList(arr)
    })
  }, [])

  // 固定头和列
  const [ height, setHeight ] = useState(0)
  useEffect(() => {
    setHeight(window.innerHeight)
  }, [])

  // 分页器
  const [ current, setCurrent ] = useState(1)
  const [ pageSize, setPageSize ] = useState(10)

  const columns = [
    {
      align: 'center',
      title: '序号',
      width: 60,
      fixed: 'left',
      render: (text, record, index) => (<span>{ (current-1) * pageSize + index + 1 }</span>)
    },
    {
      align: 'center',
      title: '图片',
      width: 120,
      fixed: 'left',
      dataIndex: 'img1',
      render: (text, record, index) => (<Image src = { text } style={{ width: 80, height: 80 }} />)
    },
    {
      align: 'center',
      title: '名称',
      width: 200,
      fixed: 'left',
      dataIndex: 'proname',
    },
    {
      align: 'center',
      title: '品牌',
      width: 110,
      dataIndex: 'brand',
    },
    {
      align: 'center',
      title: '分类',
      width: 110,
      filters: cateList,
      onFilter: (value, record) => record.category.indexOf(value) === 0,
      dataIndex: 'category',
    },
    {
      align: 'center',
      title: '原价',
      sorter: (a, b) => a.originprice - b.originprice,
      width: 101,
      dataIndex: 'originprice',
    },
    {
      align: 'center',
      title: '折扣',
      sorter: (a, b) => a.discount - b.discount,
      width: 100,
      dataIndex: 'discount',
    },
    {
      align: 'center',
      title: '销量',
      sorter: (a, b) => a.sales - b.sales,
      width: 100,
      dataIndex: 'sales',
    },
    
    {
      align: 'center',
      title: '是否推荐',
      width: 60,
      fixed: 'right',
      dataIndex: 'isrecommend',
      render: (text, record, index) => {
        return (
          <Switch checked = { text === 1 } onChange = { (checked) => {
            updateFlag({
              proid: record.proid,
              type: 'isrecommend',
              flag: checked
            }).then(() => {
              getRecommendData().then(res => {
                setProList(res.data)
              })
            })
          }}/>
        )
      }
    },
    {
      align: 'center',
      title: '操作',
      width: 320,
      fixed: 'right',
      render: (text, record, index) => (
        <Space>
          <Button>编辑</Button> 
          ｜
          <Button>删除</Button>
        </Space>
      )
    },
  ]
  const [ proList, setProList ] = useState([])

  useEffect(() => {
    getRecommendData().then(res => {
      setProList(res.data)
    })
  }, [])


  return (
    <Space direction="vertical" style={{ width: '100%' }}>
      <Table 
        dataSource = { proList } 
        columns = { columns } 
        rowKey = "proid" 
        scroll={{ x: 1500, y: height - 300 }}
        pagination = { {
          position: ['bottomRight'],
          current,
          pageSize,
          onChange: (page, pageSize) => {
            setCurrent(page)
            setPageSize(pageSize)
          },
          pageSizeOptions: ['5', '10', '15', '20'],
          showQuickJumper: true,
          showTotal: (total) => <span>共有{total}条数据</span>
        } }
      ></Table>
    </Space>
  )
}
export default Com
```

### 9.筛选列表

```jsx
import { Table, Space, Image, Button, Switch, Select, Input } from 'antd'
import { useState } from 'react'
import { getProList, getCategoryList, updateFlag, getSearchList } from '../../api/pro'
import { useEffect } from 'react'
import { useCallback } from 'react'

const { Option } = Select
const Com = () => {
  // 分类数据
  const [ cateList, setCateList ] = useState([])
  useEffect(() => {
    getCategoryList().then(res => {
      console.log(res)
      const arr = []
      res.data.forEach(item => {
        arr.push({
          text: item,
          value: item
        })
      })
      setCateList(arr)
    })
  }, [])

  // 固定头和列
  const [ height, setHeight ] = useState(0)
  useEffect(() => {
    setHeight(window.innerHeight)
  }, [])

  // 分页器
  const [ current, setCurrent ] = useState(1)
  const [ pageSize, setPageSize ] = useState(10)

  const columns = [
    {
      align: 'center',
      title: '序号',
      width: 60,
      fixed: 'left',
      render: (text, record, index) => (<span>{ (current-1) * pageSize + index + 1 }</span>)
    },
    {
      align: 'center',
      title: '图片',
      width: 120,
      fixed: 'left',
      dataIndex: 'img1',
      render: (text, record, index) => (<Image src = { text } style={{ width: 80, height: 80 }} />)
    },
    {
      align: 'center',
      title: '名称',
      width: 200,
      fixed: 'left',
      dataIndex: 'proname',
    },
    {
      align: 'center',
      title: '品牌',
      width: 110,
      dataIndex: 'brand',
    },
    {
      align: 'center',
      title: '分类',
      width: 110,
      filters: cateList,
      onFilter: (value, record) => record.category.indexOf(value) === 0,
      dataIndex: 'category',
    },
    {
      align: 'center',
      title: '原价',
      sorter: (a, b) => a.originprice - b.originprice,
      width: 101,
      dataIndex: 'originprice',
    },
    {
      align: 'center',
      title: '折扣',
      sorter: (a, b) => a.discount - b.discount,
      width: 100,
      dataIndex: 'discount',
    },
    {
      align: 'center',
      title: '销量',
      sorter: (a, b) => a.sales - b.sales,
      width: 100,
      dataIndex: 'sales',
    },
    {
      align: 'center',
      title: '是否售卖',
      width: 60,
      fixed: 'right',
      dataIndex: 'issale',
      render: (text, record, index) => {
        return (
          <Switch checked = { text === 1 } onChange = { (checked) => {
            updateFlag({
              proid: record.proid,
              type: 'issale',
              flag: checked
            }).then(() => {
              getProList().then(res => {
                setProList(res.data)
              })
            })
          }}/>
        )
      }
    },
    {
      align: 'center',
      title: '是否秒杀',
      width: 60,
      fixed: 'right',
      dataIndex: 'isseckill',
      render: (text, record, index) => {
        return (
          <Switch checked = { text === 1 } onChange = { (checked) => {
            updateFlag({
              proid: record.proid,
              type: 'isseckill',
              flag: checked
            }).then(() => {
              getProList().then(res => {
                setProList(res.data)
              })
            })
          }}/>
        )
      }
    },
    {
      align: 'center',
      title: '是否推荐',
      width: 60,
      fixed: 'right',
      dataIndex: 'isrecommend',
      render: (text, record, index) => {
        return (
          <Switch checked = { text === 1 } onChange = { (checked) => {
            updateFlag({
              proid: record.proid,
              type: 'isrecommend',
              flag: checked
            }).then(() => {
              getProList().then(res => {
                setProList(res.data)
              })
            })
          }}/>
        )
      }
    },
    {
      align: 'center',
      title: '操作',
      width: 320,
      fixed: 'right',
      render: (text, record, index) => (
        <Space>
          <Button>编辑</Button> 
          ｜
          <Button>删除</Button>
        </Space>
      )
    },
  ]
  const [ proList, setProList ] = useState([])

  useEffect(() => {
    getProList().then(res => {
      setProList(res.data)
    })
  }, [])

  // 筛选数据
  const [category, setCategory] = useState('')
  const [ search, setSearch ] = useState('')
  const handleChange = useCallback((value) => {
    setCategory(value)
    // setSearch('')
  }, [])
  const getSearch =  useCallback((e) => {
    setSearch(e.target.value)
  }, [])

  const searchFn = () => {
    console.log(6666)
    getSearchList({ category, search }).then(res => {
      console.log(888888, category)
      console.log(888888, search)
      setProList(res.data)
    })
  }
  return (
    <Space direction="vertical" style={{ width: '100%' }}>
      <Space>
        <Select value={category} style={{ width: 120 }} onChange={handleChange}>
          <Option value="">全部</Option>
          {
            cateList.map(item => {
              return (
                <Option key = { item.text } value={ item.text }>{ item.text }</Option>
              )
            })
          }
        </Select>
        <Input placeholder = "请输入需要查询的名称的关键词" value={ search } onChange = { getSearch }/>
        <Button type="primary" onClick = {
          searchFn
        }>搜索</Button>
      </Space>
      <Table 
        dataSource = { proList } 
        columns = { columns } 
        rowKey = "proid" 
        scroll={{ x: 1500, y: height - 340 }}
        pagination = { {
          position: ['bottomRight'],
          current,
          pageSize,
          onChange: (page, pageSize) => {
            setCurrent(page)
            setPageSize(pageSize)
          },
          pageSizeOptions: ['5', '10', '15', '20'],
          showQuickJumper: true,
          showTotal: (total) => <span>共有{total}条数据</span>
        } }
      ></Table>
    </Space>
  )
}
export default Com
```

## 18.账户管理

### 1.管理员管理

#### 1.接口设计

```js
// src/api/admin.js
import request from './../utils/request'

// 修改管理员接口
export function updateAdmin (params) {
  return request({
    url: '/admin/update',
    method: 'POST',
    data: params
  })
}

// 删除管理员接口
export function deleteAdmin (params) {
  return request({
    url: '/admin/delete',
    method: 'POST',
    data: params
  })
}

// 添加管理员接口
export function addAdmin (params) {
  return request({
    url: '/admin/add',
    method: 'POST',
    data: params
  })
}


// 获取管理员列表接口
export function getAdminList () {
  return request({
    url: '/admin/list'
  })
}


// 获取管理员详情接口
export function getAdminDetail (params) {
  return request({
    url: '/admin/detail',
    data: params
  })
}
```

#### 2.管理员列表的界面构建

**列表展示**

```js
import { Table, Space, Button, Tag } from 'antd'
import { useState } from 'react'
import { getAdminList } from './../../api/admin'
import { useEffect } from 'react'
const Com = () => {
  // 请求列表的数据
  const [adminList, setAdminList] = useState([])
  useEffect(() => {
    getAdminList().then(res => {
      setAdminList(res.data)
    })
  }, [])

  const columns = [
    {
      title: '序号',
      render (text, record, index) {
        return (<span>{ index + 1 }</span>)
      }
    },
    {
      title: '账号',
      dataIndex: 'adminname'
    },
    {
      title: '权限',
      dataIndex: 'role',
      render (text) {
        return (
          <>
            {
              text === 2 ? <Tag color="green">超级管理员</Tag> : <Tag>管理员</Tag>
            }
          </>
        )
      }
    },
    {
      title: '操作',
      render (text, record, index) {
        return (
          <Space>
            <Button type="ghost">编辑</Button>
            <Button danger>删除</Button>
          </Space>
        )
      }
    }
  ]
  return (
    <Space direction="vertical" style={{ width: '100%' }}>
      <Button type="primary">添加管理员</Button>
      <Table dataSource = { adminList } columns = { columns } rowKey = "adminid"/>
    </Space>
  )
}
export default Com
```

**添加管理员的界面**

```js
import { Table, Space, Button, Tag, Drawer, Form, Input, Select, Tree } from 'antd'
import { useState } from 'react'
import { getAdminList } from './../../api/admin'
import { useEffect, useCallback } from 'react'
import menus from './../../router/menus'
const Com = () => {
  // 请求列表的数据
  const [adminList, setAdminList] = useState([])
  useEffect(() => {
    getAdminList().then(res => {
      setAdminList(res.data)
    })
  }, [])

  const columns = [
    {
      title: '序号',
      render (text, record, index) {
        return (<span>{ index + 1 }</span>)
      }
    },
    {
      title: '账号',
      dataIndex: 'adminname'
    },
    {
      title: '权限',
      dataIndex: 'role',
      render (text) {
        return (
          <>
            {
              text === 2 ? <Tag color="green">超级管理员</Tag> : <Tag>管理员</Tag>
            }
          </>
        )
      }
    },
    {
      title: '操作',
      render (text, record, index) {
        return (
          <Space>
            <Button type="ghost">编辑</Button>
            <Button danger>删除</Button>
          </Space>
        )
      }
    }
  ]

  // 添加管理员
  const [addVisible, setAddVisible] = useState(false);
  const [adminname, setAdminname] = useState('');
  const [password, setPassword] = useState('');
  const [role, setRole] = useState(1);
  const [checkedKeys, setCheckedKeys] = useState(['0-0']);
  const onAddOpen = useCallback(() => {
    setAddVisible(true)
  }, [])
  const onAddClose = useCallback(() => {
    setAddVisible(false)
  }, [])
  const onCheck = useCallback((checkedKeys) => {
    setCheckedKeys(checkedKeys)
  }, [])
  return (
    <Space direction="vertical" style={{ width: '100%' }}>
      <Button type="primary" onClick = { onAddOpen } >添加管理员</Button>
      <Table dataSource = { adminList } columns = { columns } rowKey = "adminid"/>
      {/* 添加管理员抽屉 */}
      <Drawer 
        width = "500"
        title="添加管理员" 
        placement="right" 
        onClose={onAddClose} 
        visible={addVisible}>
          <Form
            labelCol={{
              span: 4,
            }}
            wrapperCol={{
              span: 14,
            }}
            layout="horizontal"
          >
            
            <Form.Item>
              <Input placeholder="管理员账户" value={adminname} onChange= { e => setAdminname(e.target.value) }/>
            </Form.Item>
            <Form.Item >
              <Input placeholder="密码" value={password} onChange= { e => setPassword(e.target.value) }/>
            </Form.Item>
            <Form.Item >
              <Select defaultValue = { 1 } value={role} onChange= { value => setRole(value) }>
                <Select.Option value={ 1 }>管理员</Select.Option>
                <Select.Option value={ 2 }>超级管理员</Select.Option>
              </Select>
            </Form.Item>
            <Form.Item >
              <Tree
                checkable
                onCheck={onCheck}
                treeData={menus}
                checkedKeys = { checkedKeys }
              />
            </Form.Item>
            <Form.Item 
              labelCol = {{
                offset: 4,
                span: 4
              }}
              >
              <Button type="primary">添加管理员</Button>
            </Form.Item>
          </Form>
      </Drawer>
    </Space>
  )
}
export default Com
```

**添加管理员**

```jsx
import { Table, Space, Button, Tag, Drawer, Form, Input, Select, Tree, message } from 'antd'
import { useState } from 'react'
import { getAdminList, addAdmin } from './../../api/admin'
import { useEffect, useCallback } from 'react'
import menus from './../../router/menus'
const Com = () => {
  // 请求列表的数据
  const [adminList, setAdminList] = useState([])
  useEffect(() => {
    getAdminList().then(res => {
      setAdminList(res.data)
    })
  }, [])

  const columns = [
    {
      title: '序号',
      render (text, record, index) {
        return (<span>{ index + 1 }</span>)
      }
    },
    {
      title: '账号',
      dataIndex: 'adminname'
    },
    {
      title: '权限',
      dataIndex: 'role',
      render (text) {
        return (
          <>
            {
              text === 2 ? <Tag color="green">超级管理员</Tag> : <Tag>管理员</Tag>
            }
          </>
        )
      }
    },
    {
      title: '操作',
      render (text, record, index) {
        return (
          <Space>
            <Button type="ghost">编辑</Button>
            <Button danger>删除</Button>
          </Space>
        )
      }
    }
  ]

  // 添加管理员
  const [addVisible, setAddVisible] = useState(false);
  const [adminname, setAdminname] = useState('');
  const [password, setPassword] = useState('');
  const [role, setRole] = useState(1);
  const [checkedKeys, setCheckedKeys] = useState(['0-0']);
  const onAddOpen = useCallback(() => {
    setAddVisible(true)
  }, [])
  const onAddClose = useCallback(() => {
    setAddVisible(false)
  }, [])
  const onCheck = useCallback((checkedKeys) => {
    setCheckedKeys(checkedKeys)
  }, [])
  const addAdminFn = useCallback(() => {
    const data = {
      adminname,
      password,
      role,
      checkedKeys
    }
    console.log(data)
    addAdmin(data).then(res => {
      console.log('leilei', res)
      if (res.code === '10004') {
        message.error('该管理员已存在')
      } else {
        // 清空表单的数据，方便下次添加，其次让抽屉消失，然后重新请求数据
        setAdminname('')
        setPassword('')
        setRole(1)
        setCheckedKeys(['0-0'])

        setAddVisible(false)

        getAdminList().then(res => {
          setAdminList(res.data)
        })
      }
    })
  }, [adminname, password, role, checkedKeys])
  return (
    <Space direction="vertical" style={{ width: '100%' }}>
      <Button type="primary" onClick = { onAddOpen } >添加管理员</Button>
      <Table dataSource = { adminList } columns = { columns } rowKey = "adminid"/>
      {/* 添加管理员抽屉 */}
      <Drawer 
        width = "500"
        title="添加管理员" 
        placement="right" 
        onClose={onAddClose} 
        visible={addVisible}>
          <Form
            labelCol={{
              span: 4,
            }}
            wrapperCol={{
              span: 14,
            }}
            layout="horizontal"
          >
            
            <Form.Item>
              <Input placeholder="管理员账户" value={adminname} onChange= { e => setAdminname(e.target.value) }/>
              { adminname }
            </Form.Item>
            <Form.Item >
              <Input placeholder="密码" value={password} onChange= { e => setPassword(e.target.value) }/>
              { password }
            </Form.Item>
            <Form.Item >
              <Select defaultValue = { 1 } value={role} onChange= { value => setRole(value) }>
                <Select.Option value={ 1 }>管理员</Select.Option>
                <Select.Option value={ 2 }>超级管理员</Select.Option>
              </Select>
              { role }
            </Form.Item>
            <Form.Item >
              <Tree
                checkable
                onCheck={onCheck}
                treeData={menus}
                checkedKeys = { checkedKeys }
              />
              { checkedKeys }
            </Form.Item>
            <Form.Item 
              labelCol = {{
                offset: 4,
                span: 4
              }}
              >
              <Button type="primary" onClick = { addAdminFn }>添加管理员</Button>
            </Form.Item>
          </Form>
      </Drawer>
    </Space>
  )
}
export default Com
```

**删除管理员**

```jsx
import { Table, Space, Button, Tag, Drawer, Form, Input, Select, Tree, message, Popconfirm } from 'antd'
import { useState } from 'react'
import { getAdminList, addAdmin, deleteAdmin } from './../../api/admin'
import { useEffect, useCallback } from 'react'
import menus from './../../router/menus'
const Com = () => {
  // 请求列表的数据
  const [adminList, setAdminList] = useState([])
  useEffect(() => {
    getAdminList().then(res => {
      setAdminList(res.data)
    })
  }, [])

  const columns = [
    {
      title: '序号',
      render (text, record, index) {
        return (<span>{ index + 1 }</span>)
      }
    },
    {
      title: '账号',
      dataIndex: 'adminname'
    },
    {
      title: '权限',
      dataIndex: 'role',
      render (text) {
        return (
          <>
            {
              text === 2 ? <Tag color="green">超级管理员</Tag> : <Tag>管理员</Tag>
            }
          </>
        )
      }
    },
    {
      title: '操作',
      render (text, record, index) {
        return (
          <Space>
            <Button type="ghost">编辑</Button>
            <Popconfirm
              title="确认删除吗?"
              onConfirm = { () => {
                deleteAdmin({
                  adminid: record.adminid
                }).then(res => {
                  getAdminList().then(res => {
                    setAdminList(res.data)
                  })
                })
              } }
              onCancel = { () => {}}
              >
              <Button danger>删除</Button>
            </Popconfirm>
          </Space>
        )
      }
    }
  ]

  // 添加管理员
  const [addVisible, setAddVisible] = useState(false);
  const [adminname, setAdminname] = useState('');
  const [password, setPassword] = useState('');
  const [role, setRole] = useState(1);
  const [checkedKeys, setCheckedKeys] = useState(['0-0']);
  const onAddOpen = useCallback(() => {
    setAddVisible(true)
  }, [])
  const onAddClose = useCallback(() => {
    setAddVisible(false)
  }, [])
  const onCheck = useCallback((checkedKeys) => {
    setCheckedKeys(checkedKeys)
  }, [])
  const addAdminFn = useCallback(() => {
    const data = {
      adminname,
      password,
      role,
      checkedKeys
    }
    console.log(data)
    addAdmin(data).then(res => {
      console.log('leilei', res)
      if (res.code === '10004') {
        message.error('该管理员已存在')
      } else {
        // 清空表单的数据，方便下次添加，其次让抽屉消失，然后重新请求数据
        setAdminname('')
        setPassword('')
        setRole(1)
        setCheckedKeys(['0-0'])

        setAddVisible(false)

        getAdminList().then(res => {
          setAdminList(res.data)
        })
      }
    })
  }, [adminname, password, role, checkedKeys])
  return (
    <Space direction="vertical" style={{ width: '100%' }}>
      <Button type="primary" onClick = { onAddOpen } >添加管理员</Button>
      <Table dataSource = { adminList } columns = { columns } rowKey = "adminid"/>
      {/* 添加管理员抽屉 */}
      <Drawer 
        width = "500"
        title="添加管理员" 
        placement="right" 
        onClose={onAddClose} 
        visible={addVisible}>
          <Form
            labelCol={{
              span: 4,
            }}
            wrapperCol={{
              span: 14,
            }}
            layout="horizontal"
          >
            
            <Form.Item>
              <Input placeholder="管理员账户" value={adminname} onChange= { e => setAdminname(e.target.value) }/>
              { adminname }
            </Form.Item>
            <Form.Item >
              <Input placeholder="密码" value={password} onChange= { e => setPassword(e.target.value) }/>
              { password }
            </Form.Item>
            <Form.Item >
              <Select defaultValue = { 1 } value={role} onChange= { value => setRole(value) }>
                <Select.Option value={ 1 }>管理员</Select.Option>
                <Select.Option value={ 2 }>超级管理员</Select.Option>
              </Select>
              { role }
            </Form.Item>
            <Form.Item >
              <Tree
                checkable
                onCheck={onCheck}
                treeData={menus}
                checkedKeys = { checkedKeys }
              />
              { checkedKeys }
            </Form.Item>
            <Form.Item 
              labelCol = {{
                offset: 4,
                span: 4
              }}
              >
              <Button type="primary" onClick = { addAdminFn }>添加管理员</Button>
            </Form.Item>
          </Form>
      </Drawer>
    </Space>
  )
}
export default Com
```

## 19.左侧菜单栏的权限验证

// ['0-0', '0-1-1', '0-2-2'] 检索 router.menus数据

// ['0-0', '0-1', '0-1-1', '0-2', '0-2-2']

```js
// src/layout/main/components/tool.js

// ['0-0', '0-1-1', '0-2', '0-2-2', '0-3-0-1'] ===>  ['0-0', '0-1', '0-1-1', '0-2', '0-2-2', '0-3', '0-3-0', '0-3-0-1']

// author gp25 宽哥
export const formatArr = (arr) => {
  const result = arr.reduce((_arr, item) => [..._arr, ...sliceStr(item)], [])
  return [...new Set(result)].sort()
}
const sliceStr = (str) => {
  if (str.split('-').length > 2) {
    return str.split('-').reduce((_arr, item) => {
      if (!Array.isArray(_arr)) return [`${_arr}-${item}`]
      return [`${_arr[0]}-${item}`, ..._arr]
    })
  }
  return [str]
}
// 截取字符串  数组合并   数组去重 --- 适用于最多只有二级菜单
export function getCheckedKeys (checkedKeys) {
  const arr = []
  checkedKeys.forEach(item => {
    arr.push(item.slice(0, 3))
  })
  const result = Array.from(new Set([...checkedKeys, ...arr])).sort()
  return result
}

// 获取左侧的菜单栏数据
export function getMenu (menus,checkedKeys) {
  let arr = []
  checkedKeys.forEach(item => {
    menus.forEach(value => {
      if(value.key === item) {
        // arr.push({...value})
        arr.push(Object.assign({}, value))
      }
    })
  })
  // 搞定第二层级
  arr.forEach(item => {
    if(item.children) {
      let newArr = getMenu(item.children,checkedKeys)
      item.children = newArr
    }
  })
  return arr
}

// 判断pathname是不是在menus中
// export function checkPathName (menus: IRoute[], pathname: string) {
//   // console.log(menus)
//   const flag =  menus.some(item => {
//     if (item.children) {
//       return checkPathName(item.children, pathname)
//     } else {
//       return item.path === pathname
//     }
//   })
//   // console.log(flag)
//   return flag
// }

export function checkPathName (routes, pathname, arr) {
  // const arr: string[] = []
  routes.forEach(item => {
    if (item.children) {
      checkPathName(item.children, pathname, arr)
    }
    arr.push(item.path)
  })
  console.log('9999', arr)
  return arr.indexOf(pathname) !== -1
}
```

```jsx
// src/layout/main/components/SideBar.jsx
import React, { Component } from 'react';
import { Layout, Menu } from 'antd';
import { withRouter } from 'react-router-dom'
import logo from '../../../logo.svg';

import { connect } from 'react-redux'
import routes from './../../../router/menus'
import { formatArr, getMenu } from './tool' //++++++++++++++++++++++
import { getAdminDetail } from '../../../api/admin';// ++++++++++++++++++++++
const { Sider } = Layout;
const { SubMenu } = Menu;

// 1.只展开二级菜单的选项 --- 注意值为数据中的path属性
const rootSubmenuKeys = []
routes.forEach(item => {
  if (item.children) {
    rootSubmenuKeys.push(item.path)
  }
})




@connect((state) => {
  return {
    collapsed: state.getIn(['app', 'collapsed']),
    adminname: state.getIn(['user', 'adminname'])// ++++++++++++++++++++++
  } 
})
@withRouter
class SideBar extends Component {
  // 2.展开的二级菜单的数据
  state = {
    openKeys: [],
    selectedKeys: [],
    checkedKeys: [], // 记录数据库中的菜单的数据标识 ++++++++++++++++++++++
    newMenus: [] // 真实渲染的菜单数据++++++++++++++++++++++
  }

  componentDidMount () {
    console.log(this.props.location.pathname)
    const pathname = this.props.location.pathname
    this.setState({
      openKeys: ['/' + pathname.split('/')[1]],
      selectedKeys: [pathname]
    })
		// ++++++++++++++++++++++
    getAdminDetail({ // 获取管理员信息
      adminname: this.props.adminname
    }).then(res => {
      console.log('admin', res)
      this.setState({
        checkedKeys: res.data[0].checkedKeys
      }, () => {
        // 处理数据
        const newCheckedKeys = formatArr(this.state.checkedKeys)
        this.setState({ // 返回真实的菜单栏数据
          newMenus: this.props.adminname === 'admin' ? routes : getMenu(routes, newCheckedKeys)
        })
      })
    })
  }

  componentDidUpdate (prevProps) { // 点击面包屑时监听路由的变化，设计选中状态
    if (prevProps.location.pathname !== this.props.location.pathname) {
      const pathname = this.props.location.pathname
      this.setState({
        selectedKeys: [pathname]
      })
    }
  }

  // 2.实现只展开其中的一个二级菜单
  onOpenChange = keys => {
    const latestOpenKey = keys.find(key => this.state.openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      this.setState({
        openKeys: keys
      })
    } else {
      this.setState({
        openKeys: latestOpenKey ? [latestOpenKey] : []
      })
    }
  }
  // 3.渲染左侧菜单栏数据，注意return的书写
  renderSideBar = (routes) => {
    return routes.map(item => {
      if (item.children) {
        return <SubMenu key={ item.path } icon={ item.icon } title={ item.title }>
          {
            this.renderSideBar(item.children)
          }
          </SubMenu>
      } else {
        return (
          item.hidden ? null : <Menu.Item key={ item.path } icon={ item.icon }>
          { item.title }
        </Menu.Item>
        )
      }
    })
  }
  changeUrl = ({ key }) => {
    // console.log(key)
    // console.log(this.props)
    this.props.history.push(key)
    this.setState({
      selectedKeys: [key]
    })
  }
  render() {
    return (
      <Sider trigger={null} collapsible collapsed={this.props.collapsed}>
        <div className="logo" >
            <img src={ logo } style={{width: '32px', height: '32px', margin: '0 10px 0 0'}} alt=""/>
            { this.props.collapsed ? null : <span className="logoname">HAIGOU_ADMIN</span>  }
          </div>
        <Menu 
          onClick = { this.changeUrl } 
          theme="dark"
          mode="inline" 
          openKeys={this.state.openKeys} 
          onOpenChange={this.onOpenChange} 
          selectedKeys={ this.state.selectedKeys }>
          {/* <Menu.Item key="1" icon={<UserOutlined />}>
            nav 1
          </Menu.Item> */}
          {
            // 3.调用递归函数渲染左侧的菜单栏 ，渲染真实 的数据 ++++++++++++++++++++++
            this.renderSideBar(this.state.newMenus)
          }
        </Menu>
      </Sider>
    );
  }
}
export default SideBar
// export default connect((state) => {
//   return {
//     collapsed: state.getIn(['app', 'collapsed'])
//   }
// })(SideBar)
```

## 20,页面权限

```js
// src/layout/main/components/appMain.jsx
import React, { Component, Suspense } from 'react'
import { Layout, Spin } from 'antd'
import { Switch, Route, Redirect, withRouter } from 'react-router-dom'
import routes from './../../../router/menus'
import NotMatch from './../../../views/error/404.jsx'
import { formatArr, getMenu, checkPathName } from './tool'
import { getAdminDetail } from '../../../api/admin';
import { connect } from 'react-redux'
const { Content } = Layout

@connect((state) => {
  return {
    adminname: state.getIn(['user', 'adminname'])
  }
})
@withRouter
class AppMain extends Component {
  state = { // ++++++++++
    checkedKeys: [], // 记录数据库中的菜单的数据标识
    newMenus: [] // 真实渲染的菜单数据
  }

  componentDidMount () { // ++++++++++
    getAdminDetail({ // 获取管理员信息
      adminname: this.props.adminname
    }).then(res => {
      console.log('admin', res)
      this.setState({
        checkedKeys: res.data[0].checkedKeys
      }, () => {
        // 处理数据
        const newCheckedKeys = formatArr(this.state.checkedKeys)
        this.setState({ // 返回真实的菜单栏数据
          newMenus: this.props.adminname === 'admin' ? routes : getMenu(routes, newCheckedKeys)
        })
      })
    })
  }
  renderRoute = (routes) => {
    return routes.map(item => {
      if (item.children) {
        return this.renderRoute(item.children)
      } else {
        return (
          <Route key = { item.path } exact path = { item.path } component = { item.component } />
        )
      }
    })
  }
  redirectRoute = (routes) => {
    // 先处理routes数据，因为并不是所有的路由都有重定向
    const redirectRoutes = routes.filter(item => item.redirect)
    return redirectRoutes.map(item => {
      return <Redirect key={ item.path } path={ item.path } exact to={item.redirect} />
    })
    
  }
  render() {
    return (
      <Content
        className="site-layout-background"
        style={{
          margin: '0 16px 24px 16px',
          padding: 24,
          minHeight: 280,
        }}
      >
        {/* Content */}
        <Suspense fallback = {  <Spin size="large"/> }>
          <Switch>
            { // ++++++++++
              this.renderRoute(this.state.newMenus)
            }
            { // ++++++++++
              this.redirectRoute(this.state.newMenus)
            }
            <Route path="*" >
              {  // ++++++++++
                checkPathName(routes, this.props.location.pathname, [])
                  ? <span>无权限</span>
                  : <NotMatch></NotMatch>
              }
            </Route>
          </Switch>
        </Suspense>
      </Content>
    )
  }
}
export default AppMain

```

## 21 按钮权限

拷贝tool.js到轮播图管理中

````jsx
// src/views/banner/Index.jsx
import { Table, Image, Button, Space, Popconfirm, message } from 'antd'
import { useState, useEffect } from 'react'
import { getBannerList, deleteBanner } from './../../api/banner'
import { getAdminDetail } from './../../api/admin'
import { connect } from 'react-redux'
import { formatArr, getMenu, checkPathName } from './tool'
import routes from './../../router/menus'
import { useLocation } from 'react-router-dom'
const Com = (props) => {
  const { pathname } = useLocation()// ++++++++++
  const [ newRoute, setNewRoute ] = useState([])// ++++++++++
  const [ checkedKeys, setCheckedKeys ] = useState([])// ++++++++++
  useEffect(() => { // ++++++++++
    getAdminDetail({ adminname: props.adminname }).then(res => {
      setCheckedKeys(res.data[0].checkedKeys)
      const newCheckedKeys = formatArr(checkedKeys)
      setNewRoute(props.adminname === 'admin' ? routes : getMenu(routes, newCheckedKeys))
    })
  }, [props.adminname])
  const columns = [
    {
      title: '序号',
      render (text, record, index) {
        return <span>{ index + 1 }</span>
      }
    },
    {
      title: '图片',
      dataIndex: 'img',
      render (text, record, index) {
        return <Image src={ text } width={200} />
      }
    },
    {
      title: '链接',
      dataIndex: 'link'
    },
    {
      title: '提示',
      dataIndex: 'alt'
    },
    {
      title: '操作',
      render (text, record, index) {
        return (
          <Popconfirm
          title="确定删除吗?"
          onConfirm={ () => {
            deleteBanner({ bannerid: record.bannerid }).then(res => {
              // console.log('删除成功')
              getBannerList().then(res => {
                // console.log('删除成功', res)
                setBannerList(res.data)
              })
            })
          }}
          onCancel={() => {}}
          okText="确定"
          cancelText="取消"
          >
            <Button danger>删除</Button>
          </Popconfirm>
        )
      }
    }
  ]
  const [bannerList, setBannerList] = useState([])

  useEffect(() => {
    getBannerList().then(res => {
      // console.log(res)
      setBannerList(res.data)
      
    })
  }, [])

  return (
    <Space direction="vertical" style={{ width: '100%'}}>
      <Button 
        type="primary" 
        style={{ marginBottom: '10px'}}
        onClick = { () => { // ++++++++++
          // console.log('aaaa', checkPathName(newRoute, pathname, []))
          // routes 应该替换为 newRoute
           checkPathName(newRoute, pathname, [])
            ? props.history.push('/banner/add')
            : message.error('无权限')
          // console.log(props)
          
        }}
        >
        添加轮播图1
      </Button>
      <Table dataSource={bannerList} columns={columns} rowKey = "bannerid"/>
    </Space>
  )
}
export default connect(state => ({
  adminname: state.getIn(['user', 'adminname'])
}))(Com)
````

## 22.数据可视化

方案：

echarts: https://echarts.apache.org/zh/index.html

> ts中使用 echarts ： https://echarts.apache.org/handbook/zh/basics/import#%E5%9C%A8-typescript-%E4%B8%AD%E6%8C%89%E9%9C%80%E5%BC%95%E5%85%A5

highCharts:https://www.highcharts.com.cn/

Antv: https://antv.gitee.io/zh/

​	g2:https://antv-g2.gitee.io/zh/

​	g2plot:https://g2plot.antv.vision/zh/

​	react中使用g2:https://charts.ant.design/zh-CN

D3:视频地址：链接: https://pan.baidu.com/s/1SVS36TjtcR27Rqj_HURDZA  密码: p9ur



Webglhttp://www.hewebgl.com/  

Three.jshttp://www.webgl3d.cn/

### 1.echarts

```
cnpm i echarts echarts-for-react -S

```

```js
{
    key: '0-5',
    path: '/data',
    title: '数据可视化',
    icon: <PictureOutlined />,
    redirect: '/data/echarts',
    children: [
      {
        key: '0-5-0',
        path: '/data/echarts',
        title: 'echarts使用示例',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/data/Echarts.jsx'))
      },
      {
        key: '0-5-1',
        path: '/data/g2',
        title: 'antv-g2使用示例',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/data/G2.jsx'))
      },
      {
        key: '0-5-2',
        path: '/data/antd',
        title: 'antdcharts',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/data/AntdCharts.jsx'))
      }
    ]
  },
```



```jsx
// src/views/data/Echarts.jsx
import ReactECharts from 'echarts-for-react';
import { Button } from 'antd'
import { getKDataFn } from './../../api/data'
import { useState, useEffect } from 'react'
const Com = () => {
  // 简单的折线图
  const getLineOption = () => {
    const option = {
      title: {
        text: 'echarts 简单折线图'
      },
      legend: {
        data: ['星期', '日期', '测试']
      },
      xAxis: {
        type: 'category',
        data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
      },
      yAxis: {
        type: 'value'
      },
      series: [
        {
          name: '星期',
          data: [820, 932, 901, 934, 1290, 1330, 1320],
          type: 'line',
          smooth: true
        },
        {
          name: '日期',
          data: [620, 1032, 801, 834, 1390, 1830, 320],
          type: 'bar',
          smooth: true
        },
        {
          name: '测试',
          data: [1820, 1932, 1901, 1934, 1290, 1330, 1320],
          type: 'line',
          smooth: true
        }
      ],
      tooltip: {
        show: true
      }
    }
    return option
  }
  // 极坐标
  const getPolarOption = () => {
    const data = [];
    for (let i = 0; i <= 100; i++) {
      let theta = (i / 100) * 360;
      let r = 5 * (1 + Math.sin((theta / 180) * Math.PI));
      data.push([r, theta]);
    }
    const option = {
      title: {
        text: '极坐标双数值轴'
      },
      legend: {
        data: ['line']
      },
      polar: {},
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross'
        }
      },
      angleAxis: {
        type: 'value',
        startAngle: 0
      },
      radiusAxis: {},
      series: [
        {
          coordinateSystem: 'polar',
          name: 'line',
          type: 'line',
          data: data
        }
      ]
    }
    return option
  }
  // K线图
  const [x, setX] = useState(['2017-10-24', '2017-10-25', '2017-10-26', '2017-10-27'])
  const [val, setVal] = useState([
    [10, 34, 10, 38],
    [20, 35, 30, 50],
    [11, 38, 33, 44],
    [28, 15, 5, 42]
  ])

  // useEffect(() => {
  //   getKDataFn().then(res => {
  //     setX(res.data.x)
  //     setVal(res.data.val)
  //   })
  // }, [])
  const changeKOption = () => {
    getKDataFn().then(res => {
      setX(res.data.x)
      setVal(res.data.val)
    })
  }
  const getKOption = () => {
    const option = {
      title: {
        text: 'K线图'
      },
      xAxis: {
        data: x
      },
      yAxis: {},
      series: [
        {
          type: 'candlestick',
          data: val
        }
      ]
    };
    return option
  }
  return (
    <div style= {{ width: '100%'}}>
      <h1>Echarts使用示例</h1>
      <ReactECharts 
        option = { getLineOption() }
      />
       <ReactECharts 
        option = { getPolarOption() }
      />
      <Button onClick = { changeKOption }>加载K数据</Button>
       <ReactECharts 
        option = { getKOption() }
      />
    </div>
  )
}
export default Com
```

### 2.g2

https://g2.antv.vision/zh

```
cnpm install @antv/g2 --save
```

```jsx
// src/views/data/G2.jsx
import { Chart } from '@antv/g2';
import { useLayoutEffect } from 'react'
const Com = () => {
  useLayoutEffect(() => {
    const data = [
      { year: '1951 年', sales: 38 },
      { year: '1952 年', sales: 52 },
      { year: '1956 年', sales: 61 },
      { year: '1957 年', sales: 145 },
      { year: '1958 年', sales: 48 },
      { year: '1959 年', sales: 38 },
      { year: '1960 年', sales: 38 },
      { year: '1962 年', sales: 38 },
    ];
    const chart = new Chart({
      container: 'container',
      autoFit: true,
      height: 500,
    });
    
    chart.data(data);
    chart.scale('sales', {
      nice: true,
    });
    
    chart.tooltip({
      showMarkers: false
    });
    chart.interaction('active-region');
    
    chart.interval().position('year*sales');
    
    chart.render();
  })
  return (
    <div style={{ width: '100%'}}>
      <h1>G2使用示例</h1>
      <div id="container"></div>
    </div>
  )
}
export default Com
```

> 可以讲操作放到useLayoutEffect hooks中

### 3.ant design charts

https://charts.ant.design/zh-CN/guide/start

```
cnpm i @ant-design/charts -S
```

```jsx
// src/views/data/AntdCharts.jsx
import { Line } from '@ant-design/charts';
const Com = () => {
  const data = [
    { year: '1991', value: 3 },
    { year: '1992', value: 4 },
    { year: '1993', value: 3.5 },
    { year: '1994', value: 5 },
    { year: '1995', value: 4.9 },
    { year: '1996', value: 6 },
    { year: '1997', value: 7 },
    { year: '1998', value: 9 },
    { year: '1999', value: 13 },
  ];

  const config = {
    data,
    height: 400,
    xField: 'year',
    yField: 'value',
    point: {
      size: 5,
      shape: 'diamond',
    },
  };
  return (
    <div style={{ width: '100%'}}>
      <h1>ant design charts 使用示例</h1>
      <Line {...config} />
    </div>
  )
}
export default Com
```

## 23.编辑器

```js
{
    key: '0-6',
    path: '/form',
    title: '编辑器',
    icon: <PictureOutlined />,
    redirect: '/form/editor',
    children: [
      {
        key: '0-6-0',
        path: '/form/editor',
        title: '富文本编辑器',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/form/Editor.jsx'))
      },
      {
        key: '0-6-1',
        path: '/form/md',
        title: 'markdown 编辑器',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/form/Md.jsx'))
      }
    ]
  },
```



### 1.富文本编辑器

```
cnpm i braft-editor -S
```

```jsx
// src/views/form/Editor.jsx
import BraftEditor from 'braft-editor'
import 'braft-editor/dist/index.css'
import { useState } from 'react'
const Com = () => {
  const [ editorState, setEditorState ] = useState(BraftEditor.createEditorState('<p>Hello <b>World!</b></p>'),)
  const [ outputHTML, setOutputHTML ] = useState('')
  const handleChange = (editorState) => {
    setEditorState(editorState)
    setOutputHTML(editorState.toHTML())
  }
  return (
    <div>
      富文本编辑器
      <BraftEditor
        value={ editorState }
        onChange={ handleChange }
      />
      渲染数据：
      { outputHTML }
      预览效果
      <div dangerouslySetInnerHTML = {{ __html: outputHTML }}></div>
    </div>
  )
}
export default Com
```

### 2.markDown编辑器

```
cnpm i react-markdown -S  阅读器 -- 预览
cnpm i react-markdown-editor-lite -S  编辑器
```

```jsx
// src/views/form/Md.jsx
import MdEditor from 'react-markdown-editor-lite';
// import style manually
import ReactMarkdown from 'react-markdown'
import 'react-markdown-editor-lite/lib/index.css';
const Com = (props) => {

  return (
    <div style= {{ width: '100%'}}>
      markdown编辑器
      <MdEditor 
        style={{ height: '500px' }} 
        renderHTML = { (text) => {
          return (
            <ReactMarkdown>
              { text }
            </ReactMarkdown>
          )
        }}
        onChange={ ({ html, text }) => {
          // console.log('handleEditorChange', html, text);
          console.log('handleEditorChange', html, text);
        }} >
        </MdEditor>
    </div>
  )
}
export default Com
```

## 24.导入以及导出

```js
{
    key: '0-7',
    path: '/excel',
    title: '导入以及导出',
    icon: <PictureOutlined />,
    redirect: '/excel/import',
    children: [
      {
        key: '0-7-0',
        path: '/excel/import',
        title: '导入excel',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/excel/Import.jsx'))
      },
      {
        key: '0-7-1',
        path: '/excel/export',
        title: '导出excel',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/excel/Export.jsx'))
      }
    ]
  },
```



### 1.导入

```jsx
// src/views/excel/Import.jsx
import { Table, Space, Input, Button } from 'antd'
import { useRef, useCallback, useState } from 'react'
const Com = () => {
  const fileRef = useRef()
  const selectExcel = useCallback(() => {
    console.log(fileRef.current)
    fileRef.current.input.click()
  },[])

  const [ proList, setProList ] = useState()
  const columns = [
    {
      title: '序号',
      render: (text, record, index) => {
        return <span>{ index + 1 }</span>
      }
    },
    {
      title: '产品名称',
      dataIndex: 'proname'
    },
    {
      title: '价格',
      dataIndex: 'originprice'
    }
  ]

  return (
    <Space direction="vertical" style = {{ width: '100%' }}>
      <Button type="primary" onClick = { selectExcel }>导入数据</Button>
      <Input ref={ fileRef } hidden type="file" />

      <Table
        dataSource = { proList }
        columns = { columns }
        rowKey = "proname"
      >
      </Table>
    </Space>
  )
}
export default Com
```

```
cnpm i xlsx -S
```

```jsx
import { Table, Space, Input, Button, Image } from 'antd'
import { useRef, useCallback, useState } from 'react'
import * as XLSX from 'xlsx'
const Com = () => {
  const fileRef = useRef()
  const selectExcel = useCallback(() => {
    // console.log(fileRef.current)
    fileRef.current.input.click()
  },[])

  const [ proList, setProList ] = useState()
  const columns = [
    {
      title: '序号',
      render: (text, record, index) => {
        return <span>{ index + 1 }</span>
      }
    },
    {
      title: '产品名称',
      dataIndex: 'proname'
    },
    {
      title: '图片',
      dataIndex: 'img1',
      render (text, record, index) {
        return (
          <Image src= {text } style={{ width: 80, height: 80 }} />
        )
      }
    },
    {
      title: '价格',
      dataIndex: 'originprice'
    }
  ]
  const uploadFile = () => {
    const file = fileRef.current.input.files[0]
    const reader = new FileReader()
    reader.readAsBinaryString(file)
    reader.onload = function () {
      const workbook = XLSX.read(this.result, { type: 'binary' });
      const t = workbook.Sheets['工作表1']
      const r = XLSX.utils.sheet_to_json(t)
      setProList(r)
      setAble(true)
    }
  }

  const [able, setAble] = useState(false)

  const upload = () => {
    // 调用接口
    setAble(false)
  }
  return (
    <Space direction="vertical" style = {{ width: '100%' }}>
      
      { able 
        ? <Button type="primary" onClick = { upload }>上传数据</Button> 
        : <Button type="primary" onClick = { selectExcel }>导入数据</Button> 
      }
      <Input ref={ fileRef } hidden type="file" onChange = { uploadFile }/>

      <Table
        dataSource = { proList }
        columns = { columns }
        rowKey = "proname"
      >
      </Table>
    </Space>
  )
}
export default Com
```

> 自定义hooks提取代码

```js
// src/views/excel/hooks/useColumns.js
import { Image } from 'antd'
function useColumns () {
  const columns = [
    {
      title: '序号',
      render: (text, record, index) => {
        return <span>{ index + 1 }</span>
      }
    },
    {
      title: '产品名称',
      dataIndex: 'proname'
    },
    {
      title: '图片',
      dataIndex: 'img1',
      render (text, record, index) {
        return (
          <Image src= {text } style={{ width: 80, height: 80 }} />
        )
      }
    },
    {
      title: '价格',
      dataIndex: 'originprice'
    }
  ]
  return columns
}

export default useColumns
```

```js
// src/views/excel/hooks/useFileButton.js
import { useCallback  } from 'react'
import * as XLSX from 'xlsx'
function useFileButton (fileRef) {

  const selectExcel = useCallback(() => {
    // console.log(fileRef.current)
    fileRef.current.input.click()
  }, [fileRef])
  const uploadFile = () => {
    const file = fileRef.current.input.files[0]
    const reader = new FileReader()
    reader.readAsBinaryString(file)
    return new Promise(resolve => {
      reader.onload = function () {
        const workbook = XLSX.read(this.result, { type: 'binary' });
        const t = workbook.Sheets['工作表1']
        const r = XLSX.utils.sheet_to_json(t)
  
        resolve(r)
      }
    })
  }
  return {
    selectExcel,
    uploadFile
  }
}

export default useFileButton
```

```
// src/views/excel/hooks/index.js
export { default as useColumns } from './useColumns'
export { default as useFileButton } from './useFileButton'
```

```jsx
// src/views/excel/Import.jsx
import { Table, Space, Input, Button } from 'antd'
import { useRef, useState } from 'react'
import { useColumns, useFileButton } from './hooks'
const Com = () => {
  const fileRef = useRef()
  const [ proList, setProList ] = useState()
  const [able, setAble] = useState(false)

  const { selectExcel, uploadFile } = useFileButton(fileRef)
  const columns = useColumns()

  const uploadFileFn = () => {
    uploadFile().then(proList => {
      setProList(proList)
      setAble(true)
    })
  }
 
  const upload = () => {
    // 调用接口
    setAble(false)
  }
  return (
    <Space direction="vertical" style = {{ width: '100%' }}>
      
      { able 
        ? <Button type="primary" onClick = { upload }>上传数据1</Button> 
        : <Button type="primary" onClick = { selectExcel }>导入数据1</Button> 
      }
      <Input ref={ fileRef } hidden type="file" onChange = { uploadFileFn }/>

      <Table
        dataSource = { proList }
        columns = { columns }
        rowKey = "proname"
      >
      </Table>
    </Space>
  )
}
export default Com
```

#### 2.导出

```
cnpm i js-export-excel -S
```

```jsx
// src/views/excel/Export.jsx
import { useColumns } from "./hooks"
import { Table, Space, Button } from 'antd'
import { useState, useEffect } from 'react'
import { getProList } from '../../api/pro'
import { useCallback } from "react"
import ExportJsonExcel from 'js-export-excel'
const Com = () => {
  const columns = useColumns()
  // 分页器
  const [ current, setCurrent ] = useState(1)
  const [ pageSize, setPageSize ] = useState(10)
  const [ proList, setProList ] = useState([])

  useEffect(() => {
    getProList().then(res => {
      setProList(res.data)
    })
  }, [])
  // 固定头和列
  const [ height, setHeight ] = useState(0)
  useEffect(() => {
    setHeight(window.innerHeight)
  }, [])

  const exportPro = useCallback(() => {
    var option = {};

    option.fileName = "产品列表";

    option.datas = [
      {
        sheetData: proList,
        sheetName: "sheet",
        sheetFilter: ['proname', 'img1', 'category', 'brand', 'originprice'],
        sheetHeader: ["产品名称", "产品图片", '分类', '品牌', '原价'],
        columnWidths: [20, 20],
      },
    ];

    var toExcel = new ExportJsonExcel(option); //new
    toExcel.saveExcel(); //保存
  }, [proList])

  return (
    <Space style={{ width: '100%' }} direction="vertical">
      <Button onClick = { exportPro }>导出数据</Button>
      <Table 
        dataSource = { proList } 
        columns = { columns } 
        rowKey = "proid" 
        scroll={{ x: 1500, y: height - 300 }}
        pagination = { {
          position: ['bottomRight'],
          current,
          pageSize,
          onChange: (page, pageSize) => {
            setCurrent(page)
            setPageSize(pageSize)
          },
          pageSizeOptions: ['5', '10', '15', '20'],
          showQuickJumper: true,
          showTotal: (total) => <span>共有{total}条数据</span>
        } }
      ></Table>
    </Space>
  )
}
export default Com
```



## 25.地图

```js
{
    key: '0-8',
    path: '/map',
    title: '地图使用',
    icon: <PictureOutlined />,
    redirect: '/map/bmap',
    children: [
      {
        key: '0-8-0',
        path: '/map/bmap',
        title: '百度地图',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/map/BMap.jsx'))
      },
      {
        key: '0-8-1',
        path: '/map/amap',
        title: '高德地图',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/map/AMap.jsx'))
      },
      {
        key: '0-8-2',
        path: '/map/tmap',
        title: '腾讯地图',
        icon: <MenuOutlined />,
        component: lazy(() => import('./../views/map/TMap.jsx'))
      }
    ]
  },
```



### 1.百度地图

```jsx
// src/views/map/BMap.jsx
import { useState, useEffect } from "react"

const Com = () => {
  var map = new window.BMapGL.Map('container');
  map.centerAndZoom(new window.BMapGL.Point(116.514, 39.915), 10);
  map.enableScrollWheelZoom(true);
  useEffect(() => {
    // var map = new window.BMapGL.Map("container");
    // // 创建地图实例 
    // var point = new window.BMapGL.Point(116.404, 39.915);
    // // 创建点坐标 
    // map.centerAndZoom(point, 15);
    // // 初始化地图，设置中心点坐标和地图级别 

    // var map = new window.BMapGL.Map('container');
    // map.centerAndZoom(new window.BMapGL.Point(116.404, 39.928), 15);
    // map.enableScrollWheelZoom(true);
    // // 创建点标记
    // var marker1 = new window.BMapGL.Marker(new window.BMapGL.Point(116.404, 39.925));
    // var marker2 = new window.BMapGL.Marker(new window.BMapGL.Point(116.404, 39.915));
    // var marker3 = new window.BMapGL.Marker(new window.BMapGL.Point(116.395, 39.935));
    // var marker4 = new window.BMapGL.Marker(new window.BMapGL.Point(116.415, 39.931));
    // // 在地图上添加点标记
    // map.addOverlay(marker1);
    // map.addOverlay(marker2);
    // map.addOverlay(marker3);
    // map.addOverlay(marker4);
  }, [])

  
  const setNewZoom = () => {
    var zoom = document.getElementById('zoominput').value;
    map.setZoom(zoom);
  }
  const  getMapZoom = () => {
    alert(map.getZoom());
  }
  return (
    <div>
      百度地图
      <div class = "info">
        <div>改变地图级别:</div>
        <input id="zoominput" type="number" step="1" min="4" max="20" value="10" />
        <button id="change-btn" onClick={setNewZoom}>设置级别</button>
        <button id="change-btn" onClick={() => {map.zoomIn()}}>放大一级</button>
        <button id="change-btn" onClick={() => {map.zoomOut()}}>缩小一级</button>
        <button id="change-btn" onClick={getMapZoom}>获取当前级别</button>
      </div>
      <div id="container" style={{ width: 500, height: 500}}></div>
    </div>
  )
}
export default Com
```

> 百度地图官方推荐了 React-BMapGL ：https://huiyan.baidu.com/github/react-bmapgl/

### 2.高德地图

```jsx
// src/views/map/AMap.jsx
const Com = () => {
  // new window.AMap.Map('container', {
  //   resizeEnable: true, //是否监控地图容器尺寸变化
  //   zoom:11, //初始化地图层级
  //   center: [116.397428, 39.90923] //初始化地图中心点
  // });

  var map = new window.AMap.Map("container", {
    zoom: 13,
    center: [116.43, 39.92],
    resizeEnable: true
  });

  window.AMap.plugin([
      'AMap.ToolBar',
  ], function(){
      // 在图面添加工具条控件，工具条控件集成了缩放、平移、定位等功能按钮在内的组合控件
      map.addControl(new window.AMap.ToolBar({
          // 简易缩放模式，默认为 false
          liteStyle: true
      }));
  });
  return (
    <div style={{ width: '100%'}}>
      高德地图
      <div id="container" style={{ width: '100%', height: '100%'}}></div> 
    </div>
  )
}
export default Com
```

### 26.客服













## 26.双token验证

https://blog.csdn.net/a704397849/article/details/90216739

## 27.首页的数据统计









# 二.node接口

## 1.创建项目

```js
npx express jdd-admin-api

// 修改项目的端口号
var port = normalizePort(process.env.PORT || '3333');
app.set('port', port);

// 配置项目的长链接
yarn add nodemon global

"scripts": {
  "start": "node ./bin/www",
    "dev": "nodemon ./bin/www"
},
```

## 2.项目配置

```
api
	banner.js
	
sql
	models // 数据库集合
	db.js // 连接数据库
	index.js // 数据库增删改查的封装
```

Api/banner.js

```js
var router = require('express').Router()

router.get('/', (req, res, next) => {
  res.send('轮播图列表')
})
router.post('/add', (req, res, next) => {
  res.send('添加轮播图')
})
router.get('/delete', (req, res, next) => {
  res.send('删除单张轮播图')
})
router.get('/deleteAll', (req, res, next) => {
  res.send('删除所有轮播图')
})
module.exports = router
```

App.js

```js
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

// restful api
var bannerApi = require('./api/banner')

var app = express();
...
app.use(express.static(path.join(__dirname, 'public')));

// 注册接口
app.use('/api/banner', bannerApi)

app.use('/', indexRouter);
app.use('/users', usersRouter);

module.exports = app;

```

## 3.数据库的基本操作

### 1.连接数据库

```js
var mongoose = require('mongoose')
var DB_URL = 'mongodb://localhost:27017/sz-gp-4'

mongoose.connect(DB_URL, { useNewUrlParser: true, useUnifiedTopology: true })

mongoose.connection.on('connected', () => {
  console.log('db connect success')
})

mongoose.connection.on('disconnected', () => {
  console.log('db connect fail')
})

mongoose.connection.on('error', () => {
  console.log('db connect error')
})

module.exports = mongoose

```

### 2.创建数据库集合

Sql/models/Banner.js

```js
const mongoose = require('./../db')
const Schema = mongoose.Schema

const schema = new Schema({
  bannerid: { type: String, required: true },
  bannerimg: { type: String, required: true },
  link: { type: String, required: true },
  alt: { type: String, required: true }
})

module.exports = mongoose.model('banner', schema)
```

### 3.封装增删改查

```js
const { Collection } = require("./db")

module.exports = {
  insert (CollectionName, insertData) {
    return new Promise((resolve, reject) => {
      // node 错误优先回调原则
      CollectionName.insertMany(insertData, (err) => {
        if (err) throw err;
        resolve()
      })
    })
  },
  delete (CollectionName, whereData, deleteNum) {
    return new Promise(resolve => {
      const deleteType = deleteNum === 1 ? 'deleteMany' : 'deleteOne'
      CollectionName[deleteType](whereData, err => {
        if (err) throw err;
        resolve()
      })
    })
  },
  update (CollectionName, whereData, updateData, updateNum) {
    return new Promise(resolve => {
      const updateType = updateNum === 1 ? 'updateMany' : 'updateOne'
      CollectionName[updateType](whereData, updateData, err => {
        if (err) throw err;
        resolve()
      })
    })
  },
  find (CollectionName, whereData, showData) {
    return new Promise(resolve => {
      CollectionName.find(whereData, showData).exec((err, data) => {
        if (err) throw err;
        resolve(data)
      })
    })
  },
  paging (CollectionName, whereData, showData, count, limitNum) {
    limitNum = limitNum || 10 
    count = count || 1
    return new Promise(resolve => {
      // 页码 不是从0  开始， 是从1开始，所以要减1
      CollectionName.find(whereData, showData).limit(limitNum).skip((count - 1) * limitNum).exec((err, data) => {
        if (err) throw err
        resolve(data)
      })
    })
  }
}
```

### 4.接口测试

```js
var router = require('express').Router()
var sql = require('./../sql')
var Banner = require('./../sql/models/Banner')

router.get('/', (req, res, next) => {
  sql.find(Banner, {}, {}).then(data => {
    res.status('200').send(data)
  })
})
module.exports = router
```

### 5.构建react后台管理系统轮播图的添加页面

参照 一 . 15.2

### 6. 实现后端插入轮播图数据

**安装生成唯一字段的模块 -- uuid / node-uuid / short-uuid**

推荐使用short-uuid

Utils/index.js

```js
const uuid = require('short-uuid')

module.exports = {
  getUuid () {
    return uuid.generate()
  }
}
```

Api/banner.js

```js
var router = require('express').Router()
var sql = require('./../sql')
var Banner = require('./../sql/models/Banner')
var utils = require('./../utils')
router.post('/add', (req, res, next) => {
  let insertData = req.body;
  insertData.bannerid = 'banner_' + utils.getUuid()
  sql.insert(Banner, insertData).then(() => {
    res.status('200').send({
      code: '200',
      message: '添加轮播图'
    })
  })
})
module.exports = router
```

### 7.实现查询轮播图数据接口

```js
var router = require('express').Router()
var sql = require('./../sql')
var Banner = require('./../sql/models/Banner')
var utils = require('./../utils')

router.get('/', (req, res, next) => {
  sql.find(Banner, {}, { _id: 0, __v: 0 }).then(data => {
    res.status('200').send({
      code: '200',
      message: '轮播图数据',
      data
    })
  })
})
module.exports = router
```

参照 一.15.6



### 8.删除接口

Api/banner.js

```js
var router = require('express').Router()
var sql = require('./../sql')
var Banner = require('./../sql/models/Banner')
var utils = require('./../utils')
...
router.get('/delete', (req, res, next) => {
  const {bannerid} = req.query
  sql.delete(Banner, { bannerid }).then(() => {
    res.status(200).send({
      code: '200',
      message: '删除轮播图'
    })
  })
})
...
module.exports = router
```

### 9.批量删除选中的数据

Api/banner.js

```js
var router = require('express').Router()
var sql = require('./../sql')
var Banner = require('./../sql/models/Banner')
var utils = require('./../utils')

router.post('/deleteAll', (req, res, next) => {
  const deletearr = req.body.deletearr
  const arr = []
  deletearr.forEach(item => {
    arr.push(sql.delete(Banner, { bannerid: item }))
  })
  Promise.all(arr).then(() => {
    res.status(200).send({
      code: '200',
      message: '删除选中的轮播图'
    })
  })
})
module.exports = router
```

参照 一.15.10



## 4.快捷导航相关接口

### 1. 导航分类

Sql/models/Navcategory.js

```js
const mongoose = require('./../db')
const Schema = mongoose.Schema

const schema = new Schema({
  categoryid: { type: String, required: true },
  name: { type: String, required: true },
  titleColor: { type: String, required: true },
  titleImg: { type: String, required: true }
})

module.exports = mongoose.model('Navcategory', schema)
```



api/nav.js

```js
var router = require('express').Router()
var sql = require('./../sql')
var Navcategory = require('./../sql/models/Navcategory')
var utils = require('./../utils')

router.get('/navcategory', (req, res, next) => {
  sql.find(Navcategory, {}, { _id: 0, __v: 0 }).then(data => {
    res.status('200').send({
      code: '200',
      message: '导航的分类数据',
      data
    })
  })
})

module.exports = router
```

参照 一.16.3

### 2.导航列表

Sql/models/Navlist.js

```js
const mongoose = require('./../db')
const Schema = mongoose.Schema

const schema = new Schema({
  categoryid: { type: String, required: true },
  categoryname: { type: String, required: true },
  navid: { type: String, required: true },
  titleImg: { type: String, required: true },
  name: { type: String, required: true },
  icon: { type: String, required: true },
  jump: { type: String, required: true }
})

module.exports = mongoose.model('Navlist', schema)
```

api/nav.js

```js

var Navlist = require('./../sql/models/Navlist')
router.get('/navlist', (req, res, next) => {
  sql.find(Navlist, {}, { _id: 0, __v: 0 }).then(data => {
    res.status('200').send({
      code: '200',
      message: '导航列表',
      data
    })
  })
})

module.exports = router
```

参照一.16.4



## 5.商品的相关的管理

### 1.处理excel表格的数据

表格命名为 api/pro.xlsx 

### 2.创建Product数据库集合

Sql/models/Product.js

```js
const mongoose = require('./../db')
const Schema = mongoose.Schema

const schema = new Schema({
  proid: { type: String, required: true },
  category: { type: String, required: true },
  brand: { type: String, required: true },
  proname: { type: String, required: true },
  banners: { type: Array, required: true },
  originprice: { type: Number, required: true },
  sales: { type: Number, required: true },
  stock: { type: Number, required: true },
  desc: { type: String, required: true },
  issale: { type: Number, required: true },
  isrecommend: { type: Number, required: true },
  discount: { type: Number, required: true },
  isseckill: { type: Number, required: true },
  img1: { type: String, required: true },
  img2: { type: String, required: true },
  img3: { type: String, required: false }, // 因为 ly 的数据 缺失
  img4: { type: String, required: true },
})

module.exports = mongoose.model('Product', schema)
```



### 3.上传解析excel表格的数据

方式一： input file 上传excel

方式二：直接读取excel表格的数据

```
yarn add node-xlsx

```

```js
const router = require('express').Router()
const xlsx = require('node-xlsx')
const sql = require('./../sql')
const Product = require('./../sql/models/Product')
const utils = require('./../utils')
router.get('/uploadProduct', (req, res, next) => {
  // xlsx.parse() [{ name: '', data: []}, {name: '', data:[]}]
  const originData = xlsx.parse(__dirname + '/pro.xlsx')[0].data // [[字段名], [数据], [数据], [数据]]
  const insertData = []
  originData.forEach((item, index) => {
    if (index !== 0) {
      // const banners = []
      // banners.push(item[12])
      // banners.push(item[13])
      // banners.push(item[14])
      // banners.push(item[15])
      insertData.push({
        "proid": "pro_" + utils.getUuid(),
        "category": item[0],
        "brand": item[1],
        "proname": item[2],
        // "banners": banners, // 重组数据，也可以按照 ， 分割item[3]
        "banners": item[3].split(','), // 重组数据，也可以按照 ， 分割item[3]
        "originprice": item[4],
        "sales": item[5],
        "stock": item[6],
        "desc": item[7],
        "issale": item[8],
        "isrecommend": item[9],
        "discount": item[10],
        "isseckill": item[11],
        "img1": item[12],
        "img2": item[13],
        "img3": item[14],
        "img4": item[15]
      })
    }
  })
  sql.insert(Product, insertData).then(() => {
    res.status(200).send({
      code: 200,
      message: '上传数据'
    })
  })
  // res.send(insertData)
})

module.exports = router
```

每一次的刷新，都会插入新的数据

### 4.先清空数据再插入

```js
const router = require('express').Router()
const xlsx = require('node-xlsx')
const sql = require('./../sql')
const Product = require('./../sql/models/Product')
const utils = require('./../utils')
router.get('/uploadProduct', (req, res, next) => {
  // 清空数据
  sql.delete(Product, {}, 1).then(() => {
    // 导入数据
    const originData = xlsx.parse(__dirname + '/pro.xlsx')[0].data // [[字段名], [数据], [数据], [数据]]
    const insertData = []
    originData.forEach((item, index) => {
      if (index !== 0) {
        insertData.push({
          "proid": "pro_" + utils.getUuid(),
          "category": item[0],
          "brand": item[1],
          "proname": item[2],
          "banners": item[3].split(','), // 重组数据，也可以按照 ， 分割item[3]
          "originprice": item[4],
          "sales": item[5],
          "stock": item[6],
          "desc": item[7],
          "issale": item[8],
          "isrecommend": item[9],
          "discount": item[10],
          "isseckill": item[11],
          "img1": item[12],
          "img2": item[13],
          "img3": item[14],
          "img4": item[15]
        })
      }
    })
    sql.insert(Product, insertData).then(() => {
      res.status(200).send({
        code: 200,
        message: '上传数据'
      })
    })
  })
})

module.exports = router
```

**思考：导入数据之前提示用户先导出数据作为备份** --- 没有数据时直接导入，有数据时提示



### ~~5.导出数据的功能 -----  经测试该模块已退休，请使用6~~

只导出excel的表头

api/pro.js

```js

const nodeExcel = require('excel-export')

router.get('/exportProduct', (req, res, next) => {
  // 创建Excel表格的对象
  const conf = {}
  // conf.stylesXmlFile = "styles.xml";
  conf.name = "mysheet"; // 表格的名称
  conf.cols = [ // 表头
    { caption: '分类', type: String },
    { caption: '品牌', type: String },
    { caption: '商品名称', type: String },
    { caption: '轮播图', type: Array },
    { caption: '原价', type: Number },
    { caption: '销量', type: Number },
    { caption: '库存', type: Number},
    { caption: '描述', type: String },
    { caption: '上架状态', type: Number },
    { caption: '是否推荐', type: Number },
    { caption: '折扣', type: Number },
    { caption: '是否秒杀', type: Number },
    { caption: '图片1', type: String },
    { caption: '图片2', type: String },
    { caption: '图片3', type: String },
    { caption: '图片4', type: String },
  ]
  conf.rows = [ // 数据
  ]

  var result = nodeExcel.execute(conf);
  res.setHeader('Content-Type', 'application/vnd.openxmlformats');
  res.setHeader("Content-Disposition", "attachment; filename=" + "productlist.xlsx"); // 导出的文件名不支持中文
  res.end(result, 'binary');
})

module.exports = router
```

### 6.导出数据

```
yarn add urlencode
```

```js
const router = require('express').Router()
const xlsx = require('node-xlsx')
// const nodeExcel = require('excel-export')
const urlencode = require('urlencode')
const sql = require('./../sql')
const Product = require('./../sql/models/Product')
const utils = require('./../utils')
router.get('/uploadProduct', (req, res, next) => {
  // 清空数据
  sql.delete(Product, {}, 1).then(() => {
    // 导入数据
    const originData = xlsx.parse(__dirname + '/pro.xlsx')[0].data // [[字段名], [数据], [数据], [数据]]
    const insertData = []
    originData.forEach((item, index) => {
      if (index !== 0) {
        insertData.push({
          "proid": "pro_" + utils.getUuid(),
          "category": item[0],
          "brand": item[1],
          "proname": item[2],
          "banners": item[3].split(','), // 重组数据，也可以按照 ， 分割item[3]
          "originprice": item[4],
          "sales": item[5],
          "stock": item[6],
          "desc": item[7],
          "issale": item[8],
          "isrecommend": item[9],
          "discount": item[10],
          "isseckill": item[11],
          "img1": item[12],
          "img2": item[13],
          "img3": item[14],
          "img4": item[15]
        })
      }
    })
    sql.insert(Product, insertData).then(() => {
      res.status(200).send({
        code: 200,
        message: '上传数据'
      })
    })
  })
})

router.get('/exportProduct', (req, res, next) => {
  // 请求数据库的数据
  sql.find(Product, {}, { _id: 0, __v: 0, proid: 0 }).then(data => {
    const rowsarr = []
    data.forEach(item => {
      const row = []
      row[0] = item.category
      row[1] = item.brand
      row[2] = item.proname
      row[3] = item.banners
      row[4] = item.originprice
      row[5] = item.sales
      row[6] = item.stock
      row[7] = item.desc
      row[8] = item.issale
      row[9] = item.isrecommend
      row[10] = item.discount
      row[11] = item.isseckill
      row[12] = item.img1
      row[13] = item.img2
      row[14] = item.img3
      row[15] = item.img4
      rowsarr.push(row)
    })
    rowsarr.unshift([ // 添加表头
      "分类", 
      "品牌",
      "商品名称",
      "轮播图",
      "原价",
      "销量",
      "库存",
      "描述",
      "上架状态",
      "是否推荐",
      "折扣",
      "是否秒杀",
      "图1",
      "图2",
      "图3",
      "图4",
    ])
    var buffer = xlsx.build([{name: "aaa.xlsx", data: rowsarr}]);
    // console.log(buffer.toString())
    // res.send(buffer)
    res.setHeader('Content-Type', 'application/octet-stream');
     // ctx.request.headers['user-agent']
    let name = urlencode('商品列表_' + (+new Date()) + '.xlsx', "utf-8");
    res.setHeader("Content-Disposition", "attachment; filename* = UTF-8''"+name);    
    // ctx.set("Content-Disposition", "attachment; filename="+ (+new Date()) + '.xlsx');
    res.send(buffer)
  })
  
})


module.exports = router
```

### 7.选择导出

表格选中可以得到比如 bannerid 组成的数组，传输到后端，后端接受数组，遍历数组，查询数据，使用promise.all 组合数据，导出数据



### 8.获取商品信息接口

分页查询 ---- 为了提升页面的运行速度 --- 数据量非常大

获取所有的数据的集合的长度。----  方便 做分页的提示 

Sql/index.js

```js
module.exports = {
  ...,
  count (CollectionName) {
    return new Promise(resolve => {
      // count() 在某些 版本 不可用
      // countDocuments() ok
      // estimatedDocumentCount() ok
      CollectionName.find().estimatedDocumentCount((err, len) => {
        if (err) throw err
        resolve(len)
      })
    })
  }
}
```

Api/pro.js

```js
/**
 * @api {get} /api/pro/list 获取商品的列表
 * @apiName GetProList
 * @apiGroup Pro
 *
 * @apiParam { Number } count 页码，默认值为1
 * @apiParam {Number } limitNum 每页显示个数，默认值为10
 * 
 * @apiSuccess {String} code 状态码.
 * @apiSuccess {String} message  描述
 * @apiSuccess {Number} total  商品的总数量
 * @apiSuccess {String} data  数据
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": "200",
 *       "message": "获取商品的列表",
 *       "total": len,
 *       "data": []
 *     }
 * @apiSampleRequest /api/pro/list
 */
router.get('/list', (req, res, next) => {
  let { count, limitNum } = req.query
  count *= 1
  limitNum *= 1
  sql.paging(Product, {}, { _id:0, __v: 0}, count, limitNum).then(data => {
    sql.count(Product).then(len => {
      res.status(200).send({
        code: '200',
        message: '获取商品的列表',
        total: len,
        data
      })
    })
  })
})
```

生成接口文档  `apidoc -i api/ -o public/apidoc` 测试

参照 一.17.3渲染数据

### 9.修改秒杀的状态接口

Api/pro.js

```js
/**
 * @api {get} /api/pro/updateIsSeckill 修改商品是否参加秒杀
 * @apiName GetProUpdateIsSeckill
 * @apiGroup Pro
 *
 * @apiParam { string } proid 商品id
 * @apiParam { boolean } isseckill 是否秒杀
 * 
 * @apiSuccess {String} code 状态码.
 * @apiSuccess {String} message  描述
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": "200",
 *       "message": "修改商品是否参加秒杀",
 *     }
 * @apiSampleRequest /api/pro/updateIsSeckill
 */
router.get('/updateIsSeckill', (req, res, next) => {
  let { isseckill, proid } = req.query
  isseckill = isseckill === 'true' ? 1 : 0
  sql.update(Product, { proid }, { $set: { isseckill }}).then(() => {
    res.status(200).send({
      code: '200',
      message: '修改商品是否参加秒杀'
    })
  })
})
```

一.17.6调用修改即可

### 10.获取商品的分类

Sql/index.js

```js

module.exports = {
  distinct(CollectionName, type) {
    return new Promise((resolve) => {
      CollectionName.distinct(type).exec((err, data) => {
        if (err) throw err
        resolve(data)
      })
    })
  }
}
```

Api/pro.js

```js
/**
 * @api {get} /api/pro/getCategory 获取商品的分类
 * @apiName GetCategory
 * @apiGroup Pro

 * @apiSuccess {String} code 状态码.
 * @apiSuccess {String} message  描述
 * @apiSuccess {String} data  数据
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": "200",
 *       "message": "获取商品的分类",
 *        data
 *     }
 * @apiSampleRequest /api/pro/getCategory
 */
router.get('/getCategory', (req, res, next) => {
  sql.distinct(Product, 'category').then(data => {
    res.status(200).send({
      code: '200',
      message: '获取商品的分类',
      data
    })
  })
})
```

### 11.筛选接口

```js
/**
 * @api {post} /api/pro/searchPro 筛选商品
 * @apiName PostSearchPro
 * @apiGroup Pro
 *
 * @apiParam { string } category 分类
 * @apiParam {string } search 搜索关键词
 * 
 * @apiSuccess {String} code 状态码.
 * @apiSuccess {String} message  描述
 * @apiSuccess {String} data  数据
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": "200",
 *       "message": "筛选商品",
 *       "data": []
 *     }
 * @apiSampleRequest /api/pro/searchPro
 */
router.post('/searchPro', (req, res, next) => {
  let { category, search } = req.body
  let whereData = {}
  // if (category === '') {
  //   if (search === '') {
  //     whereData = {}
  //   } else {
  //     whereData = {
  //       proname: new RegExp(search)
  //     }
  //   }
  // } else {
  //   if (search === '') {
  //     whereData = {
  //       category
  //     }
  //   } else {
  //     whereData = {
  //       category,
  //       proname: new RegExp(search)
  //     }
  //   }
  // }
  category === '' ? search === '' ? whereData = {} : whereData.proname = new RegExp(search) :
  search === '' ? whereData.category = category : whereData = {category,
    proname: new RegExp(search)}
  sql.find(Product, whereData, { _id: 0, __v: 0 }).then(data => {
    res.status(200).send({
      code: '200',
      message: '筛选商品',
      data
    })
  })
})
```

参照一.17.11 渲染数据

## 6.改造一下是否秒杀接口 --- 和是否推荐通用一个接口

替换二.5.9的接口

```js
/**
 * @api {post} /api/pro/updateFlag 修改商品是否推荐或者秒杀
 * @apiName PostUpdateFlag
 * @apiGroup Pro
 *
 * @apiParam { string } proid 产品的id
 * @apiParam {string } type 修改的数据 （isseckill, isrecommend）
 * @apiParam { string } flag  表示（true 选中 false 未选中）
 * 
 * @apiSuccess {String} code 状态码.
 * @apiSuccess {String} message  描述
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": "200",
 *       "message": "修改商品是否推荐或者秒杀",
 *     }
 * @apiSampleRequest /api/pro/updateFlag
 */
router.post('/updateFlag', (req, res, next) => {
  let { proid, type, flag } = req.body
  flag = flag ? 1 : 0
  let updateData = {}
  updateData[type] = flag
  sql.update(Product, { proid }, { $set: updateData }).then(() => {
    res.status(200).send({
      code: '200',
      message: '修改商品是否' + (type === 'isseckill' ? '秒杀' : '推荐')
    })
  })
})
```

## 7.获取秒杀和推荐的接口

```js
/**
 * @api {post} /api/pro/showdata 获取秒杀或者推荐的列表
 * @apiName PostShowData
 * @apiGroup Pro
 *
 * @apiParam {string } type  获取数据的依据 （isseckill, isrecommend）
 * @apiParam { number } flag  表示（1 选中 0 未选中）
 * 
 * @apiSuccess {String} code 状态码.
 * @apiSuccess {String} message  描述
 * @apiSuccess {String} data  数据
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": "200",
 *       "message": "获取秒杀或者推荐的列表",
 *       "data": []
 *     }
 * @apiSampleRequest /api/pro/showdata
 */
router.post('/showdata', (req, res, next) => {
  let { type, flag } = req.body
  let whereData = {}
  whereData[type] = flag
  sql.find(Product, whereData, { _id: 0, __v: 0}).then(data => {
    res.status(200).send({
      code: '200',
      message: '获取商品的列表',
      data
    })
  })
})
```

参照一.18

## 8.封装发送短线验证码接口

阿里云 - 短信服务

`yarn add @alicloud/pop-core`

Utils/index.js

```js
const uuid = require('short-uuid')
const Core = require('@alicloud/pop-core');

module.exports = {
  getUuid () {
    return uuid.generate()
  },
  sendCode (PhoneNumbers, code) {
    var client = new Core({
      accessKeyId: 'LTAI4GExoXbNkoRZqSFJYkWz',
      accessKeySecret: 'i8jrNC0C05b4UnCnXTHl4qsn3qcPI4',
      endpoint: 'https://dysmsapi.aliyuncs.com',
      apiVersion: '2017-05-25'
    });

    var params = {
      "RegionId": "cn-hangzhou",
      "PhoneNumbers": PhoneNumbers,
      "SignName": "大勋说",
      "TemplateCode": "SMS_171853298",
      "TemplateParam": "{ code: "+ code +" }"
    }

    var requestOption = {
      method: 'POST'
    };

    return new Promise((resolve, reject) => {
      client.request('SendSms', params, requestOption).then((result) => {
        console.log(JSON.stringify(result));
        resolve()
      }, (ex) => {
        console.log(ex);
        reject()
      })
    })

  }
}
```

utils/test.js中测试

```js

const utils = require('./index')
utils.sendCode('15979818130', Math.floor(Math.random() * 90000) + 10000)
```

## 9.创建用户的集合

```js
const mongoose = require('./../db')
const Schema = mongoose.Schema

const schema = new Schema({
  userid: { type: String, required: true },
  username: { type: String, required: false },
  nickname: { type: String, required: true },
  tel: { type: String, required: true },
  telCode: { type: String, required: true },
  password: { type: String, required: false },
  email: { type: String, required: false },
  sex: { type: Number, required: true },
  birthday: { type: String, required: false },
  avatar: { type: String, required: true }, // 头像
  regtime: { type: Date, required: true },
  flag: { type: Number, required: true }
})

module.exports = mongoose.model('User', schema)
```

### 1.验证手机号

```js
const router = require('express').Router()
const sql = require('./../sql')
const User = require('./../sql/models/User')
/**
 * @api {post} /api/users/validateTel 验证手机号是否可以继续注册
 * @apiName PostValidateTel
 * @apiGroup User
 * 
 * @apiParam {string} tel 手机号
 * 
 * @apiSuccess {String} code 状态码.
 * @apiSuccess {String} message  描述
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": "200" || "10001",
 *       "message": "可以继续注册" || "该用户已注册",
 *     }
 * @apiSampleRequest /api/users/validateTel
 */
router.post('/validateTel', (req, res, next) => {
  const { tel } = req.body
  sql.find(User, { tel }, { _id: 0, __v: 0 }).then(data1 => {
    if (data1.length === 0) { // 没有查找到该用户
      res.status(200).send({
        code: '200',
        message: '可以继续注册'
      })
    } else {
      if (data1[0].password === '') { // 没有设置密码 - 未完成注册
        res.status(200).send({
          code: '200',
          message: '可以继续注册'
        })
      } else { // 该用户已注册
        res.status(200).send({
          code: '10001',
          message: '该用户已注册'
        })
      }
    }
  })
})

module.exports = router
```

### 2.发送验证码

```js
const uuid = require('short-uuid')
const Core = require('@alicloud/pop-core');
const randomstring = require('randomstring') // yarn add randomstring

module.exports = {
  getUuid () {
  },
  randomCode () {
    return Math.floor(Math.random() * 90000 + 10000)
  },
  randomNickname () {
    return 'hg_' + randomstring.generate(10)
  },
  sendCode (PhoneNumbers, code) {
  }
}
```



```js
/**
 * @api {post} /api/users/sendCode 发送短信验证码
 * @apiName PostSendCode
 * @apiGroup User
 * 
 * @apiParam {string} tel 手机号
 * 
 * @apiSuccess {String} code 状态码.
 * @apiSuccess {String} message  描述
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": "200" ,
 *       "message": "发送短信验证码"
 *     }
 * @apiSampleRequest /api/users/sendCode
 */
router.post('/sendCode', (req, res, next) => {
  const { tel } = req.body
  // 严格来讲，还需要验证一次手机号的可用性
  const telcode = utils.randomCode()
  utils.sendCode(tel, telcode)
    .then(() => {
      // 发送短信验证码成功  ----  插入数据
      // 如果手机号不存在  ---- 插入数据
      // 如果手机号存在  密码不存在   --- 更新数据
      sql.find(User, { tel }, { _id: 0, __v: 0 }).then(data1 => {
        if (data1.length === 0) {// 如果手机号不存在  ---- 插入数据
          const insertData = {
            userid: 'user_' + utils.getUuid(),
            username: '',
            nickname: utils.randomNickname(),
            tel,
            telcode,
            password: '',
            email: '',
            sex: -1,
            birthday: '',
            avatar: 'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=1488280602,1416445856&fm=26&gp=0.jpg',
            regtime: new Date(),
            flag: 1
          }
          console.log(insertData)
          sql.insert(User, insertData).then(() => [
            res.status(200).send({
              code: '200',
              message: '短信验证码已发送'
            })
          ])
        } else {
          if (data1.password === '') {// 如果手机号存在  密码不存在   --- 更新数据
            sql.update(User, { tel }, { $set: { telcode }}).then(() => {
              res.status(200).send({
                code: '200',
                message: '短信验证码已发送'
              })
            })
          }
        }
      })
    })
})
```

### 3.验证验证码

```js
/**
 * @api {post} /api/users/validateTelCode 验证短信验证码
 * @apiName PostValidateTelCode
 * @apiGroup User
 * 
 * @apiParam {string} tel 手机号
 * @apiParam {string} telcode 验证码
 * 
 * @apiSuccess {String} code 状态码.
 * @apiSuccess {String} message  描述
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": "200" || "10002",
 *       "message": "发送短信验证码" || "验证码输入错误"
 *     }
 * @apiSampleRequest /api/users/validateTelCode
 */
router.post('/validateTelCode', (req, res, next) => {
  const { tel, telcode } = req.body
  sql.find(User, { tel, telcode }, { _id: 0, __v: 0 }).then(data => {
    if (data.length === 0) { // 验证码错误
      res.status(200).send({
        code: '10002',
        message: '验证码输入错误'
      })
    } else { // 验证码通过
      res.status(200).send({
        code: '200',
        message: '验证码验证通过'
      })
    }
  })
 })
```



### 4.注册

```js

const bcrypt = require('bcrypt')
module.exports = {
  getUuid () {
  },
  randomCode () {
  },
  randomNickname () {
  },
  encryption (password) {
    const saltRounds = 10
    return bcrypt.hash(password, saltRounds)
  },
  
  sendCode (PhoneNumbers, code) {
  }
}
```



```js
/**
 * @api {post} /api/users/register 注册
 * @apiName PostRegister
 * @apiGroup User
 * 
 * @apiParam {string} tel 手机号
 * @apiParam {string} password 密码
 * 
 * @apiSuccess {String} code 状态码.
 * @apiSuccess {String} message  描述
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": "200",
 *       "message": "注册成功" 
 *     }
 * @apiSampleRequest /api/users/register
 */
router.post('/register', (req, res, next) => {
  let { tel, password } = req.body
  // 密码一般都要加密
  utils.encryption(password).then(result => {
    sql.update(User, { tel }, { $set: { password: result }}).then(() => {
      res.status(200).send({
        code: '200',
        message: '注册成功'
      })
    })
  })
})
```

## 10.获取用户信息的接口

```js

/**
 * @api {get} /api/users/list 用户列表
 * @apiName GetUserList
 * @apiGroup User
 * 
 * 
 * @apiSuccess {String} code 状态码.
 * @apiSuccess {String} message  描述
 * @apiSuccess {Array} data  数据
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": "200",
 *       "message": "用户列表",
 *       "data":[]
 *     }
 * @apiSampleRequest /api/users/list
 */
router.get('/list', (req, res, next) => {
  sql.find(User, { }, { _id:0, __v: 0, password: 0}).then(data => {
    res.status(200).send({
      code: '200',
      message: '用户列表',
      data
    })
  })
})
```

查看一.20



## 11.管理员登录

### 1. 设计管理员数据库集合

```js
const mongoose = require('./../db')
const Schema = mongoose.Schema

const schema = new Schema({
  adminid: { type: String, required: true },
  adminname: { type: String, required: true },
  password: { type: String, required: true },
  // 2超级管理员  1 普通管理员
  // ['super', 'normal']    ['normal']
  role: { type: Number, required: true }
})

module.exports = mongoose.model('Admin', schema)
```

### 2.插入默认的超级管理员账户

Utils/test.js

```js
const bcrypt = require('bcrypt')
const Admin = require('./../sql/models/Admin')
const sql = require('./../sql')
const utils = require('./index')
const saltRounds = 10

bcrypt.hash('haigou123321', saltRounds, (err, data) => {
  console.log(data)
  sql.insert(Admin, {
    adminid: 'admin_' + utils.getUuid(),
    adminname: 'admin',
    password: data,
    role: 2
  }).then(() => {
    console.log('插入超级管理员成功')
  })
})
```

### 3.实现管理系统登录接口

Api/admin.js

```js
const router = require('express').Router()
const bcrypt = require('bcrypt')
const Admin = require('./../sql/models/Admin')
const sql = require('./../sql')
const utils = require('./../utils/index')

/**
 * @api {Post} /api/admin/login 管理系统登录
 * @apiName PostCategory
 * @apiGroup Admin
 * 
 * @apiParam { string } adminname
 *  管理员账号
 * @apiParam { string } password 密码
 * 
 * @apiSuccess {String} code 状态码.
 * @apiSuccess {String} message  描述
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": "200" || '10003',
 *       "message": "登录成功" || 输入信息有误,
 *     }
 * @apiSampleRequest /api/admin/login
 */
router.post('/login', (req, res, next) => {
  let { adminname, password } = req.body
  sql.find(Admin, { adminname }, { _id: 0, __v: 0 }).then(data => {
    if (data.length !== 0) {
      bcrypt.compare(password, data[0].password).then(result => {
        if (result) { // 密码验证通过 --- 通过
          res.status(200).send({
            code: '200',
            message: '登录成功'
          })
        } else {
          res.status(200).send({
            code: '10004',
            message: '密码错误'
          })
        }
      })
    } else {
      res.status(200).send({
        code: '10003',
        message: '没有该管理员账号'
      })
    }
  })
})

module.exports = router
```

参照一.21.3

### 4.为登录添加token

```
yarn add jsonwebtoken
```

Utils/index.js

```js
const jwt = require('jsonwebtoken')
module.exports = {
  createToken (payload, secret, options) {
    return jwt.sign(payload, secret, options)
  }
}
```

Api/admin.js

```js
const router = require('express').Router()
const bcrypt = require('bcrypt')
const Admin = require('./../sql/models/Admin')
const sql = require('./../sql')
const utils = require('./../utils/index')

/**
 * @api {Post} /api/admin/login 管理系统登录
 * @apiName PostCategory
 * @apiGroup Admin
 * 
 * @apiParam { string } adminname
 *  管理员账号
 * @apiParam { string } password 密码
 * 
 * @apiSuccess {String} code 状态码.
 * @apiSuccess {String} message  描述
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": "200" || '10003',
 *       "message": "登录成功" || 输入信息有误,
 *     }
 * @apiSampleRequest /api/admin/login
 */
router.post('/login', (req, res, next) => {
  let { adminname, password } = req.body
  sql.find(Admin, { adminname }, { _id: 0, __v: 0 }).then(data => {
    if (data.length !== 0) {
      bcrypt.compare(password, data[0].password).then(result => {
        if (result) { // 密码验证通过 --- 通过
          // **********************
          const token = utils.createToken({ adminname }, '嗨购-admin', {
            expiresIn: 30 // 单位为 秒
          })
          res.status(200).send({
            code: '200',
            message: '登录成功',
            data: {
              adminname,
              token,
              role: data[0].role
            }
          })
        } else {
          res.status(200).send({
            code: '10004',
            message: '密码错误'
          })
        }
      })
    } else {
      res.status(200).send({
        code: '10003',
        message: '没有改管理员账号'
      })
    }
  })
})

module.exports = router
```

### 5.验证token

Utils/index.js

```js

module.exports = {
  createToken (payload, secret, options) {
  },
  validateToken (token, secret) {
    return new Promise((resolve, reject) => {
      jwt.verify(token, secret, (err, decoded) => {
        if (err) {
          reject()
        } else {
          resolve(decoded)
        }
      })
    })
  },
  getUuid () {
  },
  randomCode () {
  },
  randomNickname () {
  },
  encryption (password) {
  },
  
  sendCode (PhoneNumbers, code) {
  }
}
```

App.js

```js
app.use(express.static(path.join(__dirname, 'public')));

// 针对 以 api为开头的所有的接口的一个设置
app.all('/api/*', (req, res, next) => {
  if (req.url !== '/api/admin/login') {
    const token = req.headers.token || req.query.token || req.body.token
    if (token) {
      utils.validateToken(token, '嗨购-admin')
        .then((decoded) => {
          // req.decoded = decoded
          next()
        })
        .catch(() => {
          res.status(200).send({
            code: '10119',
            message: '无效的token'
          })
        })
    } else {
      res.status(200).send({
        code: '10119',
        message: '无效的token'
      })
    }
  } else {
    next()
  }
})

// 注册接口
app.use('/api/banner', bannerApi)
app.use('/api/nav', navApi)
app.use('/api/pro', proApi)
app.use('/api/users', userApi)
app.use('/api/admin', adminApi)

```

给已有的接口 添加接口文档的参数。@apiHeader 

```
@apiHeader {string} token 描述
```

先登录，复制，填入token字段，在有效期内请求数据，如果超出有效期，则会显示无效的token

参照一.21.3.4

## 12.添加管理员

### 1.改造数据库集合

```js
const mongoose = require('./../db')
const Schema = mongoose.Schema

const schema = new Schema({
  adminid: { type: String, required: true },
  adminname: { type: String, required: true },
  password: { type: String, required: true },
  // 2超级管理员  1 普通管理员
  // ['super', 'normal']    ['normal']
  role: { type: Number, required: true },
  checkedKeys: { type: Array, required: true }
})

module.exports = mongoose.model('Admin', schema)
```

### 2.添加管理员的数据

```js
/**
 * @api {Post} /api/admin/add 添加管理员
 * @apiName PostAddAdmin
 * @apiGroup Admin
 * 
 * @apiParam { string } adminname
 * @apiParam { string } password
 * @apiParam { string } role
 * @apiParam { string } checkedKeys
 * 
 * @apiHeader { string } token
 * 
 * @apiSuccess {String} code 状态码.
 * @apiSuccess {String} message  描述
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": "200" || '10004',
 *       "message": "添加管理员成功" || 该管理员已存在,
 *     }
 * @apiSampleRequest /api/admin/add
 */
router.post('/add', (req, res, next) => {
  let insertData = req.body
  let password = insertData.password
  insertData.adminid = 'admin_' + utils.getUuid()
  utils.encryption(password).then((result) => { // 密码加密
    insertData.password = result
    // 判断有没有该管理员账号
    sql.find(Admin, { adminname: insertData.adminname}, {_id: 0, __v: 0}).then(data => {
      if (data.length === 0) { // 没有当前想要添加的管理员
        sql.insert(Admin, insertData).then(() => {
          res.status(200).send({
            code: '200',
            message: '添加管理员成功'
          })
        })
      } else { // 已有该管理员
        res.status(200).send({
          code: '10004',
          message: '该管理员已存在'
        })
      }
    })
  })
  
})
```

参照一.22.3

## 13.获取管理员的权限列表

```js
// api/admin.js
router.get('/detail', (req, res, next) => {
  const { adminname } = req.query
  sql.find(Admin, { adminname }, { _id: 0, __v: 0, password: 0}).then(data => {
    res.status(200).send({
      code: '200',
      message: '获取管理员信息',
      data
    })
  })
})
// 查询管理员列表
router.get('/list', (req, res, next) => {
  sql.find(Admin, {}, { _id: 0, __v: 0, password: 0}).then(data => {
    res.status(200).send({
      code: '200',
      message: '获取管理员列表信息',
      data
    })
  })
})
// 更新管理员权限
router.post('/update', (req, res, next) => {
  let updateData = req.body
  sql.update(Admin, { adminname: updateData.adminname }, { $set: updateData}).then(() => {
    res.status(200).send({
      code: '200',
      message: '修改管理员权限成功'
    })
  })
})
```

参照一.22.4





# 三.如何添加接口文档

## 1.安装模块

`yarn add apidoc --global`

## 2.配置package.json

```json
"apidoc": {
  "title": "深圳好程序员4期 admin pro",
  "url" : "http://localhost:3001/"
}
```

## 3.编写接口文档

文档接口前配置如下的类似代码

```js
/**
 * @api {get} /api/nav/navcategory 获取导航分类
 * @apiName GetNavCategory
 * @apiGroup Nav
 *
 * @apiSuccess {String} code 状态码.
 * @apiSuccess {String} message  描述
 * @apiSuccess {String} data  数据
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": "200",
 *       "message": "导航的分类数据",
 *       "data": []
 *     }
 * @apiSampleRequest /api/nav/navcategory
 */
```

```js
var router = require('express').Router()
var sql = require('./../sql')
var Navcategory = require('./../sql/models/Navcategory')
var Navlist = require('./../sql/models/Navlist')
var utils = require('./../utils')
/**
 * @api {get} /api/nav/navcategory 获取导航分类
 * @apiName GetNavCategory
 * @apiGroup Nav
 *
 * @apiSuccess {String} code 状态码.
 * @apiSuccess {String} message  描述
 * @apiSuccess {String} data  数据
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": "200",
 *       "message": "导航的分类数据",
 *       "data": []
 *     }
 * @apiSampleRequest /api/nav/navcategory
 */
router.get('/navcategory', (req, res, next) => {
  sql.find(Navcategory, {}, { _id: 0, __v: 0 }).then(data => {
    res.status('200').send({
      code: '200',
      message: '导航的分类数据',
      data
    })
  })
})
/**
 * @api {get} /api/nav/navlist 获取导航列表
 * @apiName GetNavList
 * @apiGroup Nav
 *
 * @apiSuccess {String} code 状态码.
 * @apiSuccess {String} message  描述
 * @apiSuccess {String} data  数据
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": "200",
 *       "message": "导航列表",
 *       "data": []
 *     }
 * @apiSampleRequest /api/nav/navlist
 */
router.get('/navlist', (req, res, next) => {
  sql.find(Navlist, {}, { _id: 0, __v: 0 }).then(data => {
    res.status('200').send({
      code: '200',
      message: '导航列表',
      data
    })
  })
})

module.exports = router
```

## 4.生成在线接口文档

`apidoc -i api/ -o public/apidoc`

通过 http://localhost:3001/apidoc 访问当前的接口文档



# 四.补充功能

## 1.git多人的操作

### 1.基本介绍

工作区 -->暂存区--> 版本库 --> 远程版本库 

工作区：文件的增加，修改，删除操作都在工作区执行 

暂存区：文件修改后且add后，到暂存区 

版本库：文件commit后，到版本库 

远程仓库：本地版本库的文件push到远程仓库，从远程仓库pull/fetch文件到本地 



HEAD保存的是最后一次提交点（当前），指向当前工作的分支 

HEAD^上一个版本 

HEAD^^上上个版本 

HEAD~10上10个版本 

### 2.配置用户名及邮箱 

`git config --global user.name 'wudaxun' ` 使用域账号

 `git config --global user.email '522864637@qq.com' `使用公司邮箱

### 3.配置ssh

为了避免每次提交代码都需要 输入账户名和密码

运行以下命令

`ssh-keygen -t rsa -C '522864637@qq.com'` 一般建议，远程仓库的 绑定的邮箱账号

然后一路按回车

![image-20201123170401525](proimg/ssh.png)

可以使用任何类型的软件打开该文件，推荐可以使用记事本， 复制其中的代码

打开gitee，找到设置 - 安全设置 - ssh公钥 - 填写信息即可

![image-20201123170905589](proimg/ssh-公钥.png)

### 4.基本的操作
1.查看配置信息
`git config -l`

2.初始化仓库（本地仓库） 
`git init`

3.克隆远程代码
`git clone url`

3.拉取远程代码
`git pull` 相当于 git fetch 和git merge 

5.4.从其他分支合并代码到当前分支
`git merge branch-name`

4.比较文件
`git diff [filename]`

4.添加文件
`git add [.|filename]`

5.提交文件
`git commit -[a]m ‘备注信息’`

6.查看仓库状态git status
`git status`
```
On branch master 处于master分支Initial commit 初始化提交Untracked files: 未跟踪的文件
  (use "git add <file>..." to include in what will be committed) 使用add命令来添加文件
nothing added to commit but untracked files present (use "git add" to track) 没有提交但未添加文件
（用“git add”追踪）
```

7.查看日志
`git log`
```
commit cbc220915fa1039e475b7865cc05bc42c6a5e826 提交的编号Author: huz02 <huz02@vanke.com> 作 者
Date: Tue Nov 28 14:23:10 2017 +0800 提交日期
     add test.txt 提交的内容（添加test.txt文件） git log --pretty=oneline 格式化查看日志
```

8.查看某个提交修改的内容
`git show commitID`

12.查看某个文件修改记录
`git log -p filename `

### 5.分支管理
查看分支
`git branch -a` // all 全部分支
`git branch -r` // remote 远程分支
`git branch -l` // local 本地分支

创建分支
`git branch branch_name`

切换分支
`git checkout branch_name`

创建分支并切换到分支
`git checkout -b new_branch` （默认为空，从master分支拉取代码）

删除分支，如果正在当前分支，则不能删除
`git branch -d branch_name

删除远程分支
`git push origin --delete branch_name`

合并自分支代码，先切换到master分支
`git checkout master` 切换到主分支

`git merge sub_branch` 合并sub分支代码到主分支

`git merge sub`
```
Updating 5ce93f7..b59cc48 Fast-forward

 hahaha.txt | 17 ++---------------

 1 file changed, 2 insertions(+), 15 deletions(-)

注意：Fast-forward   表示快速合并（将master的指针指向brh），不会产生新的commit  id，只是利用了子分支 的commit id继续操作

注意：如果在子分支修改了代码，未commit就切换到master，master也会显示文件被修改了   推送分支代码到远程分支
```
git push origin master
git push origin sub_branch 推送sub分支

推送本地分支到远程，如果远程分支没有，则使用下面命令
`git push --set-upstream origin invoice`

### 5、代码回退

1. 修改本地文件，还未add操作，注意：所有修改将丢失
 `git checkout -- filename`

2. 添加新文件且执行了add，想返回未add状态，保留修改的把内容
`git reset HEAD filename`

3. 如果文件被删除后，想要恢复源文件
`git checkout HEAD -- filename`

4. 文件执行了commit后，想回到上一个版本（log会被删除）
`git reset --soft commit_id` 回退到制定版本，回到add后，commit前的代码
`git reset commit_id` 回退到指定版本修改后，回到修改后，add前的代码（默认--mixed，可省略）
`git reset --hard commit_id` 回退到指定版本，回到修改前的代码

### 6.如何团队操作

**组长第一次**

```nginx
// 1.创建一个项目  -- gp-4-git-course  - 初始化项目git
cd gp-4-git-course
git init

// 2.添加一个文件  test.md.  ---   git course
git add .
git commit -m '初始化项目'
```

创建远程仓库 - gp4-test (分为git协议和https协议，如果配置过ssh，默认就是选中ssh)

```nginx
git remote add origin git@gitee.com:daxunxun/gp4-test.git

git push -u origin master  // -u 只有在第一次提交代码时才会添加
```

给当前的项目添加项目组成员

当前项目 - 管理 - 仓库成员管理 - 添加仓库成员 - 建议全部添加为 管理员，复制链接，发给队友，队友同意之后即可加入

**组员第一次**

```nginx
// 1.同意加入项目组
// 2.找到自己的代码管理文件夹
// 3.克隆远程的仓库   选择ssh 的克隆地址
git clone git@gitee.com:daxunxun/gp4-test.git

// 4.git默认是master分支，开发人员不可以在master分支直接开发

git checkout -b 'adev'
// 5.编写代码
// 6.提交代码
git add .
git commit -m 'a 开发的*** 功能实现'
// 7.提交到远程的adev分支
git push origin adev
// 8.第一天的活结束
```

如果组长也是要写代码，参考组员的操作

```nginx
git checkout -b 'testdev'
// 编写代码
// 提交代码
git add .
git commit -m 'test 开发的*** 功能实现'
git push origin testdev
```

**组长负责合并代码**

```nginx
// 一定要切换到master分支
git checkout master   
// 合并 自己分支代码----- 
git pull origin testdev 
git add .
git commit -m '合并testdev分支代码'

// 合并其他分支代码
git pull origin adev
// 此时可能会有代码的冲突问题，建议手动修改冲突
git add .
git commit -m '合并adev分支代码'
git push origin master

// 合并完成，切回自己的分支
git checkout testdev
// 拉取最新的代码
git pull origin master
// 开发 提交 合并 。。。。
```

**组员得知代码合并完成**

```nginx
git pull origin master

// 开发 提交
```

```
vi模式下
输入完成 按esc

输入 :wq!   保存并且退出
:q!  退出
```

