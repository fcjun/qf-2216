# 1.创建项目

```sh
npx create-react-app router-app
cd router-app
cnpm i react-router-dom@5 axios redux react-redux redux-thunk redux-saga immutable redux-immutable prop-types antd-mobile -S
```

> 近期react-router-dom 版本升级，为了稳定，选择 v5版本

配置装饰器语法（但并不是必须）

```
cnpm i @babel/plugin-proposal-decorators customize-cra react-app-rewired -D
```

根目录下创建config-overrides.js

```js
// 参考配置连接：https://www.npmjs.com/package/customize-cra
const {override,addDecoratorsLegacy}=require("customize-cra");

module.exports=override(
    addDecoratorsLegacy(),//配置装饰器模式
);
```

修改package.json运行命令

```js
"scripts": {
  "start": "react-app-rewired start", // update
  "build": "react-app-rewired build", // update
  "test": "react-app-rewired test",  // update
  "eject": "react-scripts eject"
},
```

配置UI库 https://antd-mobile-doc-v2.gitee.io/docs/react/introduce-cn

```js
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.jsx';
import reportWebVitals from './reportWebVitals';
import 'antd-mobile/dist/antd-mobile.css'

ReactDOM.render(
  <React.StrictMode>
  	<App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

```



```
yarn start
```



# 2.整理目录结构

```
src
	- api
	- components
	- router
	- store
	- utils
	- views
	App.jsx
	index.js
	logo.svg
	reportWebVitals.js 性能优化

```

> 移动端项目参考vue项目流程，可以拷贝同等部分，数据请求以及封装的接口
>
> ```js
> // src/utils/request.js
> import axios from 'axios'
> const isDev = process.env.NODE_ENV === 'development'
> 
> const request = axios.create({
>   baseURL: isDev ? 'http://121.89.205.189/api' : 'http://121.89.205.189/api',
>   timeout: 6000
> })
> 
> // 拦截器
> request.interceptors.request.use(config => {
>   return config
> }, err => Promise.reject(err))
> request.interceptors.response.use(response => {
>   return response
> }, err => Promise.reject(err))
> 
> export default request
> 
> ```
>
> 拷贝以前的api文件夹下的内容
>
> ```js
> // src/api/cart.js
> // src/api/cart.js
> import request from './../utils/request'
> 
> // 加入购物车
> export function addCart (parmas) {
>   return request.post('/cart/add', parmas)
> }
> // 获取购物车数据（以用户为单位）
> export function getCartList (parmas) {
>   return request.post('/cart/list', parmas)
> }
> 
> // 更新购物车数量
> export function updateNum (params) {
>   return request.post('/cart/updatenum', params)
> }
> 
> // 更新购物车单条数据选中状态
> export function selectone (params) {
>   return request.post('/cart/selectone', params)
> }
> 
> // 更新购物车所有数据选中状态
> export function selectall (params) {
>   return request.post('/cart/selectall', params)
> }
> 
> // 删除购物车单条数据
> export function remove (params) {
>   return request.post('/cart/remove', params)
> }
> 
> // 删除当前用户的所有的数据
> export function removeall (params) {
>   return request.post('/cart/removeall', params)
> }
> 
> export function getRecommendList (params) {
>   return request.get('/pro/recommendlist', params)
> }
> 
> ```
>
> ```js
> // src/api/detail.js
> import request from '../utils/request'
> 
> export function getProDetail (proid) {
>   return request.get('/pro/detail/' + proid)
> }
> 
> ```
>
> ```js
> // src/api/home.js
> import request from '../utils/request'
> 
> /**
>  * 请求轮播图
>  */
> export function getBannerList () {
>   return request.get('/banner/list')
> }
> /**
>  * 列表数据
>  * @param { Object } params { count, limitNum }
>  */
> export function getProList (params) {
>   return request.get('/pro/list', { params })
> }
> 
> /**
>  * 秒杀数据
>  * @param { Object } params { count, limitNum }
>  */
> export function getSeckillList (params) {
>   return request.get('/pro/seckilllist', { params })
> }
> 
> ```
>
> ```js
> // src/api/user.js
> import request from '../utils/request'
> 
> export function docheckphone (params) {
>   return request.post('/user/docheckphone', params)
> }
> 
> export function dosendmsgcode (params) {
>   return request.post('/user/dosendmsgcode', params)
> }
> 
> export function docheckcode (params) {
>   return request.post('/user/docheckcode', params)
> }
> 
> export function dofinishregister (params) {
>   return request.post('/user/dofinishregister', params)
> }
> 
> export function doLogin (params) {
>   return request.post('/user/login', params)
> }
> 
> ```
>
> > 虽然本接口 不会遇到跨域问题，但是以后可能遇到
> >
> > https://www.html.cn/create-react-app/docs/proxying-api-requests-in-development/
> >
> > * 方式1 `package.json` 中添加 `proxy` 字段
> >
> >   ```js
> >    "proxy": "http://121.89.205.189",
> >    
> >    // baseURL: '/api'
> >   ```
> >
> > * 方式2 手动配置
> >
> >   ```
> >   cnpm install http-proxy-middleware --save
> >   ```
> >
> >   src/setupProxy.js
> >
> >   ```js
> >   const proxy = require('http-proxy-middleware');
> >   
> >   module.exports = function(app) {
> >     app.use(proxy('/api', { target: 'http://121.89.205.189/api' }));
> >   };
> >   
> >   // baseURL: '/api'
> >   ```

# 3.构建主界面

```
cnpm install node-sass@5 --save
```



```jsx
// src/App.jsx
import React from 'react'
import './common.scss'
function App () {
  return (
    <div className="container">
      <div className="box">
        <header className="header"></header>
        <div className="content"></div>
      </div>
      <footer className="footer"></footer>
    </div>
  )
}

export default App
```

```scss
// src/commone.scss
* { padding: 0; margin: 0; list-style: none;}
html, body, #root, .container {
  width: 100%;
  height: 100%;
}
html {
  // font-size 100px // 为了方便计算
  // iphone6 750 * 1334 ==> 375 * 667
  // 100 / 375 * 100
  // * 100 为了好计算
  font-size: 26.666667vw // iphone6
}
@media all and (min-width: 996px) {
  html {
    font-size: 100px
  }
}
body {
  font-size: 14px // 根据设计师给的设计稿的基础字体的大小
}
.container {
  display: flex;
  flex-direction: column;
  .box {
    flex: 1;
    overflow: auto;
    display: flex;
    flex-direction: column;
    .header {
      height: 0.44rem;
      background-color: #f66;
    }
    .content {
      flex: 1;
      overflow: auto;
    }
  }
  .footer {
    height: 0.5rem;
    background-color: #efefef;
  }
}
```

# 4.创建相关页面

```jsx
// src/views/home/Index.jsx
import React, { Component } from 'react';

export default class Index extends Component {
  render() {
    return (
      <div className="box">
        <header className="header">home</header>
        <div className="content">home</div>
      </div>
    );
  }
}

```

```jsx
// src/views/kind/Index.jsx
import React, { Component } from 'react';

export default class Index extends Component {
  render() {
    return (
      <div className="box">
        <header className="header">kind</header>
        <div className="content">kind</div>
      </div>
    );
  }
}
```

```jsx
// src/views/cart/Index.jsx
import React, { Component } from 'react';

export default class Index extends Component {
  render() {
    return (
      <div className="box">
        <header className="header">cart</header>
        <div className="content">cart</div>
      </div>
    );
  }
}

```

```jsx
// src/views/user/Index.jsx
import React, { Component } from 'react';

export default class Index extends Component {
  render() {
    return (
      <div className="box">
        <header className="header">user</header>
        <div className="content">user</div>
      </div>
    );
  }
}

```

```jsx
// src/views/error/404.jsx
import React, { Component } from 'react';

export default class Index extends Component {
  render() {
    return (
      <div className="box">
        <header className="header">404</header>
        <div className="content">404</div>
      </div>
    );
  }
}

```

```
// src/views/detail/Index.jsx
import React, { Component } from 'react';

export default class Index extends Component {
  render() {
    return (
      <div className="box">
        <header className="header">detail</header>
        <div className="content">detail</div>
      </div>
    );
  }
}


```



# 5.构建路由

React-router-dom@5

React-router-dom讲究的其实就是 **一切皆组件**

```js
// src/components/Footer.jsx
import React from 'react'

function Footer () {
  return (
    <footer className="footer">
      footer
    </footer>
  )
} 

export default Footer
```

```jsx
// src/App.jsx
import React from 'react'
import { HashRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import Home from './views/home/Index'
import Kind from './views/kind/Index'
import Cart from './views/cart/Index'
import User from './views/user/Index'
import NotMatch from './views/error/404'
import Detail from './views/detail/Index'

import Footer from './components/Footer'
import './common.scss'
function App () {
  return (
    <Router>
      <div className="container">
        <Switch>
          <Route path="/home" component = { Home } />
          <Route path="/kind" component = { Kind } />
          <Route path="/cart" component = { Cart } />
          <Route path="/user" component = { User } />
          <Route path="/detail" component = { Detail } />
          <Redirect path="/" exact to="/home" />
          <Route path="*" component = { NotMatch } />
        </Switch>
        {/* 控制底部的显示 */}
        <Switch>
          <Route path="/home" component = { Footer } />
          <Route path="/kind" component = { Footer } />
          <Route path="/cart" component = { Footer } />
          <Route path="/user" component = { Footer } />
        </Switch>
      </div>
    </Router>
  )
}

export default App
```

点击底部选项卡切换页面

```js
// src/fomponents/Footer.jsx
import React from 'react'
import { NavLink } from 'react-router-dom'
function Footer () {
  return (
    <footer className="footer">
      <ul>
      <NavLink to="/home">
          <span className="iconfont icon-shouye"></span>
          <p>首页</p>
        </NavLink>
        <NavLink to="/kind">
          <span className="iconfont icon-fenlei"></span>
          <p>分类</p>
        </NavLink>
        <NavLink to="/cart">
          <span className="iconfont icon-gouwuche"></span>
          <p>购物车</p>
        </NavLink>
        <NavLink to="/user">
          <span className="iconfont icon-wodeguanzhu"></span>
          <p>我的</p>
        </NavLink>
      </ul>
    </footer>
  )
} 

export default Footer
```

```scss
// src/common.scss
* { padding: 0; margin: 0; list-style: none; text-decoration: none;}

@font-face {
  font-family: "iconfont"; /* Project id 2867560 */
  src: url('https://at.alicdn.com/t/font_2867560_s94v9bdlx3e.woff2?t=1634192797928') format('woff2'),
       url('https://at.alicdn.com/t/font_2867560_s94v9bdlx3e.woff?t=1634192797928') format('woff'),
       url('https://at.alicdn.com/t/font_2867560_s94v9bdlx3e.ttf?t=1634192797928') format('truetype');
}

.iconfont {
  font-family: "iconfont" !important;
  font-size: 16px;
  font-style: normal;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}

.icon-shouye:before {
  content: "\e64f";
}

.icon-gouwuche:before {
  content: "\e899";
}

.icon-fenlei:before {
  content: "\e71b";
}

.icon-wodeguanzhu:before {
  content: "\e8bc";
}


html, body, #root, .container {
  width: 100%;
  height: 100%;
}
html {
  // font-size 100px // 为了方便计算
  // iphone6 750 * 1334 ==> 375 * 667
  // 100 / 375 * 100
  // * 100 为了好计算
  font-size: 26.666667vw // iphone6
}
@media all and (min-width: 996px) {
  html {
    font-size: 100px
  }
}
body {
  font-size: 14px // 根据设计师给的设计稿的基础字体的大小
}
.container {
  display: flex;
  flex-direction: column;
  .box {
    flex: 1;
    overflow: auto;
    display: flex;
    flex-direction: column;
    .header {
      height: 0.44rem;
      background-color: #f66;
    }
    .content {
      flex: 1;
      overflow: auto;
    }
  }
  .footer {
    height: 0.5rem;
    background-color: #efefef;
    ul {
      width: 100%;
      height: 100%;
      display: flex;
      a{
        flex: 1;
        height: 100%;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        color: #333;

        &.active {
          color: #f66;
        }
        span {
          font-size: 0.24rem;
        }
        p {
          font-size: 12px;
        }
      }
    }
  }
}
```

> 加入你喜欢vue的配置的那种模式
>
> ```js
> // src/router/index.js
> import Home from '../views/home/Index'
> import Kind from '../views/kind/Index'
> import Cart from '../views/cart/Index'
> import User from '../views/user/Index'
> import NotMatch from '../views/error/404'
> import Detail from '../views/detail/Index'
> const routes = [
>   {
>     path: '/',
>     redirect: '/home'
>   },
>   {
>     path: '/home',
>     component: Home
>   },
>   {
>     path: '/kind',
>     component: Kind
>   },
>   {
>     path: '/cart',
>     component: Cart
>   },
>   {
>     path: '/user',
>     component: User
>   },
>   {
>     path: '/detail/:proid',
>     component: Detail
>   },
>   {
>     path: '*',
>     component: NotMatch
>   }
> ]
> 
> export default routes
> ```
>
> ```js
> // src/App.jsx
> import React from 'react'
> import { HashRouter as Router, Switch, Route } from 'react-router-dom'
> 
> import Footer from './components/Footer'
> import RouterView from './router/RouterView'
> 
> import './common.scss'
> function App () {
>   return (
>     <Router>
>       <div className="container">
>         <RouterView />
>         {/* 控制底部的显示 */}
>         <Switch>
>           <Route path="/home" component = { Footer } />
>           <Route path="/kind" component = { Footer } />
>           <Route path="/cart" component = { Footer } />
>           <Route path="/user" component = { Footer } />
>         </Switch>
>       </div>
>     </Router>
>   )
> }
> 
> export default App
> ```
>
> ```jsx
> // src/router/RouterView.jsx
> import { Switch, Route, Redirect } from 'react-router-dom' 
> import routes from './index'
> function RouterView () {
>   return (
>     <Switch>
>       {
>         routes.map(item => {
>           if (item.path === '/') {
>             return <Redirect key={ item.path } path="/" exact to="/home"/>
>           } else {
>             return (
>               <Route key={ item.path } path={ item.path } component = { item.component } />
>             )
>           }
>         })
>       }
>     </Switch>
>   )
> }
> 
> export default RouterView
> ```



**其他定义路由的方式**

><Route path="/home" component = { Home }/> ✅ ✅ ✅ ✅
>
><Route path="/home" ><Home /></Route> ✅
>
><Route path="/" render = {() => <Home/>} /> ✅

能否像vue一样实现路由的懒加载

> 需要 Lazy 和 Suspense
>
> `React.lazy` 函数能让你像渲染常规组件一样处理动态引入（的组件）
>
> ```js
> // src/router/index.js
> import React from 'react'
> import Home from '../views/home/Index'
> const Kind = React.lazy(() => import('../views/kind/Index'))
> const Cart = React.lazy(() => import('../views/cart/Index'))
> const User = React.lazy(() => import('../views/user/Index'))
> const NotMatch = React.lazy(() => import('../views/error/404'))
> const Detail = React.lazy(() => import('../views/detail/Index'))
> 
> const routes = [
>   {
>     path: '/',
>     redirect: '/home'
>   },
>   {
>     path: '/home',
>     component: Home
>   },
>   {
>     path: '/kind',
>     component: Kind
>   },
>   {
>     path: '/cart',
>     component: Cart
>   },
>   {
>     path: '/user',
>     component: User
>   },
>   {
>     path: '/detail/:proid',
>     component: Detail
>   },
>   {
>     path: '*',
>     component: NotMatch
>   }
> ]
> 
> export default routes
> ```
>
> ```jsx
> // src/App.js
> import React from 'react'
> import { HashRouter as Router, Switch, Route } from 'react-router-dom'
> 
> import Footer from './components/Footer'
> import RouterView from './router/RouterView'
> 
> import './common.scss'
> function App () {
>   return (
>     <Router>
>       <React.Suspense fallback = { <div>加载中...</div> }>
>         <div className="container">
>           <RouterView />
>           {/* 控制底部的显示 */}
>           <Switch>
>             <Route path="/home" component = { Footer } />
>             <Route path="/kind" component = { Footer } />
>             <Route path="/cart" component = { Footer } />
>             <Route path="/user" component = { Footer } />
>           </Switch>
>         </div>
>       </React.Suspense>
>     </Router>
>   )
> }
> 
> export default App
> ```

# 6.设计状态管理器

Redux + react-redux + redux-saga + immutable

```js
// src/utils/navList.js
export const navList = [
  { navid: 1, text: '嗨购超市', icon: 'https://m.360buyimg.com/mobilecms/s120x120_jfs/t1/125678/35/5947/4868/5efbf28cEbf04a25a/e2bcc411170524f0.png' },
  { navid: 2, text: '数码电器', icon: 'https://m.360buyimg.com/mobilecms/s120x120_jfs/t1/178015/31/13828/6862/60ec0c04Ee2fd63ac/ccf74d805a059a44.png!q70.jpg' },
  { navid: 3, text: '嗨购服饰', icon: 'https://m.360buyimg.com/mobilecms/s120x120_jfs/t1/41867/2/15966/7116/60ec0e0dE9f50d596/758babcb4f911bf4.png!q70.jpg' },
  { navid: 4, text: '嗨购生鲜', icon: 'https://m.360buyimg.com/mobilecms/s120x120_jfs/t1/177902/16/13776/5658/60ec0e71E801087f2/a0d5a68bf1461e6d.png!q70.jpg.dpg' },
  { navid: 5, text: '嗨购到家', icon: 'https://m.360buyimg.com/mobilecms/s120x120_jfs/t1/196472/7/12807/7127/60ec0ea3Efe11835b/37c65625d94cae75.png!q70.jpg.dpg' },
  { navid: 6, text: '充值缴费', icon: 'https://m.360buyimg.com/mobilecms/s120x120_jfs/t1/185733/21/13527/6648/60ec0f31E0fea3e0a/d86d463521140bb6.png!q70.jpg.dpg' },
  { navid: 7, text: '9.9元拼', icon: 'https://m.360buyimg.com/mobilecms/s120x120_jfs/t1/36069/14/16068/6465/60ec0f67E155f9488/595ff3e606a53f02.png!q70.jpg.dpg' },
  { navid: 8, text: '领券', icon: 'https://m.360buyimg.com/mobilecms/s120x120_jfs/t1/186080/16/13681/8175/60ec0fcdE032af6cf/c5acd2f8454c40e1.png!q70.jpg.dpg' },
  { navid: 9, text: '领金贴', icon: 'https://m.360buyimg.com/mobilecms/s120x120_jfs/t1/196711/35/12751/6996/60ec1000E21b5bab4/38077313cb9eac4b.png!q70.jpg.dpg' },
  { navid: 10, text: 'plus会员', icon: 'https://m.360buyimg.com/mobilecms/s120x120_jfs/t1/37709/6/15279/6118/60ec1046E4b5592c6/a7d6b66354efb141.png!q70.jpg.dpg' }
]
```

```js
// src/store/modules/home.js
import { Map } from 'immutable'
import { navList } from './../../utils/navList'
const reducer = (state = Map({
  bannerList: [],
  navList: navList,
  proList: []
}), action) => {
  switch (action.type) {
    case 'CHANGE_BANNER_LIST':
      return state.set('bannerList', action.payload)
    case 'CHANGE_PRO_LIST':
      return state.set('proList', action.payload)
    default:
      return state
  }
}

export default reducer
```

```js
// src/store/sagas/home.js
import { call, put } from 'redux-saga/effects'

import { getBannerList, getProList } from './../../api/home'


export function * getBannerListAction () {
  const res = yield call(getBannerList)
  yield put({
    type: 'CHANGE_BANNER_LIST',
    payload: res.data.data
  })
}
export function * getProListAction (action) {
  const res = yield call(getProList, action.payload)
  yield put({
    type: 'CHANGE_PRO_LIST',
    payload: res.data.data
  })
}


```

```js
// src/store/mySaga.js
import { takeLatest } from 'redux-saga/effects'

import { getBannerListAction, getProListAction } from './sagas/home'

function * mySaga () {
  yield takeLatest('GET_HOME_BANNER_LIST', getBannerListAction)
  yield takeLatest('GET_HOME_PRO_LIST', getProListAction)
}

export default mySaga
```

```js
// src/store/index.js
import { createStore, applyMiddleware } from 'redux'

import createSagaMiddleware from 'redux-saga'

import { combineReducers } from 'redux-immutable'

import mySaga from './mySaga'

import home from './modules/home'

const reducer = combineReducers({
  home
})

const sagaMiddleware = createSagaMiddleware()

const store = createStore(reducer, applyMiddleware(sagaMiddleware))

sagaMiddleware.run(mySaga)

export default store

```

```js
// src/index.js
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.jsx';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux'
import store from './store'

ReactDOM.render(
  <React.StrictMode>
    <Provider store = { store }>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

```

# 7.首页的数据渲染

## 1.划分组件

```jsx
// src/views/home/Swiper.jsx
function Swiper () {
  return (
    <div>Swiper</div>
  )
}

export default Swiper
```

```jsx
// src/views/home/NavList.jsx
function NavList () {
  return (
    <div>navList</div>
  )
}

export default NavList
```

```jsx
// src/views/home/ProList.jsx
function ProList () {
  return (
    <div>ProList</div>
  )
}

export default ProList
```

```jsx
// src/views/home/Index.jsx
import React, { Component } from 'react';
import Swiper from './Swiper'
import NavList from './NavList'
import ProList from './ProList'
export default class Index extends Component {
  render() {
    return (
      <div className="box">
        <header className="header">home</header>
        <div className="content">
          <Swiper />
          <NavList />
          <ProList />
        </div>
      </div>
    );
  }
}

```

## 2.请求数据

```jsx
import React, { Component } from 'react';
import Swiper from './Swiper'
import NavList from './NavList'
import ProList from './ProList'
import { connect } from 'react-redux'
class Index extends Component {
  componentDidMount () {
    this.props.getBannerList()
    this.props.getProList()
  }
  render() {
    const { bannerList, navList, proList} = this.props
    return (
      <div className="box">
        <header className="header">home</header>
        <div className="content">
          <Swiper bannerList = { bannerList }/>
          <NavList navList = { navList }/>
          <ProList proList = { proList }/>
        </div>
      </div>
    );
  }
}
export default connect(
  (state) => {
    return {
      bannerList: state.getIn(['home', 'bannerList']),
      navList: state.getIn(['home', 'navList']),
      proList: state.getIn(['home', 'proList'])
    }
  }, 
  (dispatch) => {
    return {
      getBannerList () {
        dispatch({ type: 'GET_HOME_BANNER_LIST' })
      },
      getProList () {
        dispatch({ type: 'GET_HOME_PRO_LIST' })
      }
    }
  })(Index)
```

> 此处可以使用 装饰器语法
>
> ```js
> import React, { Component } from 'react';
> import Swiper from './Swiper'
> import NavList from './NavList'
> import ProList from './ProList'
> import { connect } from 'react-redux'
> 
> @connect(
>   (state) => {
>     return {
>       bannerList: state.getIn(['home', 'bannerList']),
>       navList: state.getIn(['home', 'navList']),
>       proList: state.getIn(['home', 'proList'])
>     }
>   }, 
>   (dispatch) => {
>     return {
>       getBannerList () {
>         dispatch({ type: 'GET_HOME_BANNER_LIST' })
>       },
>       getProList () {
>         dispatch({ type: 'GET_HOME_PRO_LIST'})
>       }
>     }
>   }
> )
> class Index extends Component {
>   componentDidMount () {
>     this.props.getBannerList()
>     this.props.getProList()
>   }
>   render() {
>     const { bannerList, navList, proList} = this.props
>     return (
>       <div className="box">
>         <header className="header">home</header>
>         <div className="content">      
>           <Swiper bannerList = { bannerList }/>
>           <NavList navList = { navList }/>
>           <ProList proList = { proList }/>
>         </div>
>       </div>
>     );
>   }
> }
> export default Index
> ```
>
> 

## 3.渲染数据

```jsx
// src/views/home/Swiper.jsx
import { Carousel } from 'antd-mobile';
function SwiperCom ({ bannerList }) {
  return (
    <Carousel
      autoplay={true}
      infinite
    >
      { bannerList && bannerList.map(item => (
        <a
          key={item.bannerid}
          href={ item.link }
          style={{ display: 'inline-block', width: '100%', height: '150px', overflow: 'hidden' }}
        >
          <img
            src={ item.img }
            alt={ item.alt }
            style={{ width: '100%', verticalAlign: 'top' }}
            onLoad={() => {
              // fire window resize event to change height
              window.dispatchEvent(new Event('resize'));
            }}
          />
        </a>
      ))}
    </Carousel>
  )
}

export default SwiperCom
```

```jsx
// src/views/home/NavList.jsx
import { Grid } from 'antd-mobile';

function NavList ({ navList }) {
  return (
    <>
       <Grid data={navList} columnNum={5}  />
    </>
  )
}

export default NavList
```

```jsx
// src/views/home/ProList.jsx
import { List } from 'antd-mobile';
const Item = List.Item;
function ProList ({ proList }) {
  return (
    <List>
      {
        proList && proList.map((item) => (
          <Item
            thumb={ item.img1 }
            multipleLine
            onClick={() => {}}
          >
            { item.proname }
          </Item>
        ))
      }
    </List>
  )
}

export default ProList
```

# 8 点击列表，进入产品详情

## 1.声明式渲染

> NavLink  Link

```jsx
// src/views/home/ProList.jsx
import { List } from 'antd-mobile';
import { Link } from 'react-router-dom'
const Item = List.Item;
function ProList ({ proList }) {
  return (
    <List>
      {
        proList && proList.map((item) => (
          <Link key = { item.proid } to={ '/detail/' + item.proid }>
            <Item
              thumb={ item.img1 }
              multipleLine
              onClick={() => {}}
            >
              { item.proname }
            </Item>
          </Link>
         
        ))
      }
    </List>
  )
}

export default ProList
```



## 2.编程式渲染

> 注意是否有 history 对象

````jsx
import { List } from 'antd-mobile';
// import { Link } from 'react-router-dom'
const Item = List.Item;
function ProList ({ proList, history }) {
  console.log(history)
  return (
    <List>
      {
        proList && proList.map((item) => (
          // <Link key = { item.proid } to={ '/detail/' + item.proid }>
          //   <Item
          //     thumb={ item.img1 }
          //     multipleLine
          //     onClick={() => {}}
          //   >
          //     { item.proname }
          //   </Item>
          // </Link>
         
          <Item
            key = { item.proid } 
            thumb={ item.img1 }
            multipleLine
            onClick={() => {
              history.push('/detail/' + item.proid)
            }}
          >
            { item.proname }
          </Item>
        
        ))
      }
    </List>
  )
}

export default ProList
````

> 首页组件是有这个对象的，但是 ProList的组件是首页组件的子组件
>
> * 方式1:通过父子组件逐层传值搞定. { ...this.props }
>
>   ```jsx
>   import React, { Component } from 'react';
>   import Swiper from './Swiper'
>   import NavList from './NavList'
>   import ProList from './ProList'
>   import { connect } from 'react-redux'
>   
>   @connect(
>     (state) => {
>       return {
>         bannerList: state.getIn(['home', 'bannerList']),
>         navList: state.getIn(['home', 'navList']),
>         proList: state.getIn(['home', 'proList'])
>       }
>     }, 
>     (dispatch) => {
>       return {
>         getBannerList () {
>           dispatch({ type: 'GET_HOME_BANNER_LIST' })
>         },
>         getProList () {
>           dispatch({ type: 'GET_HOME_PRO_LIST' })
>         }
>       }
>     }
>   )
>   class Index extends Component {
>     componentDidMount () {
>       // console.log(this.props)
>       this.props.getBannerList()
>       this.props.getProList()
>     }
>     render() {
>       const { bannerList, navList, proList} = this.props
>       return (
>         <div className="box">
>           <header className="header">home</header>
>           <div className="content">      
>             <Swiper bannerList = { bannerList }/>
>             <NavList navList = { navList }/>
>             <ProList proList = { proList } { ...this.props }/>
>           </div>
>         </div>
>       );
>     }
>   }
>   export default Index
>   ```
>
>   * 方式2: Context 传值 ---- 新增代码量
>
>   * 方式3:高阶组件 withRouter，组件自己想办法
>
>     ```jsx
>     import { List } from 'antd-mobile';
>     // import { Link } from 'react-router-dom'
>     import { withRouter } from 'react-router-dom'
>     const Item = List.Item;
>     function ProList ({ proList, history }) {
>       console.log(history)
>       return (
>         <List>
>           {
>             proList && proList.map((item) => (
>               <Item
>                 key = { item.proid } 
>                 thumb={ item.img1 }
>                 multipleLine
>                 onClick={() => {
>                   history.push('/detail/' + item.proid)
>                 }}
>               >
>                 { item.proname }
>               </Item>
>             
>             ))
>           }
>         </List>
>       )
>     }
>     
>     export default withRouter(ProList)
>     ```
>
>   * 方式4: 如果该组件是函数式组件，可以使用。useHistory hooks
>
>   ```jsx
>   import { List } from 'antd-mobile';
>   // import { Link } from 'react-router-dom'
>   import { useHistory } from 'react-router-dom'
>   const Item = List.Item;
>   function ProList ({ proList }) {
>     const history = useHistory()
>     return (
>       <List>
>         {
>           proList && proList.map((item) => (
>             <Item
>               key = { item.proid } 
>               thumb={ item.img1 }
>               multipleLine
>               onClick={() => {
>                 history.push('/detail/' + item.proid)
>               }}
>             >
>               { item.proname }
>             </Item>
>           
>           ))
>         }
>       </List>
>     )
>   }
>   
>   export default ProList
>   ```
>
>   

# 9.详情页面的数据渲染

> 获取数据 proid 的方式
>
> 方式1:  this.props.match.params.proid
>
> 方式2:  如果组件是函数式组件，通过 useParams
>
> ```jsx
> import { useParams } from 'react-router-dom'
> unction Index () {
>   const params = useParams()
>   console.log(params)
>   return (
>     <div className="box">
>       <header className="header">detail</header>
>       <div className="content">detail</div>
>     </div>
>   );
> }
> ```

````jsx
import React, { Component } from 'react';
import { useParams } from 'react-router-dom'
class Index extends Component {
  state = {
    proid: ''
  }
  componentDidMount () {
    this.setState({
      proid: this.props.match.params.proid
    })
  }
  render() {
    // console.log(this.props.match.params.proid)
    return (
      <div className="box">
        <header className="header">detail</header>
        <div className="content">detail</div>
      </div>
    );
  }
}

// function Index () {
//   const params = useParams()
//   console.log(params)
//   return (
//     <div className="box">
//       <header className="header">detail</header>
//       <div className="content">detail</div>
//     </div>
//   );
// }

export default Index

````

# 10.项目上线

https://blog.csdn.net/daxunshuo/article/details/102976306?spm=1001.2014.3001.5501

## 1.登录阿里云服务器

> 找到公网IP地址

## 2.重做系统

> 如果系统不是 centos系统，需要重做

![img](https://img-blog.csdnimg.cn/20191108164721394.png)

![img](https://img-blog.csdnimg.cn/20191108164725688.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2RheHVuc2h1bw==,size_16,color_FFFFFF,t_70)

更换操作系统

![img](https://img-blog.csdnimg.cn/20200402094937903.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2RheHVuc2h1bw==,size_16,color_FFFFFF,t_70)

## 3 准备连接服务器的工具

点击[finalshell](http://mydown.yesky.com/pcsoft/413551229.html)下载

## 4.连接服务器

![img](https://img-blog.csdnimg.cn/20191108164726404.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2RheHVuc2h1bw==,size_16,color_FFFFFF,t_70)

![img](https://img-blog.csdnimg.cn/20191108164728202.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2RheHVuc2h1bw==,size_16,color_FFFFFF,t_70)

![img](https://img-blog.csdnimg.cn/20191108164725113.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2RheHVuc2h1bw==,size_16,color_FFFFFF,t_70)

![img](https://img-blog.csdnimg.cn/20191108164725707.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2RheHVuc2h1bw==,size_16,color_FFFFFF,t_70)

![img](https://img-blog.csdnimg.cn/20191108164724508.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2RheHVuc2h1bw==,size_16,color_FFFFFF,t_70)

## 5、nginx介绍
Nginx是一款轻量级的Web 服务器/反向代理服务器及电子邮件（IMAP/POP3）代理服务器，在BSD-like 协议下发行。其特点是占有内存少，并发能力强，事实上nginx的并发能力确实在同类型的网页服务器中表现较好，中国大陆使用nginx网站用户有：百度、京东、新浪、网易、腾讯、淘宝等。
负载均衡、反向代理

## 6.安装nginx

```
yum install -y nginx
```

![img](https://img-blog.csdnimg.cn/20191108164720768.png)

设置开机启动

> systemctl start nginx.service
> systemctl enable nginx.service

![img](https://img-blog.csdnimg.cn/20191108164723552.png)

浏览器打开此地址查看效果

![img](https://img-blog.csdnimg.cn/20191108164724318.png)

![img](https://img-blog.csdnimg.cn/20191108164726607.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2RheHVuc2h1bw==,size_16,color_FFFFFF,t_70)

nginx默认使用端口 80， ecs实例没有开启端口80，默认只有 22 和 3389

安全组 --- 配置规则 - 手动添加 源指的是 0.0.0.0/0

## 7.将文件放到nginx服务器上

```
cd /usr/share/nginx
cd html

```

本地新建 test.html

```
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  这就是一个测试的页面
</body>
</html>
```

将这个文件拖入 上面目录即可

> 访问 http://47.93.246.252/test.html即可



