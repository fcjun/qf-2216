# typescript

https://www.tslang.cn/

https://gitee.com/daxunxun/sz-gp-04/blob/wudaxundev/note/typescript/typescript.md

## 1.安装typescript

```
cnpm i typescript -g
```

## 2.体验

目前，html文件还无法直接解析 ts文件，所以需要先将ts文件转换成为js文件

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  
</body>
<script src="./app.js"></script>
</html>
```

```
// app.ts
// java int a = 10
// js   var a = 100

// ts --  类型注解： 在变量的后面添加数据类型
var a: number = 1000
console.log(a)
```

> tsc app.ts

运行html文件查看效果

分析转换出来的 app.js

```
// app.js
// java int a = 10
// js   var a = 100
// ts --  类型注解： 在变量的后面添加数据类型
var a = 1000;
console.log(a);

```

> ts的优势就是在未运行代码时就知道了代码的错误，但是并不会阻碍程序的运行

```
// app.ts
// java int a = 10
// js   var a = 100

// ts --  类型注解： 在变量的后面添加数据类型
var a: number = 1000
var b: string = 10 // 鼠标移动到b变量的红色区域
console.log(a)
console.log(b)
```

> tsc app.ts
>
> Type 'number' is not assignable to type 'string'.

运行时正常可以获取到值

## 3.类型注解

02 类型注解.ts

```
// 类型注解
// 对变量的数据类型的说明
// 变量       可以是函数参数的变量
// 数据类型   可以是基本的数据类型，还可以是 数组， 对象。。。。

var a: number = 10
var b: string = '100'
var c: boolean = true
// var d: object = { // 还是不知道该对象下有哪些字段，每种字段的数据类型
//   name: '吴大勋',
//   age: 18
// }
// 接口 - interface 关键字 接口名称约定以I开头再加功能
// 接口内部使用 ; 要不就不写，要写就写 ;
interface IUser {
  name: string;
  age: number;
}
// var d: IUser = {
//   name: '吴大勋',
//   age: 18
// }
var d: {
  name: string;
  age: number;
} = {
  name: '吴大勋',
  age: 18
}

// 数组的类型注解
// Array<数据类型>    泛型
// 数据类型[]
var e: Array<number> = [1, 2, 3, 4]
var f: number[] = [5, 6, 7, 8]

// 对象类型的数组的类型注解
var g: Array<IUser> = [
  {
    name: '吴大勋',
    age: 18
  },
  {
    name: '李泽阳',
    age: 28
  }
]
var h: IUser[] = [
  {
    name: '吴大勋',
    age: 18
  },
  {
    name: '李泽阳',
    age: 28
  }
]

// 有子集时 接口可以在内部继续使用，要注意 选项是不是必选项
interface IRoute {
  path: string;
  title: string;
  children?: Array<IRoute>; // ? 代表当前的对象可有可无的选项 
}

var i: IRoute[] = [
  {
    path: '/pro',
    title: '产品管理',
    children: [
      {
        path: '/pro/list',
        title: '产品列表'
      },
      {
        path: '/pro/recommend',
        title: '推荐列表'
      }
    ]
  }
]
```

> 假如数据的类型注解不唯一

## 4. 元组 Tuple

元组类型允许表示一个已知元素数量和类型的数组，各元素的类型不必相同。 比如，你可以定义一对值分别为 string和number类型的元组。

03 元祖tuple.ts。 --------   已知元素数量和类型的数组

```js
// 元祖Tuple
// 元组类型允许表示一个已知元素数量和类型的数组，各元素的类型不必相同。 
// 比如，你可以定义一对值分别为 string和number类型的元组。

var aa: [string, number, boolean] = ['a', 1, true]

interface IUser {
  name: string;
  age: number;
}
interface IBook {
  title: string;
  price: number;
}
var bb: [IUser, IBook] = [
  {
    name: '吴大勋',
    age: 18
  },
  {
    title: 'typescript 权威指南',
    price: 199
  }
]

var cc: [{
  name: string;
  age: number;
}, {
  title: string;
  price: number
}] = [
  {
    name: '吴大勋',
    age: 18
  },
  {
    title: 'typescript 权威指南',
    price: 199
  }
]


```

> 要解决 一个 变量可以拥有多种数据类型

04 多数据类型.ts

```js
var aaa: string = 10
var bbb: number = '100'
var ccc: string | number = '1000'
var ddd: string | number = 100
```

## 5.枚举Enum

enum类型是对JavaScript标准数据类型的一个补充。 使用枚举类型可以为一组数值赋予友好的名字。

05 枚举.ts

```js
// enum Color { Red, Green, Blue }

// var a4: Color = Color.Red
// var b4: Color = Color.Green
// var c4: Color = Color.Blue
// console.log(a4) // 0 // 打印的下标，从0 开始
// console.log(b4) // 1
// console.log(c4) // 2

// enum Color { Red=10, Green, Blue }

// var a4: Color = Color.Red
// var b4: Color = Color.Green
// var c4: Color = Color.Blue
// console.log(a4) // 10 // 打印的下标，从10 开始，因为第一个值为 10（指定初始下标）
// console.log(b4) // 11
// console.log(c4) // 12

// enum Color { Red=10, Green = 20, Blue = 30 }

// var a4: Color = Color.Red
// var b4: Color = Color.Green
// var c4: Color = Color.Blue
// console.log(a4) // 10 // 打印的下标，从10 开始，因为第一个值为 10（指定初始下标）
// console.log(b4) // 20
// console.log(c4) // 30

// console.log(Color[20]) // Green

enum Direction { UP = 38, LEFT=37, RIGHT=39, BOTTOM=40}

// event.keyCode === 38  上
// event.keyCode === Direction.UP

// 枚举 周-至周日  1月到12月  上下左右 。。。。。。

```

## 6.any类型

06 any.ts

```js
// any 类型
// 如果不知道是什么数据类型，但是还想要写数据类型时，写为any
var a5: any
```

## 7.类型断言

07 类型断言.ts

```js
// 类型断言
var str = 'hello world'

// var len = str.length
// var len = (str as string).length
var len = (<string>str).length

console.log(len)
```

## 8.函数的类型

> 函数的声明式(function fn () {})。 函数表达式（const fn = () => {}）

08 函数的类型.ts

```js
// 函数的类型

// 声明式函数
function sum (a: number, b: number): number {
  return a + b
}
// 表达式函数
const sum1: Function = function (a: string, b: string): string {
  return a + b
}
const sum2: Function = (a: string, b: string): string => {
  return a + b
}
const sum3: (a: string, b: string) => string = function (a: string, b: string): string {
  return a + b
}
// 注意不要混淆了 TypeScript 中的 => 和 ES6 中的 =>。

// 在 TypeScript 的类型定义中，=> 用来表示函数的定义，左边是输入类型，需要用括号括起来，
// 右边是输出类型。

// 在 ES6 中，=> 叫做箭头函数，应用十分广泛，可以参考 [ES6 中的箭头函数][]
const sum4: (a: string, b: string) => string = (a: string, b: string): string => {
  return a + b
}

// 如果函数没有返回值 写 void
function test (a: number, b: number): void { console.log(a+b) }
const test1: (a: number, b: number) => void = (a: number, b: number): void => {console.log(a+b)}
```

09 函数补充.ts

```ts
// 用接口定义函数的形状

const fn: (a: number, b: number) => number = (a: number, b: number): number => {
  return a + b
}

interface IFn {
  (a: number, b: number): number
}
const fn1: IFn = (a: number, b: number): number => {
  return a + b
}

// 可选参数  ? 
function fn2 (a: number, b: number, c?: number): number {
  return a + b
}

// 默认值  
function fn3 (a: number = 10, b: number = 20, c?: number): number {
  return a + b
}

// 如果参数是可选参数，不能给它设置默认值
function fn4 (a: number, b: number, c?: number = 30): number {
  return a + b + c
}
// 剩余参数
function push(array, ...items) {
  items.forEach(function(item) {
    array.push(item)
  })
}

let a = []
push(a, 1, 2, 3)
console.log(a)

// 重载  ----   联合类型情况
// 重载允许一个函数接受不同数量或类型的参数时，作出不同的处理。
// 比如，我们需要实现一个函数 reverse，输入数字 123 的时候，输出反转的数字 321，输入字符串 'hello' 的时候，输出反转的字符串 'olleh'。
function reverse (x: string | number): number | string {
  if (typeof x === "number") {
    return Number(x.toString().split('').reverse().join(''))
  } else {
    return x.split('').reverse().join('')
  }
}
// 这样有一个缺点，就是不能够精确的表达，输入为数字的时候，输出也应该为数字，输入为字符串的时候，输出也应该为字符串。
// 这时，我们可以使用重载定义多个 reverse 的函数类型：

function reverse1 (x: number): number
function reverse1 (x: string): string
function reverse1 (x: string | number): string | number {
  if (typeof x === "number") {
    return Number(x.toString().split('').reverse().join(''))
  } else {
    return x.split('').reverse().join('')
  }
}
// 重复定义了多次函数 reverse，前几次都是函数定义，最后一次是函数实现
// TypeScript 会优先从最前面的函数定义开始匹配，
// 所以多个函数定义如果有包含关系，需要优先把精确的定义写在前面。

```

## 9.接口

在 TypeScript 中，我们使用接口（Interfaces）来定义对象的类型。

在面向对象语言中，接口（Interfaces）是一个很重要的概念，它是对行为的抽象，而具体如何行动需要由类（classes）去实现（implement）。 TypeScript 中的接口是一个非常灵活的概念，除了可用于对类的一部分行为进行抽象以外，也常用于对「对象的形状（Shape）」进行描述。

**赋值的时候，变量的形状必须和接口的形状保持一致**。

有时我们希望不要完全匹配一个形状，那么可以用可选属性。 ？

有时候我们希望一个接口允许有任意的属性，可以使用如下方式：一旦定义了任意属性，**那么确定属性和可选属性的类型都必须是它的类型的子集**：-----一般不轻易使用

```
interface IPerson {
  name: string
  age?: number
  [propName: string]: string | number   // any
}

let tom: IPerson = {
  name: 'Tom',
  age: 18,
  gender: 'male'
}
```

有时候我们希望对象中的一些字段只能在创建的时候被赋值，那么可以用 `readonly` 定义只读属性(对象的唯一值)

```ts
interface IPerson {
  readonly id: string
  name: string
  age?: number
  [propName: string]: string | number
}

let tom: IPerson = {
  id: '1',
  name: 'Tom',
  age: 18,
  gender: 'male'
}

tom.id = '2' // 无法分配到 "id" ，因为它是只读属性
```

## 10.类

传统方法中，JavaScript 通过构造函数实现类的概念，通过原型链实现继承。而在 ES6 中，我们终于迎来了 `class`。

TypeScript 除了实现了所有 ES6 中的类的功能以外，还添加了一些新的用法。

- 类(Class)：定义了一件事物的抽象特点，包含它的属性和方法
- 对象（Object）：类的实例，通过 `new` 生成
- 面向对象（OOP）的三大特性：封装、继承、多态
- 封装（Encapsulation）：将对数据的操作细节隐藏起来，只暴露对外的接口。外界调用端不需要（也不可能）知道细节，就能通过对外提供的接口来访问该对象，同时也保证了外界无法任意更改对象内部的数据
- 继承（Inheritance）：子类继承父类，子类除了拥有父类的所有特性外，还有一些更具体的特性
- 多态（Polymorphism）：由继承而产生了相关的不同的类，对同一个方法可以有不同的响应。比如 `Cat` 和 `Dog` 都继承自 `Animal`，但是分别实现了自己的 `eat` 方法。此时针对某一个实例，我们无需了解它是 `Cat` 还是 `Dog`，就可以直接调用 `eat` 方法，程序会自动判断出来应该如何执行 `eat`
- 存取器（getter & setter）：用以改变属性的读取和赋值行为
- 修饰符（Modifiers）：修饰符是一些关键字，用于限定成员或类型的性质。比如 `public` 表示公有属性或方法， private,`protected`
- 抽象类（Abstract Class）：抽象类是供其他类继承的基类，抽象类不允许被实例化。抽象类中的抽象方法必须在子类中被实现
- 接口（Interfaces）：不同类之间公有的属性或方法，可以抽象成一个接口。接口可以被类实现（implements）。一个类只能继承自另一个类，但是可以实现多个接口

##  ES6 中类的用法

###  属性和方法

使用 `class` 定义类，使用 `constructor` 定义构造函数。

通过 `new` 生成新实例的时候，会自动调用构造函数。

```js
class Animal {
  constructor(public name:string) {
  	this.name = name
  }
  sayHi() {
  	return `My name is ${this.name}`
  }
}

let a = new Animal('Jack')
console.log(a.sayHi()) // My name is Jack
```

### 类的继承

使用 `extends` 关键字实现继承，子类中使用 `super` 关键字来调用父类的构造函数和方法 --- es5和es6继承机制。

```js
class Cat extends Animal {
  constructor(name:string) {
    super(name) // 调用父类的 constructor(name)
    console.log(this.name)
  }
  sayHi() {
  	return 'Meow, ' + super.sayHi() // 调用父类的 sayHi()
  }
}

let c = new Cat('Tom') // Tom
console.log(c.sayHi()) // Meow, My name is Tom


class React {
  constructor(public props: any) {
    this.props = props
  }
  render () {
    console.log(this.props)
  }
}

class Header extends React {
  constructor (props: any) {
    super(props)
  }
}

let h = new Header('hello ')
h.render()
```

### 存取器

使用 getter 和 setter 可以改变属性的赋值和读取行为：

```
class Animal {
  constructor(name) {
  	this.name = name
  }
  get name() {
  	return 'Jack'
  }
  set name(value) {
  	console.log('setter: ' + value)
  }
}

let a = new Animal('Kitty') // setter: Kitty
a.name = 'Tom' // setter: Tom
console.log(a.name) // Jack
```

### 静态方法

使用 `static` 修饰符修饰的方法称为静态方法，它们不需要实例化，而是直接通过类来调用：

```
class Animal {
  static isAnimal(a) {
  	return a instanceof Animal
  }
}

let a = new Animal('Jack')
Animal.isAnimal(a) // true
a.isAnimal(a) // TypeError: a.isAnimal is not a function
```

## ES7 中类的用法







# React项目说明





## 一.通过脚手架创建项目

https://umijs.org/zh-CN/docs/getting-started

>cnpm i yarn tyarn -g
>
>mkdir  umi-app
>
>cd umi-app
>
>yarn create @umijs/umi-app
>
>> npx @umijs/create-umi-app
>
>yarn
>
>yarn start

## 二.熟悉目录和文件

1.mock 

* http://rap2.taobao.org/

 - 模拟数据 http://mockjs.com/

   > 生成随机数据，拦截 Ajax 请求
   >
   > *  前后端分离
   >
   >   让前端攻城师独立于后端进行开发
   >
   >
   > * 开发无侵入
   >
   >   不需要修改既有代码，就可以拦截 Ajax 请求，返回模拟的响应数据
   >
   > * 数据类型丰富
   >
   >   支持生成随机的文本、数字、布尔值、日期、邮箱、链接、图片、颜色等。
   >
   > * 增加单元测试的真实性
   >
   >   通过随机数据，模拟各种场景。
   >
   > * 用法简单
   >
   >   符合直觉的接口。
   >
   > * 方便扩展
   >
   >   支持扩展更多数据类型，支持自定义函数和正则

2.src

  - pages 页面 --- 等同于vue脚手架中views，存放的就是页面组件
      - index.less
      - index.tsx 注意引入的less是局部生效的，使用`:global`放成全局的

3.editorconfig 编辑器配置

4.gitignore git上传的忽略文件

5.prettierignore 格式化忽略配置文件

6.prettierrc  格式化配置文件

7.umirc.ts 项目配置文件，类似vue的vue.config.js,老版本umi中config/config.js

8.package.json 项目记录文件

9.README.md 项目说明书

10.tsconfig.json  ts的配置文件

11.typings.d.ts   ts的声明文件，不声明不可以引入

> 如果代码提示：无法使用 JSX，除非提供了 "--jsx" 标志。修改tsconfig.json文件中的 
>
> "jsx": "react-jsx",   为 ‘"jsx": "react"

## 三.搭建项目基本结构

查看umi提供的umi插件

https://umijs.org/zh-CN/plugins/plugin-layout

### 1.构建时配置

可以通过配置文件配置 `layout` 的主题等配置, 在 [`config/config.ts`](https://github.com/ant-design/ant-design-pro/blob/4a2cb720bfcdab34f2b41a3b629683329c783690/config/config.ts#L15)  或者 `umirc.ts` 中这样写：

```tsx
import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  routes: [
    { path: '/', component: '@/pages/index' },
  ],
  fastRefresh: {},
  layout: {
    name: '嗨购管理系统',
    logo: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fwww.lgstatic.com%2Fthumbnail_300x300%2Fi%2Fimage%2FM00%2F1C%2F95%2FCgpEMlkBrbuANDoAAABOTfWdJhc845.png&refer=http%3A%2F%2Fwww.lgstatic.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg'
  }
});

```

### 2.运行时配置

https://umijs.org/zh-CN/plugins/plugin-layout#%E8%BF%90%E8%A1%8C%E6%97%B6%E9%85%8D%E7%BD%AE

src文件夹下创建 app.tsx

```tsx
import React from 'react'

export const layout = () => {
  return {
    // 自定义头右部的 render 方法
    rightContentRender: () => <header>页面头部</header>,
    footerRender: () => <footer>欢迎来到嗨购管理系统</footer>
  }
}

```



### 3.扩展路由配置

https://umijs.org/zh-CN/plugins/plugin-layout#%E6%89%A9%E5%B1%95%E7%9A%84%E8%B7%AF%E7%94%B1%E9%85%8D%E7%BD%AE

config/route.ts

Pages/home/index.tsx

```tsx
import * as React from 'react';

export interface IAppProps {
}

export default function App (props: IAppProps) {
  return (
    <div style = { { height: '100%' } }>
      系统首页
    </div>
  );
}

```

pages/login/index.tsx

```tsx
import * as React from 'react';

export interface IAppProps {
}

export default function App (props: IAppProps) {
  return (
    <div style = { { height: '100%' } }>
      登录
    </div>
  );
}

```

// config/route.ts

```ts
export interface IRouteConfig {
  path: string
  component?: string
  redirect?: string
  exact?: boolean
  name?: string // 兼容此写法
  icon?: string
  // 更多功能查看
  // https://beta-pro.ant.design/docs/advanced-menu
  // ---
  // 新页面打开
  target?: string
  // 不展示顶栏
  headerRender?: boolean
  // 不展示页脚
  footerRender?: boolean
  // 不展示菜单
  menuRender?: boolean
  // 不展示菜单顶栏
  menuHeaderRender?: boolean
  // 权限配置，需要与 plugin-access 插件配合使用
  access?: string
  // 隐藏子菜单
  hideChildrenInMenu?: boolean
  // 隐藏自己和子菜单
  hideInMenu?: boolean
  // 在面包屑中隐藏
  hideInBreadcrumb?: boolean
  // 子项往上提，仍旧展示,
  flatMenu?: boolean
}

const routes: IRouteConfig[] = [
  { path: '/', exact: true, redirect: '/home'},
  { path: '/home', name: '系统首页', component: '@/pages/home/Index' },
  { path: '/login', component: '@/pages/login/Index', menuRender: false,  headerRender: false, footerRender: false, },
]

export default routes

```

umirc.ts中配置路由

```js
import { defineConfig } from 'umi';
import routes from './config/routes'
export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  routes,
  fastRefresh: {},
  layout: {
    title: '嗨购管理系统',
    logo: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fwww.lgstatic.com%2Fthumbnail_300x300%2Fi%2Fimage%2FM00%2F1C%2F95%2FCgpEMlkBrbuANDoAAABOTfWdJhc845.png&refer=http%3A%2F%2Fwww.lgstatic.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg',
    // pure: true // 删除所有的自带的配置项
    // loading: true
  }
});

```

src/gloable.less

```
html, body, #root {
  height: 100%;
}

```

src/app.tsx

```ts
import React from 'react'
import './global.less'
export const layout = () => {
  return {
    // 自定义头右部的 render 方法
    rightContentRender: () => <header>页面头部</header>,
    footerRender: () => <footer>欢迎来到嗨购管理系统</footer>
  }
}

```



## 四.配置基本路由

### 1.轮播图管理

src/pages/banner/index.tsx

```tsx
import * as React from 'react';

export interface IBannerListProps {
}

export default function BannerList (props: IBannerListProps) {
  return (
    <div  style={{ height: '100%'}}>
      轮播图列表
    </div>
  );
}

```

src/pages/banner/add.tsx

```tsx
import * as React from 'react';

export interface IAppProps {
}

export default function App (props: IAppProps) {
  return (
    <div style={{ height: '100%'}}>
      添加轮播图
    </div>
  );
}

```



二级路由：子路由https://umijs.org/zh-CN/docs/routing#routes

```ts
export interface IRouteConfig {
  path: string
  component?: string
  redirect?: string
  routes?: IRouteConfig[],
  exact?: boolean
  name?: string // 兼容此写法
  icon?: string
  // 更多功能查看
  // https://beta-pro.ant.design/docs/advanced-menu
  // ---
  // 新页面打开
  target?: string
  // 不展示顶栏
  headerRender?: boolean
  // 不展示页脚
  footerRender?: boolean
  // 不展示菜单
  menuRender?: boolean
  // 不展示菜单顶栏
  menuHeaderRender?: boolean
  // 权限配置，需要与 plugin-access 插件配合使用
  access?: string
  // 隐藏子菜单
  hideChildrenInMenu?: boolean
  // 隐藏自己和子菜单
  hideInMenu?: boolean
  // 在面包屑中隐藏
  hideInBreadcrumb?: boolean
  // 子项往上提，仍旧展示,
  flatMenu?: boolean
}

const routes: IRouteConfig[] = [
  { path: '/', exact: true, redirect: '/home'},
  { path: '/banner', exact: true, redirect: '/banner/list'},
  { path: '/home', name: '系统首页', component: '@/pages/home/Index' },
  { path: '/login', component: '@/pages/login/Index', menuRender: false,  headerRender: false, footerRender: false, },
  {
    path: '/banner',
    name: '轮播图管理',
    routes: [
      { path: '/banner/list', name: '轮播图列表', component: '@/pages/banner/index' },
      { path: '/banner/add', name: '添加轮播图', component: '@/pages/banner/add' },
    ]
  }
]

export default routes

```

### 2.产品管理

src/pages/pro/index.tsx

```tsx
import * as React from 'react';

export interface IAppProps {
}

export default function App (props: IAppProps) {
  return (
    <div style={{ height: '100%'}}>
      产品列表
    </div>
  );
}

```

src/pages/pro/recommend.tsx

```ts
import * as React from 'react';

export interface IAppProps {
}

export default function App (props: IAppProps) {
  return (
    <div style={{ height: '100%'}}>
      推荐列表
    </div>
  );
}

```

src/pages/pro/seckill.tsx

```tsx
import * as React from 'react';

export interface IAppProps {
}

export default function App (props: IAppProps) {
  return (
    <div style={{ height: '100%'}}>
      秒杀列表
    </div>
  );
}

```

src/pages/pro/search.tsx

```
import * as React from 'react';

export interface IAppProps {
}

export default function App (props: IAppProps) {
  return (
    <div style={{ height: '100%'}}>
      筛选列表
    </div>
  );
}

```



```ts
export interface IRouteConfig {
  path: string
  component?: string
  redirect?: string
  routes?: IRouteConfig[],
  exact?: boolean
  name?: string // 兼容此写法
  icon?: string
  // 更多功能查看
  // https://beta-pro.ant.design/docs/advanced-menu
  // ---
  // 新页面打开
  target?: string
  // 不展示顶栏
  headerRender?: boolean
  // 不展示页脚
  footerRender?: boolean
  // 不展示菜单
  menuRender?: boolean
  // 不展示菜单顶栏
  menuHeaderRender?: boolean
  // 权限配置，需要与 plugin-access 插件配合使用
  access?: string
  // 隐藏子菜单
  hideChildrenInMenu?: boolean
  // 隐藏自己和子菜单
  hideInMenu?: boolean
  // 在面包屑中隐藏
  hideInBreadcrumb?: boolean
  // 子项往上提，仍旧展示,
  flatMenu?: boolean
}

const routes: IRouteConfig[] = [
  { path: '/', exact: true, redirect: '/home'},
  { path: '/banner', exact: true, redirect: '/banner/list'},
  { path: '/pro', exact: true, redirect: '/pro/list'},
  { path: '/home', name: '系统首页', component: '@/pages/home/Index' },
  { path: '/login', component: '@/pages/login/Index', menuRender: false,  headerRender: false, footerRender: false, },
  {
    path: '/banner',
    name: '轮播图管理',
    routes: [
      { path: '/banner/list', name: '轮播图列表', component: '@/pages/banner/index' },
      { path: '/banner/add', name: '添加轮播图', component: '@/pages/banner/add' },
    ]
  },
  {
    path: '/pro',
    name: '产品管理',
    routes: [
      { path: '/pro/list', name: '产品列表', component: '@/pages/pro/index' },
      { path: '/pro/recommend', name: '推荐列表', component: '@/pages/pro/recommend' },
      { path: '/pro/seckill', name: '秒杀列表', component: '@/pages/pro/seckill' },
      { path: '/pro/search', name: '筛选列表', component: '@/pages/pro/search' },
    ]
  }
]

export default routes

```



## 五.登录页面实现

使用ant-design-pro

```
cnpm i @ant-design/pro-form -S
```

https://ant.design/components/form-cn/#components-form-demo-normal-login

移动端接口地址：http://121.89.205.189/apidoc/

pc后台管理系统：http://121.89.205.189/admindoc/

https://procomponents.ant.design/components/form/#%E7%99%BB%E5%BD%95

```tsx
import * as React from 'react';
import { LoginForm, ProFormText } from '@ant-design/pro-form';
import {
  UserOutlined,
  LockOutlined,
} from '@ant-design/icons';

export interface IAppProps {
}

export default function App (props: IAppProps) {
  const onFinish = (values) => {
    console.log(values)
  }
  return (
    <div style = { { height: '100%' } }>
      <LoginForm
        onFinish = { onFinish }
      >
        <ProFormText
          name="adminname"
          fieldProps={{
            size: 'large',
            prefix: <UserOutlined className={'prefixIcon'} />,
          }}
          placeholder={'管理员账户'}
          rules={[
            {
              required: true,
              message: '请输入管理员账户!',
            },
          ]}
        />
        <ProFormText.Password
          name="password"
          fieldProps={{
            size: 'large',
            prefix: <LockOutlined className={'prefixIcon'} />,
          }}
          placeholder={'密码'}
          rules={[
            {
              required: true,
              message: '请输入正确的密码！',
            },
          ]}
        />
      </LoginForm>
    </div>
  );
}

```

> 如何给 表单添加验证规则
>
> https://ant.design/components/form-cn/#Rule

## 六.封装axios

> umi中必须使用axios请求数据吗？
>
> 不是，可以使用umi的插件： https://umijs.org/zh-CN/plugins/plugin-request

> cnpm i axios -S

// utils/request.ts

```ts
import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios'
import { history } from 'umi';
const isDev: boolean = process.env.NODE_ENV === 'development'

const ins: AxiosInstance = axios.create({
  baseURL: isDev ? 'http://121.89.205.189/admin' : 'http://121.89.205.189/admin',
  timeout: 6000
})
// 请求拦截器
ins.interceptors.request.use(function (config: AxiosRequestConfig): AxiosRequestConfig {
  // 在发送请求之前做些什么
  // 设定加载的进度条  / 统一传递token 信息(从本地获取)
  config.headers ? config.headers['token'] = localStorage.getItem('token') || '' : config.headers = {}
  return config;
}, function (error: any): Promise<never> {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 添加响应拦截器
ins.interceptors.response.use(function (response: AxiosResponse<any>): AxiosResponse<any> {
  if (response.data.code === '10119') {
    history.push('/login')
  }
  return response;
}, function (error: any): Promise<never> {
  // 对响应错误做点什么
  return Promise.reject(error);
});

// http://www.axios-js.com/zh-cn/docs/#axios-config
// 自定义各种数据请求 axios({})
export default function request(config: AxiosRequestConfig): Promise<AxiosResponse<any>>{
  let { url = '', method = 'GET', data = {}, headers = {} } = config
  // url = url || ''
  // method = method || 'get'
  // data = data || {}
  // headers = headers || ''
  // method 转换为大写
  switch (method.toUpperCase()) {
    case 'GET':
      return ins.get(url, { params: data })
    case 'POST':
      // 表单提交  application/x-www-form-url-encoded
      if (headers['content-type'] === 'application/x-www-form-url-encoded') {
        // 转参数 URLSearchParams/第三方库qs
        const p = new URLSearchParams()
        for(let key in data) {
          p.append(key, data[key])
        }
        return ins.post(url, p, {headers})
      }
      // 文件提交  multipart/form-data
      if (headers['content-type'] === 'multipart/form-data') {
        const p = new FormData()
        for(let key in data) {
          p.append(key, data[key])
        }
        return ins.post(url, p, {headers})
      }
      // 默认 application/json
      return ins.post(url, data)
    case 'PUT': // 修改数据 --- 所有的数据的更新
      return ins.put(url, data)
    case 'DELETE': // 删除数据
      return ins.delete(url, {data})
    case 'PATCH': // 更新局部资源
      return ins.patch(url, data)
    default:
      return ins(config)
  }
}



```

封装数据请求 src/services/admin.ts

```ts
import request from './../utils/request'
import { AxiosResponse } from 'axios'
export interface ILoginParams {
  adminname: string
  password: string
}
export function loginFn (params: ILoginParams) {
  return request(
    {
      url: '/admin/login',
      data: params,
      method: 'POST'
    }
  )
}

```

## 七.dva数据流 - 状态管理器

- **内置 dva**，默认版本是 `^2.6.0-beta.20`，如果项目中有依赖，会优先使用项目中依赖的版本。
- **约定式的 model 组织方式**，不用手动注册 model
- **文件名即 namespace**，model 内如果没有声明 namespace，会以文件名作为 namespace
- **内置 dva-loading**，直接 connect `loading` 字段使用即可
- **支持 immer**，通过配置 `immer` 开启



约定式的 model 组织方式

符合以下规则的文件会被认为是 model 文件，

- `src/models` 下的文件
- `src/pages` 下，子目录中 models 目录下的文件
- `src/pages` 下，所有 model.ts 文件(不区分任何字母大小写)

### 1.配置

https://umijs.org/zh-CN/plugins/plugin-dva#%E9%85%8D%E7%BD%AE

// umirc.ts

```ts
import { defineConfig } from 'umi';
import routes from './config/route'
export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  // routes: [
  //   { path: '/', component: '@/pages/index' },
  // ],
  routes,
  fastRefresh: {},
  layout: {
    name: '嗨购管理系统',
    logo: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fwww.lgstatic.com%2Fthumbnail_300x300%2Fi%2Fimage%2FM00%2F1C%2F95%2FCgpEMlkBrbuANDoAAABOTfWdJhc845.png&refer=http%3A%2F%2Fwww.lgstatic.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg'
  },
  dva: {
    immer: true,
    hmr: false
  }
});

```

### 2.dva数据流引入
约定式的 model 组织方式

符合以下规则的文件会被认为是 model 文件，

- `src/models` 下的文件 - 常用  -- 全局性的redux数据
- `src/pages` 下，子目录中 models 目录下的文件
- `src/pages` 下，所有 model.ts 文件(不区分任何字母大小写) - 常用 - 表示某个模块下的redux数据



### 3.配置登录模块dva

使用第三种使用dva的方式

src/pages/login/model.ts

```ts
import { message } from 'antd'
import { loginFn } from '../../services/admin'
import { Effect, ImmerReducer, history } from 'umi'
export interface IAdminModel {
  namespace: 'admin'
  state: {
    isLogin: boolean,
    adminname: string,
    role: number,
    token: string
  },
  effects: {
    loginAction: Effect;
  },
  reducers: {
    CHANGE_LOGIN_STATE: ImmerReducer,
    CHANGE_ADMIN_NAME: ImmerReducer,
    CHANGE_ROLE: ImmerReducer,
    CHANGE_TOKEN: ImmerReducer,

  }
}
const AdminModel: IAdminModel = {
  namespace: 'admin',

  state: {
    isLogin: localStorage.getItem('isLogin') === 'true',
    adminname: localStorage.getItem('adminname') || '',
    role: Number(localStorage.getItem('role')) || 1,
    token: localStorage.getItem('token') || ''
  },

  effects: {
    *loginAction({ payload }: any, { call, put }: { call: Function, put: Function}) {
      const res = yield call(loginFn, payload)
      if (res.data.code === '10003') {
        message.error('密码错误')
      } else if (res.data.code === '10005') {
        message.error('该账户不可用')
      } else {
        message.success('登录成功')
        console.log(res)
        yield put({
          type: 'CHANGE_LOGIN_STATE',
          payload: true
        })
        yield put({
          type: 'CHANGE_ADMIN_NAME',
          payload: res.data.data.adminname
        })
        yield put({
          type: 'CHANGE_ROLE',
          payload: res.data.data.role
        })
        yield put({
          type: 'CHANGE_TOKEN',
          payload: res.data.data.token
        })
        history.push('/')
      }

    },
  },
  reducers: {
    CHANGE_LOGIN_STATE(state, action) {
      console.log(1)
      localStorage.setItem('isLogin',  String(action.payload))
      return {
        ...state,
        isLogin: action.payload,
      };
    },
    CHANGE_ADMIN_NAME(state, action) {
      console.log(2)
      localStorage.setItem('adminname',  action.payload)
      return {
        ...state,
        adminname: action.payload,
      };
    },
    CHANGE_ROLE(state, action) {
      console.log(3)
      localStorage.setItem('role',  String(action.payload))
      return {
        ...state,
        role: action.payload,
      };
    },
    CHANGE_TOKEN(state, action) {
      console.log(4)
      localStorage.setItem('token',  action.payload)
      return {
        ...state,
        token: action.payload,
      };
    },
  },
}
export default AdminModel

```



登录页面测试登录接口

```tsx
import { message } from 'antd'
import { loginFn } from '../services/admin'
import { Effect, ImmerReducer, history } from 'umi'
export interface IAdminModel {
  namespace: 'admin'
  state: {
    isLogin: boolean,
    adminname: string,
    role: number,
    token: string
  },
  effects: {
    loginAction: Effect;
  },
  reducers: {
    CHANGE_LOGIN_STATE: ImmerReducer,
    CHANGE_ADMIN_NAME: ImmerReducer,
    CHANGE_ROLE: ImmerReducer,
    CHANGE_TOKEN: ImmerReducer,

  }
}
const AdminModel: IAdminModel = {
  namespace: 'admin',

  state: {
    isLogin: localStorage.getItem('isLogin') === 'true',
    adminname: localStorage.getItem('adminname') || '',
    role: Number(localStorage.getItem('role')) || 1,
    token: localStorage.getItem('token') || ''
  },

  effects: {
    *loginAction({ payload }: any, { call, put }: { call: Function, put: Function}) {
      const res = yield call(loginFn, payload)
      if (res.data.code === '10003') {
        message.error('密码错误')
      } else if (res.data.code === '10005') {
        message.error('该账户不可用')
      } else {
        message.success('登录成功')
        console.log(res)
        yield put({
          type: 'CHANGE_LOGIN_STATE',
          payload: true
        })
        yield put({
          type: 'CHANGE_ADMIN_NAME',
          payload: res.data.data.adminname
        })
        yield put({
          type: 'CHANGE_ROLE',
          payload: res.data.data.role
        })
        yield put({
          type: 'CHANGE_TOKEN',
          payload: res.data.data.token
        })
        history.push('/')
      }

    },
  },
  reducers: {
    CHANGE_LOGIN_STATE(state, action) {
      console.log(1)
      localStorage.setItem('isLogin',  String(action.payload))
      return {
        ...state,
        isLogin: action.payload,
      };
    },
    CHANGE_ADMIN_NAME(state, action) {
      console.log(2)
      localStorage.setItem('adminname',  action.payload)
      return {
        ...state,
        adminname: action.payload,
      };
    },
    CHANGE_ROLE(state, action) {
      console.log(3)
      localStorage.setItem('role',  String(action.payload))
      return {
        ...state,
        role: action.payload,
      };
    },
    CHANGE_TOKEN(state, action) {
      console.log(4)
      localStorage.setItem('token',  action.payload)
      return {
        ...state,
        token: action.payload,
      };
    },
  },
}
export default AdminModel

```



> 得到服务器的响应，如果登录成功，应该将信息保存到本地并且跳转到系统的首页

### 4.首页测试状态

```tsx
import * as React from 'react';
import { connect, IAdminModel } from 'umi';

export interface IAppProps {
  adminname: string
}

function App (props: IAppProps) {
  return (
    <div style = { { height: '100%' } }>
      系统首页 - { props.adminname }
    </div>
  );
}

export default connect((state: any) => {
  console.log(state)
  return {
    adminname: state.admin.adminname
  }
})(App)

```



## 八.头部 - 退出

```tsx
import * as React from 'react';
import { connect, IAdminModel, history } from 'umi';
import { Button } from 'antd'
export interface IAppProps {
}

function App (props: IAppProps) {
  const exit = () => {
    localStorage.clear()
    history.push('/login')
  }
  return (
    <header>
      111
      <Button onClick = { exit }>退出</Button>
    </header>
  );
}

export default App

```

```tsx
import React from 'react'
import RightHeader from './components/RightHeader'
import './global.less'
export const layout = () => {
  return {
    // 自定义头右部的 render 方法
    rightContentRender: () => <RightHeader />,
    footerRender: () => <footer>欢迎来到嗨购管理系统</footer>
  }
}

```



## 九.产品列表

### 1.新增一个声明文件 pro.d.ts

> *.d.ts 称之为 ts中的声明文件, 写代码时可以自动提示 数据类型
>
> Src/pages/pro/pro.d.ts

```ts
/**
 * /**
 * {
    "banners": [],
    "proid": "pro_e374dae5-22d7-40d8-ade2-5aa6449624c3",
    "category": "手机",
    "brand": "Apple",
    "proname": "Apple***",
    "originprice": 4999,
    "sales": 4070000,
    "stock": 50000,
    "desc": "商品名称***",
    "issale": 0,
    "isrecommend": 0,
    "discount": 9,
    "isseckill": 1,
    "img1": "https://*.jpg",
    "img2": "https://*.jpg",
    "img3": "https://*.jpg",
    "img4": "https://*.jpg"
},
 */
export interface IPro {
  banners: Array<string>
  proid: string
  category: string
  brand: string
  proname: string
  originprice: number
  sales: number
  stock: number
  desc: string
  issale: number
  isrecommend: number
  discount: number
  isseckill: number
  img1: string
  img2: string
  img3: string
  img4: string
}
```

### 2.构建关于产品管理的dva数据流

dva数据流有三种使用方式

* Src/models/*.ts.      * 要和定义的模块的namespace保持一致。 ------  登录数据流
* src/pages/models/*.ts



//src/models/pro.ts

```ts
import { getProListData } from '../services/pro'
import { Effect, Reducer } from 'umi'
import { IPro } from '@/pages/pro/pro'
export interface IProState {
  proList: IPro[]
  recommendList: IPro[]
  seckillList: IPro[]
  searchList: IPro[]
}
export interface IProModel {
  namespace: 'pro',
  state: IProState,
  effects: {
    getProListAction: Effect
  },
  reducers: {
    changeProList: Reducer,
    changeRecommendList: Reducer,
    changeSeckillList: Reducer,
    changeSearchList: Reducer,
  }
}

const userModel: IProModel = {
  namespace: 'pro',
  state: {
    proList: [],
    recommendList: [],
    seckillList: [],
    searchList: []
  },
  effects: {
    * getProListAction ({ payload }: any, { call, put }: any) {
      const res = yield call(getProListData, payload)
      yield put({
        type: 'changeProList',
        payload: res.data.data
      })
    }

  },
  reducers: {
    changeProList (state: any, action: any) {
      state.proList = action.payload
    },
    changeRecommendList (state: any, action: any) {
      state.recommendList = action.payload
    },
    changeSeckillList (state: any, action: any) {
      state.seckillList = action.payload
    },
    changeSearchList (state: any, action: any) {
      state.searchList = action.payload
    }
  }
}

export default userModel


```

### 3.封装产品相关的数据请求

//src/services/pro.ts

```ts
import request from './../utils/request'

export function getProListData () {
  return request(
    {
      url: '/pro/list'
    }
  )
}


```



### 4.产品列表页面请求数据并且展示

```
cnpm i ahooks -S
```



// src/pages/pro/inedex.tsx

```tsx
import * as React from 'react';
import { IPro } from './pro';
import { Button, Space } from 'antd';
import { EllipsisOutlined } from '@ant-design/icons';
import type { ProColumns } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import { connect, Dispatch } from 'umi'
export interface IAppProps {
  proList: IPro[]
  dispatch: Dispatch
}
function App (props: IAppProps) {
  console.log(props)
  const columns: ProColumns<IPro>[] = [
    {
      title: '序号',
      render: (text: never, record: IPro, index: number) => <span>{ index + 1}</span>,
    },
    {
      title: '产品名称',
      dataIndex: 'proname',
    },
    {
      title: '操作',
      render: () => (
        <Space>
          <Button type="ghost">编辑</Button>
          <Button danger>删除</Button>
        </Space>
      )
    }
  ];
  React.useEffect(() => {
    props.dispatch({
      type: 'pro/getProListAction'
    })
  }, [])
  return (
    <div style={{ height: '100%'}}>
      <ProTable<IPro>
      columns={columns}
      dataSource = { props.proList }
      cardProps={{ title: '业务定制', bordered: true }}
      search = {{}}
      headerTitle={
        <Button
          key="primary"
          type="primary"
          onClick={() => {
            alert('add');
          }}
        >
          添加
        </Button>
      }
      rowKey="key"
      search={false}
    />
    </div>
  );
}


export default connect((state: any) => ({ proList: state.pro.proList}))(App)

```

## 十.权限管理



### 1.请求管理员的登录状态接口

> 本接口文档没有专门的验证用户登录状态的接口，使用 查看管理员信息的接口代替专门的接口
>
> 请求的数据的长度大于0 表示用户登录的

//service/index.ts

```ts
import request from '@/utils/request'
import { getItem } from '@/utils/cookie'

// 不要使用常量或者变量，只会执行一次
// const adminname = getItem('adminname') || 'safjkhsdgjhsdjhsdgfjdshgfjhdsgfds'
export const checkAuthReq = () => {
  // 通过获取管理员的信息接口去判断有无登录
  // 真实情况应该是 调用 获取管理员登录状态的 接口去判断
  // 如果输出的数据的长度大于0 认为是登录的
  return request({
    url: '/admin/detail',
    method: 'GET',
    data: {
      adminname: getItem('adminname') || 'safjkhsdgjhsdjhsdgfjdshgfjhdsgfds'
    }
  })
}


```

### 2.封装验证用户的登录状态的hooks

// src/hooks/useAuth.tsx

```tsx
import { checkAuthReq } from './..//services/admin'
import { useRequest } from 'umi'
function useAuth () {
  const test = useRequest(() => {
    return checkAuthReq()
  })
  const { loading, error, data } = test
  if (loading) {
    return 1
  }
  if (error) {
    return 2
  }
  return data
}
export default useAuth

```

### 3.设定权限校验的组件 auth.tsx

Src/wrappers/auth.tsx

```tsx
import React from 'react'
import { Redirect } from 'umi'
import useAuth from './useAuth'
function App (props: any) {
  const data = useAuth();
  if (data === 1) {
    return <div>正在加载</div>;
  } else  if (data === 2) {
    return <div>出错了</div>;
  } else {
    if (data.data.length > 0) {
      return <div>{ props.children }</div>;
    } else {
      return <Redirect to="/login" />;
    }
  }
}

export default App

```

### 4.配置路由的权限

// 其实是登录的权限

```ts
export interface IRouteConfig {
  path: string
  component?: string
  redirect?: string
  routes?: IRouteConfig[],
  exact?: boolean
  name?: string // 兼容此写法
  icon?: string
  // 更多功能查看
  // https://beta-pro.ant.design/docs/advanced-menu
  // ---
  // 新页面打开
  target?: string
  // 不展示顶栏
  headerRender?: boolean
  // 不展示页脚
  footerRender?: boolean
  // 不展示菜单
  menuRender?: boolean
  // 不展示菜单顶栏
  menuHeaderRender?: boolean
  // 权限配置，需要与 plugin-access 插件配合使用
  access?: string
  // 隐藏子菜单
  hideChildrenInMenu?: boolean
  // 隐藏自己和子菜单
  hideInMenu?: boolean
  // 在面包屑中隐藏
  hideInBreadcrumb?: boolean
  // 子项往上提，仍旧展示,
  flatMenu?: boolean
  wrappers?: string[]  // ++++++++++
}

const routes: IRouteConfig[] = [
  { path: '/', exact: true, redirect: '/home'},
  { path: '/banner', exact: true, redirect: '/banner/list'},
  { path: '/pro', exact: true, redirect: '/pro/list'},
  { path: '/home', name: '系统首页', component: '@/pages/home/Index' },
  { path: '/login', component: '@/pages/login/Index', menuRender: false,  headerRender: false, footerRender: false, },
  {
    path: '/banner',
    name: '轮播图管理',
    routes: [
      { path: '/banner/list', name: '轮播图列表', component: '@/pages/banner/index' },
      { path: '/banner/add', name: '添加轮播图', component: '@/pages/banner/add',
        wrappers: [ // ++++++++++
          '@/wrappers/auth'
        ]
      },
    ]
  },
  {
    path: '/pro',
    name: '产品管理',
    routes: [
      { path: '/pro/list', name: '产品列表', component: '@/pages/pro/index' },
      { path: '/pro/recommend', name: '推荐列表', component: '@/pages/pro/recommend' },
      { path: '/pro/seckill', name: '秒杀列表', component: '@/pages/pro/seckill' },
      { path: '/pro/search', name: '筛选列表', component: '@/pages/pro/search' },
    ]
  }
]

export default routes

```

### 5.如果某些页面需要超级管理员才能访问

页面权限

s r c/wrappers/auth.tsx

```tsx
import React from 'react'
import { Redirect } from 'umi'
import useAuth from './useAuth'
function App (props: any) {
  const data = useAuth();
  if (data === 1) {
    return <div>正在加载</div>;
  } else  if (data === 2) {
    return <div>出错了</div>;
  } else {
    if (data.data.length > 0) {
      if (data.data[0].role > 1) {
        return <div>{ props.children }</div>;
      } else {
        return <div>无权限</div>;
      }
    } else {
      return <Redirect to="/login" />;
    }
  }
}

export default App

```



> 如果在设置权限时，可以在wrappers下写多个文件，在定义路由规则时使用不同文件即可 

## 十二.轮播图相关

Src/pages/banner-manager/banner.d.ts

```ts
export interface IBanner {
  bannerid: string
  img: string
  alt: string
  link: string
  flag: boolean
}

```

Src/pages/banner-manager/list.tsx

```tsx
import * as React from 'react';
import { Table, Button, Image, Space } from 'antd'
import { IBanner } from './banner';
export interface IBannerListProps {
  bannerList: IBanner[]
}

export default function BannerList (props: IBannerListProps) {
  const { bannerList } = props
  const columns = [
    {
      title: '序号',
      render (text: any, record: IBanner, index: number) {
        return <span>{ index + 1}</span>
      }
    },
    {
      title: '链接',
      dataIndex: 'link'
    },
    {
      title: '描述',
      dataIndex: 'alt'
    },
    {
      title: '图片',
      dataIndex: 'img',
      render (text: string, record: IBanner, index: number) {
        return (
          <Image src = { text} width="200" />
        )
      }
    },
    {
      title: '操作',
      render (text: any, record: IBanner, index: number) {
        return (
          <Space>
              <Button type="primary">编辑</Button>
              <Button danger>删除</Button>
          </Space>
        )
      }
    }
  ]
  return (
    <div>
      <Button onClick = { () => {

      }}>添加轮播图</Button>
      <Table dataSource = { bannerList } columns = { columns } rowKey = "bannerid"/>
    </div>
  );
}

```



### 1.构建以及请求轮播图的数据

// services/banner.ts

```ts
import request from '@/utils/request'
export const getBannerList = () => {
  return request({
    url: '/banner/list'
  })
}

export const addBanner = (params: { img: string, alt: string, link: string }) => {
  return request({
    url: '/banner/add',
    method: 'POST',
    data: params
  })
}


export const removeBanner = (params: { bannerid: string }) => {
  return request({
    url: '/banner/delete',
    data: params
  })
}

export const removeAllBanner = () => {
  return request({
    url: '/banner/removeAll'
  })
}

```

### 2.构建数据流

> Dva 数据流的使用方式
>
> **src/models/*.ts**
>
> **src/pages/models/*.ts**
>
> **src/pages/model.ts**
>
> ****

// src/pages/banner-manager/banner.d.ts

```
export interface IBanner {
  bannerid: string
  img: string
  alt: string
  link: string
  flag: boolean
}

```

// banner-manager/model.ts

```tsx
import { getBannerList } from "@/services/banner"
import { IBanner } from './banner'
import { Effect, ImmerReducer } from 'umi'
export interface IBannerState {
  bannerList: IBanner[]
}
export interface IBannerModel {
  namespace: 'banner',
  state: IBannerState,
  effects: {
    getBannerListReq: Effect
  },
  reducers: {
    changeBannerList: ImmerReducer<IBannerState>
  }
}
const bannerModel: IBannerModel = {
  namespace: 'banner',
  state: {
    bannerList: []
  },
  effects: {
    *getBannerListReq ({ payload }: any, { call, put }: any) {
      const res = yield call(getBannerList, payload)
      yield put({
        type: 'changeBannerList',
        payload: res.data.data
      })
    }
  },
  reducers: {
    changeBannerList (state, action) {
      state.bannerList = action.payload
    }
  }
}

export default bannerModel

```



### 3.列表页面请求数据

```tsx
import * as React from 'react';
import { Table, Button, Image, Space } from 'antd'
import { IBanner } from './banner';
import { useMount } from 'ahooks';
import { ConnectRC, connect } from 'umi';
export interface IBannerListProps {
  bannerList: IBanner[]
}

const BannerList: ConnectRC<IBannerListProps> =  (props) =>  {
  useMount(() => {
    props.dispatch({
      type: 'banner/getBannerListReq'
    })
  })
  const { bannerList } = props
  const columns = [
    {
      title: '序号',
      render (text: any, record: IBanner, index: number) {
        return <span>{ index + 1}</span>
      }
    },
    {
      title: '链接',
      dataIndex: 'link'
    },
    {
      title: '描述',
      dataIndex: 'alt'
    },
    {
      title: '图片',
      dataIndex: 'img',
      render (text: string, record: IBanner, index: number) {
        return (
          <Image src = { text} width={200} />
        )
      }
    },
    {
      title: '操作',
      render (text: any, record: IBanner, index: number) {
        return (
          <Space>
              <Button type="primary">编辑</Button>
              <Button danger>删除</Button>
          </Space>
        )
      }
    }
  ]
  return (
    <div>
      <Button onClick = { () => {

      }}>添加轮播图</Button>
      <Table dataSource = { bannerList } columns = { columns } rowKey = "bannerid"/>
    </div>
  );
}

export default connect(({ banner }: any) => ({ bannerList: banner.bannerList}))(BannerList)

```

### 

### 5.数据的删除

```tsx
import * as React from 'react';
import { Table, Button, Image, Space, Popconfirm } from 'antd'
import { IBanner } from './banner';
import { useMount } from 'ahooks';
import { ConnectRC, connect } from 'umi';
import { removeBanner } from '@/services/banner';
export interface IBannerListProps {
  bannerList: IBanner[]
}

const BannerList: ConnectRC<IBannerListProps> =  (props) =>  {
  useMount(() => {
    props.dispatch({
      type: 'banner/getBannerListReq'
    })
  })
  const { bannerList } = props
  const columns = [
    {
      title: '序号',
      render (text: any, record: IBanner, index: number) {
        return <span>{ index + 1}</span>
      }
    },
    {
      title: '链接',
      dataIndex: 'link'
    },
    {
      title: '描述',
      dataIndex: 'alt'
    },
    {
      title: '图片',
      dataIndex: 'img',
      render (text: string, record: IBanner, index: number) {
        return (
          <Image src = { text} width={200} />
        )
      }
    },
    {
      title: '操作',
      render (text: any, record: IBanner, index: number) {
        return (
          <Space>
              <Button type="primary">编辑</Button>
              <Popconfirm
                title="确认删除此轮播图吗?"
                onConfirm={ () => {
                  removeBanner({ bannerid: record.bannerid }).then(() => {
                    // 删除此数据成功
                    // 重新获取一次数据
                    props.dispatch({
                      type: 'banner/getBannerListReq'
                    })
                  })
                }}
                onCancel={ () => {}}
              >
                <Button danger >删除</Button>
              </Popconfirm>
          </Space>
        )
      }
    }
  ]
  return (
    <div>
      <Button onClick = { () => {

      }}>添加轮播图</Button>
      <Table dataSource = { bannerList } columns = { columns } rowKey = "bannerid"/>
    </div>
  );
}

export default connect(({ banner }: any) => ({ bannerList: banner.bannerList}))(BannerList)

```

### 6.添加轮播图数据

// src/pages/banner_manager/add.tsx

```tsx
import * as React from 'react';
import { Input,Button } from 'antd'
import { addBanner } from '@/services/banner';
import { history } from 'umi'
export interface IBannerAddProps {
}

function BannerAdd (props: IBannerAddProps) {
  const linkRef = React.useRef('link')
  const altRef = React.useRef('alt')
  const hideRef = React.useRef('hide')
  const fileRef = React.useRef('file')
  const imgRef = React.useRef('img')
  return (
    <div>
      <input ref = {linkRef} style={{ width: 200 }} placeholder = "请输入link" />
      <input ref = {altRef} style={{ width: 200 }} placeholder = "请输入alt" />
      <input ref = {fileRef} type="file" multiple onChange = {() => {
        const file = fileRef.current.files[0]
        const img = imgRef.current
        const reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onload = function () {
          img.src = this.result
          hideRef.current.value = this.result
        }
      }}/>
      <input type="text" ref = { hideRef } hidden/>
      <img src = "" ref = {imgRef}></img>
      <Button onClick = {
        () => {
          const link = linkRef.current.value
          const alt = altRef.current.value
          const img = hideRef.current.value
          console.log({
            link, alt, img
          })
          addBanner({
            link, alt, img
          }).then(() => {
            history.push('/banner/list')
          })
        }
      }>上传</Button>
    </div>
  );
}
export default BannerAdd

```

// 配置路由

```ts
export interface IChildRoute {
  path: string
  name?: string
  component: string,
  wrappers?: string[]
}
export interface IBestAFSRoute {
  routes?: IChildRoute[] // Array<IChildRoute>
  path: string
  redirect?: string,
  exact?: boolean,
  component?: string
  name?: string // 兼容此写法
  icon?: string
  // 更多功能查看
  // https://beta-pro.ant.design/docs/advanced-menu
  // ---
  // 新页面打开
  target?: string
  // 不展示顶栏
  headerRender?: boolean
  // 不展示页脚
  footerRender?: boolean
  // 不展示菜单
  menuRender?: boolean
  // 不展示菜单顶栏
  menuHeaderRender?: boolean
  // 权限配置，需要与 plugin-access 插件配合使用
  access?: string
  // 隐藏子菜单
  hideChildrenInMenu?: boolean
  // 隐藏自己和子菜单
  hideInMenu?: boolean
  // 在面包屑中隐藏
  hideInBreadcrumb?: boolean
  // 子项往上提，仍旧展示,
  flatMenu?: boolean,
  wrappers?: string[],
  role?: number
}
const routes: IBestAFSRoute[] = [
  {
    path: '/',
    exact: true,
    redirect: '/home'
  },
  {
    path: '/home',
    icon: 'HomeOutlined',
    name: '首页', // 如果需要出现在左侧的菜单栏
    component: '@/pages/home/index'
  },
  {
    // 登录页面不需要侧边菜单栏等
    path: '/login',
    // name: '登录',
    component: '@/pages/login/index',
    // 不展示顶栏
    headerRender: false,
    // 不展示页脚
    footerRender: false,
    // 不展示菜单
    menuRender: false
  },
  {
    path: '/banner',
    name: '轮播图管理',
    icon: 'FileImageOutlined',
    routes: [
      {
        path: '/banner/list',
        name: '轮播图列表',
        wrappers: [
          '@/wrappers/auth',
        ],
        component: '@/pages/banner-manager/list'
      },
      {
        path: '/banner/add',
        // name: '添加轮播图',
        wrappers: [
          '@/wrappers/auth',
        ],
        component: '@/pages/banner-manager/add'
      }
    ]
  },
  {
    path: '/pro',
    name: '产品管理',
    icon: 'UnorderedListOutlined',
    routes: [
      {
        path: '/pro/list',
        name: '产品列表',
        wrappers: [
          '@/wrappers/role',
        ],
        component: '@/pages/pro-manager/list'
      },
      {
        path: '/pro/recommend',
        name: '推荐列表',
        wrappers: [
          '@/wrappers/role',
        ],
        component: '@/pages/pro-manager/recommend'
      },
      {
        path: '/pro/seckill',
        name: '秒杀列表',
        wrappers: [
          '@/wrappers/role',
        ],
        component: '@/pages/pro-manager/seckill'
      }
    ]
  }
]

export default routes

```

## 十三.面包屑导航

// src/components/MyBreadcrumb/index.tsx

```tsx
import * as React from 'react';
import { Breadcrumb } from 'antd'
import { useMount } from 'ahooks';
import { useLocation } from 'umi';
interface IMyBreadcrumbProps {
}

const MyBreadcrumb: React.FunctionComponent<IMyBreadcrumbProps> = (props) => {
  const { pathname } = useLocation()
  console.log(pathname)
  const [ title, setTitle ] = React.useState('')
  const [ subTitle, setSubTitle ] = React.useState('')
  const breadcrumbList = {
    '/banner/list': {
      title: '轮播图管理',
      subTitle: '轮播题列表'
    },
    '/pro/list': {
      title: '产品管理',
      subTitle: '产品列表'
    },
    '/pro/recommend': {
      title: '产品管理',
      subTitle: '推荐列表'
    },
    '/pro/seckill': {
      title: '产品管理',
      subTitle: '秒杀列表'
    }
  }
  useMount(() => {
    // 获取地址栏的 pathname
    setTitle(breadcrumbList[pathname].title)
    setSubTitle(breadcrumbList[pathname].subTitle)
  })
  return (
    <Breadcrumb>
      <Breadcrumb.Item>首页</Breadcrumb.Item>
      <Breadcrumb.Item>
        <a href="">{ title }</a>
      </Breadcrumb.Item>
      <Breadcrumb.Item>
        <a href=""> { subTitle }</a>
      </Breadcrumb.Item>
    </Breadcrumb>
  );
};

export default MyBreadcrumb;

```

// 页面中使用 以产品列表为例

```tsx
import React from 'react';
import {MyBreadcrumb} from '@/components/myBreadcrumb'
type ProListProps = {

};

const index:React.FC<ProListProps> = () => {

  return (
    <>
      <MyBreadcrumb />
      <div>产品列表</div>
    </>
  )
}
export default index;

```

## 十四、管理员操作

详情见代码

编辑使用抽屉，添加使用对话框



token的使用 --- 拦截器

```ts
import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse, Method, AxiosError } from 'axios'
import { getItem } from './cookie';
import { history } from 'umi'

const isDev: boolean = process.env.NODE_ENV === 'development'

const ins: AxiosInstance = axios.create({
  baseURL: isDev ? 'http://121.89.205.189/admin' : 'http://121.89.205.189/admin',
  timeout: 6000
})
// 请求拦截器
ins.interceptors.request.use(function (config: AxiosRequestConfig): AxiosRequestConfig {
  // 在发送请求之前做些什么
  // 设定加载的进度条  / 统一传递token 信息(从本地获取) **********************************
  config.headers.common['token'] = getItem('token') || ''
  return config;
}, function (error: any): Promise<never> {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 添加响应拦截器
ins.interceptors.response.use(function (response: AxiosResponse<any>): AxiosResponse<any> {
  // 对响应数据做点什么
  // 进度条消失 /  验证token的有效性    **********************************
  if (response.data.code === '10119') { // 我的接口token失效 { code: '10119', message: 'token无效'}
    //跳转到登录页面
    window.location.href = "/login"
    return response;
  }
  return response;
}, function (error: any): Promise<never> {
  // 对响应错误做点什么
  return Promise.reject(error);
});

// http://www.axios-js.com/zh-cn/docs/#axios-config
// 自定义各种数据请求 axios({})
export default function request(config: AxiosRequestConfig): Promise<AxiosResponse<any>>{
  let { url = '', method = 'GET', data = {}, headers = '' } = config
  // url = url || ''
  // method = method || 'get'
  // data = data || {}
  // headers = headers || ''
  // method 转换为大写
  switch (method.toUpperCase()) {
    case 'GET':
      return ins.get(url, { params: data })
    case 'POST':
      // 表单提交  application/x-www-form-url-encoded
      if (headers['content-type'] === 'application/x-www-form-url-encoded') {
        // 转参数 URLSearchParams/第三方库qs
        const p = new URLSearchParams()
        for(let key in data) {
          p.append(key, data[key])
        }
        return ins.post(url, p, {headers})
      }
      // 文件提交  multipart/form-data
      if (headers['content-type'] === 'multipart/form-data') {
        const p = new FormData()
        for(let key in data) {
          p.append(key, data[key])
        }
        return ins.post(url, p, {headers})
      }
      // 默认 application/json
      return ins.post(url, data)
    case 'PUT': // 修改数据 --- 所有的数据的更新
      return ins.put(url, data)
    case 'DELETE': // 删除数据
      return ins.delete(url, {data})
    case 'PATCH': // 更新局部资源
      return ins.patch(url, data)
    default:
      return ins(config)
  }
}

```



### 十三作业：

需要自主完成管理员列表的相关功能

adminname

password

role    2 超级管理员。1管理员



## 

## 十五.富文本编辑器的使用

https://braft.margox.cn/demos/basic

```
cnpm i braft-editor -S
```

创建页面以及路由

### 1.创建表单页面

Src/pages/editor/Rich.tsx

```tsx
import * as React from 'react';
// 引入编辑器组件
import BraftEditor from 'braft-editor'
// 引入编辑器样式
import 'braft-editor/dist/index.css'

interface IRichProps {
}

const Rich: React.FunctionComponent<IRichProps> = (props) => {
  const [ editorState, setEditorState ] = React.useState('')
  const [ text, setText ] = React.useState('')
  const handleEditorChange = (editorState) => {
    console.log('change', editorState )
    setEditorState(editorState)
  }
  const submitContent = () => {
    const html = editorState.toHTML()
    setText(html)
    console.log(html)
  }
  return (
    <div>
      <BraftEditor
        value={editorState}
        onChange={handleEditorChange}
        onSave={submitContent}
      />
      asdasdasdsads
      <div dangerouslySetInnerHTML = {{ __html: text}} style={{ height: 600}}>
      </div>
    </div>
  );
};

export default Rich;

```

```
{
    path: '/rich',
    icon: 'HomeOutlined',
    name: '富文本编辑器', // 如果需要出现在左侧的菜单栏
    component: '@/pages/editor/Rich'
  },
```

可以使用markdown编辑器

https://gitee.com/uiw/react-markdown-editor#https://github.com/jaywcjlove/react-monacoeditor

s r c/pages/editor/MarkDown.tsx

```tsx
import * as React from 'react';
import MarkdownEditor from '@uiw/react-markdown-editor';
interface IMarkDownProps {
}

const MarkDown: React.FunctionComponent<IMarkDownProps> = (props) => {
  const [markdown, setMarkdown] = React.useState('')
  const updateMarkdown = (editor,data,value) => {
    console.log(editor)
    console.log(data)
    console.log(value)
    setMarkdown(value)

  }
  return (
    <div>
      markdown编辑器
      <MarkdownEditor
        value={markdown}
        onChange={updateMarkdown}
        height = { 600 }
      />
      { markdown }
    </div>
  )
};

export default MarkDown;

```

```ts
export interface IChildRoute {
  path: string
  name?: string
  component: string,
  wrappers?: string[]
}
export interface IBestAFSRoute {
  routes?: IChildRoute[] // Array<IChildRoute>
  path: string
  redirect?: string,
  exact?: boolean,
  component?: string
  name?: string // 兼容此写法
  icon?: string
  // 更多功能查看
  // https://beta-pro.ant.design/docs/advanced-menu
  // ---
  // 新页面打开
  target?: string
  // 不展示顶栏
  headerRender?: boolean
  // 不展示页脚
  footerRender?: boolean
  // 不展示菜单
  menuRender?: boolean
  // 不展示菜单顶栏
  menuHeaderRender?: boolean
  // 权限配置，需要与 plugin-access 插件配合使用
  access?: string
  // 隐藏子菜单
  hideChildrenInMenu?: boolean
  // 隐藏自己和子菜单
  hideInMenu?: boolean
  // 在面包屑中隐藏
  hideInBreadcrumb?: boolean
  // 子项往上提，仍旧展示,
  flatMenu?: boolean,
  wrappers?: string[],
  role?: number
}
const routes: IBestAFSRoute[] = [
  ....
  {
    path: '/mk',
    icon: 'HomeOutlined',
    name: 'markdown编辑器', // 如果需要出现在左侧的菜单栏
    component: '@/pages/editor/MarkDown'
  },
]

export default routes

```



## 十六.数据可视化

ECharts https://echarts.apache.org/zh/index.html

### 1.html中使用echarts

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>ECharts</title>
    <!-- 引入 echarts.js -->
    <script src="echarts.min.js"></script>
</head>
<body>
    <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
    <div id="main" style="width: 600px;height:400px;"></div>
    <script type="text/javascript">
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('main'));

        // 指定图表的配置项和数据
        var data = genData(50);

option = {
    title: {
        text: '同名数量统计',
        subtext: '纯属虚构',
        left: 'center'
    },
    tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)'
    },
    legend: {
        type: 'scroll',
        orient: 'vertical',
        right: 10,
        top: 20,
        bottom: 20,
        data: data.legendData,

        selected: data.selected
    },
    series: [
        {
            name: '姓名',
            type: 'pie',
            radius: '55%',
            center: ['40%', '50%'],
            data: data.seriesData,
            emphasis: {
                itemStyle: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
};




function genData(count) {
    var nameList = [
        '赵', '钱', '孙', '李', '周', '吴', '郑', '王', '冯', '陈', '褚', '卫', '蒋', '沈', '韩', '杨', '朱', '秦', '尤', '许', '何', '吕', '施', '张', '孔', '曹', '严', '华', '金', '魏', '陶', '姜', '戚', '谢', '邹', '喻', '柏', '水', '窦', '章', '云', '苏', '潘', '葛', '奚', '范', '彭', '郎', '鲁', '韦', '昌', '马', '苗', '凤', '花', '方', '俞', '任', '袁', '柳', '酆', '鲍', '史', '唐', '费', '廉', '岑', '薛', '雷', '贺', '倪', '汤', '滕', '殷', '罗', '毕', '郝', '邬', '安', '常', '乐', '于', '时', '傅', '皮', '卞', '齐', '康', '伍', '余', '元', '卜', '顾', '孟', '平', '黄', '和', '穆', '萧', '尹', '姚', '邵', '湛', '汪', '祁', '毛', '禹', '狄', '米', '贝', '明', '臧', '计', '伏', '成', '戴', '谈', '宋', '茅', '庞', '熊', '纪', '舒', '屈', '项', '祝', '董', '梁', '杜', '阮', '蓝', '闵', '席', '季', '麻', '强', '贾', '路', '娄', '危'
    ];
    var legendData = [];
    var seriesData = [];
    for (var i = 0; i < count; i++) {
        var name = Math.random() > 0.65
            ? makeWord(4, 1) + '·' + makeWord(3, 0)
            : makeWord(2, 1);
        legendData.push(name);
        seriesData.push({
            name: name,
            value: Math.round(Math.random() * 100000)
        });
    }

    return {
        legendData: legendData,
        seriesData: seriesData
    };

    function makeWord(max, min) {
        var nameLen = Math.ceil(Math.random() * max + min);
        var name = [];
        for (var i = 0; i < nameLen; i++) {
            name.push(nameList[Math.round(Math.random() * nameList.length - 1)]);
        }
        return name.join('');
    }
}

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
    </script>
</body>
</html>
```

### 2.在react中使用echarts

https://echarts.apache.org/zh/tutorial.html#%E5%9C%A8%E6%89%93%E5%8C%85%E7%8E%AF%E5%A2%83%E4%B8%AD%E4%BD%BF%E7%94%A8%20ECharts

```
cnpm install echarts --save
```

Src/pages/data-manager/echarts.tsx

```tsx
import React from 'react';
import * as echarts from 'echarts';

export interface IAppProps {
}

export interface IAppState {
  myChart: any,
  option: any
}

export default class App extends React.Component<IAppProps, IAppState> {
  constructor(props: IAppProps) {
    super(props);

    this.state = {
      myChart: null,
      option: {}
    }
  }
  componentDidMount () {
    this.setState({
      myChart: echarts.init((document.getElementById('box') as any)),
      option: { // https://echarts.apache.org/zh/option.html#title
        title: {
          text: 'ECharts 入门示例',
          subtext: 'echarts简单',
          left: '50%',
          textAlign: 'center',
          // show: false
          link: 'https://www.baidu.com',
          textStyle: {
            color: '#f66'
          }
        },
        legend: {
          bottom: 10
        },
        grid: {
          show: true
        },
        brush: {
          xAxisIndex: 'all',
          brushLink: 'all',
          outOfBrush: {
              colorAlpha: 0.1
          }
      },
        tooltip: {},
        axisPointer: {
          show: true,
          link: {xAxisIndex: 'all'},
          label: {
              backgroundColor: '#777'
          }
        },
        toolbox: {
          show: true,
        showTitle: false, // 隐藏默认文字，否则两者位置会重叠
        feature: {
            saveAsImage: {
                show: true,
                title: 'Save As Image'
            },
            dataView: {
                show: true,
                title: 'Data View'
            },
        },
        tooltip: { // 和 option.tooltip 的配置项相同
            show: true,
            formatter: function (param) {
                return '<div>' + param.title + '</div>'; // 自定义的 DOM 结构
            },
            backgroundColor: '#222',
            textStyle: {
                fontSize: 12,
            },
            extraCssText: 'box-shadow: 0 0 3px rgba(0, 0, 0, 0.3);' // 自定义的 CSS 样式
        }
        },
        xAxis: {
          data: ['衬衫', '羊毛衫', '雪纺衫', '裤子', '高跟鞋', '袜子']
        },
        dataZoom: {},
        yAxis: {},
        // darkMode: 'dark',
        // backgroundColor: 'rgba(0,0,0, 0.9)',
        // polar: {},
        series: [{
            name: '销量',
            type: 'bar',
            data: [5, 20, 36, 10, 10, 20]
        },
        {
          name: '库存',
          type: 'line',
          data: [15, 120, 136, 110, 110, 120]
      }]
      }
    }, () => {
      this.state.myChart.setOption(this.state.option)
    })
  }
  changeLine = () => {
    this.setState({
      option: {
        title: {
            text: 'ECharts 入门示例'
        },
        tooltip: {},
        xAxis: {
            data: ['衬衫', '羊毛衫', '雪纺衫', '裤子', '高跟鞋', '袜子']
        },
        yAxis: {},
        series: [{
            name: '销量',
            type: 'line',
            data: [5, 20, 36, 10, 10, 20]
        }]
      }
    }, () => {
      this.state.myChart.setOption(this.state.option)
    })
  }
  changeLineAndData = () => {
    this.setState({
      option: {
        title: {
          text: 'ECharts 入门示例'
        },
        tooltip: {},
        xAxis: {
            data: ['衬衫', '羊毛衫', '雪纺衫', '裤子', '高跟鞋', '袜子']
        },
        yAxis: {},
        series: [{
            name: '销量',
            type: 'line',
            data: [15, 25, 16, 50, 70, 120]
        }]
      }
    }, () => {
      this.state.myChart.setOption(this.state.option)
    })
  }
  public render() {
    return (
      <div>
        <h1>echarts案例</h1>
        <button onClick = { this.changeLine }>折线图</button>
        <button onClick = { this.changeLineAndData }>折线图-改变数据</button>
        <div id="box" style={{ width: 1000, height: 800, backgroundColor: '#fff'}}></div>
      </div>
    );
  }
}


```

```ts
{
    path: '/data',
    name: '数据可视化',
    icon: 'PictureOutlined',
    routes: [
      {
        path: '/data/echarts',
        name: 'echarts数据可视化',
        component: '@/pages/data-manager/echarts',
      }
    ]
  }
```

```tsx
import React from 'react'
import * as echarts from 'echarts'
import { useMount } from 'ahooks';
export default function Echarts() {
  let myChart = ''
  const [option, setOption] = React.useState({
    xAxis: {
      type: 'category',
      data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
    },
    yAxis: {
      type: 'value'
    },
    series: [{
      data: [150, 230, 224, 218, 135, 147, 260],
      type: 'line'
    }]
  })
  useMount(() => {

    draw()
  })
  function draw (){
    myChart = echarts.init(document.getElementById('main'))
    myChart.setOption(option)
  }
  function changeData1 () {
    setOption({
      xAxis: {
        type: 'category',
        data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
      },
      yAxis: {
        type: 'value'
      },
      series: [{
        data: [150, 230, 224, 218, 135, 147, 260],
        type: 'bar'
      }]
    })
    draw()
  }
  function changeData2 () {
    setOption({
      xAxis: {
        type: 'category',
        data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
      },
      yAxis: {
        type: 'value'
      },
      series: [{
        data: [150, 230, 224, 218, 135, 147, 260],
        type: 'pie'
      }]
    })
    draw()
  }
  return (
    <div>
      <button onClick = { changeData1 }>改变数据-柱状图</button>
      <button onClick = { changeData2 }>改变数据-饼状图</button>
      <div id="main" style={{ width: '600px',height: '500px'}}></div>
    </div>
  );
}

```



### 3.其余的数据可视化工具

highcharts https://www.highcharts.com.cn/ 使用方法类似于echarts，但是。。。。

antv https://antv.vision/   https://antv.gitee.io/zh/

react - https://charts.ant.design/

### 在 React / Vue / Angular 中使用 G2

基于 AntV 技术栈还有许多优秀的项目，在 React 环境下使用 G2，我们推荐使用 Ant Design Charts，BizCharts 和 Viser。这三个产品都是基于 G2 的 React 版本封装，使用体验更符合 React 技术栈的习惯，他们都与 AntV 有着紧密的协同，他们很快也将同步开源和发布基于 G2 4.0 的版本。Viser 除了 React 外，还提供了 Vue 和 Angular 不同的分发版本。

- Ant Design Charts 地址：[https://charts.ant.design](https://charts.ant.design/)
- BizCharts 地址：[https://bizcharts.net](https://bizcharts.net/)
- Viser 地址：https://viserjs.github.io/



### 自己写

D3.js自定义数据可视化



Access-token. Refresh-token

80. vue里面有一个MVVM的模型，对此怎么理解？它有什么作用？
81. 双向数据绑定怎么实现？
82. 立即反映到视图层或模型层是怎么做到的？
83. 简单实现一下双向数据绑定，用JS去实现，这个要怎么做？
84. 它可以监听哪些事件？
85. 有自己封装过vue组件吗？
86. Vue组件之间参数传递
87. 遇到跨域问题，是怎么处理的？
88. 页面上有比较复杂的数据结构，页面的表格可能比较复杂，当我要修改其中内容提交的时候，需要将这些数据提交上去，这个表格可能是一个list，里面是个对象，对象里可能又有一些数组，这样多层嵌套的数据结构，页面处理的时候需要怎么做？需要注意些什么？（比如调一些接口，服务端接口去查询，然后给返回了一个这样的列表，你要再页面上去展示，需要做些什么？）
89. 正常显示一个列表是怎么显示的？通过什么组件？
90. 开发过程中对组件库不熟，平时用到一个组件的时候怎么办呢？去哪找这个组件？
91. 平常你在工作当中你是怎么去找我现在要用一个什么组件来完成现在的工作，因为你说你没封装过组件，那工作当中就是找现成的组件用，那你是从哪找的？
92. 举个例子讲讲你平常工作中某一个业务的开发过程，当时你拿到了一个什么样的需求，你是怎么去理解这个需求的？怎么去跟后端开发人员以及其他相关人员交互的，最后怎么去完成这个工作？
93. 这些接口是你告诉后端你需要什么接口，还是后端人员告诉你他有什么接口，然后你来想页面上怎么用？
94. 需求给过来是个什么样子，是文档还是原型图，还是一些其他的形式呢？
95. 从原型图效果图到实际的页面，这中间切图的工作也是你们做的吗？
96. 你的首页和详情页是两个不同的页面，你们是单页面模式还是多页面模式？（我回答的单页面）
97. 那首页跟详情页你是怎么路由的
98. 假如在首页定义了一个定时器，（你是单页面），跳转到详情页面去了后，定时器还会继续生效吗？是还在后台继续运行还是已经被销毁了？
99. 会在生命周期哪个阶段销毁？
100. destroy这个阶段其实有两个方法会被调用，beforeDestroy和destroyed，beforeDestroy里面一般会做什么事情？
说一下你工作中处理过最难的一个前端的问题是什么？

