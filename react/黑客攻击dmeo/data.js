

let lineData=[
    {
        fromName: '北京',
        toName: 'Lakshadweep',
        coords: [[116.46,39.92], [72.7811,11.2249]]
    },
    {
        fromName:'北京',
        toName:'British Columbia',
        coords:[[116.46,39.92],[-124.662,54.6943]]
    },

    {   fromName:'北京',
        toName:'北京',
        coords:[[116.46,39.92],[116.46,39.92]]
    }
]
let scatterData =[
    {
        name: 'BeiJing', //城市名称
        value:[116.46,39.92,180],//城市经纬度信息
        symbolSize:[4,4]
    },
    {
        name: 'Lakshadweep',
        value:[72.7811,11.2249,130]
    },
    {
        name:'British Columbia',
        value:[-124.662,54.6943,200]
    }
]

