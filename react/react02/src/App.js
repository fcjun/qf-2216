import React from "react";
import Event from "./demo/Event.jsx";
import Form from "./demo/Form.jsx";
// import PropsDemo from "./demo/nest/PropsDemo.jsx"
import ContextDemo from "./demo/nest/ContextDemo.jsx";
import RefDemo from "./demo/ref/RefDemo.jsx";
import ThemeContext from "./demo/nest/ThemeContext.js";
import SlotDemo from "./demo/slot/Slot.jsx";
import LifeCycle from "./demo/lifecycle/Index.jsx";
import TabDemo from "./demo/lifecycle/Tab.jsx";
console.log(ThemeContext);
class App extends React.Component {
  state = {
    us:123,
    theme: "red"
  }
  changeUs = (payload) => {
    this.setState({ us: payload})
  }
  render() {
    const { us,theme } = this.state
    return (
      <div>
        <h1>react 02 </h1>
        {/* <Table></Table> */}
        {/* <Event/> */}
        {/* <Form /> */}
        {/* <PropsDemo></PropsDemo> */}
        {/* <ThemeContext.Provider value={{ us, theme,changeUs:this.changeUs }}>
          <ContextDemo></ContextDemo>
        </ThemeContext.Provider> */}
        {/* <RefDemo></RefDemo> */}
        {/* <SlotDemo>
          <div>123</div>
          <div>3333</div>
        </SlotDemo> */}
        {/* <LifeCycle></LifeCycle> */}
        <TabDemo></TabDemo>
      </div>
    );
  }
  // jsx中只允许有一个根元素
}

export default App;

/*


*/
