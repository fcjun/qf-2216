import React from "react";

class SlotDemo extends React.Component {
  state = {};
  componentDidMount() {
    console.log(this)
  }
  render() {
  
    return (
      <div>
        <h1>Slot </h1>
        { this.props.children[1] }
      </div>
    );
  }
}

export default SlotDemo;

/*
在react 组件标签内部的内容默认不渲染
但是会将组件标签中的内容放到 组件props.children 
*/
