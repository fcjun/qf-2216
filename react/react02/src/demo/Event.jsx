import React from "react";

class Event extends React.Component {
  state = {
    age: 16
  }
  add = (num,e) => {
    console.log("点我",this,e,num)
    this.setState({ age: ++ this.state.age})
  }
  render() {
    return (
      <div>
        <h1>事件 </h1>
        {this.state.age}
        {/* <button onClick={() => {
          console.log("点我",this)
          this.setState({ age: ++ this.state.age})
        }}>add</button> */}
        {/* 事件函数不能直接+ () 相当于立即执行 */}
        {/* <button onClick={this.add()}>add</button> */}
        {/* <button onClick={this.add.bind(null,5)}> add</button> */}
        <button onClick={(e) => {
          // 事件对象一定是紧跟事件的处理函数
          console.log(e)
          this.add(666,e)
        }}> add</button>
      </div>
    );
  }
  // jsx中只允许有一个根元素
}

export default Event;

/*
https://note.youdao.com/s/I7fSsefh
react 合成事件
原生js onclick  react onClick
在写事件方法的时候如果需要this 记得使用 => 函数
事件函数中默认参数是事件对象
事件函数不能直接+ () 相当于立即执行
有其他参数的时候默认的事件参数会放到参数的最后方
*/
