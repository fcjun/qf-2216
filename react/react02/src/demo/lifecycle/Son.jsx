import React from "react";


class Son extends React.PureComponent {
  p = React.createRef()
  state = {
    name: "韩梅梅"
  }
  render() {
    console.log("son render");
    return (
      <div>
        <h1>子组件</h1>
        <p ref={this.p}>{ this.state.name}</p>
        <button onClick={() => {
          this.setState({ name : "李雷雷" })
        }}>changeName</button>
      </div>
    );
  }
  getSnapshotBeforeUpdate() {
    console.log("更新之前")
    // 不能单独使用
    // 数据新的  dom 老的 
    // 保存数据
    // 返回的数据可以在更新后进行获取
    console.log(this.state.name)
    console.log(this.p.current)
    console.log(this.p.current.innerHTML)
    return { us:123,ps:456}
  }
  componentDidUpdate(props,state,snapshot) {
    console.log("更新完成")
    // props state 是更新前的数据
    // this.porps  this.state  是更新后的数据
    // 数据和dom 都是最新的
    // snapshot 是更新前的生命周期返回的数据
    // 小心死循环
    // this.setState({ name: Math.random()})
  }
}

export default Son;
/*
控制组件的render 
shouldComponetUpdate  数据多的时候逻辑不好处理
PureComponent  内部自带shouldComponetUpdate 以浅比较的方式进行对比
*/