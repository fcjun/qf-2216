import React from "react";
import Son from "./Son.jsx";

class LifeCycle extends React.Component {
  state = {
    movie: "午夜凶铃",
    age: 1
  };
  render() {
  
    return ( 
      <div>
        <h1>Slot </h1>
        {this.state.age }
        <button onClick={() => {
          this.setState({ age: ++ this.state.age})
        } }>changeMovie</button>
        <hr />
        <Son movie={this.state.movie}></Son>
      </div>
    );
  }
}

export default LifeCycle;

/*
https://projects.wojtekmaj.pl/react-lifecycle-methods-diagram/
*/
