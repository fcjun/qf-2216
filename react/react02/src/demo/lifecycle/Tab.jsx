import React from "react";

class TabDemo extends React.Component {
  state = {
    movies: ["动作", "喜剧", "恐怖", "科幻", "爱情"],
    selIndex: "动作",
    list: ["动a","动b"]
  };

  // static getDerivedStateFromProps(props, state) {
  //   console.log(state.selIndex)
  //    setTimeout(() => {
  //     return { list: ["1",2,3]}
  //    },1000)
  //   return null;
  // } 
  componentDidUpdate(props,state) {
    if(state.selIndex !== this.state.selIndex) {
      setTimeout(() => {
        this.setState({ list: ["a",'b','c']})
      },1000)
    }
  }
  render() {
    return (
      <div>
        <h1>Tab选项卡 </h1>
        <div>
          <ul>
            {(this.state.movies || []).map((item, index) => {
              return (
                <li
                  onClick={() => {
                    this.setState({ selIndex:item})
                  }}
                  key={index}
                  style={{
                    background: item === this.state.selIndex ? "red" : "green",
                  }}
                >
                  {item}
                </li>
              );
            })}
          </ul>
          <div>
            <h1>列表</h1>
            {this.state.list}
          </div>
        </div>
      </div>
    );
  }
}

export default TabDemo;

/*
https://projects.wojtekmaj.pl/react-lifecycle-methods-diagram/
*/
