import React from "react";

// class Son extends React.Component {
//   state = {
//     name: "韩梅梅"
//   }
//   shouldComponentUpdate(props, state) {
//     // 根据return的值判断是否需要更新
//     // 参数是修改后的state 和 props 
//     // this.state 和 porps 是修改前的
//     console.log("shouldComponentUpdate")
//     console.log("props",props)
//     console.log("state",state)
//     console.log("this.props",this.props)
//     console.log("this.state", this.state)
//     if(this.state.name === state.name) {
//       return false
//     }
//     return true
//   }
//   render() {
//     console.log("son render");
//     return (
//       <div>
//         <h1>子组件</h1>
//         {this.props.movie}
//         { this.state.name}
//         <button onClick={() => {
//           this.setState({ name : "李雷雷" })
//         }}>changeName</button>
//       </div>
//     );
//   }
// }

class Son extends React.PureComponent {
  state = {
    name: "韩梅梅"
  }
  render() {
    console.log("son render");
    return (
      <div>
        <h1>子组件</h1>
        {this.props.movie}
        { this.state.name}
        <button onClick={() => {
          this.setState({ name : "李雷雷" })
        }}>changeName</button>
      </div>
    );
  }
}

export default Son;
/*
控制组件的render 
shouldComponetUpdate  数据多的时候逻辑不好处理
PureComponent  内部自带shouldComponetUpdate 以浅比较的方式进行对比
*/