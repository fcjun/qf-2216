import React from "react";

class Son extends React.Component {
  state = {
    age: 12,
  };
  static getDerivedStateFromProps(newprops, newState) {
    // 数据发生改变就会触发 props state
    // 是一个静态方法没有this
    // 返回一个null 或者一个对象
    // 返回的数据会添加到State里
     console.log("数据发生改变就会触发 props state")
     console.log('newProps',newprops)
     console.log("newState",newState)
     return {hehe: "韩梅梅",...newprops};
  }
  render() {
    console.log("render",this);
    return (
      <div>
        <h1>子组件</h1>
        {this.state.age}
        {this.state.hehe}
        <button
          onClick={() => {
            this.setState({ age: ++this.state.age });
          }}
        >
          add
        </button>
      </div>
    );
  }
}

/*
 

*/

export default Son;
