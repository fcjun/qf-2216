import React from "react";

class Son extends React.Component {
  constructor() {
    // 调用父类构造函数
    super();
    console.log("构造函数")
    this.state = {
      us:123,
    }
    this.state.us = 7788
    // 构造函数中修改数据 不需要 setState 不会多次render
  }

  // state = {
  //   us: 7788
  // }
  // UNSAFE_componentWillMount() {
  //   // 修改数据不需要 setState
  //   console.log("组件挂载之前") 
  //   this.state.us = 9999
  // }
  componentDidMount() {
    // 获取更新后的dom元素
    // 在生命周期做网络请求
    // dom的初始化操作 echart swiper
    console.log("组件挂载")
    this.setState({ us: 999 })
  }
  render() {
    console.log("render")
    return (
      <div>
        <h1>子组件</h1>
        {this.state.us}
      </div>
    );
  }
}

/*
  即将废弃的生命周期, 不能和新的生命周期混用
  UNSAFE_componentWillMount
  UNSAFE_componentWillReceiveProps
  UNSAFE_componentWillUpdate

  render 前的生命周期 修改数据都不需要 setState 
  render 后的生命周期 修改数据都需要 setState

*/ 

export default Son;