import React from "react";
import ThemeContext from "./ThemeContext";
// class Son2 extends React.Component {
//   componentDidMount() {
//     console.log("son2", this);
//   }
//   render() {
//     return (
//       <div>
//         <h1>son2</h1>
//         {/* 通过消费者使用上下文数据 */}
//         <ThemeContext.Consumer>
//           {(value) => {
//             console.log("消费者", value);
//             return (
//               <div>
//                 <p>{value.us}</p>
//                 <button
//                   onClick={() => {
//                     value.changeUs(777);
//                   }}
//                 >
//                   changeUs
//                 </button>
//               </div>
//             );
//           }}
//         </ThemeContext.Consumer>
//       </div>
//     );
//   }
// }
// export default Son2;

class Son2 extends React.Component {
  componentDidMount() {
    console.log("son2", this);
  }
  render() {
    return (
      <div>
        <h1>son2</h1>
        <p>{this.context.us}</p>
        <p>{this.context.theme}</p>
        <button onClick={() => {
          this.context.changeUs(999)
        }}>changeUs</button>
      </div>
    );
  }
}
export default Son2;
// 将我们provider提供的数据放到 this.context
Son2.contextType = ThemeContext;