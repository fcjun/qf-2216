import React from "react";

class Son extends React.Component {
  componentDidMount() {
    console.log("子组件", this)
  }
  render() {
    console.log("儿子render")
    return (
      <div>
        <h1>son </h1>
        来自爸爸的关爱: { this.props.hehe}
      </div>
    );
  }
}

class PropsDemo extends React.Component {
  state = {
    money: 666 
  }
  
  render() {
    console.log("父亲render")
    return (
      <div>
        <h1>父子通信 </h1>
        <h2>this is father</h2>
        <button onClick={() => {
          this.setState({ money: 999})
        }}>give money</button>
        <hr />
        <Son hehe={this.state.money}></Son>


      </div>
    );
  }

}

/*
父子通信
通过props 自定义属性传递数据
在组件通过props 使用数据
保持单项数据流 props数据不能改只能用

父组件render  子组件一定render? 
*/ 

export default PropsDemo;

