import React from "react";
import Son1 from "./Son1.jsx";
import Son2 from "./Son2.jsx";
class ContextDemo extends React.Component {
  
  render() {
    return (
      <div>
        <h1>context </h1>
        <Son1 ></Son1>
        <Son2 ></Son2>
      </div>
    );
  }

}

/*
跨组件通信

1. 创建上下文对象
2. 在父组件上通过provider 提供数据
3. 在子组件里通过 consumer 消费数据
4. 优化
*/ 

export default ContextDemo;

