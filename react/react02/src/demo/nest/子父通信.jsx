import React from "react";

class Son extends React.Component {
  componentDidMount() {
    console.log("子组件", this)
  }
  render() {
    console.log("儿子render")
    return (
      <div>
        <h1>son </h1>
        <button onClick={() => {
          this.props.buy(5)
        }}>buy</button>
      </div>
    );
  }
}

class PropsDemo extends React.Component {
  state = {
    money: 20,
  }
  buybuybuy = (num) => {
    console.log(num)
    this.setState({ money: --this.state.money})
  }
  render() {
    console.log("父亲render")
    return (
      <div>
        <h1>子父通信 </h1>
        <h2>this is father</h2>
        <p>霸霸的钱包: {this.state.money}</p>
        <Son buy={this.buybuybuy}></Son>


      </div>
    );
  }

}

/*
子父通信
将父组件的修改数据方法传给子组件调用
*/ 

export default PropsDemo;

