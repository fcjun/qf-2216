import React from "react";
import "./test.less";
class Son1 extends React.Component {
  render() {

    return (
      <div>
        <h1>son1</h1>
        <button onClick={() => {
          this.props.son1Toggle()
        }}>toggle</button>
      </div>
    );
  }
}

class Son2 extends React.Component {
  render() {
    console.log(this)
    return (
      <div>
        <h1>son1</h1>
        { this.props.son2Show &&  <div className="test"> </div>}
      
      </div>
    );
  }
}

class PropsDemo extends React.Component {
  state = {
    show: false
  }
  toggle = () => {
    this.setState({ show: !this.state.show})
  }
  render() {
    console.log("父亲render")
    return (
      <div>
        <h1>子父通信 </h1>
        <Son1 son1Toggle={this.toggle}></Son1>
        <Son2 son2Show ={this.state.show}></Son2>


      </div>
    );
  }

}

/*
子父通信
将父组件的修改数据方法传给子组件调用
*/ 

export default PropsDemo;

