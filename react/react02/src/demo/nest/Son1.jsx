import React from "react"
import ThemeContext from "./ThemeContext";
class Son1 extends React.Component {
  componentDidMount() {
    console.log("son2",this)
  }
  render() {

    return (
      <div>
        <h1>son1</h1>
        <ThemeContext.Consumer>
          {
            (value) => {
              return <p>{value.us} {value.theme}</p>
            }
          }
        </ThemeContext.Consumer>
      </div>
    );
  }
}
export default Son1