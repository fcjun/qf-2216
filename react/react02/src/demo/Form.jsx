import React from "react";

class Form extends React.Component {
  state = {
    age: 16,
    string: 123
  }

  render() {
    return (
      <div>
        <h1>受控组件 和 非受控组件 </h1>
        {/* 受控组件  数据和state关联*/}
         <input type="text" value={this.state.age} onChange={(e) => {
          console.log(e.target.value)
          this.setState({ age: e.target.value })
         }}/>
        {/* 非受控组件  数据从dom获取 */}
         <input type="text" defaultValue={this.state.string} ref="hehe"/>
         <button onClick={() => {
          console.log(this.refs.hehe.value)
         }}>getValue</button>

      </div>
    );
  }
  // jsx中只允许有一个根元素
}

export default Form;

/*
表单里的value 和 state数据进行关联 （被state值控制） 受控组件，不被控制叫非受控组件

*/
