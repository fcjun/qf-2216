import React, { Component } from "react";
import "./table.less";
class TableDemo extends Component {
  state = {
    arr: [[1], [1, 2], [1, 2, 3]], // 循环乘法表的数组
    colors: [['red','blue'],['pink','yellow'],['red','pink']], // 渲染下拉菜单控制隔行变色
    selColor:0, // 选中的下拉菜单
    selTr: 0, // 鼠标滑过选中的行
    setTd: 0, // 鼠标滑过选中的列
  };
  // 渲染表格
  renderTd(trIndex,trItem) {
    // 将td的渲染单独的抽离出来
    const { selTr,selTd } = this.state;

    return trItem.map((tdItem,tdIndex)=> {
      // 鼠标滑过事件 记录滑过的行列
      // 通过对比滑过的行列 和 td的行列判断那个需要特效
      return <td 
              style={  {opacity: (selTr === trIndex && selTd === tdIndex)?0:1}  }
              onMouseOver={this.move.bind(null,trIndex,tdIndex)} 
              key={tdIndex}>{trIndex + 1} * {tdItem} = { (trIndex +1 ) * tdItem}
            </td>
    })
  }
  move = (trIndex,tdIndex) => {
    console.log("鼠标滑过", trIndex, tdIndex)
    this.setState({selTr: trIndex,selTd: tdIndex})
  }
  // 菜单切换
  colorChange = (e) => {
    console.log(e.target.value)
    this.setState({ selColor: e.target.value});
  }
  render() {
    const { arr,colors,selColor} = this.state;
    return (
      <div>
        <h2>9 * 9</h2>
        <select onChange={this.colorChange}>
          {
            (colors||[]).map((item,index) => {
              return <option key={index} value={index}>{item}</option>
            })
          }
          {/* <option value={0}>0</option>
          <option value={1}>1</option> */}
        </select>
        {/* table */}
        <table>
          <tbody>
            {(arr || []).map((trItem, trIndex) => {
              return (
                <tr key={trIndex} className={trIndex%2?colors[selColor][0]:colors[selColor][1]}>
                  {/* 循环td */}
                 {/* {
                  (trItem || []).map((tdItem,tdIndex) => {
                    return (
                      <td key={tdIndex}>{trIndex + 1} * {tdItem} = { (trIndex +1 ) * tdItem}</td>
                    )
                  })
                 } */}
                 { this.renderTd(trIndex,trItem)}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
export default TableDemo;

/*
1. 9*9
2. 隔行变色
3. 滑过特效
4. 控制隔行变色

*/
