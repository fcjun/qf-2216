import React from "react";

class Son1 extends React.Component {
  state = {
    name: "韩梅梅",
  };
  changeName = () => {
    this.setState({ name: "李雷雷" });
  };
  render() {
    return (
      <div>
        <h1>这里是子组件 </h1>

        {this.state.name}
      </div>
    );
  }
  // jsx中只允许有一个根元素
}

class RefDemo extends React.Component {
  xixi = React.createRef();
  hehe = React.createRef();
  son = React.createRef();
  render() {
    return (
      <div>
        <h1>Ref </h1>
        <div ref={this.hehe}>hehe</div>
        <div ref={this.xixi}>xixi</div>
        <button
          onClick={() => {
            console.log(this);
            console.log(this.xixi.current)
            console.log(this.hehe.current)
            // 调用子组件的方法
            this.son.current.changeName()
          }}
        >
          获取dom元素
        </button>
        <hr />
        <Son1 ref={this.son}></Son1>
      </div>
    );
  }
  // jsx中只允许有一个根元素
}

export default RefDemo;

/*
1. 创建一个引用类型的数据
2. 可以绑定一个dom元素
3. 可以绑定一个类组件 获取得到类组件的一切 state 方法 可以实现父子通信
4. 可以绑定一个函数组件

1. 创建ref的对象  hehe = React.createRef();
2. 和dom元素 组件做绑定  <div ref={this.hehe}></div>
3. 通过变量里的 current 获取dom 和组件实例  this.hehe.current
*/
