import React from "react";

class Son1 extends React.Component {
  state = {
    name: "韩梅梅",
  };
  changeName = () => {
    this.setState({ name: "李雷雷" });
  };
  render() {
    return (
      <div>
        <h1>这里是子组件 </h1>

        {this.state.name}
      </div>
    );
  }
  // jsx中只允许有一个根元素
}

class RefDemo extends React.Component {
  render() {
    return (
      <div>
        <h1>Ref </h1>
        <div ref="hehe">hehe</div>
        <div ref="xixi">xixi</div>
        <button
          onClick={() => {
            console.log(this);
            console.log(this.refs.son.state.name);
            // 调用子组件的方法
            this.refs.son.changeName()
          }}
        >
          获取dom元素
        </button>
        <hr />
        <Son1 ref="son"></Son1>
      </div>
    );
  }
  // jsx中只允许有一个根元素
}

export default RefDemo;

/*
1. 创建一个引用类型的数据
2. 可以绑定一个dom元素
3. 可以绑定一个类组件 获取得到类组件的一切 state 方法 可以实现父子通信
4. 可以绑定一个函数组件

*/
