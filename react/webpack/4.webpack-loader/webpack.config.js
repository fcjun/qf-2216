const path = require("path");

module.exports = {
  mode: "production", //"development",//production
  entry: "./src/main.js",
  output: {
    //出口
    path: path.resolve(__dirname, "dist"),
    filename: "index.js",
    // hash 是一个变量 将源代码产生一个hash字符串内容不变 hash串不变
    // name: 多入口的key值
  },
  //处理特殊模块
  module: {
    rules: [
      // 处理css css-loader style-loader loader的顺序是从后往前
      { test: /\.css$/, use: ["style-loader","css-loader"] },
      // 处理less
      { test: /\.less$/,use:["style-loader","css-loader","less-loader"]},
      // 处理图片资源  webpack4 file-loader path-loader  webpack5直接可以使用
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/inline',
      }
    ],
  },
};
// index_8f66a8b08b03b95c3a12.js
// index_8f66a8b08b03b95c3a12.js
// index_076de6cb258395e508a7.js
// 浏览器缓存策略 协商缓存 强缓存
