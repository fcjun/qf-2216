const path = require('path');

module.exports = {
  mode: "production",//"development",//production
  entry: './src/main.js', // 入口
  output: { //出口
    path: path.resolve(__dirname, 'dist'),
    filename: 'index_[hash].js', // hash 是一个变量 将源代码产生一个hash字符串内容不变 hash串不变
  },
};
// index_8f66a8b08b03b95c3a12.js
// index_8f66a8b08b03b95c3a12.js
// index_076de6cb258395e508a7.js
// 浏览器缓存策略 协商缓存 强缓存