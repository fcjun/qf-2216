import React from "react";
import ReactDOM from "react-dom";

// react 组件
// jsx javascriptxml 在js中写html
class HelloMessage extends React.Component {
  render() {
    return (
      <div>
        Hello 123123
      </div>
    );
  }
}

// 将HelloMessage 组件渲染到 挂载到id为app的元素伤
ReactDOM.render(
  <HelloMessage name="Taylor" />,
  document.getElementById('app')
);