## 
1. npm run build？打包指令的作用
2. vue-cli,vite,webpack 
## vite webpack  打包器
打包器的作用 将vue，less .... 其他的文件都编译成 js  css html文件 
vite 小而快
webpack 大而全

vue中最新的创建项目的工具
npm init vue@latest  基于vite
vue2 
npm install -g @vue/cli 基于webpack

## 一个cli工具功能
1. 解析sfc xxx.vue 文件
2. 将代码压缩合并
3. 使用预处理语言
4. 开发环境 npm run serve 
5. 生产环境  npm run build

## 参考文档
https://note.youdao.com/s/RYeLQo9D

## 依赖分析
依赖就是文件之间的引入关系，import xxxx from 'xxx.js'
webpack 会以入口文件作为起点，分析所有的依赖文件进行打包

## 在webpack中一切万物皆模块 所有的文件都可以导入  将特殊的文件png css 处理成js能用的模块
import "xxx.js" 
import "xxx.css"
import "xxx.png"

## 基本使用
npm init --yes 
npm install webpack -D  
npm install webpack-cli -D // webpac-cli 会在运行webpack的时候提示安装

## mode 
生产模式 压缩代码和丑化代码
开发模式 不需要压缩
在package.json 中进行配置
```javascript
  "scripts": {
    "hehe": "webpack --mode development"
  },
```
## 入口文件
默认的入口文件是 是当前路径下的src/index.js


## 出口文件
默认的出口文件  是默认当前路径下的src/main.js

## 默认的配置文件
webpack 命令 会默认的读取 当前路径下的 webpack.config.js文件

## 热跟新机制
在编辑器改完代码后浏览器自动更新