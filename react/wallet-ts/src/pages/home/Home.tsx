import { TabBar } from 'antd-mobile';
import {
  AppOutline,
  UserOutline,
} from 'antd-mobile-icons';
import "./index.less";
// import style from  "./index.module.less";
function Home (props: any) {
  const tabs = [
    {
      key: 'home',
      title: '钱包',
      icon: <AppOutline />,
    },
    {
      key: 'personalCenter',
      title: '我的',
      icon: <UserOutline />,
    },
  ]
  return (
    <div className='home'>
      {props.children}
      <div className='tab'>
       <TabBar >
          {tabs.map(item => (
            <TabBar.Item key={item.key} icon={item.icon} title={item.title} />
          ))}
        </TabBar>
      </div>
    </div>
  )
}
export default Home