import { useHistory } from "react-router-dom"

import img from "../../assets/921671680179_.pic.jpg";
import style from "./guide.module.less";
function Guide () {
  const history  = useHistory();

  return (
    <div className={style.guide}>
      {/* 图片 */}
      <img className={style.topImg} src={img} alt="" />
      {/* 按钮跳转 */}
      <div className={style.btns}>
        <div onClick={() => {
          history.replace("/createidentity")
        }}>
          <p>创建身份</p>
          <p>使用新的多链钱包</p>
        </div>

        <div onClick={() => {
          history.replace("/recoveridentity")
        }}>
          <p>恢复身份</p>
          <p>使用新的多链钱包</p>
        </div>
      </div>
    </div>
  )
}
export default Guide