import { useState } from "react";
import { PullToRefresh, List, InfiniteScroll } from "antd-mobile";
function getNextData() {
  const ret: number[] = [];
  for (let i = 0; i < 18; i++) {
    ret.unshift(i);
  }
  return ret;
}

// function Guide() {
//   const [data, setData] = useState(() => getNextData());
//   return (
//     <div>
//       {/* <h1>下拉刷新 和上拉加载</h1> */}
      // <PullToRefresh
      //   onRefresh={async () => {
      //     const arr = [...data, ...getNextData()]
      //     setData(arr)
      //   }}
      // >
//         <List style={{ minHeight: "100vh" }}>
//           {data.map((item, index) => (
//             <List.Item key={index}>{item}</List.Item>
//           ))}
//         </List>
//       </PullToRefresh>
//     </div>
//   );
// }

function Guide() {
  const [data, setData] = useState([1]);
  const [hasMore, setHasMore]  = useState(true)
  async function loadMore() {
    // setHasMore(false)
    console.log("数据加载")
    const  tmpdata = [...data,...getNextData()];
    const total = 100;
    if(tmpdata.length >=total) {
     return setHasMore(false)
    }
    setData([...data,...getNextData()])

  } 
  return (
    <div>
      <PullToRefresh
        onRefresh={async () => {
          const arr = [...data, ...getNextData()]
          setData(arr)
        }}
      >
        <List style={{ minHeight: "100vh" }}>
          {data.map((item, index) => (
            <List.Item key={index}>{item}</List.Item>
          ))}
        </List>
      </PullToRefresh>
      <InfiniteScroll loadMore={loadMore} hasMore={hasMore} threshold={60}/>
    </div>
  );
}
export default Guide;
