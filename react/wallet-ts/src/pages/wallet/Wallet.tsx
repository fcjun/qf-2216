import React from "react"
import  List from "../../components/FixedSizeList"
interface  Props {
  // name: string,
  [propsName: string]: any
}
interface RowPros {
  style: Object,   //React.CSSProperties,
  index: number,
}

const Row = (props: RowPros) => {
  const {style,index} = props
  return (
    <div style={style} key={index}>
      {index}
    </div>
  )
}
const Wallet:React.FC<Props> = (Props:Props ) => {
  return (
    <div>
      <List
        height={300}
        itemSize={30}
        itemCount={1000}
        width={"100%"}
      >
        { Row }
      </List>
    </div>
  )
}

export default Wallet
