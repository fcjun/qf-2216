// import { createStore,combineReducers } from "redux";
// import Areducer from "./reducer";
// import Breducer from "./bReducer";
// const reducer = combineReducers({
//   a: Areducer,
//   b: Breducer
// })

// export default createStore(reducer)




import { configureStore, } from '@reduxjs/toolkit'
import nameReducer from "./nameSlice";
import ageReducer from "./ageSlice";
export default configureStore({
  reducer: {
    nameModule: nameReducer,
    ageModule: ageReducer
  }
})