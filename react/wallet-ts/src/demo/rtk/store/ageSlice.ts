import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

function getData(){
 return new Promise((resolve,reject) => {
   setTimeout(() =>{
    resolve("hehe")
   },3000)
 })
}

// 在切片中通过createAsyncThunk 创建一个异步的方法
// createAsyncThunk('模块名/方法名')
export const fetchTodos:any = createAsyncThunk('ageModule/fetchTodos', async () => {
  const response = await getData()
  return response
})
const ageSlice = createSlice({
  name: "ageMoudule",
  initialState: {
    age: 16,
  },
  reducers: {
    changeAge(state,action) {
      console.log("改年级")
      const { payload } = action;
      state.age = payload
    },
    // changeAgeSync(...arg): any {
    //   console.log('12313',...arg)
    //   // return (dispatch) => {
    //   //     console.log(dispatch);
    //   // }  
    //   return (dispatch: any) => {
    //     dispatch({type:"changeAge", payload: "66"})
    //   }
    // }
  },
  extraReducers: builder => {
    builder
      .addCase(fetchTodos.pending, (state, action) => {
        // 发起请求 等待状态
        console.log("发起请求")
      })
      .addCase(fetchTodos.fulfilled, (state, action) => {
        // 成功状态
        console.log("请求结束")
        console.log(state, action)
        state.age = action.payload
      })
  }
})

export const {changeAge } = ageSlice.actions;
export default ageSlice.reducer
