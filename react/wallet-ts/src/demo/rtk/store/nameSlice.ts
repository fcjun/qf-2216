import { createSlice } from "@reduxjs/toolkit";

const nameSlice = createSlice({
  name: "nameModule", // 模块名
  initialState: {
    name: "韩梅梅",
    hehe: 123,
  },
  reducers: {
    // 修改数据的方法
    changeName(state, action) {
      console.log("changeName", state, action);
      const { payload } = action;
      state.name = payload;
    },
    hehe() {}
  },
});

export const { changeName } = nameSlice.actions
// 抛出reducer
export default nameSlice.reducer;