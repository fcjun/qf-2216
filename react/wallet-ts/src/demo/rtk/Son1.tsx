
import React, { Component} from "react";
import store from "./store/store";
import {changeName} from "./store/nameSlice" 
class Son1 extends Component {
  componentDidMount() {
    store.subscribe(() => {
      this.setState({})
    })
  }
  render() {
    const state = store.getState();
    console.log(state)
    return (
      <div>
        this is render
        <hr />
        { state.nameModule.name}
        {state.ageModule.age}
        <hr />
        <button onClick={() => {
          store.dispatch(changeName("李雷雷"))
        }}>改名</button>
      </div>
    )
  }
}

export default  Son1