
import {useDispatch, useSelector, useStore} from "react-redux"
import {changeAge, fetchTodos} from "./store/ageSlice"
function Son2() {
  // const store = useStore(); // 快速的获取store对象
  // console.log("store",store)
  // 获取全局状态数据
  const state = useSelector((obj:any) => {
    return { ...obj.ageModule}
  })
  const dispatch = useDispatch();
  console.log(state)
  return (
    <div>
        <h1>Son2</h1>
        {state.age}
        <button onClick={() => {
           dispatch(changeAge(11));

        }}>改年纪</button>
        <hr />
        <button onClick={() => {
           dispatch(fetchTodos());

        }}>改年纪-异步</button>
    </div>
  )
}

export default Son2;
// mapStateToprops mapDispatchToprops
// export default connect(state => state)(Son2);