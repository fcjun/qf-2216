import Son1 from "./Son1";
import store from "./store/store";
import { Provider } from "react-redux"
import Son2 from "./Son2";

console.log(store)
function RtkDemo() {

  return (
    <Provider store={store}>
    <div>
        <h1>RTKDEMO</h1>
        <Son1></Son1>
        <Son2></Son2>
    </div>
    </Provider>
  )
}

export default RtkDemo;