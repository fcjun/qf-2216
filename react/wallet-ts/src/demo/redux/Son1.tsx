import store from "./store/store";
import actionCreatore from "./store/actionCreatore";
import { useEffect,useRef } from "react";
console.log(store);
function Son1() {
  const state = store.getState();
  let unSubscribe = useRef();
  console.log(state);
  // 挂载
  useEffect(() => {
    unSubscribe.current = store.subscribe(() => {
      console.log("更新")
    })
    console.log( unSubscribe.current)
    // return () => {
    //   unSubscribe && unSubscribe();
    // }
  },[])
  // 销毁
  useEffect(() => {
    return () => {
      const fun: any = unSubscribe.current;
      if(!fun) return
      fun();
      console.log("销毁")
    }
  })


  return (
    <div>
        <h1>Son1</h1>
        {state.name}
        <button onClick={() => {
          const action = actionCreatore.changeName("李雷雷")
          store.dispatch(action)
        }}>changeName</button>
    </div>
  )
}

export default Son1;