
interface InitData  {
  name: string,
  age: number,
  [propsName: string]: any
}
interface Action {
  type: string,
  payload: Object|number|String
}

const init:InitData = {
  name: "韩梅梅",
  age: 16
}
function reducer(preState = init, action:Action ): InitData {
  const newData = JSON.parse(JSON.stringify(preState));
  const { type, payload } = action;
  switch (type) {
    case "changeName":
       newData.name = payload;
      break;
    case "changeAge":
      newData.age = payload;
      break
    default:
      break;
  }
  return newData
}

export default reducer;