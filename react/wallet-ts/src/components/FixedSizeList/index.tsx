import React, { Component, createRef, RefObject, CSSProperties } from "react";

//修饰porops的接口
interface Props {
  // [propsName:string]:any
  height: number; // 滚动层的高度
  itemSize: number; // 每一项的高度
  itemCount: number; // 总共有多少项
  width: string; // 容器宽度
  children: (props: any) => JSX.Element;
  // children: (props:any) => React.ReactNode
}
// 修饰 State的接口
interface State {
  start: number,
}

interface Style {
  width: string;
  height: number;
  background: string;
  overflow: string;
}

export class FixedSizeList extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
  }

  state: State = {
    start: 0
  }

  // 给容器元素添加滚动监听
  containRef: RefObject<HTMLDivElement> = createRef();

  scrollHandler = () => {
    let scrollTop = this.containRef.current?.scrollTop || 0;
    const start = Math.floor(scrollTop/this.props.itemSize);
    // start 是滚动过的个数
    console.log("滚起来了", start);
    this.setState({ start })
  };
  componentDidMount() {
    // 添加滚动事件监听
    if (!this.containRef.current) return false;
    this.containRef.current.addEventListener("scroll", this.scrollHandler);
  }
  componentWillUnmount() {
    // 添加滚动事件监听销毁
    this.containRef.current?.removeEventListener("scroll", this.scrollHandler);
  }
  render() {
    const { height, children, itemCount, itemSize } = this.props;
    const containStyle: CSSProperties = {
      width: "100%",
      height: height,
      background: "red",
      overflow: "auto",
    };
    // 
    let pageSize =  Math.floor(height / itemSize) + 5; // 每一页的数量
    const max = Math.min(itemCount, this.state.start + pageSize)
    const childrens: any = [];
    console.log("this.state.start",this.state.start)
    for (let index = this.state.start; index < max; index++) {
      let itemStyle = {
        height: itemSize,
        width: "100%",
        position: "absolute",
        left: 0,
        top:0,
        fontSize: "20px",
        color: "#fff"
      }
      const style = { ...itemStyle,top: index * itemSize + 'px'}
      // 每一个元素的高度 由外层确定
      childrens.push(<>{children({style, index})}</>);
    }

    return (
      <div ref={this.containRef} style={containStyle}>
        <div style={{ width: "50%",position: "relative",  background: "blue", height: itemSize * itemCount }}>
          {childrens}
        </div>
      </div>
    );
  }
}

export default FixedSizeList;
