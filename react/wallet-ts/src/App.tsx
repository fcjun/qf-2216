import { HashRouter, Switch, Redirect, Route } from "react-router-dom";
import Guide from "./pages/guide/Guide";
import Home from "./pages/home/Home";
import Wallet from "./pages/wallet/Wallet";
import My from "./pages/my/my";
import CreateIdentity from "./pages/createidentity/CreateIdentity";
function App() {
  return (
    <HashRouter>
      <Switch>
        <Route path="/guide" component={Guide}></Route>
        <Route path="/home">
          <Home>
            <Route path="/home/wallet" component={Wallet}></Route>
            <Route path="/home/my" component={My}></Route>
          </Home>
        </Route>
        <Route path="/createidentity" component={CreateIdentity}></Route>
      </Switch>
    </HashRouter>
  );
}

export default App;
