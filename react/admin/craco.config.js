const CracoLessPlugin = require("craco-less");
const path = require("path");
// const resolve = dir => path.resolve(__dirname, dir);
// 将参数路径处理成绝对路径
function resolve(dir) {
  return path.resolve(__dirname, dir);
}
module.exports = {
  devServer: {
    proxy: {
      "/admin": {
        target: "http://www.lrfc.vip:3001",
        // target: "http://localhost:3001",
        changeOrigin:true,
      },
      "/banner": {
        target: "http://www.lrfc.vip:3001",
        changeOrigin:true,
      },
      "/html": {
        target: "http://www.lrfc.vip:3001",
        changeOrigin:true,
      },
      "/asyncExport": {
        target: "http://www.lrfc.vip:3001",
        changeOrigin:true,
      },
      "/menus": {
        target: "http://www.lrfc.vip:3001",
        changeOrigin:true,
      },
      "/rule": {
        target: "http://www.lrfc.vip:3001",
        changeOrigin:true,
      },
      "/role": {
        target: "http://www.lrfc.vip:3001",
        changeOrigin:true,
      },
      "/upload": {
        target: "http://www.lrfc.vip:3001",
        changeOrigin:true,
      }
      
    }
  },
  webpack: {
    alias: {
      // '@': path.resolve(__dirname, "./src"),
      "@": resolve("./src"),
    },
  },
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: { "@primary-color": "#1DA57A" },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
  babel:{  
    // 按需加载antd
    plugins: [
      [   
        "import", 
        {
          "libraryName": "antd",
          "libraryDirectory": "es",
           "style": true 
         }
     ]
    ]
 },
};
