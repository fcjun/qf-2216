const express = require("express");
const mail = require("./utils/mail");
const  app = express();

const codes = {}


// * 实现发送验证码的接口
//   ** 参数获取邮箱
//   ** 产生随机的验证码
//   ** 将验证码发送到对应的邮箱中 ？？
//   ** 将邮箱和验证码绑定保存在内存中

app.get("/sendCode",async(req,res) => {
  const { user } = req.query;
  const code = parseInt(Math.random()*99999);
  const text = `欢迎注册,验证码为:${code},有效期15分钟`
  const status = await mail.send(user,"2216",text)
  codes[user] = code

  if(status) return res.send({code:0,msg: "验证码发送成功"})
  res.send({code:-1,msg: "验证码发送失败"})
})

app.get("/reg",(req,res) => {
  const { user,pass,code} = req.query;
  console.log(codes,user,code)
  if(!codes[user] || codes[user]!=code) {
    return res.send({code:-1,msg: "验证码错误"})
  }
  // 注册 添加数据库
  // 把使用过的邮箱从内存删除
  delete codes[user]
  res.send({msg:"注册成功",code:0})
})
// *  实现注册的验证码
//   ** 获取下 用户名 密码 验证码
//   ** 看下用户名和密码是否匹配
//   ** 如果匹配 入库注册
//   ** 不匹配验证码错误  
app.listen(2333,() => {
  console.log("服务器启动")
})