import {
  MailOutlined,
  SettingOutlined,
  AppstoreOutlined,
} from "@ant-design/icons";
import { Menu } from "antd";
import { useHistory } from "react-router-dom";
import {connect} from "react-redux"
//后端返回的数据和前端数据不一致 数据处理
function handleMenuData(arr) {
  const data = arr.map(item => {
    const { nameCn, path, children} = item
    let childrenData = undefined
    // 只要碰到子元素就递归一波
    if(children && children.length>0) {
      childrenData = handleMenuData(children);
    }
    return {
      label: nameCn,
      key: path,
      children: childrenData,
      icon: <MailOutlined />,
    }
  })
  return data
}

const data = [
  {
    label: "首页",
    key: "/home",
    icon: <MailOutlined />,
  },
  {
    label: "轮播图",
    key: "/banner",
    icon: <MailOutlined />,
    children: [
      {
        label: "轮播图列表",
        key: "/banner/list",
        icon: <MailOutlined />,
      },
      {
        label: "轮播图添加",
        key: "/banner/add",
        icon: <MailOutlined />,
      },
    ],
  },
  {
    label: "活动发布",
    key: "/activepush",
    icon: <MailOutlined />,
  },
  {
    label: "富文本",
    key: "/richeditor",
    icon: <MailOutlined />,
  },
  {
    label: "管理员管理",
    key: "/admin",
    icon: <MailOutlined />,
  },
  {
    label: "导出管理",
    key: "/export",
    icon: <MailOutlined />,
  },
  {
    label: "图表",
    key: "/echart",
    icon: <MailOutlined />,
    children: [
      {
        label: "基础图表",
        key: "/echartbase",
        icon: <MailOutlined />,
      },
      {
        label: "封装图表",
        key: "/echartcmp",
        icon: <MailOutlined />,
      },
      {
        label: "地图",
        key: "/echartmap",
        icon: <MailOutlined />,
      },
    ],
  },
  {
    label: "权限管理",
    key: "/auth",
    icon: <MailOutlined />,
    children: [
      {
        label: "菜单管理",
        key: "/auth/menus",
      },
      {
        label: "规则管理",
        key: "/auth/rules",
      },
      {
        label: "角色管理",
        key: "/auth/roles",
      },
    ],
  },
]
function MyMenu(props) {
  console.log(props)
  const history = useHistory();
  const onClick = (e) => {
    console.log("点到我了", e);
    history.push(e.key);
  };
  const items = handleMenuData(props.menus)
  return (
    <Menu
      onClick={onClick}
      // style={{
      //   width: 256,
      // }}
      mode="vertical"
      items={data}
    />
  );
}
export default connect(state => {
  console.log("侧边栏",state)
  const menus = state.auth.menus
  return { menus }
})(MyMenu);

/*

[
        {
          label: "首页",
          key: "/home",
          icon: <MailOutlined />,
        },
        {
          label: "轮播图",
          key: "/banner",
          icon: <MailOutlined />,
          children: [
            {
              label: "轮播图列表",
              key: "/banner/list",
              icon: <MailOutlined />,
            },
            {
              label: "轮播图添加",
              key: "/banner/add",
              icon: <MailOutlined />,
            },
          ],
        },
        {
          label: "活动发布",
          key: "/activepush",
          icon: <MailOutlined />,
        },
        {
          label: "富文本",
          key: "/richeditor",
          icon: <MailOutlined />,
        },
        {
          label: "管理员管理",
          key: "/admin",
          icon: <MailOutlined />,
        },
        {
          label: "导出管理",
          key: "/export",
          icon: <MailOutlined />,
        },
        {
          label: "图表",
          key: "/echart",
          icon: <MailOutlined />,
          children: [
            {
              label: "基础图表",
              key: "/echartbase",
              icon: <MailOutlined />,
            },
            {
              label: "封装图表",
              key: "/echartcmp",
              icon: <MailOutlined />,
            },
            {
              label: "地图",
              key: "/echartmap",
              icon: <MailOutlined />,
            },
          ],
        },
        {
          label: "权限管理",
          key: "/auth",
          icon: <MailOutlined />,
          children: [
            {
              label: "菜单管理",
              key: "/auth/menus",
            },
            {
              label: "规则管理",
              key: "/auth/rules",
            },
            {
              label: "角色管理",
              key: "/auth/roles",
            },
          ],
        },
      ]
*/ 