import { createStore, combineReducers } from "redux"
import auth from "./reducer/auth";
const reducer = combineReducers({
  auth
})
export default createStore(reducer)