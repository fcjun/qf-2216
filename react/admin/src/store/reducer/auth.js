
const init = {
  menus:JSON.parse(localStorage.getItem("menus") || "[]"),
  rules: JSON.parse(localStorage.getItem("rules") || "[]")
}
function reducer(preState = init, actions) {
  console.log("老佛爷",preState, actions)
  const newData = JSON.parse(JSON.stringify(preState))
  const { type, payload } = actions;
  switch (type) {
    case "INIT_AUTH":
      const { menus, rules } = payload;
      newData.menus = menus
      newData.rules = rules
      localStorage.setItem("menus",JSON.stringify(menus));
      localStorage.setItem("rules",JSON.stringify(rules));
      break;
  
    default:
      break;
  }
  
  return newData;
}
export default reducer