import AuthService from "@/service/index.js"
const actionCreators = {
  initAuth(menus, rules) {
    return {
      type: "INIT_AUTH",
      payload:  {menus, rules}
    }
  }
}

export default actionCreators;