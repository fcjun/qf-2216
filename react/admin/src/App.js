import MyRouter from "@/router/Index.js";
import { Provider } from "react-redux";
import store from "@/store/index";
import { useEffect } from "react";
function App() {
 // hash 监听
  function listenHash() {
    // hash 通过 hashchange
    // 历史模式怎么办  地址通过刷新按钮和回车修改路径 触发不到更新监听
    window.addEventListener("hashchange",(...arg) => {
      console.log("hashchange",arg)
    })
  }
  useEffect(() => {
    listenHash();
  },[])
  return (
    <div className="App">
      <Provider store={store}>
        <MyRouter></MyRouter>
      </Provider>
    </div>
  );
}

export default App;
