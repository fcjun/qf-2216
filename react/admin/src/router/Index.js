import React, { Suspense } from "react";
import { HashRouter, Route, Redirect, Switch } from "react-router-dom";
import Home from "@/pages/home/Index";
import Login from "@/pages/login/Index";
import MyLayout from "@/layout/Layout";
import Banner from "@/pages/banner/Index";
import BannerAdd from "@/pages/banner/add";
import Reg from "@/pages/reg/Index";
import ActivePush from "@/pages/active/Index";
import ActivePushAdd from "@/pages/active/Add";
import RichEditor from "@/pages/richEditor/Index";
import EchartBase from "@/pages/echarts/Index";
import EchartCmp from "@/pages/echarts/Cmp";
import EchartMap from "@/pages/echarts/Map";
import AuthMenuPage from "@/pages/auth/menus/Index";
import AuthRulePage from "@/pages/auth/rule/Index";
import AuthRolePage from "@/pages/auth/role/Index";
import AuthRoute from "@/components/AuthRoute";
import Admin from "@/pages/admin/Index";

// import Export from "@/pages/export/Index";
const Export = React.lazy(() => import("@/pages/export/Index"));

function Loading() {
  return (
    <div style={{ width: "1000px", height: "1000px", background: "red" }}>
      {" "}
      this is 菊花
    </div>
  );
}

function MyRouter() {
  return (
    <div className="App">
      <Suspense fallback={Loading}>
        <HashRouter>
          <Switch>
            <Redirect exact from="/" to="/home"></Redirect>
            <Route path="/login">
              <Login></Login>
            </Route>
            <Route path="/reg">
              <Reg></Reg>
            </Route>
            <Route path="/">
              <MyLayout>
                <Route path="/home" component={Home}></Route>
                <Route exact path="/banner" component={Banner}></Route>
                <Route path="/banner/add" component={BannerAdd}></Route>

                <Route exact path="/activepush" component={ActivePush}></Route>
                <Route
                  exact
                  path="/activepush/add"
                  component={ActivePushAdd}
                ></Route>
                <Route exact path="/richeditor" component={RichEditor}></Route>
                <Route exact path="/echartbase" component={EchartBase}></Route>
                <Route exact path="/echartcmp" component={EchartCmp}></Route>
                <Route exact path="/echartmap" component={EchartMap}></Route>
                <Route exact path="/admin" component={Admin}></Route>
                <Route exact path="/export">
                  <Export></Export>
                </Route>
                {/* <AuthRoute exact path="/export" component={Export}></AuthRoute> */}
                {/* 权限管理 */}
                <Route
                  exact
                  path="/auth/menus"
                  component={AuthMenuPage}
                ></Route>
                <Route
                  exact
                  path="/auth/roles"
                  component={AuthRolePage}
                ></Route>

                <Route
                  exact
                  path="/auth/rules"
                  component={AuthRulePage}
                ></Route>
              </MyLayout>
            </Route>
          </Switch>
        </HashRouter>
      </Suspense>
    </div>
  );
}

export default MyRouter;
