import { Card } from "antd";
import * as echarts from "echarts";
import { useEffect, useRef } from "react";
function EchartComponent(props) {
  const { options, title } = props;
  const boxRef = useRef();
  // 挂载的初始化操作
  const echartsRef = useRef()
  useEffect(() => {
    echartsRef.current = echarts.init(boxRef.current);
    echartsRef.current.setOption(options);
  }, []);

  // 监听数据改变
  useEffect(() => {

    if(!echartsRef.current)return;
    echartsRef.current.setOption(options);
  },[options]);
  return (
    <Card title={title} style={{ width: 300, height: 300 }}>
      <div style={{ width: 300, height: 300 }} ref={boxRef}></div>
    </Card>
  );
}
export default EchartComponent;

/*
1.根据外部传递的props数据显示不同的图表
2.props数据发生该改变的时候我们也要更新

*/
