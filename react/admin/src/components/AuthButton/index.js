import React, { Component } from "react";
import { connect } from "react-redux";
class AuthButton extends Component {
  render() {
    const { code,rules } = this.props;

    const btns = rules.map((item) => item.btnCode);
    return (
      <>
       {/* { btns.includes(code) && this.props.children } */}
       { this.props.children }
      </>
    );
  }
}
export default connect((state) => {
  return {
    rules: state.auth.rules,
  };
})(AuthButton);
