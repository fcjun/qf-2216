import React, { Component } from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
class AuthRoute extends Component {
  render() {
    const { exact, component, path, rules } = this.props;
    console.log(rules);
    const paths = rules.map((item) => item.path);
    return (
      <Route path={path} exact={exact} component={component}></Route>
      // <>
      //   {paths.includes(path) ? (
      //     <Route path={path} exact={exact} component={component}></Route>
      //   ) : (
      //     <Redirect to="/403" />
      //   )}
      // </>
    );
  }
}
export default connect((state) => {
  return {
    rules: state.auth.rules,
  };
})(AuthRoute);
