import axios from "@/utils/axios";
// 获取权限列表
function getAuthList() {
  const url = "/admin/getAuth";
  return axios.get(url, );
}
// 文件上传
function upload(data) {
  return axios.post("/upload",data)
}

export default {
  getAuthList,
  upload
};