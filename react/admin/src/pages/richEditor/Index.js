import React, { Component, createRef } from "react";
import styles from "./index.module.less";
class RichEditor extends Component {
  edirRef = createRef();
  previewRef = createRef();
  img =
    "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fc-ssl.duitang.com%2Fuploads%2Fblog%2F202106%2F09%2F20210609081952_51ef5.thumb.1000_0.jpg&refer=http%3A%2F%2Fc-ssl.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1680243825&t=361d0eed4837fbe6f6975b94f346ea82";
  render() {
    return (
      <>
        <button
          onClick={() => {
            document.execCommand("bold", false, null);
          }}
        >
          {" "}
          加粗
        </button>
        <button
          onClick={() => {
            document.execCommand("formatblock", false, "<p>");
          }}
        >
          段落
        </button>
        <button
          onClick={() => {
            document.execCommand("formatblock", false, "<h1>");
          }}
        >
          h1
        </button>
        {/* 插入图片 */}
        <button
          onClick={() => {
            document.execCommand("insertImage", false, this.img);
          }}
        >
          图片
        </button>
        {/* 插入链接 */}
        <button
          onClick={() => {
            document.execCommand("createLink", false, "http://www.baidu.com");
          }}
        >
          图片
        </button>
        {/* 预览 */}
        <button
          onClick={() => {
            const value = this.edirRef.current.innerHTML;
            console.log(value);
            this.previewRef.current.innerHTML = value;
          }}
        >
          预览
        </button>
        <div className={styles.box}>
          {/* 编辑区域 */}
          <div
            className={styles.ediotrbox}
            contentEditable
            ref={this.edirRef}
          ></div>
          {/* 预览区域 */}
          <div className={styles.previewbox} ref={this.previewRef}></div>
        </div>
      </>
    );
  }
}

export default RichEditor;

/*
{ user: hehe, job:"前端工程师", money: 15k}
<div><b>劳动合同</b>
</div><div><b>甲方：xxx公司</b>
</div><div><b>乙方：%user%</b><
/div><div><b>&nbsp;所发生的发生的地方职位:%job%_xxxx</b>
</div><div><b>xxxx</b></div><div><b>xxxxxx</b></div><div><b>xxxx</b></div><div>
<b>xx 薪资:%money%</b></div><div><b><br></b></div><div><b><br></b></div><div><b><br>
</b></div>

*/
