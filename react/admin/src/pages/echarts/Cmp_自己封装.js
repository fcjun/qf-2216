import { memo, useState } from 'react';
import * as echarts from 'echarts';
import {Card} from "antd";
import { useEffect,useRef } from 'react';
import EchartComponent from '@/components/EchartComponent/Index';

const initData =  {
  title: {
    text: 'ECharts 入门示例'
  },
  tooltip: {},
  xAxis: {
    data: ['衬衫', '羊毛衫', '雪纺衫', '裤子', '高跟鞋', '袜子']
  },
  yAxis: {},
  series: [
    {
      name: '销量',
      type: 'bar',
      data: [1,1,1,1,1,1]
    }
  ]
}
const initData1 =  {
  xAxis: {
    type: 'category',
    data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
  },
  yAxis: {
    type: 'value'
  },
  series: [
    {
      data: [1, 1, 1, 1, 1, 1, 1],
      type: 'line'
    }
  ]
}
function EchartBase() {
  const [option,setOption] = useState(initData)
  const [option1,setOption1] = useState(initData1)
  function getData() {
    // 第一个图形
    option.series[0].data = [1,11,21,41,123,1]
    setOption(JSON.parse(JSON.stringify(option)))
    // 第二个
    option1.series[0].data = [1,11,21,41,123,1]
    setOption1(JSON.parse(JSON.stringify(option1)))
  }
  useEffect(() => {
    setTimeout(() => {
      getData();
    },1000)
  },[])
  console.log("redner");
  return (
    <>
    <h1>图表-base</h1>
      <EchartComponent title="饼状图" options={option}></EchartComponent>
      <EchartComponent title="柱状图" options={option1}></EchartComponent>
    </>
  )
}

export default EchartBase;
/*


*/
