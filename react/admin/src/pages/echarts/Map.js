import { memo } from 'react';
import * as echarts from 'echarts';
import {Card} from "antd";
import { useEffect,useRef } from 'react';
import china from "@/map/json/china.json"
import anhui from "@/map/json/province/anhui.json";
console.log(china)
function Map() {
  const divRef = useRef();
  let myChart;
  useEffect(()=>{
    // 注册地图
    echarts.registerMap("china",china)
    myChart = echarts.init(divRef.current);
    myChart.on('click', function (params) {
      click();
  });
    myChart.setOption({
      geo: {
        map: "china"
      }
    });
    // setTimeout(() => {
    //   click();
    // },1000)
  },[])

  function click() {
    echarts.registerMap("anhui",anhui)
    myChart.setOption({
      geo: {
        map: "anhui"
      }
    });
  }


  return (
    <>
    <h1>图表-base</h1>
    <Card title="地图">
      <div style={{width:200, height:200}} ref={divRef}></div>
    </Card>
    </>
  )
}

export default Map;
/*


*/
