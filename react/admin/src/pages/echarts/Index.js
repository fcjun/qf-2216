import { memo } from 'react';
import * as echarts from 'echarts';
import {Card} from "antd";
import { useEffect,useRef } from 'react';
function EchartBase() {
  const divRef = useRef();
  const option =  {
    title: {
      text: 'ECharts 入门示例'
    },
    tooltip: {},
    xAxis: {
      data: ['衬衫', '羊毛衫', '雪纺衫', '裤子', '高跟鞋', '袜子']
    },
    yAxis: {},
    series: [
      {
        name: '销量',
        type: 'bar',
        data: [1,1,1,1,1,1]
      }
    ]
  }
  let myChart;
  useEffect(()=>{
    myChart = echarts.init(divRef.current);
    myChart.setOption(option);
    // 请求数据
    getData();
    
  },[])

  function getData(){
    setTimeout(() => {
      option.series[0].data = [10, 11, 21, 1, 1, 5];
      myChart.setOption(option)
    },1000)
  }
  return (
    <>
    <h1>图表-base</h1>
    <Card title="饼状图">
      <div style={{width:200, height:200}} ref={divRef}></div>
    </Card>
    </>
  )
}

export default memo(EchartBase);
/*


*/
