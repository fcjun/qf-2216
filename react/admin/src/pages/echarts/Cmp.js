import { memo, useState } from 'react';
import * as echarts from 'echarts';
import {Card} from "antd";
import { useEffect,useRef } from 'react';
// import EchartComponent from '@/components/EchartComponent/Index';
import ReactECharts from 'echarts-for-react';
const initData =  {
  title: {
    text: 'ECharts 入门示例'
  },
  tooltip: {},
  xAxis: {
    data: ['衬衫', '羊毛衫', '雪纺衫', '裤子', '高跟鞋', '袜子']
  },
  yAxis: {},
  series: [
    {
      name: '销量',
      type: 'bar',
      data: [1,1,1,1,1,1]
    }
  ]
}
const initData1 =  {
  xAxis: {
    type: 'category',
    data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
  },
  yAxis: {
    type: 'value'
  },
  series: [
    {
      data: [1, 1, 1, 1, 1, 1, 1],
      type: 'line'
    }
  ]
}
function EchartBase() {
  const [option,setOption] = useState(initData)
  const [option1,setOption1] = useState(initData1)
  function getData() {
    // 第一个图形
    setOption((option) => {
      const tmp = JSON.parse(JSON.stringify(option));
      tmp.series[0].data = [1,11,21,41,123,1]
      return  tmp
    })
    setOption1((option1) => {
      const tmp = JSON.parse(JSON.stringify(option1))
      tmp.series[0].data = [1,11,21,41,123,1]
      return tmp
    })
  }
  useEffect(() => {
    setTimeout(() => {
      getData();
    },1000)
  },[])
  console.log("redner",option);
  return (
    <>
    <h1>图表-base</h1>
      <ReactECharts  option={option}></ReactECharts>
      <ReactECharts  option={option1}></ReactECharts>
    </>
  )
}

export default EchartBase;
/*


*/
