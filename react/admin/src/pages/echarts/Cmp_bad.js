import { memo } from 'react';
import * as echarts from 'echarts';
import {Card} from "antd";
import { useEffect,useRef } from 'react';
function EchartBase() {
  const divRef = useRef();
  const div1Ref = useRef();
  const option =  {
    title: {
      text: 'ECharts 入门示例'
    },
    tooltip: {},
    xAxis: {
      data: ['衬衫', '羊毛衫', '雪纺衫', '裤子', '高跟鞋', '袜子']
    },
    yAxis: {},
    series: [
      {
        name: '销量',
        type: 'bar',
        data: [1,1,1,1,1,1]
      }
    ]
  }
  const option1 =  {
    xAxis: {
      type: 'category',
      data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
    },
    yAxis: {
      type: 'value'
    },
    series: [
      {
        data: [1, 1, 1, 1, 1, 1, 1],
        type: 'line'
      }
    ]
  }
  let myChart;
  let myChart1;
  useEffect(()=>{
    // 第一个图表
    myChart = echarts.init(divRef.current);
    myChart.setOption(option);
    // 第二个图表
    myChart1 = echarts.init(div1Ref.current);
    myChart1.setOption(option1);
    // 请求数据
    getData();
    
  },[])

  function getData(){
    setTimeout(() => {
      option.series[0].data = [10, 11, 21, 1, 1, 5];
      myChart.setOption(option)

      option1.series[0].data = [150, 230, 224, 218, 135, 147, 260];
      myChart1.setOption(option1)


    },1000)
  }
  return (
    <>
    <h1>图表-base</h1>
    <Card title="饼状图">
      <div style={{width:200, height:200}} ref={divRef}></div>
      <hr />
      <div style={{width:200, height:200}} ref={div1Ref}></div>
    </Card>
    </>
  )
}

export default memo(EchartBase);
/*


*/
