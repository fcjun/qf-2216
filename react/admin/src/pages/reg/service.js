import axios from "@/utils/axios";

function sendCode(userName) {
  const url = "/admin/sendCode";
  return axios.post(url, { userName });
}

function reg(payload) {
  const url = "/admin/reg";
  return axios.post(url, payload);
}

export default {
  sendCode,
  reg,
};
