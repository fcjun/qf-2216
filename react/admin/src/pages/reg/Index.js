import { Card, Row, Col, Button, Input } from "antd";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import CustomInput from "./CustomInput";
import service from "./service"

function Reg() {
  const history = useHistory()
  console.log("render");
  const [userName, setUserName] = useState("352186537@qq.com");
  const [passWord, setPassword] = useState("123");
  const [code, setCode] = useState("");

  async function sendCode() {
    const {msg} = await service.sendCode(userName) || {};
    alert(msg)
  }
  async function reg() {
    // 参数验证
    const data = await service.reg({userName,passWord,code}) 
    alert(data.msg);
    if(!data.code) {
      history.replace("/login")
    }
  }
  return (
    <div className="reg">
      <Card title="用户注册">
        <CustomInput label="用户名：" value={userName} finish={setUserName}></CustomInput>
        <CustomInput label="密码" value={passWord} finish={setPassword}></CustomInput>
        <Row>
          <Col span={16}>
          <CustomInput label="验证码" value={code} finish={setCode}></CustomInput>
          </Col>
          <Col span={8}>
            <Button onClick={sendCode}>发送验证码</Button>
          </Col>
        </Row>
        <Button onClick={reg}>提交</Button>
      </Card>
    </div>
  );
}

export default Reg;
/*
如何减少受控组件的render次数
*/ 