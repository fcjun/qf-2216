import {Row,Col,Input} from "antd";
import { useState } from "react";
function CustomInput(props) {
  const {label,value, finish} =props
  const [val, setVal] =useState(value)
  return (
    <Row>
          <Col span={3}>{label}</Col>
          <Col span={16}>
            <Input 
            value={val} 
            onChange={(e) => {
              setVal(e.target.value)
            }}
            onBlur={() => {
              finish(val)
            }}
            ></Input>
          </Col>
        </Row>
  )
}
export default CustomInput;