import axios from "@/utils/axios";
function getList(page = 1, pageSize = 5) {
  const url = "/admin";
  return axios.get(url, {
    params: {
      page,
      pageSize,
    },
  });
}
// 管理员修改
function update(id,data) {
  const url = "/admin/"+id;
  return axios.put(url,data);
}
// 同步导出
function exportSync() {
  const url = "/admin/export";
  return axios({
    url,
    method: "post",
    responseType: "arraybuffer",
  });
}

function exportAsync() {
  const url = "/admin/export";
  return axios.post(url, { async: true });
}

export default {
  getList,
  exportSync,
  exportAsync,
  update
};