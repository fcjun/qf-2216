import React, { Component } from "react";
import { Modal, Row, Col, Input, Select } from "antd";
import authService from "../auth/service";
import adminService from "./service";
const { Option } = Select;
class UpdateModal extends Component {
  state = { open: false, roles: [] };
  show = () => {
    this.setState({ open: true })
  }
  close = () => {
    this.setState({ open: false });
  };
  ok = async () => {
    const {_id, leavel, userName } = this.state;
    const {code} = await adminService.update(_id, { leavel, userName})
    if(code) return false;
    // 关闭窗口刷新表格
    this.setState({ open: false })
    this.props.refresh();
    

  };
  static getDerivedStateFromProps(props, state) {

    const {user} = props;
    // 将prosp数据映射到state
    return {
      ...user,
      ...state,
    }
  }
  render() {
    const { user } = this.props;
    const { open, roles, userName, leavel } = this.state;
    return (
      <Modal open={open} onCancel={this.close} onOk={this.ok}>
        <Row>
          <Col>用户名:</Col>
          <Col>
            <Input value={userName} onChange={(e) => {
              this.setState({ userName: e.target.value})
            }}></Input>
          </Col>
        </Row>
        <Row>
          <Col>角色信息:</Col>
          <Col>
             <Select style={{width: "200px"}} value={leavel} onChange={(e) => {
                this.setState({ leavel: e})
             } }>
              {(roles||[]).map(item => {
                const {key,name} = item
                return  <Option value={key} key={key}>{name}</Option>
              })}
              
             </Select>
          </Col>
        </Row>
      </Modal>
    );
  }
  async componentDidMount() {
    // 获取项目里所有的角色信息列表
    const { code, roles, msg } = await authService.getRoleList();
    if (code) return alert(msg);
    this.setState({ roles });
  }
}

export default UpdateModal;
