import ProTable from "@ant-design/pro-table";
import { Card, Button, message } from "antd";
import { exportFile } from "@/utils/utils"
import service from "./service";
import UpdateModal from "./UpdateModal";
import { useState, useRef } from "react";
function Admin() {
  const [user, setUser] = useState({});
  const modalRef = useRef();
  const actionRef = useRef();
  const columns = [
    {
      title: "id",
      dataIndex: "_id",
      search: false,
    },
    {
      title: "用户名",
      dataIndex: "userName",
    },
    {
      title: "角色",
      dataIndex: "leavel",
    },
    {
      title: "操作",
      render(row) {
        return [
          <Button key="del">删除</Button>,
          <Button key="update" onClick={showModal.bind(null,row)}>修改</Button>
        ]
      }
    }
  ];
  async function request() {
    const { adminList, code, count } = await service.getList();

    return {
      data: adminList,
      success: !code,
      total: count,
    };
  }
  // 同步导出
  async function exportSync() {
    const res = await service.exportSync();
    exportFile(res)
  }
  // 异步导出
  async function exportAsync() {
    const {code} = await service.exportAsync();
    if(code) return message.error("导出失败请重试")
    message.success("数据导出中,稍后请到导出列表产看")
  }
  // 显示模态框
  function showModal(row) {
    setUser(row)
    modalRef.current.show();

  }
  // 刷新的方法 
  function refresh() {
    actionRef.current.reload();
  }
  return (
    <Card title="管理员管理">
      <ProTable
        actionRef={actionRef}
        columns={columns}
        request={request}
        toolBarRender={() => {
          return [
            <Button type="primary" onClick={exportSync}>
              同步导出
            </Button>,
            <Button type="primary" onClick={exportAsync}>异步导出</Button>,
          ];
        }}
      ></ProTable>
      <UpdateModal user={user} ref={modalRef} refresh={refresh}></UpdateModal>
    </Card>
  );
}

export default Admin;
