import React, { Component, createRef } from "react";
import ReactDOM from "react-dom";
import { UploadOutlined } from "@ant-design/icons";
import { Button, message, Upload } from "antd";

function Test() {
  return (
    <p className="test">

      this is test
    </p>
  )
}

class Banner extends Component {
  fileRef = createRef();
  state = {
    url: "",
  };
  // upload = async () => {
  //   const file = this.fileRef.current.files[0]
  //   const formData = new FormData();
  //   formData.append("hehe",file)
  //   const {code, data} = await service.upload(formData);
  //   this.setState({ url: data.url})
  // }
  render() {
    const token = localStorage.getItem("token");
    return (
      <>
        <Upload
          name="hehe"
          action="/upload"
          onChange={(...arg) => {
            console.log("上传改变", arg);
          }}
          headers={{ Authorization: `Bearer ${token}` }}
        >
          <Button icon={<UploadOutlined />}>Click to Upload</Button>
        </Upload>
        <hr />
        <div className="testBox">
            {/* <Test></Test> */}
            {ReactDOM.createPortal( <Test></Test>,document.getElementById("hehe"))}
        </div>
      </>
    );
  }
}

export default Banner;

/*
1. 获取图片
2. 一定调用后端接口
3. 返回图片的上传链接

*/
