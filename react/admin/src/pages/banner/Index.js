import React, { Component, createRef, } from "react";
import  ProTable  from "@ant-design/pro-table";
import  {Select, Button} from "antd";
import service from "./service";
class Banner extends Component {
  actionRef = createRef();
  columns = [
    {
      title: "id",
      dataIndex: "_id",
      width: 230,
      copyable: true,
    },
    {
      title: "name",
      dataIndex: "name",
      width: 150,
    },
    {
      title: "状态",
      dataIndex: "publish",
      width: 100,
      renderFormItem() {
        //自定义表单的信息
        return (
          <Select>
            <Select.Option>123123</Select.Option>
          </Select>
        )
      }
    },
    {
      title: "缩略图",
      dataIndex: "path",
      search: false,
    },
    {
      title: "缩略图1",
      dataIndex: "path",
      search: false, //控制表单是否出现
      hideInTable: true, // 控制表头是否出现
    },
    {
      title: "缩略图2",
      dataIndex: "path",
      search: false,
      render(path) {
        return <img src={path} alt="" />
      }
    },
    {
      title: "link",
      dataIndex: "link",
      render(link) {
        return <a href={link}>{link}</a>
      }
    },
    {
      title: "操作",
      render:(row) => {

        return (
          <>
            <Button danger onClick={this.del.bind(null,row)}>删除</Button>
            <Button >修改</Button>
          </>
        )
      }
    }
  ]
  del = (row) => {
    console.log(row);
    console.log(this.actionRef.current)
    // 通过内部封装的方法重新调用request
    this.actionRef.current.reload();

  } 
  request = async (params) => {
    console.log("request",params)
    // 在做网路请求  1.初始化 2.点击表单查询  3.分页修改 
    // 返回一个对象 对象中的 data 就是表格要渲染的数据 datasource
    // success 本次请求的成功状态
    const { current, pageSize } = params
    const {code, list, count} = await service.getList(current,pageSize) || {};
    return {
      data:list,
      success: !code,
      total: count,
    }
  }
  render() {
    return (
     <>
        <ProTable 
          actionRef={this.actionRef}
          request={this.request}
          columns={this.columns} 
          rowKey="_id"
          scroll={{x:1300,y: 400}}
          pagination={{
            pageSize: 5,
            onChange: (page) => console.log(page),
          }}
        ></ProTable>
     </>
    );
  }
}

export default Banner;

/*


*/

