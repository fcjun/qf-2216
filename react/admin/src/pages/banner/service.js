import axios from "@/utils/axios";
function getList(page = 1, pageSize = 5) {
  const url = "/banner";
  return axios.get(url, {
    params: {
      page,
      pageSize,
    },
  });
}

export default {
  getList,
};
