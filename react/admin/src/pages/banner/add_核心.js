import React, { Component, createRef,} from "react";
import service from "@/service/index.js";
class Banner extends Component {
  fileRef = createRef();
  state = {
    url: "",
  }
  upload = async () => {
    const file = this.fileRef.current.files[0]
    const formData = new FormData();
    formData.append("hehe",file)
    const {code, data} = await service.upload(formData);
    this.setState({ url: data.url})
  } 
  render() {
    return (
     <>
        <input type="file"  ref={this.fileRef}/>
        <button onClick={this.upload}>图片上传</button>
        <img src={this.state.url} alt="" />
     </>
    );
  }
}

export default Banner;

/*
1. 获取图片
2. 一定调用后端接口
3. 返回图片的上传链接

*/

