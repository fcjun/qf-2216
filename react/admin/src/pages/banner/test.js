class Fa extends Componet {
  componentDidMount() {
    console.log("父挂载")
  }
  render() {
    console.log("父render")
    return (
      <>
        this is fa
        <Son></Son>
      </>
    )
  }
  componentDidUpdate() {
    console.log("父更新完成")
  }
}

class Son extends Componet {
  componentDidMount() {
    console.log("子挂载")
  }
  render() {
    console.log("子render")
    return (
      <>
        this is Son
      </>
    )
  }
  
  componentDidUpdate() {
    console.log("子更新完成")
  }
}