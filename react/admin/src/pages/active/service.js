import axios from "@/utils/axios";


function add(code, title) {
  const url = "/html";
  return axios.post(url, {code, title});
}

function get() {
  const url = "/html";
  return axios.get(url);
}

function getById(id) {
  const url = `/html/${id}`;
  return axios.get(url);
}
export default {
  add,
  get,
  getById
};
