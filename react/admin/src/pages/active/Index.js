import React from "react";
import { Card, Table, Button } from "antd";
import { withRouter } from "react-router-dom";
import service from "./service";
import AuthButton from "../../components/AuthButton";
class ActivePush extends React.Component {
  columns = [
    {
      title: "id",
      dataIndex: "_id",
    },
    {
      title: "活动主题",
      dataIndex: "title",
    },
    {
      title: "预览",
      dataIndex: "_id",
      render(_id) {
        // render第一个参数根据dataIndex取值 如果没有就是整行数据
        // render第二个参数就是整行数据
        // 第三个参数就是第几行
        return <a href={`http://www.lrfc.vip:3001/html/${_id}`}>{`http://www.lrfc.vip:3001/html/${_id}`}</a>
      }
    },
    {
      title: "操作",
      render() {
        return (
          <>
            <AuthButton code="btn-del-push">
                <Button danger>删除</Button>
            </AuthButton>
            <AuthButton code="btn-active-push">
                <Button type="primary">修改</Button>
            </AuthButton>
            
            
          </>
        )
      }
    }
  ];
  state = {
    dataSource: [],
  };
  add = () => {
    this.props.history.push("/activepush/add");
  };
  async componentDidMount() {
    const res = await service.get();
    console.log(res);
    this.setState({ dataSource: res.list})
  }
  render() {
    return (
      <Card title="活动列表">
        <Button type="primary" onClick={this.add}>
          添加
        </Button>
        <Table columns={this.columns} dataSource={this.state.dataSource}></Table>
      </Card>
    );
  }
}

export default withRouter(ActivePush);
