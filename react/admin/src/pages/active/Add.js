import React, { createRef } from "react";
import { Card, Input, message } from "antd";
import { UnControlled as CodeMirror } from "react-codemirror2";
import {withRouter} from "react-router-dom"
import service from "./service";
// 获取react 组件
import "codemirror/lib/codemirror.css";
import "codemirror/theme/material.css";
// 代码编辑器的样式
require("codemirror/mode/javascript/javascript");
// 代码编辑器的高亮主题


class ActivePushAdd extends React.Component {
  titleRef = createRef();
  value="<h1> welcome ♥ lazy coder</h1>"
  submit = async () =>{
    const code = this.value;
    const title = this.titleRef.current.input.value
    const res = await service.add(code,title) || {};
    if(res.code) {
      return message.error(res.msg);
    }
    message.success(res.msg);
    this.props.history.back();
    

  }
  render() {
    console.log("render")
    return (
      <Card title="活动添加">
        <button onClick={this.submit}>提交</button>
        <Input ref={this.titleRef}></Input>
        <CodeMirror
          value={this.value}
          options={{
            mode: "javascript",
            theme: "material",
            lineNumbers: true,
          }}
          onChange={(editor, data, value) => {
            this.value = value
          }}
        />
      </Card>
    );
  }
}

export default withRouter(ActivePushAdd);
