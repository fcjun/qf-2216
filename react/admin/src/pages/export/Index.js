import ProTable from "@ant-design/pro-table";
import { Card, Button, message } from "antd";
import service from "./service";
function ExportAsync() {
  const columns = [
    {
      title: "id",
      dataIndex: "_id",
      search: false,
    },
    {
      title: "导出类型",
      dataIndex: "type",
    },
    {
      title: "下载路径",
      dataIndex: "path",
    },
    {
      title:"操作",
      dataIndex:"path",
      search: false,
      render(path) {
        return (
          <Button onClick={() => {
            window.location.href = `http://www.lrfc.vip:3001${path}`
          }}>下载</Button>
        )
      }
    }
  ];
  async function request() {
    const { list, code, count } = await service.getList();
    return {
      data: list,
      success: !code,
      total: count,
    };
  }

  return (
    <Card title="导出管理">
      <ProTable
        columns={columns}
        request={request}
        rowKey="_id"
      ></ProTable>
    </Card>
  );
}

export default ExportAsync;
