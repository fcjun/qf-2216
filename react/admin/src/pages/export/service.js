import axios from "@/utils/axios";
function getList(page = 1, pageSize = 5) {
  const url = "/asyncExport";
  return axios.get(url);
}


export default {
  getList,
}