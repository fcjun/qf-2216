import axios from "@/utils/axios";

function login(payload) {
  const url = "/admin/login";
  return axios.post(url, payload);
}

export default {
  login
};
