import { Button, Checkbox, Form, Input, message } from "antd";
import service  from "./service";
import AuthService from "@/service/index.js";
import actionCreators from "@/store/actionCreators";
import {useDispatch} from "react-redux"
function Login() {
  // react-redux 提供的特性直接获取dispatch 不需要connect
  const dispath = useDispatch();
  console.log("dispath",dispath)
  const onFinish = async (form) => {
    console.log(form)
    const {code,msg,token} = await service.login(form) || {};
    if(code) {
     return message.error(msg)
    }
    message.success(msg)
    // 登录成功
    localStorage.setItem("token",token)
    // 获取权限信息
    const data= await AuthService.getAuthList()
    if(data.code) return false;
    const action = actionCreators.initAuth(data.menus,data.rules);
    dispath(action);

  }
  return (
    <div>
      <Form
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        style={{
          maxWidth: 600,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        autoComplete="off"
      >
        <Form.Item
          label="用户名"
          name="userName"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="密码"
          name="passWord"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button type="primary" htmlType="submit" >
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}

export default Login;
