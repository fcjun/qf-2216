

import axios from "@/utils/axios";
function getTreeList() {
  const url = "/menus";
  return axios.get(url);
}
function addMenus(data) {
  const url = "/menus";
  return axios.post(url,data);
}

function getMenuById(id) {
  const url = "/menus/"+id;
  return axios.get(url);
}
function delMenuById(id) {
  const url = "/menus/"+id;
  return axios.delete(url);
}

// 规则相关
function getRuleList() {
  const url = "/rule";
  return axios.get(url);
}
// 添加规则
function addRule(data) {
  const url = "/rule";
  return axios.post(url,data);
}

// 删除规则
function delRule(id) {
  const url = "/rule/" +id;
  return axios.delete(url);
}

// 角色相关
// 获取角色列表
function getRoleList() {
  const url = "/role";
  return axios.get(url);
}
// 添加角色
function addRole(data) {
  const url = "/role";
  return axios.post(url,data);
}
// 删除角色
function delRole(id) {
  const url = "/role/" +id;
  return axios.delete(url);
}
// 修改角色
function putRole(id,data) {
  const url = "/role/"+id;
  return axios.post(url,data);
}
export default {
  getTreeList,
  addMenus,
  getMenuById,
  delMenuById,
  getRuleList,
  addRule,
  delRule,
  getRoleList,
  addRole,
  delRole,
  putRole
};
