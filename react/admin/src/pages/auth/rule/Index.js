import { render } from "@testing-library/react";
import { Button, Table } from "antd";
import { useEffect, useState, useRef } from "react";
import service from "../service";
import AddRuleModal from "./components/AddRuleModal";



function handleData(arr) {
  const result  = arr.map(item => {
    const {_id,items} =item; 
    return {
      ..._id[0],
      children: items,
    }
  })
  return result
}
// const dataSource = handleData(data)
function RulePage() {
  const columns = [
    {
      title: "id",
      dataIndex: "_id",
    },
    {
      title: "名称",
      dataIndex: "name",
    },
    {
      title: "中文名称",
      dataIndex: "nameCn",
    },
    {
      title: "路径",
      dataIndex: "path",
    },
    {
      title: "权限类型",
      render(row) {
        const {routeId,btnCode} =row;
        // routeId 是权限 按钮 和 路径的规则
        if(!routeId) {
          return <Button type="primary">菜单</Button>
        }
        if(btnCode) {
          return <Button danger>按钮权限</Button>
        } else  {
          return <Button success>页面权限</Button>
        }
  
      }
    },
    {
      title: "按钮code嘛",
      dataIndex: "btnCode",
    },
    {
      title: "权限所属的路由id",
      dataIndex: "routeId",
    },
    {
      title:"操作",
      render(row) {
        const { routeId,_id } = row;
        if(routeId) {
          // 规则 
          return <Button size="small" type="primary" danger onClick={() => { del(_id)}}>删除权限</Button>
        } else {
          return <Button size="small" type="primary" primary onClick={()=> {add(_id)}}>添加权限</Button>
        }
      }
    }
  ];
  const [dataSource,setDataSource] = useState([])
  const  modalRef = useRef();
  const [routeId,setRouteId] = useState('');
  async function getData() {
    const {code,msg,rule} = await service.getRuleList();
    if(code) return alert(msg)
    const data = handleData(rule || [])
    setDataSource(data)
  }
  async function del(id) {
    const {code, msg} = await service.delRule(id);
    if(code) return alert(msg)
    // 删除完成后刷新界面
    getData();
  }

  function add(id){
    setRouteId(id)
    // 添加权限
    modalRef.current.show();
  }

  function refresh() {
    getData();
  }

  useEffect(() => {
    getData();
  },[])
  return (
    <>
      <Button primary onClick={() => {
          modalRef.current.show();
      }}>添加规则</Button>
      <Table scroll={{y:600}} rowKey="_id"columns={columns} dataSource={dataSource} />
      <AddRuleModal ref={modalRef} routeId={routeId} refresh={refresh}></AddRuleModal>
    </>
  );
}
export default RulePage;
