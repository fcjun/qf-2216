import { Tree, Card, Button, Modal, Form, Input } from "antd";
import { useState, forwardRef, useImperativeHandle, useEffect } from "react";
import service from "../../service.js";

function AddRuleModal(props, ref) {
  const { refresh, routeId } = props;
  const [open, setOpen] = useState(false);
  const [form] = Form.useForm()
  useImperativeHandle(ref, () => {
    return {
      show,
    };
  });
  function show() {
    setOpen(true);
  }
  async function ok() {
    console.log(form)
    const { getFieldsValue } = form;
    const params = getFieldsValue()
    console.log(params)
    const {code,msg} = await service.addRule(params);

    if(code) return
    refresh(); // 调用父组件刷新方法
    setOpen(false);
  }

  useEffect(() => {
    const { setFieldValue } =form;
    // form 提供修改表单数据的方法
    setFieldValue("routeId",routeId)
  },[routeId])

  return (
    <Modal
      title={routeId}
      destroyOnClose
      open={open}
      onOk={ok}
      onCancel={() => {
        setOpen(false);
      }}
    >
      <Form  form={form}>
       
        {/* 路由的名字 */}
        <Form.Item label="名称" name="name">
          <Input />
        </Form.Item>
        {/* 路由的名字 */}
        <Form.Item label="路径" name="path">
          <Input />
        </Form.Item>
         {/* 路由的名字 */}
         <Form.Item label="权限按钮" name="btnCode">
          <Input />
        </Form.Item>
        <Form.Item label="路由节点" name="routeId" >
          <Input />
        </Form.Item>
      </Form>
    </Modal>
  );
}
export default forwardRef(AddRuleModal);
