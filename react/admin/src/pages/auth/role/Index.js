import { render } from "@testing-library/react";
import { Button, Table } from "antd";
import { useEffect, useState, useRef } from "react";
import service from "../service";
import AddRoleModal from "./components/AddRoleModal";



function RolePage() {
  const columns = [
    {
      title: "id",
      dataIndex: "_id",
    },
    {
      title: "角色名称",
      dataIndex: "name",
    },
    {
      title: "角色标识",
      dataIndex: "key",
    },
    {
      title: "操作",
      render(row) {
        return [
          <Button key="del" type="primary" danger onClick={() => { delRole(row)}}>删除</Button>,
          <Button key="edit" type="primary" onClick={() => { editRow(row)}}>编辑</Button>
        ]
      }
    }

  ];
  const modalRef = useRef();
  const [dataSource, setDataSource] = useState([])
  const [editId, setEditId] = useState("")
  useEffect(() => {
    getData();
  },[])
  // 处理函数
  async function delRole(row) {
    const {_id} = row;
    const {code, msg} = await service.delRole(_id);
    if(code) return alert("删除失败");
    getData();

  }
  function editRow(row) {
    const {_id} = row;
    modalRef.current.show();
    setEditId(_id)
  }
  
  // 网络请求
  async function getData() {
    const {code , roles} = await service.getRoleList();
     if(code) return false;
     setDataSource(roles)
  }
  return (
    <>
      <Button primary onClick={() => {
        modalRef.current.show();
        setEditId("");
      }}>添加角色</Button>
      <Table columns={columns} dataSource={dataSource}/>
      <AddRoleModal ref={modalRef} refresh={getData} editId={editId}></AddRoleModal>
    </>
  );
}
export default RolePage;
