import { Tree, Card, Button, Modal, Form, Input, Drawer, Checkbox } from "antd";
import { useState, forwardRef, useImperativeHandle, useEffect } from "react";
import service from "../../service.js";

function AddRoleModal(props, ref) {
  useImperativeHandle(ref, () => {
    return {
      show: () => {
        setOpen(true);
      },
    };
  });
  const { refresh, editId } = props
  const [rules, setRules] = useState([]);
  const [name, setName] = useState("");
  const [key, setKey] = useState("");
  const [selRule, setSelRule] = useState([]);
  const [open, setOpen] = useState(false);
  // 获取规则列表

  async function getRuleList() {
    const { rule } = await service.getRuleList();

    setRules(rule);
  
  }

  // 挂在
  useEffect(() => {
    getRuleList();
  }, []);

  // 渲染复选框
  function renderCheckBox(arr) {
    return arr.map((item) => {
      const { name, _id } = item;
      return (
        <Checkbox key={_id} value={item}>
          {name}
        </Checkbox>
      );
    });
  }
  // 添加角色
  async function addRole() {
    const data = { name, key, rules: selRule };
    const res = await service.addRole(data);
    console.log(res);
    refresh();
    setOpen(false)
  }
  return (
    <Drawer
      title={editId?"编辑角色":"新增角色"}
      placement="right"
      open={open}
      onClose={() => {
        setOpen(false);
      }}
    >
      角色名称:{" "}
      <Input
        value={name}
        onChange={(e) => {
          setName(e.target.value);
        }}
      ></Input>{" "}
      <br />
      角色标识:{" "}
      <Input
        value={key}
        onChange={(e) => {
          setKey(e.target.value);
        }}
      ></Input>{" "}
      <br />
      {/*  */}
      <Checkbox.Group
        onChange={(values) => {
          setSelRule(values);
        }}
      >
        {(rules || []).map((mapItem, index) => {
          return (
            <>
              <p>{mapItem._id[0].nameCn}</p>
              {renderCheckBox(mapItem.items)}
            </>
          );
        })}

        {/* <p>导航</p>
      <Checkbox value={4}>列表</Checkbox>
      <Checkbox value={5}>添加</Checkbox>
      <Checkbox value={6 }>删除</Checkbox> */}
      </Checkbox.Group>
      <Button onClick={addRole}>添加角色</Button>
    </Drawer>
  );
}
export default forwardRef(AddRoleModal);
