import { Card, Button, Row, Col } from "antd";
import { useState, useEffect, memo } from "react";
import service from "../../service.js";

function MenusInfo(props) {
  const { menuId, refresh, add } = props;
  const [info, setInfo] = useState({});
  async function getInfoById() {
    console.log(menuId);
    if (!menuId) return false;
    const { code, menus, msg } = await service.getMenuById(menuId);

    if (code) return alert(msg);
    setInfo(menus);
  }
  // 删除
  async function del() {
    const { code, msg } = await service.delMenuById(info._id);
    if (code) return alert(msg);
    // 刷新父页面
    refresh();
  }
  useEffect(() => {
    getInfoById();
  }, [menuId]);
  return (
    <Card title={info.nameCn} style={{ width: "900px" }}>
      <Button onClick={del}>删除</Button>
      <Button
        onClick={() => {
          add();
        }}
      >
        添加子节点
      </Button>
      <Row>
        <Col>当前节点: {info?._id}</Col>
      </Row>
      <Row>
        <Col>父节点: {info?.parent?._id}</Col>
      </Row>
      <Row>
        <Col span={12}>菜单名称(中文): {info.nameCn}</Col>
        <Col span={12}>菜单名称: {info.name}</Col>
      </Row>
      <Row>
        <Col span={12}>菜单路径: {info.path}</Col>
        <Col span={12}>是否有子节点: {info.name}</Col>
      </Row>
    </Card>
  );
}
export default memo(MenusInfo);
