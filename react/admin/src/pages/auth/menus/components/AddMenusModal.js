import { Tree, Card, Button, Modal, Form, Input } from "antd";
import { useState, forwardRef, useImperativeHandle } from "react";
// import { useEffect, useState  } from "react";
// import style from "./index.module.less";
import service from "../../service.js";

function AddMenusModal(props, ref) {
  const { refresh } = props;
  const [open, setOpen] = useState(false);
  const [form] = Form.useForm()
  useImperativeHandle(ref, () => {
    return {
      show,
    };
  });
  function show() {
    setOpen(true);
  }
  async function ok() {
    console.log(form)
    const { getFieldsValue } = form;
    const params = getFieldsValue()
    console.log(params)
    const {code,msg} = await service.addMenus(params);
    alert(msg)
    if(code) return
    refresh(); // 调用父组件刷新方法
    setOpen(false);
  }

  return (
    <Modal
      destroyOnClose
      open={open}
      onOk={ok}
      onCancel={() => {
        setOpen(false);
      }}
    >
      <Form initialValues={{}} form={form}>
        {/* 路由的名字 */}
        <Form.Item label="名称中文" name="nameCn">
          <Input />
        </Form.Item>
        {/* 路由的名字 */}
        <Form.Item label="名称" name="name">
          <Input />
        </Form.Item>
        {/* 路由的名字 */}
        <Form.Item label="路径" name="path">
          <Input />
        </Form.Item>
        <Form.Item label="父节点" name="parent">
          <Input />
        </Form.Item>
      </Form>
    </Modal>
  );
}
export default forwardRef(AddMenusModal);
