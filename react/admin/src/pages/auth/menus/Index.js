import {
  DownOutlined,
  FrownFilled,
  MehOutlined,
  SmileOutlined,
} from "@ant-design/icons";
import { Tree, Card, Button } from "antd";
import { useEffect, useState, useRef, useCallback } from "react";
import style from "./index.module.less";
import service from "../service.js";
import AddMenusModal from "./components/AddMenusModal";
import MenusInfo from "./components/MenusInfo";

function AuthMenuPage() {
  const [treeData, setTreeData] = useState([]);
  const [selId, setSelId] = useState()
  const modelRef = useRef();

  const add = useCallback(() => {
    modelRef.current.show();
    
  },[])
  useEffect(() => {
    getTreeData();
  }, []);
  function refresh() {
    //刷寻页面
    getTreeData();
  }
  // 网络请求
  async function getTreeData() {
    const { code, msg, menus } = await service.getTreeList();
    if (code) return alert(msg);
    setTreeData(menus);
    // 将树列表的第一项作为默认id 
    setSelId(menus[0]._id)
    // console.log(data)
  }

  return (
    <div className={style.box}>
      {/* 左侧 树结构 */}
      <div className={style.tree}>
        <Tree
          showIcon
          treeData={treeData}
          fieldNames={{ title: "nameCn", key: "path" }}
          onSelect={(_,{ selectedNodes }) => {
            setSelId(selectedNodes[0]._id)
          }}
        />
      </div>
      {/* 详情展示 */}
      <Card style={{width: "1000px"}}>
        <Button
          onClick={() => {
            modelRef.current.show();
          }}
          添加一级导航
        >
        </Button>
       
        <AddMenusModal ref={modelRef} refresh={refresh}></AddMenusModal>
        <MenusInfo menuId = { selId } refresh={refresh} add={add}></MenusInfo>
      </Card>
    </div>
  );
}
export default AuthMenuPage;
