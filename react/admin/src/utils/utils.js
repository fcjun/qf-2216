export const exportFile = (data) => {
  const loadstream = window.URL.createObjectURL(new Blob([data]));
    // const url = window.URL.createObjectURL(new Blob([res], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8' }))
    // const contentDisposition = res.headers["content-disposition"] || "";
    // const encodedFilename = contentDisposition.split("=")[1];
    // const filename = decodeURIComponent(encodedFilename);
    const link = document.createElement("a");
    link.style.display = "none";
    link.href = loadstream;
    link.download = "hehe.xlsx"; // 下载后文件名
    document.body.appendChild(link);
    if (navigator.userAgent.indexOf("Firefox") > -1) {
      // 火狐浏览器不支持click()
      const evt = document.createEvent("MouseEvents");
      evt.initEvent("click", true, true);
      link.dispatchEvent(evt);
    } else {
      link.click();
    }
    document.body.removeChild(link);
    window.URL.revokeObjectURL(loadstream); // 释放掉blob对象
}