/*
reducer 本质是一个纯函数
接受2个参数 一个是修改前的数据 一个修改数据的类型
在内部根据修改的类型处理数据 
返回最新的数据

reducer里返回的数据就是全局状态值
*/
const data = {
  name: "韩梅梅",
  age: 16,
};
function reducer(prevState = data, action) {
  console.log("老佛爷执行了");
  console.log("修改前的数据", prevState);
  console.log("action", action);
  const newData = prevState;
  // type 告知你要改哪个数据
  // payload是你要改成啥样
  const { type, payload } = action;
  switch (type) {
    case "changename":
      newData.name = payload;
      break;
    case "changeage":
      newData.age = payload;
      break;

    default:
      break;
  }
  return newData;
}

export default reducer;
