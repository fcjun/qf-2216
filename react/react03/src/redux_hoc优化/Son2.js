import React, { Component } from "react";
import Hoc from "./Hoc";

class Son2 extends Component {
  render() {
    const { name, age } = this.props;
    return (
      <div className="box">
        <h1>Son2</h1>
        {name} {age}
      </div>
    );
  }
}

export default Hoc(Son2);
