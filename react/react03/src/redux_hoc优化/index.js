import React, { Component } from "react";
import store from "./store/index";
import Son2 from "./Son2";
import Son1 from "./Son1";
console.log(store);
class ReduxDemo extends Component {
  state = {
    show: true,
  };
  render() {
    const data = store.getState();
    console.log(data);
    return (
      <div className="box">
        <h1>ReduxDemo</h1>
        <button
          onClick={() => {
            // dispatch触发reducer重新执行
            store.dispatch({ type: "changename", payload: "李雷雷" });
          }}
        >
          changeName
        </button>

        <button
          onClick={() => {
            store.dispatch({ type: "changeage", payload: 18 });
          }}
        >
          changeage
        </button>

        <hr />
        {/* 销毁组件2 */}
        <button
          onClick={() => {
            this.setState({ show: false })
          }}
        >
          销毁组件2
        </button>
        <hr />
        <Son1></Son1>
        <hr />
        {this.state.show && <Son2></Son2>}
      </div>
    );
  }
}

export default ReduxDemo;
