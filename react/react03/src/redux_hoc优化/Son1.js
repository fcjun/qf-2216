import React, { Component } from "react";
import Hoc from "./Hoc";
class Son1 extends Component {
  render() {
    const { name, age } = this.props;
    return (
      <div className="box">
        <h1>Son1</h1>
        {name}
        {age}
      </div>
    );
  }
}

export default Hoc(Son1);
