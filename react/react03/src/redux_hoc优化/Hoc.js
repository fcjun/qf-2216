import React, { Component, Fragment } from "react";
import store from "./store/index";

function Hoc(TempCmp) {
  class NewCmp extends Component {
    componentDidMount() {
      // subscribe执行后返回一个销毁的函数
      this.unsubscribe = store.subscribe(() => {
        console.log("全局状态值变了");
        this.setState({});
      });
    }
    componentWillUnmount() {
      this.unsubscribe();
    }
    render() {
      const state = store.getState();
      return (
        <Fragment>
          <TempCmp {...state}></TempCmp>
        </Fragment>
      );
    }
  }

  return NewCmp;
}
export default Hoc;
