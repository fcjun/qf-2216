import React, { createRef, PureComponent } from "react";
import TodoContext from "./TodoContext";
class MyInput extends PureComponent{
  inputRef = createRef();
  render() {
    return (
      <div>
        <input type="text"  ref={this.inputRef} /> <button onClick={() => {
          const msg = this.inputRef.current.value;
          this.context.add(msg)
        }}>add</button>
      </div>
    )
  }
}
MyInput.contextType = TodoContext;
export default MyInput;