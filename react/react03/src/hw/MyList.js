import React, { Component } from "react";
import TodoContext from "./TodoContext";

class MyList extends Component {
  componentDidMount() {
    console.log(this);
  }
  render() {
    const { list } = this.context;
    return (
      <div>
        <ul>
          {(list || []).map((item, index) => {
            return (
              <li key={index}>
                {index}.{item.msg}
                <button
                  onClick={() => {
                    this.context.del(index);
                  }}
                >
                  删除
                </button>
                {item.finish ? (
                  <span>已完成</span>
                ) : (
                  <button onClick={() => {
                    this.context.update(index);
                  }}>完成</button>
                )}
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}
MyList.contextType = TodoContext;
export default MyList;
