import React, { Component } from "react";
import MyInput from "./MyInput";
import MyList from "./MyList";
import TodoContext from "./TodoContext";
class TodoList extends Component {
  state = {
    list: [
      { msg: "今晚到天亮", finish: true },
      { msg: "明晚到天亮", finish: false },
      { msg: "后天继续到天亮", finish: false },
    ],
  };
  add = (msg) => {
    // 第一个可以是一个函数 函数的参数是修改前的state 
    // 函数里return的数据可以去修改state
    this.setState((state) => {
      //修改前的state数据
      const { list } =JSON.parse(JSON.stringify(state));

      list.push({ msg, finish: false});
      console.log(list)
      return { list }
    });
  };
  del= (index) => {
    const { list } = this.state;
    list.splice(index,1);
    console.log(list)
    this.setState({ list })
  }
  update = (index) => {
    const { list } = this.state;
    list[index].finish = true
    this.setState({ list })
  }
  render() {
    const { list } = this.state;
    return (
      <TodoContext.Provider value={{ list, add:this.add, del: this.del, update: this.update }}>
        <div>
          <h1>todolist</h1>
          <MyInput></MyInput>
          <MyList></MyList>
        </div>
      </TodoContext.Provider>
    );
  }
}

export default TodoList;
