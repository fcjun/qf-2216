import React, { Component } from "react";
import Hoc from "./hoc";
class Hello extends Component{

  render() {
    console.log(this)
    return (
      <div className="hello">
        欢迎: { this.props.username }
      </div>
    )
  }
}
export default Hoc(Hello)