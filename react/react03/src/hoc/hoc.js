/*
hoc 是一个函数，接受一个组件作为参数
返回一个新的组件
在新的组件里 渲染接受到的组件
hoc 处理过的组件 获取数据一般从props里获取
*/ 
import React, {Component,Fragment} from "react";
function hoc(TempCmp) {
  console.log("hoc 接受的组件",TempCmp)
  class NewCmp extends Component {
    state = {
      username: "暂无数据",
      age:15,
      test:"hehe",
    }
    componentDidMount(){
      const username = localStorage.getItem("username");
      this.setState({ username })
     }
    render() {
      return (
        <Fragment>
        {/* <TempCmp username={this.state.username}></TempCmp> */}
        <TempCmp {...this.state}></TempCmp>
        </Fragment>
      )
    }
  }

  return NewCmp
}

export default hoc;