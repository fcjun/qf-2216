// function welcome() {
//   let username = localStorage.getItem("username");
//   console.log("welcome " + username);
// }

// function goodbey() {
//   let username = localStorage.getItem("username");
//   console.log("goodbey " + username);
// }

// welcome();
// goodbey();

function hof(fun) {
  console.log("执行hof")
  console.log(fun)
  let username = localStorage.getItem("username");
  // hof 内部返回一个函数
  return () => {
    fun(username)
  }
}


function goodbey(username) {
  console.log("goodbey " + username);
}
// function welcome(username) {
  
//   console.log("welcome " + username);
// }

hof(goodbey)()
// const result = hof(goodbey)
// result();

// export const hehe = 123;
// export const xixi = 456;

// hof 是个函数 接受一个函数作为参数 返回一个新的函数， 在新的函数里执行参数函数
