import React, { PureComponent } from "react";
// 引入的js文件执行一遍 该文件不需要抛出东西
import "./hof.js"; 
// 模块的内部通过export default 抛出 不能解构
// import hof from "./hof.js"
// console.log(hof, hof.us)
// import { hehe,xixi} from "./hof.js"
// console.log(hehe,xixi)
class Hoc extends PureComponent{
 
  render() {
    return (
      <div >
       <h1>高阶组件</h1>
      </div>
    )
  }
}

export default Hoc;