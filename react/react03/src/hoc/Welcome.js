import React, { Component } from "react";
import Hoc from "./hoc";
class Welcome extends Component{
//   state = {
//     username: "暂无数据"
//   }
//  componentDidMount(){
//   const username = localStorage.getItem("username");
//   this.setState({ username })
//  }
  render() {
    console.log(this)
    return (
      <div className="welcome">
        欢迎: { this.props.username }
      </div>
    )
  }
}
const NewCmp = Hoc(Welcome);
export default NewCmp;
// export default Welcome;