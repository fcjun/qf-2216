import React, { PureComponent } from "react";
import Welcome from "./Welcome";
import Byby from "./ByBy";
import Hello from "./Hello";
class Hoc extends PureComponent{
 
  render() {
    return (
      <div className="box">
       <h1>高阶组件</h1>
       <Welcome></Welcome>
       <Byby></Byby>
       <Hello></Hello>
      </div>
    )
  }
}

export default Hoc;