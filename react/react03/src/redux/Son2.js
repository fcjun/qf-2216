import React, { Component } from "react";
import store from "./store/index";

class Son2 extends Component {
  componentDidMount() {
    // subscribe 返回一个取消监听的函数
    this.unsubscribe = store.subscribe(() => {
      // 全局状态值修改的时候执行
      this.setState({})
    })
  }
  componentWillUnmount() {
    // 销毁的时候取消监听
    this.unsubscribe();
  }
  render() {
    // 通过getState获取数据
    const { name, age } = store.getState();
    return (
      <div className="box">
        <h1>Son2</h1>
        {name} {age}
        <button
          onClick={() => {
            // 提交奏折action 触发老佛爷修改数据
            store.dispatch({ type: "changeName", payload: "李雷雷" });
          }}
        >
          改名
        </button>
      </div>
    );
  }
}

export default Son2;
