import React, { Component } from "react";
import store from "./store/index";
console.log("store", store);
class Son1 extends Component {
  render() {
    const { name, age } = store.getState();
    return (
      <div className="box">
        <h1>Son1</h1>
        {name} {age}
      </div>
    );
  }
}

export default Son1;
