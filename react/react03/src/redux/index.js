import React, { Component } from "react";
import Son2 from "./Son2";
import Son1 from "./Son1";

class ReduxDemo extends Component {
  state = {
    show: true,
  };
  render() {
    return (
      <div className="box">
        <h1>ReduxDemo</h1>
        <hr />
        <Son1></Son1>
        <hr />
        <Son2></Son2>
      </div>
    );
  }
}

export default ReduxDemo;
