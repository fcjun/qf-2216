const initData = { name: "韩梅梅", age: 16 };
function reducer(preState = initData, actions) {
  const newData = preState; // 小心引用类型 和 purecomponent 深拷贝和浅拷贝
  // 根据action 修改数据
  // type: 要修改那个数据
  // payload: 新值
  const { type, payload } = actions;
  switch (type) {
    case "changeName":
      newData.name = payload;
      break;
    case "changeAge":
      newData.age = payload;
      break;
    default:
      break;
  }
  return newData; // 切记切记返回数据
}

export default reducer;
