import React, { Component } from "react";
import store from "./store/index";

class Son1 extends Component{
  componentDidMount() {
    // subscribe执行后返回一个销毁的函数
   this.unsubscribe =  store.subscribe(() => {
      console.log("全局状态值变了")
      this.setState({})
    })
  }
  componentWillUnmount() {
    this.unsubscribe()
  }
  render() {
    const data = store.getState();
    return (
      <div className="box">
       <h1>Son1</h1>
        { data.name }{data.age}
      </div>
    )
  }
}

export default Son1;