import React, { PureComponent } from  "react";
// import ToDoList  from "./hw/ToList"
// import Login from "./login/Login"
// import "./index.less";
// import Hoc from "./hoc/index";
import ReduxDemo from "./redux";
class App extends PureComponent{
  render() {
    return (
      <div>
        {/* this is app
        <div className="test">app的div</div>
        <div className="xixi">123123</div> */}
        <hr />
        {/* <ToDoList></ToDoList> */}
        {/* <Login></Login> */}
        {/* <Hoc></Hoc> */}
        <ReduxDemo></ReduxDemo>
      </div>
    )
  }
}

export default App;