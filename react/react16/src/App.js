import logo from './logo.svg';
import './App.css';
import EventDemo from "./event/index"
import EventBugDemo from "./event/bug"
function App() {
  return (
    <div className="App">
     {/* <EventDemo></EventDemo> */}
     <EventBugDemo></EventBugDemo>
    </div>
  );
}

export default App;
