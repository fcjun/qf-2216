import React, { PureComponent } from "react";
class Demo extends PureComponent {
  constructor() {
    super();
  }
  state = {
    show:false
  }

  render() {
    return (
    <div>
      {this.state.show ? "true": "false"}
      <button onClick={(e) => {
        // e.nativeEvent.stopPropagation()
        // e.nativeEvent.stopImmediatePropagation()
        this.setState({show: !this.state.show})
      }}> toggle</button>
      { this.state.show && <div>modal</div> }
    </div>);
  }

  componentDidMount() {
    document.addEventListener('click',() => {
      this.setState({show:false})
    })
  }
}

export default Demo;
