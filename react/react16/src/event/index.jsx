import { useEffect, useRef } from "react";
import "./index.css";
function Index() {
  const faRef = useRef();
  const sonRef = useRef();
  // 合成事件
  function faClick() {
    console.log("React合成 冒泡", "父元素");
  }
  function faClickCapture() {
    console.log("React合成 捕获", "父元素");
  }
  function sonClick() {
    console.log("React合成 冒泡", "子元素");
  }
  function sonClickCapture() {
    console.log("React合成 捕获", "子元素");
  }

  useEffect(() => {
    console.log(faRef)
    // 顶层事件委托
    document.addEventListener(
      "click",
      () => {
        console.log("原生事件 document  捕获");
      },
      true
    );
    document.addEventListener("click", () => {
      console.log("原生事件 document  冒泡");
    });
    // 父元素的事件
    faRef.current.addEventListener(
      "click",
      () => {
        console.log("原生事件 父元素 捕获");
      },
      true
    );
    faRef.current.addEventListener("click", () => {
      console.log("原生事件 父元素 冒泡");
    });
    // 子元素的事件
    sonRef.current.addEventListener(
      "click",
      () => {
        console.log("原生事件 子元素 捕获");
      },
      true
    );
    sonRef.current.addEventListener("click", () => {
      console.log("原生事件 子元素 冒泡");
    });
  }, []);
  return (
    <div>
      <h1>16版本的合成事件执行</h1>
      <div
        className="fa"
        ref={faRef}
        onClick={faClick}
        onClickCapture={faClickCapture}
      >
        <div
          className="son"
          ref={sonRef}
          onClick={sonClick}
          onClickCapture={sonClickCapture}
        ></div>
      </div>
    </div>
  );
}

/*
合成事件 和 原生事件 执行顺序的对比 

事件的捕获
事件的冒泡
顶层事件委托

16 
{current: div.fa}
index.jsx:26 原生事件 document  捕获
index.jsx:37 原生事件 父元素 捕获
index.jsx:48 原生事件 子元素 捕获
index.jsx:53 原生事件 子元素 冒泡
index.jsx:42 原生事件 父元素 冒泡
index.jsx:11 React合成 捕获 父元素
index.jsx:17 React合成 捕获 子元素
index.jsx:14 React合成 冒泡 子元素
index.jsx:8 React合成 冒泡 父元素
index.jsx:31 原生事件 document  冒泡


17 
原生事件 document  捕获
index.jsx:11 React合成 捕获 父元素
index.jsx:17 React合成 捕获 子元素
index.jsx:37 原生事件 父元素 捕获
index.jsx:48 原生事件 子元素 捕获
index.jsx:53 原生事件 子元素 冒泡
index.jsx:42 原生事件 父元素 冒泡
index.jsx:14 React合成 冒泡 子元素
index.jsx:8 React合成 冒泡 父元素
index.jsx:31 原生事件 document  冒泡

16 版本 在 document的冒泡之前 执行 react的合成事件 捕获和冒泡不区分
17 版本 将合成事件的捕获和冒泡进行了拆分  原生事件的捕获阶段 执行合成事件 捕获    在原生事件的冒泡阶段执行合成事件的冒泡
*/
export default Index;
