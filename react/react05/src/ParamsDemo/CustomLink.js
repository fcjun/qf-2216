import React, { Component } from "react";
import { withRouter } from "react-router-dom";
class CustomLink extends Component {
  jump = () => {
    // console.log(this);
    const { to, history } = this.props;
    // history.push(to);
    console.log(2222);
    // query 传参 search 在路由对象里 location search 获取
    // 需要自己把数据进行处理
    // history.push(`${to}?us=123&ps=456`);
    // state 传参 传递一个对象 state key 可以随便改
    // history.push({ pathname: to, state: { us: 123, ps: 456 } });
    // parmas 传参
    history.push("/my/123/xixi/456");

  };
  renderLink = () => {
    const { tag, children } = this.props;
    switch (tag) {
      case "li":
        return <li onClick={this.jump}>{children}</li>;
        break;
      case "div":
        return <div onClick={this.jump}>{children}</div>;
        break;

      default:
        break;
    }
  };
  render() {
    return this.renderLink();
  }
}

// 普通的组件没有路由对象
// react-router-dom 提供了一个hoc withRoute 将路由对象映射到组件的props里
// 路由里的所有的组件 api 方法必须放到 hashrouter BrowserRouter 内部
export default withRouter(CustomLink);
