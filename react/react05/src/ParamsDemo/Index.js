import React, { Component } from "react";
import {
  BrowserRouter,
  HashRouter,
  Link,
  NavLink,
  Route,
  Switch,
} from "react-router-dom";
import CustomLink from "./CustomLink";
class Home extends Component {
  render() {
    return <div>this is home</div>;
  }
}

class My extends Component {
  componentDidMount() {
    console.log("my",this)
  }
  render() {
    return <div>this is my</div>;
  }
}

class Index extends Component {
  render() {
    return (
      <BrowserRouter>
        <h1>路由传参</h1>
        {/* 控制地址栏改变 */}
        <CustomLink to="/" tag="li">首页</CustomLink>
        <CustomLink to="/my" tag="li">我的</CustomLink>
        {/* 根据地址栏的改变渲染不同的组件 */}
        {/* 通过route path 和  component 控制渲染的组件  */}
        <Switch>
          <Route exact path="/" component={Home}></Route>
          <Route path="/my/:id/xixi/:hehe" component={My}></Route>
        </Switch>
      </BrowserRouter>
    );
  }
}
export default Index;

/*

*/
