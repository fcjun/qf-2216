import React, { Component } from "react";

import { Redirect, Route } from "react-router-dom";
class AuthRoute extends Component {
  render() {
    const token = localStorage.getItem("token");
    // path  AuthRoute path 属性
    // children AuthRoute 标签中的内容 组件
    const { path, children } = this.props;
    // return <Route path="/my" render={() => {
    //   return (<div>this is my</div>)
    // }}></Route>;
    return (
      <>
        <Route path={path}>{token ? children : <Redirect to="/login" />}</Route>
      </>
    );
  }
}

export default AuthRoute;
