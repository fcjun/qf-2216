import React, { Component } from "react";
import {
  BrowserRouter,
  HashRouter,
  Link,
  NavLink,
  Route,
  Switch,
} from "react-router-dom";

class Login extends Component {
  render() {
    return <div>这里是登录</div>;
  }
}

class Info extends Component {
  render() {
    return <div>这里是用户信息</div>;
  }
}

class Home extends Component {
  render() {
    return <div>this is home</div>;
  }
}

class My extends Component {
  render() {
    return (<div>
      this is my
      <hr />
      <NavLink to="/my/login">我的 - 登录12</NavLink>
      <NavLink to="/my/info">我的 - 用户信息</NavLink>
      {/* 嵌套路由 */}
      <Route path="/my/login" component={Login}></Route>
      <Route path="/my/info" component={Info}></Route>
      </div>);
  }
}

class Index extends Component {
  render() {
    return (
      <BrowserRouter>
        <h1>嵌套路由</h1>
        {/* 控制地址栏改变 */}
        <NavLink to="/" activeClassName="hehe">首页</NavLink>
        <NavLink to="/my" activeClassName="hehe">我的</NavLink>
        {/* 根据地址栏的改变渲染不同的组件 */}
        {/* 通过route path 和  component 控制渲染的组件  */}
        <Switch>
          <Route exact path="/" component={Home}></Route>
          <Route path="/my" component={My}></Route>
        </Switch>
      </BrowserRouter>
    );
  }
}
export default Index;

/*
第一级 首页 和我的 
第二级  我的页面下 有 用户信息  登录 
*/
