import React, { Component } from "react";
import {
  BrowserRouter,
  HashRouter,
  Link,
  NavLink,
  Redirect,
  Route,
  Switch,
} from "react-router-dom";

class Home extends Component {
  render() {
    return <div>this is home</div>;
  }
}

class My extends Component {
  render() {
    return <div>this is my</div>;
  }
}
class Hehe extends Component {
  render() {
    return <div>this is hehe</div>;
  }
}

const routes = [
  {
    path: "/home",
    exact: true,
    component: Home
  },
  {
    path: "/my",
    exact: false,
    component: My,
  },
  {
    path: "/hehe",
    exact: false,
    component: Hehe
  },
  {
    exact: false,
    redirect: true,
    from: '/',
    to: "/home"
  }
]
class Index extends Component {
  render() {
    return (
      <BrowserRouter>
        <h1>路由表</h1>
        {/* 控制地址栏改变 */}
        <NavLink to="/home" activeClassName="hehe">首页</NavLink>
        <NavLink to="/my" activeClassName="hehe">我的</NavLink>
        <NavLink to="/hehe" activeClassName="hehe">hehe</NavLink>
        {/* 根据地址栏的改变渲染不同的组件 */}
        {/* 通过route path 和  component 控制渲染的组件  */}
        <Switch>
          {/* <Route exact path="/" component={Home}></Route>
          <Route path="/my" component={My}></Route> */}
          {routes.map(item => {
            if(item.redirect) {
              return <Redirect from={item.from} to={item.to}></Redirect>
            }
            return <Route exact={item.exact} path={item.path} component={item.component}></Route>
          })}
        </Switch>
      </BrowserRouter>
    );
  }
}
export default Index;

/*
react 路由默认是包含匹配
exact 精准匹配 父路由千万不能用精准匹配
Switch 百里挑一 匹配到一个满足条件的就终止 
*/
