import React, { Component } from "react";
import {
  BrowserRouter,
  HashRouter,
  Link,
  NavLink,
  Route,
  Switch,
} from "react-router-dom";
import  CustomLink from "./CustomLink";
class Home extends Component {
  render() {
    return <div>this is home</div>;
  }
}

class My extends Component {
  render() {
    return <div>this is my</div>;
  }
}

class Index extends Component {
  render() {
    return (
      <>
      <BrowserRouter>
        <h1>自定义导航</h1>
        <CustomLink to="/" tag="div">首页</CustomLink>
        <CustomLink to="/my" tag="li">我的</CustomLink>
        <hr />
        {/* 控制地址栏改变 */}
        <NavLink to="/" activeClassName="hehe">首页</NavLink>
        <NavLink to="/my" activeClassName="hehe">我的</NavLink>
        {/* 根据地址栏的改变渲染不同的组件 */}
        {/* 通过route path 和  component 控制渲染的组件  */}
        <Switch>
          <Route exact path="/" component={Home}></Route>
          <Route path="/my" component={My}></Route>
        </Switch>
      </BrowserRouter>
      </>
    );
  }
}
export default Index;

/*
react 路由默认是包含匹配
exact 精准匹配 父路由千万不能用精准匹配
Switch 百里挑一 匹配到一个满足条件的就终止 
*/
