// import StateDemo from "./StateDemo";
import { useState } from "react";
import EffectDemo from "./EffectDemo";
function Index(props) {
  console.log("函数组件", this);
  const [show, setShow] = useState(true);
  return (
    <div>
      this is my Fun Cmp
      {props.name}
      {props.us}
      <hr />
      {/* <StateDemo></StateDemo> */}
      <button
        onClick={() => {
          setShow((preShow) => {
            return !preShow;
          });
        }}
      >
        切换
      </button>
      {show && <EffectDemo></EffectDemo>}
    </div>
  );
}

export default Index;
