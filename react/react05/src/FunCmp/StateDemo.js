import { useState } from "react";
import "./index.less";
function StateDemo() {
  console.log("render")
  let [num, setNum] = useState(1);
  let [show, setShow] = useState(true)
  // toggle 方法
  function toggle() {
    setShow(!show)
  }
  return (
    <>
      <h2>state-demo</h2>
      {num}
      <button onClick={() => {
        // setNum(++num)
        setNum((preNum) => {
          console.log("修改前的数据",preNum)
          return ++preNum
        })
      }}>add</button>
      <hr />
      {show &&  <div className="test"></div>}
      <button onClick={toggle}>toggle</button>
    </>
  )
}
export default StateDemo;
/*
函数组件类似于类组件的render
为了让函数组件有state的能力 引入useState 
const [变量名,修改变量的方法] = useState("默认值")
*/ 