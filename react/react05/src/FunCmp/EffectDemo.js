import { useEffect, useState } from "react";

function EffectDemo() {
  // // 挂载的生命周期
  // useEffect(() => {
  //   console.log("挂载");
  // },[]);
  // // 销毁的生命周期
  // useEffect(() => {
  //   return () => {
  //     console.log("组件销毁调用")
  //   }
  // },[])
  // let isFirst = true;
  // useEffect(() => {
  //   console.log("数据更新了")
  //   if(isFirst) {
  //     // 第一次
  //     isFirst = false
  //   } else {
  //     // 更新的时候触发 

  //   }
  // })
 
  const [num, setNum] = useState(1);
  const [name, setName] = useState("韩梅梅");
  useEffect(() => {
    console.log("name变了")
  },[name])
  return (
    <>
      <h1>useEffectDemo</h1>
      {num} {name}
      <button
        onClick={() => {
          setNum((preNum) => ++preNum);
        }}
      >
        add
      </button>
      <button
        onClick={() => {
          setName("李雷雷");
        }}
      >
        改名
      </button>
    </>
  );
}

export default EffectDemo;
/*
useEffect
componentDidMount
componetDidupdate
componetWillUnMount

useEffect(回调函数, 依赖项)

1. useEffect 会在组件挂载完成 数据更新完成调用
2. 依赖项 依赖的数据才会触发更新
    默认所有的数据修改都能触发更新调用
    []  所有的数据修改都不触发更新调用
    [a,b] a和b 修改的时候才触发更新
3. useEffect 第一个参数返回一个函数 在组件销毁的时候调用
*/
