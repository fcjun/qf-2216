import React, { Component } from "react";
import { BrowserRouter, NavLink, Route, Switch } from "react-router-dom";
class Home extends Component {
  componentDidMount() {
    console.log("Home", this);
  }
  render() {
    return <div>this is home</div>;
  }
}

class My extends Component {
  render() {
    return <div>this is my</div>;
  }
}

class Index extends Component {
  render() {
    return (
      <>
        <BrowserRouter>
          <h1>Route渲染组件</h1>
          {/* <Home></Home> */}
          <hr />
          {/* 控制地址栏改变 */}
          <NavLink to="/" activeClassName="hehe">
            首页
          </NavLink>
          <NavLink to="/my" activeClassName="hehe">
            我的
          </NavLink>
          {/* 根据地址栏的改变渲染不同的组件 */}
          {/* 通过route path 和  component 控制渲染的组件  */}
          <Switch>
            {/* component 优先级大于render  优先级低的会被忽略 */}
            {/* <Route exact path="/" component={Home} render={My}></Route> */}
            {/* <Route exact path="/"  render={(routerObj) => {
            // console.log(arg)
            // 路由对象
            // return <div>12313</div>
            // render是一个函数 参数是路由对象 内部使用的组件是没有路由对象的 手动传参 或者是withrouter
            return <Home {...routerObj}></Home>
          }}></Route> */}
            {/* <Route
              exact
              path="/"
              component={Home}
              children={(routerObj) => {
                console.log(routerObj)
                return <div>123</div>;
              }}
            ></Route> */}
            {/* <Route exact path="/" component={Home}> </Route> */}
            <Route exact path="/">
              <Home></Home>
            </Route>
            <Route path="/my" component={My}></Route>
          </Switch>
        </BrowserRouter>
      </>
    );
  }
}
export default Index;

/*
Route 组件中 
component 渲染组件 该渲染的组件自带路由对象
children >component >render  优先级低的会被忽略
children render 参数是一个函数 函数的内部返回jsx
Route 标签内部也可以写组件 没有路由对象自己获取 注意 “空格”
*/
