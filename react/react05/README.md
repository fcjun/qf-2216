## webpack 配置1
将cra默认的webpack配置暴露出来 npm run eject 
注：暴露的过程中有一个错误和git  弹射前要求本地工作区是干净的
## 配置代理
在webpack文件中 搜索 proxy 配置代理 同vue一样

## 配置预处理less lessmodule
全局搜索和sass 相关复制一份，改成less
安装 less less-loader 

## 样式模块化 
react 没有样式作用域的问题 
创建文件 xxx.module.less 可以在配置文件里修改
import styles from “xxx.module.less” 
<div className={style.xx}></div> 
样式模块化的原理 根据文件名 和 类名 + 随机字符串生成一个唯一不重复的类名
保证项目中所有的类名都是不重复
12312312

## 全局状态工具 redux
1. 基本使用
2. hoc封装版本
3. redux react-redux 插件优化版
4. 异步版本
5. 手写redux

核心： 1. 多组件共享状态 有一些数据所有的组件都可以用
      2. 任何一个数据发生修改所有的组件都要更新
### 基本使用
1. 创建store对象 和 reducer 关联
```javascript
import {createStore} from "redux";
import reducer from "./reudcer";

export default createStore(reducer)
```
2. 创建reducer 
 * 就是一个纯函数
 * 接受2个参数 1 修改前的数据 2.修改数据的动作action
 * 根据action修改数据
 * 返回修改后的数据

 ```javascript
const initData = { us:123,ps:456}
function reducer(preState = initData,actions) {

  // 根据actions 修改数据
  return 修改后的数据
}
export default reducer
```
3. 使用全局状态值
```javascript
store.getState()获取状态值并使用
```
4. 修改值
```javascript
通过提交action促使reducer重新执行 action就是个对象 一定要有type属性
store.dispatch()
```
5. 更新页面
redux 没有响应式 不会自动更新界面 监听数据的改变手动更新
```javascript
store.subscribe(() => {
  回调再全局状态数据更新的时候触发
})
```

## 异步中间件 与 actionCreatore
处理在actionCreatore中写异步的问题

## react-redux 

通过context上下文 和 hoc 实现简化redux的使用的一个插件
1. 过去全局状态数据 从props中获取
2. 简化更新的监听
3. 简化卸载监听 

根组件使用provider 注册全局store
``` javascript
import React, { PureComponent } from "react";

import ReactReduxDemo from "./react-redux-demo/Index";
import { Provider } from "react-redux";
import store from "./react-redux-demo/store/index";
class App extends PureComponent {
  render() {
    return (
      <div>
        // react自带上下文通过value 注册数据
        <Provider store={store}>
          <ReactReduxDemo></ReactReduxDemo>
        </Provider>
      </div>
    );
  }
}

export default App;

```

在子组件使用connect 获取全局状态值到props
``` javascript
import React, { Component } from "react";
import { connect } from "react-redux"
import actionCreator from "./store/actionCreators"
class Son1 extends Component {
  changeName = () => {
    this.props.dispatch(actionCreator.changeNameAction("李乐乐"))
  }
  render() {
    const  {age,name} = this.props;
    return (
      <>
        <h2>son1</h2>
        <p>age： {age}</p>
        <p>name： {name}</p>
        <button onClick={this.changeName}>改名</button>
      </>
    );
  }
}

export default connect(state => state)(Son1);
```

## 路由
https://v5.reactrouter.com/web/guides/quick-start

根据地址栏的改变匹配路由表渲染对应的组件
1. 修改地址栏改变 path 
histroy  hash 模式
编程式(js)导航  this.$router.push() this.$router.replace() 
声明式(组件标签)导航 <a herf="/home">  <router-link to="/home"/> <Link to="/home"/> <NavLink to="/"/>

2. 根据路由表匹配组件 路径path 和组件之间的关系
vue  routes = [{ path: '/home',componet: Home}]
React  <Route path="/home" componet={Home}></Route>
3. 子路由
在route里套route 父路由千万不要用精准匹配exact
4. 动态路由
4. 路由传参 接受参数
 通过路由对象接受对应的参数
 query 
 params
 state
5. 重定向
6. 404 页面
7. 路由守卫 




