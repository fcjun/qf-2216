import { createStore, applyMiddleware } from "redux";
// 引入异步插件
import thunk from "redux-thunk";
import reducer from "./reducer";
//使用中间件applyMiddleware 和 异步插件 redux-thunk 来处理aciton中的异步问题
// 异步aciton 异步中间件
export default createStore(reducer,applyMiddleware(thunk));
