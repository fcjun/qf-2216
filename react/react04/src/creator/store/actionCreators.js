/*
actionCreatore antion的创建者
就是一个对象 对象里面放一堆的方法 
在方法里返回aciton
统一管理action

常规情况下 actionCreatore 里的方法一定要返回action 同步
如果要处理异步问题 要使用中间件(异步中间件)
处理异步的插件
redux-thunk
redux-sage
redux-promise 

同步action 和之前的使用没有任何区别
异步action  在函数里return 一个函数 手动的进行dispatch
*/

import { getList } from "../service";
// eslint-disable-next-line import/no-anonymous-default-export
export default {
  // 同步的写法
  init_list_action(payload) {
    return { type: "INIT_LIST", payload };
  },
  // init_list_action_async(payload) {
  //   return (dispatch) => {
  //     // 在函数的内部写异步, 接受参数为dispatch
  //     setTimeout(() => {
  //       const action = { type: "INIT_LIST", payload };
  //       dispatch(action)
  //     },3000)
      
  //   }
  // }
   init_list_action_async(payload) {
    return async (dispatch) => {
      const res = await getList();
      const action =  { type: "INIT_LIST", payload: res.data.list }
      // 在函数的内部写异步, 接受参数为dispatch
      dispatch(action)
    }
  }
};

/*
store.dispatch(action) 
如果aciton 是一个对象 直接触发老佛爷
如果aciton 是一个函数 将dispatch 作为参数 传到函数内部，需要我们在函数内部自己调用
dispatch方法
dispatch 本身只能接受对象作为参数，因为使用了异步中间件 才能接受一个函数作为参数

*/ 
