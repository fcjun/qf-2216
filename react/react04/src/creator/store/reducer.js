const initData = {
  list: [],
  token: localStorage.getItem("token"),
};
// eslint-disable-next-line import/no-anonymous-default-export
export default (prestate = initData, actions) => {
  console.log(actions)
  const newData = prestate; // 深拷贝或者是浅拷贝 { ... } Object.assign() json.parse 递归循环   loadsh
  const { type, payload } = actions;

  switch (type) {

    case "ADD_MSG":
      newData.list.push({
        id: new Date().getTime(),
        msg: payload,
        state: false,
      });
      break;
    case "DEL_MSG":
      newData.list = newData.list.filter((item) => item.id !== payload);
      break;
    case "INIT_LIST": 
      newData.list = payload;
    break;
    case "SET_TOKEN": 
     newData.token = payload;
     localStorage.setItem("token",payload)
    break;
    default:
      break;
  }

  return newData;
};
