import React, { Component } from "react";
import store from "./store/index";
import { addMatter,getList } from "./service"
import actionCreators from "./store/actionCreators";

class Input extends Component {
  state = {
    msg: "1123",
  };
  add =  async () => {
    const res = await addMatter(this.state.msg)
    console.log(res);
    // 成功的话更新界面
    // this.refresh();
    store.dispatch(actionCreators.init_list_action_async())
  };
  // refresh = async () => {
  //   constactionCreatorsT_LIST",payload: res.data.list})
  // }
  render() {
    return (
      <>
        <input
          type="text"
          value={this.state.msg}
          onChange={(e) => {
            this.setState({ msg: e.target.value });
          }}
        />
        <button onClick={this.add}>add</button>
      </>
    );
  }
}
export default Input;
