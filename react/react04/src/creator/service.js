import axios from "../utils/axios";

// 获取列表数据

export const getList = () => {
  const url = "/v1/matter/get";
  const token = localStorage.getItem("token");

  const uid = localStorage.getItem("uid");
  return axios.post(url, { token, uid });
};
// 登录

export const userLogin = (us, ps) => {
  const url = "/v1/user/login";

  return axios.post(url, { us, ps });
};
// 添加
export const addMatter = (matter) => {
  const url = "/v1/matter/add";
  const token = localStorage.getItem("token");

  const uid = localStorage.getItem("uid");

  return axios.post(url, { token, uid, matter });
};
