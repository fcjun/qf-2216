import React, { Component } from "react";
import ToDo from "./ToDo";
import Login from "./Login";
import store from "./store/index";
class Index extends Component {
  componentDidMount() {
    this.unsubscribe = store.subscribe(() => {
      this.setState({})
    })
  }
  componentWillUnmount() {
    // 确定this.unsubscribe方法一定促窜在
    this.unsubscribe && this.unsubscribe();
  }
  render() {
    const { token } = store.getState();
    return <>{token ? <ToDo></ToDo> : <Login />}</>;
  }
}
export default Index;
