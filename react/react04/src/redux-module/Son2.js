import React, { Component } from "react";
import { connect } from "react-redux";
class Son2 extends Component {
  render() {
    console.log("son2render",this)
    const {age} = this.props;
    return (
      <>
        <h2>son2</h2>
        <p>age: {age}</p>
      </>
    );
  }
}



export default connect(state => { 
  console.log(state)
  return {
    ...state.age
  }
})(Son2);
