import { createStore, applyMiddleware,combineReducers } from "redux";
// 引入异步插件
import thunk from "redux-thunk";
import age from "./agereducer";
import name from "./namereducer";
//使用中间件applyMiddleware 和 异步插件 redux-thunk 来处理aciton中的异步问题
// 异步aciton 异步中间件
// const reducer = combineReducers({
//   age:age,
//   name:name
// })
const reducer = combineReducers({name,age})
/*
通过combineReducers实现模块的划分 与vue一样只有状态值会自动划分模块
reducer不会划分 不管拆分几个都会触发到， 手动的添加模块名

*/ 
export default createStore(reducer,applyMiddleware(thunk));
