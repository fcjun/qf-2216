import React, { Component } from "react";
import { connect } from "react-redux"
import actionCreator from "./store/actionCreators"
class Son1 extends Component {
  changeName = () => {
    this.props.dispatch(actionCreator.changeNameAction("李乐乐"))
  }
  render() {
    const  {age,name} = this.props;
    return (
      <>
        <h2>son1</h2>
        <p>age： {age}</p>
        <p>name： {name}</p>
        <button onClick={this.changeName}>改名</button>
      </>
    );
  }
}
/*
connect 获取去上下文注册的全局状态值 然后映射到组件的props里
connect 是一个函数 返回一个高级组件(高阶组件也是一个函数)
*/ 
// const hoc = connect(state => state);
// export default hoc(Son1);
export default connect(state => {
  return {...state.age,...state.name}
})(Son1);
// export default Son1;
