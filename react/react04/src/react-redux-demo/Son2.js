import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import actionCreators from "./store/actionCreators";
class Son2 extends Component {
  render() {
    console.log("son2render",this)
    const {age} = this.props;
    return (
      <>
        <h2>son2</h2>
        <p>age: {age}</p>
        {/* <button onClick={this.props.changeAge.bind(this,77)}>长大</button> */}
      </>
    );
  }
}

/*
connect 接受2个参数 
参数1 mapSateToProps
参数2 mapDisPatchToProps
mapSateToProps
1.mapSateToProps 是一个函数 
2.接受从上下文获取的state作为参数 
3. return 的数据会映射到当前组件的props里
4. 如果一个组件通过connect和state关联之后 任何一个值的改变都会引起组件的更新 效率是低下的,
 一般情况下是在omapstateTprops中使用哪个值就获取那个值


mapDisPatchToProps
1. mapDisPatchToProps是个函数
2. 接受从上下文获取的dispatch作为参数 
3. return 的数据会映射到当前组件的props里
4. 可以省略，省略后直接将dispatch映射到props里
*/ 


function mapStateToProps(state) {
  console.log("mapStateToProps",state);
  // return {hehe:123,xixi:456}
  return {age:state.age}
}

// function mapDisPatchToProps(dispatch) {
//   console.log("mapDisPatchToProps",dispatch)
//   return {
//     changeAge(age) {
//       const aciton = actionCreators.changeAgeAction(age);
//       dispatch(aciton)
//     }
//   }
// }

function mapDisPatchToProps(dispatch) {
  // 结合bindActionCreator可以将actionCreators所有的方法映射到prop里
  const acitons = bindActionCreators(actionCreators,dispatch);
  return acitons
}


export default connect(mapStateToProps, mapDisPatchToProps)(Son2);
// export default connect(state => state)(Son2);
