const initData = {
  age: 16,
  name: "韩梅梅",
};
// eslint-disable-next-line import/no-anonymous-default-export
export default (prestate = initData, actions) => {
  console.log(actions);
  const newData = {...prestate}; // 深拷贝或者是浅拷贝 { ... } Object.assign() json.parse 递归循环   loadsh
  const { type, payload } = actions;

  switch (type) {
    case "change_name":
      newData.name = payload;
      break;
    case "change_age":
      newData.age = payload;
      break;
    default:
      break;
  }
  console.log("newData",newData)
  return newData;
};
