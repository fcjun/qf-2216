import React, { Component } from "react";
import store from "./store/index";
class List extends Component {
  componentDidMount() {
    this.unsubscribe = store.subscribe(() => {
      this.setState({})
    })
  }
  componentWillUnmount() {
    // 确定this.unsubscribe方法一定促窜在
    this.unsubscribe && this.unsubscribe();
  }
  del = (id) => {
    store.dispatch({ type: "DEL_MSG", payload: id})
  }
  render() {
    const { list } = store.getState();
    return (
      <>
        <ul>
          {(list || []).map((item) => {
            return (
              <li key={item._id}>
                {item._id}-
                <span>{item.matter}</span>
                <button onClick={this.del.bind(this, item.id)}>del</button>
              </li>
            );
          })}
        </ul>
      </>
    );
  }
}
export default List;
