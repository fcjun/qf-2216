import React, { Component } from "react";
import List from "./List";
import Input from "./Input";
import store from "./store/index";
import { getList } from "./service";
console.log(getList)
class Todo extends Component {
  async componentDidMount() {
    // 挂载完毕后请求数据 初始化数据
    const res = await getList();
    console.log(res)
    store.dispatch({type: "INIT_LIST",payload: res.data.list})
  }
  render() {
    return (
      <>
      <Input></Input>
      <List></List>
      </>
    )
  }
}
export default Todo