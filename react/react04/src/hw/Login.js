import React, { Component } from "react";
import store from "./store/index";
import { userLogin } from "./service";
class Login extends Component {
  state = {
    us: "root",
    ps: "root",
  };
  login = async () => {
    const {us,ps} = this.state;
    const  res = await userLogin(us,ps)
    const {_id,token} = res?.data?.info || {};
    localStorage.setItem("uid",_id);
    store.dispatch({ type: "SET_TOKEN", payload: token})
  };
  render() {
    return (
      <>
        用户名:<input
          type="text"
          value={this.state.us}
          onChange={(e) => {
            this.setState({ us: e.target.value });
          }}
        />
        <br />
        密码:<input
          type="text"
          value={this.state.ps}
          onChange={(e) => {
            this.setState({ ps: e.target.value });
          }}
        />
        <button onClick={this.login}>登录</button>
      </>
    );
  }
}
export default Login;
