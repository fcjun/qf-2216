import React, { PureComponent } from "react";
// import CreatorDemo from "./creator/Index";
// import HW from "./hw/Index";
// import ReactReduxDemo from "./react-redux-demo/Index";
import { Provider } from "react-redux";
// import store from "./react-redux-demo/store/index";
import store from "./redux-module/store/index";
import ReduxModule from "./redux-module/Index";
import RouterDemo from "./routerDemo/Index";
class App extends PureComponent {
  render() {
    return (
      <div>
        {/* <HW></HW> */}
        {/* <CreatorDemo></CreatorDemo> */}
        {/* <Provider store={store}>
          <ReactReduxDemo></ReactReduxDemo>
        </Provider> */}
         {/* <Provider store={store}>
          <ReduxModule></ReduxModule>
        </Provider> */}
        <RouterDemo></RouterDemo>
      </div>
    );
  }
}

export default App;
