import React, { Component } from "react";

// class ClassCmp extends Component {
//   state= {
//     num:1
//   }
//   componentDidMount() {
//     setTimeout(() => {

//       this.setState({num:2})
//       this.setState({num:3})
//       this.setState({num:4})
//     },1000)
//   }
//   render() {
//     console.log('render')
//     return (
//       <div>
//         <h1 ref="hehe">123</h1>
//         {this.state.num}
//       </div>
//     );
//   }
// }
// /*
// vue模板 -> 渲染函数 createElement -> 创建虚拟dom
// react     渲染函数 createElement  -> 创建虚拟dom

// */
// export default ClassCmp;
// const obj = {
//   us:123,
//   ps:123
// }
class Index extends Component {
  render() {
    // return <h1>123</h1>;

    // return React.createElement("div", { className: "hehe", xixi: 123 }, [
    //   123,
    //   React.createElement("p",{},['this is p']),
    //   React.createElement("span",{},['this is span'])
    // ]);
    return (
      <div className="hehe" xixi='123'>
        123
        <p>this is p</p>
        <span> this is span</span>
      </div>
    )
  }
}

/*
虚拟dom就是内存中的一个对象,能描述真实dom 

[
  {
    tag: 'div',
    attr: {
      xixi:123,
      class: "hehe"
    },
    child: [
      123,
      {
        tag: "p",
        attr: {},
        child: [
          "this is p"
        ]
      }
    ]
  }
]

[
  {
    tag: 'div',
    attr: {
      xixi:123,
      class: "hehe"
    },
    child: [
      124,
      {
        tag: "p",
        attr: {},
        child: [
          "this is p"
        ]
      }
    ]
  }
]
<div xixi='123' class="hehe"> 
123
<p> this is p</p>
</div>
*/

export default Index;
