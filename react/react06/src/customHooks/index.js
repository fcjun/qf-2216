import {  useState } from "react";
import useUpdate from "./useUpdate";
function Index() {
  const [num,setNum] = useState(1)
  const [name,setName] = useState("韩梅梅")
  useUpdate(() => {
    console.log("网络请求")
  },[num]);
  return (
    <div>
      <h1>自定义hooks</h1>
      {num} {name}
      <button onClick={() => {
        setNum(preNum => ++preNum)
      }}>add</button>
      <button onClick={() => {
        setName("lll")
      }}>change</button>
    </div>
  )
}


export default Index;