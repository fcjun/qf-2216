/*
内置hooks都是函数
自定义hooks 也是hooks 也是函数
在内部可以调用其他hooks
*/ 
import { useEffect,useRef } from "react";
function useUpdate(cb,dep) {
  const isFirst = useRef(true);
  useEffect(() => {
    if(isFirst.current) {
      isFirst.current = false;
    }else {
      // 挂载的处理
      // console.log("更新")
      cb && cb();
    }
  },dep) 
}

export default useUpdate