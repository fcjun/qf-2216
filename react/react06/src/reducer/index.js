import { useReducer } from "react";
function reducer(state, action) {
  console.log("老佛爷", state, action);
  let newData = { ...state };
  const { type, payload } = action;
  switch (type) {
    case "changeName":
      newData.name = payload;
      break;
    case "changeAge":
      newData.age = payload;
      break;

    default:
      break;
  }
  console.log("newData", newData);
  return newData;
}

function Index() {
  const [state, dispatch] = useReducer(reducer, { name: "韩梅梅", age: 16 });

  return (
    <>
      <h1>UseReducer</h1>
      {state.name}
      {state.age}
      <button
        onClick={() => {
          dispatch({ type: "changeName", payload: "李雷雷" });
        }}
      >
        改名
      </button>

      <button
        onClick={() => {
          let age = ++state.age;
          dispatch({ type: "changeAge", payload:age });
        }}
      >
        改年领
      </button>
    </>
  );
}

/*

const [state数据, dipatch方法] = useReducer(老佛爷函数,初始化数据)

*/

export default Index;
