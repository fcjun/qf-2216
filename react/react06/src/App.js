import React, { PureComponent } from "react";

// import HW from "./hw/index"
// import EffectUpdate from "./update/EffectUpdate"
// import CustomHooks from "./customHooks/index"
// import UseoContextDemo from "./context/index"
// import MemoDemo from "./memo/index"
// import CallBackDemo from "./callback/index";
// import RefDemo from "./ref/index"
// import  ReducerDemo from "./reducer/index";
// import LayoutEffect from "./LayoutEffect/index"
import SetStateDemo from './setState/index';
class App extends PureComponent {
  render() {
    return (
      <div>
       {/* <HW></HW> */}
       {/* <EffectUpdate></EffectUpdate> */}
       {/* <CustomHooks></CustomHooks> */}
       {/* <UseoContextDemo></UseoContextDemo> */}
       {/* <RefDemo></RefDemo> */}
       {/* <MemoDemo></MemoDemo> */}
       {/* <CallBackDemo></CallBackDemo> */}
       {/* <ReducerDemo></ReducerDemo> */}
       {/* <LayoutEffect></LayoutEffect> */}
       <SetStateDemo></SetStateDemo>
      </div>
    );
  }
}

export default App;
