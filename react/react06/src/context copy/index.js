import { useContext, useState } from "react";
import ShowContext from "./showContext";
// 第一个子组件
function Son1() {
  // 从上下文获取数据和方法
  const ctx = useContext(ShowContext);
  console.log(ctx);
  return (
    <>
      <h2>Son1</h2>
      <button onClick={() => {
        // 调用上下文上的方法
       ctx.setShow(preShow => !preShow) 
      }}>控制显示</button>
    </>
  );
}
function Son2() {
   // 从上下文获取数据和方法
  const ctx = useContext(ShowContext);
  return (
    <>
      <h2>Son2</h2>
      {ctx.show && (
        <div style={{ width: 100, height: 100, background: "red" }}></div>
      )}
    </>
  );
}

function Index() {
  // 根组件上数据 和修改数据的方法
  const [show,setShow]  = useState(true)
  return (
    // 通过Provider 将数据和方法放到上下文上
    <ShowContext.Provider value={{ show, setShow }}>
      <h1>useContext</h1>
      <Son1></Son1>
      <Son2></Son2>
    </ShowContext.Provider>
  );
}

/*
上下文在函数组件中使用流程和类组件完全一样只是获取方式 使用 useContext
useContext
*/
export default Index;
