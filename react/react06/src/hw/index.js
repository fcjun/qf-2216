import { useState } from "react";
import "./index.less";
function Index() {
  const [table, setTable] = useState([[1], [1, 2], [1, 2, 3]]);
  // const [colors, setColors] =useState([['red','blue'],['purple','pink']])
  const [rowcol, setRowCol] = useState([1, 1]);
  const colors = [
    ["red", "blue"],
    ["purple", "pink"],
  ];
  const [selColor, setSelColor] = useState(0);

  const change = (e) => {
    console.log(e.target.value);
    setSelColor(e.target.value);
  };
  return (
    <div>
      <h1>9*9</h1>
      <select onChange={change}>
        {colors.map((item, index) => {
          return (
            <option key={index} value={index}>
              {item}
            </option>
          );
        })}
      </select>
      <hr />
      <table>
        <tbody>
          {(table || []).map((trItem, trIndex) => {
            return (
              <tr
                key={trIndex}
                className={
                  trIndex % 2 ? colors[selColor][0] : colors[selColor][1]
                }
              >
                {(trItem || []).map((tdItem, tdIndex) => {
                  return (
                    <td
                      onMouseOver={() => {
                        setRowCol([trIndex,tdIndex])
                      }}
                      key={tdIndex}
                      style={{
                        opacity:
                          trIndex === rowcol[0] && tdIndex === rowcol[1]
                            ? 0.1
                            : 1,
                      }}
                    >
                      {trIndex + 1} * {tdItem} = {(tdIndex + 1) * tdItem}
                    </td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default Index;
