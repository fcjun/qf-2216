import React, { Component } from "react";
import Son from "./Son";

class Index extends Component {
  state = {
    num: 1,
    age: 16,
  };
  log() {
    console.log(111)
  }
  render() {
    console.log("父render");
    return (
      <div>
        <h1>类组件</h1>
        {this.state.num}
        <button
          onClick={() => {
            this.setState({ num: ++this.state.num });
          }}
        >
          add
        </button>
        {/* <Son age={this.state.age}></Son> */}
        {/* 子组件render */}
        {/* 每次render重新执行 函数相当于重新创建 */}
        <Son fun={() => {
          console.log(111)
        }}></Son>
        {/* 子组件burender */}
        {/* <Son fun={this.log}></Son> */}
      </div>
    );
  }
}

export default Index;
