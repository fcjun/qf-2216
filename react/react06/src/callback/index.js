import React, { Component, useCallback, useState } from "react";
import Son from "./Son";


 function Index() {
  const [num,setNum] = useState(1)
    console.log("父render");

    // function test () {
    //   console.log("test")
    // }
    const test = useCallback(() =>{
      console.log("test")
    },[num])
    return (
      <div>
        <h1>函数组件</h1>
        {num}
        <button
          onClick={() => {
           setNum(num => ++num)
          }}
        >
          add
        </button>
        <Son test={test}></Son>
      </div>
    );
  }

/*
useCallback 在函数组件中 根据依赖项 控制一个函数是否重新创建 减少render 的执行 优化作用
const 新的函数 = useCallback(要处理的函数,[])
记忆函数

*/ 

export default Index;
