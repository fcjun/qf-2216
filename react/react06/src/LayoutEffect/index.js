import { useEffect, useRef, useLayoutEffect } from "react";
import  "./index.less";
function Index() {
  const divRef = useRef();
  // useEffect(() => {
  //   // divRef.current.style.transform = `translateX(100px)`
  //   // divRef.current.style.transition = "all 2s linear 2s"
  //   const div = divRef.current
  //   div.style.transform = 'translateX(100px)'
  //   div.style.transition = 'all 5000ms ease-in 2s'
  // })
  useLayoutEffect(() => {
    // divRef.current.style.transform = `translateX(100px)`
    // divRef.current.style.transition = "all 2s linear 2s"
    const div = divRef.current
    div.style.transform = 'translateX(100px)'
    div.style.transition = 'all 5000ms ease-in 2s'
  })
  return (
    <>
      <h1>UselayoutEffect</h1>
       <div ref={divRef} className="test" ></div>
    </>
  );
}

/*



*/

export default Index;
