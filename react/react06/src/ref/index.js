import ClassCmp from "./ClassCmp";
import FunCmp from "./FunCmp";
function Index() {
  return (
    <>
      <h1>Ref</h1>;
      {/* <ClassCmp></ClassCmp> */}
      <FunCmp></FunCmp>
    </>
  );
}
/*
ref 
绑定dom 获取dom
绑定组件 
  类组件 获取组件实例
  函数组 获取组件实例 ?
创建一个引用数据

类组件中 createRef 创建
函数组件中 使用useRef 创建
*/

export default Index;
