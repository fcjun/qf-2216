import React, { Component, useRef } from "react";
import FunSon from "./FunSon";
class ClassSon extends Component {
  test() {
    console.log("test")
  }
  render() {
    return (
      <div>
        <h2>类子组件1</h2>
      </div>
    );
  }
}




function FumCmp() {
  const numRef= useRef(123);
  const divRef = useRef();
  const sonRef = useRef();
  const sonFunRef = useRef();
  return (
    <>
    <h2>函数组件</h2>
    <div ref={divRef}>123</div>
    <ClassSon ref={sonRef}></ClassSon>
    <FunSon ref={sonFunRef}></FunSon>
    <button onClick={() => {
      console.log(numRef)
      console.log(divRef)
      // 类组件的ref
      console.log(sonRef)
      sonRef.current.test();
      // ref 绑定函数组件
      console.log(sonFunRef)
      console.log(sonFunRef.current)
      sonFunRef.current.test();
    }}>获取ref</button>
    </>
  )
}

export default FumCmp;
