import React, { useImperativeHandle, useRef } from "react";

function FunSon(props,ref) {
  const divRef = useRef()
  const pRef = useRef()
  useImperativeHandle(ref,() => {
    return {
      div:divRef.current,
      p:pRef.current,
      test:test,
    }
  })
  function test() {
    console.log('son1231231231313')
  }
  return (
    <>
    <div>这里是函数子组件</div>
    <p ref={pRef}>12312321</p>
    <div ref={divRef}>77777</div>
    </>
  )
}
/*
函数组件默认没有实例 ref 绑定没有可绑定的对象
React.forwardRef 透传 转发 将函数组件的第二个参数进行传递
可以在函数组件里自己绑定方法或者 dom元素 
只能绑定一个
useImperativeHandle 自定义抛出的实例属性 结合forwardRef 一起使用
*/
// export default FunSon;
export default React.forwardRef(FunSon);