import React, { Component, createRef } from "react";

class ClassSon extends Component {
  render() {
    return (
      <div>
        <h2>类子组件1</h2>
      </div>
    );
  }
}

class ClassCmp extends Component {
  // 类组件中绑定dom
  divRef = createRef();
  // 类组件中绑定组件
  sonRef = createRef();
  render() {
    return (
      <div>
        <h1 ref="hehe">123</h1>
        <div ref={this.divRef}>12313</div>
        <ClassSon ref={this.sonRef}></ClassSon>
        <button onClick={() => {

        }}>获取ref</button>
      </div>
    );
  }
}

export default ClassCmp;
