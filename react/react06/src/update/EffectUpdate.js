import { useEffect, useState, useRef } from "react";

function EffectUpdate() {
  const [num,setNum] = useState(1)
  // let isFirst = true
  const  isFirst = useRef(true);
  // 挂载
  useEffect(() => {
    console.log("挂载")
  },[])
  //更新
  useEffect(() => {
    if(isFirst.current) {
      // 第一次 挂载 
      // console.log("挂载")
      // isFirst = false;
      isFirst.current = false
    }else  {
      console.log("更新")
    }
  })
  return (
    <div>
      <h1>模拟更新</h1>
      {num}
      <button onClick={() => {
        setNum(preNum => ++preNum)
      }}>add</button>
    </div>
  )
}
/*
useEffect(() => {
  return () => {
    销毁
  }
},[])
useEffect 会在首次挂载和更新的时候都会执行 第一个参数
模拟更新的思路 就是将首次挂载的去掉 不使用

配合UseRef 和 useEffect 实现 更新生命周期
函数组件每次更新函数都会重新执行 普通的变量都会被重置
如果想保存一个值使用useRef 和 vue ref 差不多
*/ 
export default EffectUpdate;