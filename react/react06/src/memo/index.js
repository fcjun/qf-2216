import { useState, useMemo } from "react";

import Son from "./Son";
function Index() {
  const [a, setA] = useState(1);
  console.log("父 render")
  return (
    <>
      <h1>Memo</h1>
      a:{a}
      <button
        onClick={() => {
          setA((prevA) => ++prevA);
        }}
      >
        addA
      </button>
      <hr />
      <Son></Son>
    </>
  );
}

/*
正常情况下 父组件render 子组件一定重新render 
子组件有非必要的render 解决方案 shouldComponetUpdate  purecomponent memo
 */
export default Index;
