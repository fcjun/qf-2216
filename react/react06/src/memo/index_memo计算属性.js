
import { useState, useMemo } from "react";
function Index() {
  const [a,setA] = useState(1);
  const [b,setB] = useState(2);
  // function sum() {
  //   console.log("sum")
  //   return a+1;
  // }
  const sum = useMemo(() => {
    console.log("sum")
    return a+1;
  },[a])
  return (
    <>
      <h1>UseMemo</h1>
      a:{a}
      <hr />
      b:{b}
      <hr />
      sum:{sum}
      <button onClick={() => {
        setA(prevA => ++prevA)
      }}>addA</button>
      <button onClick={() => {
        setB(prevB => ++prevB)
      }}>addB</button>
    </>
  );
}

/*
memo 函数组件中的purecompoennt
useMemo react中的计算属性 缓存性 无关数据发生改变 并不会引起重新计算
const variable = useMemo(要优化的函数,依赖项)
*/
export default Index;
