const { defineConfig } = require("@vue/cli-service");
module.exports = defineConfig({
  transpileDependencies: true,
  publicPath: "/hehe",
  lintOnSave: false,
  devServer: {
    proxy: {
      "/hehe": {
        target: "http://www.lrfc.vip:3001",
        changeOrigin: true,
        pathRewrite: {
          "^/hehe": "",
        },
      },
      "/admin": {
        target: "http://www.lrfc.vip:3001",
        changeOrigin: true,
      },
      "/banner": {
        target: "http://www.lrfc.vip:3001",
        changeOrigin: true,
      },
      "/upload": {
        target: "http://www.lrfc.vip:3001",
        changeOrigin: true,
      },
      "/menus": {
        target: "http://www.lrfc.vip:3001",
        changeOrigin: true,
      },
    },
  },
  // 覆盖vue-cli中webpack的配置
  // chainWebpack(){
  //   return {
  //   }
  // }
});
//代理的原理 跨域是浏览器的同源策略限制 服务器请求没有跨域问题
//目标接口 http://www.lrfc.vip:3001/admin/login
//代码    /hehe/admin/login = http://localhost:8080/hehe/admin/login
// 转发 http://www.lrfc.vip:3001/hehe/admin/login
// 路径重写  http://www.lrfc.vip:3001/admin/login

// vue.config.js 代理仅限于开发环境 上线没有本地服务器 失效 -> 后端或者运维 做nginx

// 去vue的官方文档查询 https://cli.vuejs.org/zh/config/
// 一定要重启
