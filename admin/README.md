## 网络请求
原生的ajax 
fetch
jq 
request
axios
### 
  1. url      http(80)  + ssl  =   https(443)
  2. methods  get  post put delete 
  3. params  
          get   query   xxxx?us=123&ps=123
          post  body(消息体) 
               json 
               x-www-form-urlendcode 表单数据
          header 
          cookie  readonly
  4. response
      状态码   200  404  403 ..
      code码  -1 登录 失败  -2 登录超时  0  登录成功
      请求信息 描述文本
      正式数据  正式的数据 
必须要学会使用network

### 文件到处导出
https://note.youdao.com/s/VOijiN5W
1. 直接将table dom 变成excel 进行导出
2. 后端请求接口数据 json    将json数据通过插件进行导出
3. 后端生成一个excel 文件 前端直接导出  a 
4. 调用后端接口 后端返回二进制数据流 前端做导出 

### 权限控制
不同的权限，能够调用不同的功能
在权限控制中 前端的控制都是假的 都是可以越过去的
前端的控制是为了用户的体验 没有权限的让用户直接看不到
1. 控制侧边栏的展示  根据后端数据展示侧边栏
2. 控制路由跳转  根据后端数据控制页面是否前忘403
3. 控制按钮是否可见 根据后端返回的数据控制按钮的可见性
4. 接口的统一处理 
   a.调用接口之前判断是否存在token
   b.