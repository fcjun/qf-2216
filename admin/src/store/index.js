import { createStore } from "vuex";
import login from "./login.js";
export default createStore({
  state: {
    name: 123,
    xixi: "柯南",
  },
  getters: {},
  mutations: {
    changeName(state) {
      state.name = 444;
    },
  },
  actions: {},
  modules: {
    // 模块1
    auth: {
      namespaced: true,
      state() {
        return {
          menus: JSON.parse(localStorage.getItem("menus") || "[]"),
          rules: JSON.parse(localStorage.getItem("rules") || "[]"),
        };
      },
      mutations: {
        changeMenus(state, data) {
          state.menus = data;
          // 存储到localstorage中
          localStorage.setItem("menus", JSON.stringify(data));
        },
        changeRules(state, data) {
          state.rules = data;
          // 存储到localstorage中
          localStorage.setItem("rules", JSON.stringify(data));
        },
      },
    },
    // 模块2
    login,
  },
});
