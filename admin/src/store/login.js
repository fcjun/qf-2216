export default {
  namespaced: true,
  state() {
    return {
      modalShow: false,
    };
  },
  mutations: {
    toggle(state, bool) {
      state.modalShow = bool;
    },
  },
};
