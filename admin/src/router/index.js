import { createRouter, createWebHashHistory } from "vue-router";
// import HomeView from "../views/HomeView.vue";
import Login from "@/pages/login/login.vue";
import Home from "@/pages/home/home.vue";
import Root from "@/pages/root/root.vue";
import Banner from "@/pages/banner/Banner.vue";
import AddBanner from "@/pages/banner/AddBanner.vue";
import Layout from "@/layout/Layout.vue";
import NoAuth from "@/pages/noAuth/noAuth.vue";

const routes = [
  {
    path: "/",
    redirect: "/home",
  },
  {
    path: "/",
    name: "layout",
    component: Layout,
    children: [
      {
        path: "home",
        name: "home",
        component: Home,
      },
      {
        path: "root",
        name: "root",
        component: Root,
      },
      {
        path: "/banner/list",
        name: "banner",
        component: Banner,
      },
      {
        path: "/banner/:editType",
        name: "banneradd",
        component: AddBanner,
      },
    ],
  },
  {
    path: "/403",
    name: "403",
    component: NoAuth,
  },
  {
    path: "/login",
    name: "login",
    component: Login,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  console.log("拦截器", to);
  const path = to.path;
  const notNeedLogin = ["/login"];
  const token = localStorage.getItem("token");
  // 获取后端给的权限信息
  const rules = JSON.parse(localStorage.getItem("rules") || "[]");
  // 允许访问的路径信息
  // const rulesPath = rules.map((item) => item.path);
  const rulesPath = ["/home", "/root", "/banner/list", "/403"];
  console.log(rules, rulesPath);
  if (notNeedLogin.includes(path)) {
    next();
  } else {
    if (token) {
      // token存在路由权限
      if (rulesPath.includes(path)) {
        next();
      } else {
        next("/403");
      }
      return;
    }
    // token 不存在直接去登录
    next("/login");
  }
});
export default router;
