import { createApp } from "vue";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import App from "./App.vue";
import router from "./router";
import store from "./store";
const app = createApp(App);
app.use(ElementPlus);
app.use(store);
app.use(router);
app.mount("#app");

/*
use 使用vue的插件
vue3 app.use();
vue2 vue.use();
vue 规定所有的插件需要抛出一个install方法 use 调用install方法 将vue的实例传递给插件内部
插件就可以在实例上注册 全局组件 挂载对象 全局mixin ... 
全局组件  router-link router-view
全局对象 $router  $route  $store
*/
