import axios from "@/utils/axios";

function BannerList(page = 1, pageSize = 10) {
  const url = "/banner";
  return axios.get(url, { params: { page, pageSize } });
}

function upload(data) {
  const url = "/upload";
  return axios.post(url, data);
}
function BannerAdd(data) {
  const url = "/banner";
  return axios.post(url, data);
}
// 根据id获取轮播图信息

function getBannerById(id) {
  const url = `/banner/${id}`;
  return axios.get(url, {});
}

// 轮播图更新
function updateaBanner(id, data) {
  const url = `/banner/${id}`;
  return axios.put(url, data);
}

export default {
  BannerList,
  upload,
  BannerAdd,
  getBannerById,
  updateaBanner,
};
