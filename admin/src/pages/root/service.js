import axios from "@/utils/axios";

function adminList(page = 1) {
  const url = "/admin";
  return axios.get(url, { params: { page, pageSize: 5 } });
}

function delAdmin(id) {
  const url = `/admin/${id}`;
  return axios.delete(url, {});
}

function addAdmin(userName, passWord) {
  const url = "/admin";
  return axios.post(url, { userName, passWord });
}
function updateAdmin(id, userName, passWord, leavel = "admin") {
  const url = `/admin/${id}`;
  return axios.put(url, { userName, passWord, leavel });
}

function exportAdmin() {
  // 导出管理员信息
  const url = "/admin/export";
  return axios({
    url,
    method: "post",
    responseType: "arraybuffer",
  });
}
export default {
  adminList,
  delAdmin,
  addAdmin,
  updateAdmin,
  exportAdmin,
};
