import axios from "@/utils/axios";

function Login(userName, passWord) {
  const url = "admin/login";
  return axios.post(url, { userName, passWord });
}
// 登录后获取权限
function getAuth() {
  const url = "/admin/getAuth";
  return axios.get(url, {});
}

// 登录后获取侧边栏信息

function getMenus() {
  const url = "/menus";
  return axios.get(url, {});
}

export default {
  Login,
  getAuth,
  getMenus,
};
