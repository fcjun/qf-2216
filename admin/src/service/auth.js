import axios from "@/utils/axios";

function getAuth() {
  const url = "/admin/getAuth";
  return axios.get(url, {});
}

export default {
  getAuth,
};
