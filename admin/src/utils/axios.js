import axios from "axios";
import store from "../store/index";
// axios.defaults.baseURL = 'https://api.example.com';
// 1. token 放在请求头里
// 2. key Authorization
// 3. value "Bearer token"
let token = localStorage.getItem("token");

// 设置请求头
// axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
// axios.defaults.headers.common["hehe"] = `123`;
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
// 添加请求拦截器
axios.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    // console.log("请求拦截器", config);
    // config.headers.hehe = 123;
    // 每次调用接口的时候判断token在不在不在，不在就获取一次
    if (!token) {
      token = localStorage.getItem("token");
    }
    config.headers["Authorization"] = `Bearer ${token}`;
    return config;
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

// 添加响应拦截器
axios.interceptors.response.use(
  function (response) {
    // 2xx 范围内的状态码都会触发该函数。
    // 对响应数据做点什么
    console.log("响应拦截器");
    console.log("xxx", response);
    // 响应拦截器里return就是 axios.then的数据
    const { headers } = response;
    // 判断状态码  402 跳转登录
    const codes = [402];
    if (codes.includes(response.data.code)) {
      // token 有问题要重新登录
      store.commit("login/toggle", true);
      return;
    }
    if (
      headers &&
      headers["content-type"] &&
      headers["content-type"] === "application/octet-stream"
    ) {
      // 返回的结果是二进制
      return response;
    }
    return response.data;
  },
  function (error) {
    // 超出 2xx 范围的状态码都会触发该函数。
    // 对响应错误做点什么
    return Promise.reject(error);
  }
);

export default axios;
