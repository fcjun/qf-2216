function exportExcel(res) {
  // 将数据流转化为blob
  // 转化为url对象
  const loadstream = window.URL.createObjectURL(new Blob([res.data]));

  //  从响应头里获取文件名
  const contentDisposition = res.headers["content-disposition"] || "";
  const encodedFilename = contentDisposition.split("=")[1];
  const filename = decodeURIComponent(encodedFilename);
  // 创建一个隐藏的a标签
  const link = document.createElement("a");
  link.style.display = "none";
  link.href = loadstream;
  link.download = filename; // 下载后文件名
  document.body.appendChild(link);
  // 模拟点击a标签
  if (navigator.userAgent.indexOf("Firefox") > -1) {
    // 火狐浏览器不支持click()
    const evt = document.createEvent("MouseEvents");
    evt.initEvent("click", true, true);
    link.dispatchEvent(evt);
  } else {
    link.click();
  }
  // 删除a标签
  document.body.removeChild(link);
  window.URL.revokeObjectURL(loadstream); // 释放掉blob对象
}

export default {
  exportExcel,
};
