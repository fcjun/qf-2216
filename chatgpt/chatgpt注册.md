# 小手一抖，chatGPT 到手

**马斯克投资了一家叫OpenAI的公司，最近推出了一个特别火的应用叫做ChatGPT，最近ChatGPT刷爆了各大平台。同样以为一些原因的限制国内的用户注册略有困难，这篇教程就教大家如何注册**

注册的前提条件：
1、拥有一个自己的邮箱账号，用于注册账号。

2、你能够科学上网，具备代理网络的环境。

3、拥有一个国外手机号，用于接收注册验证码。

## **注册步骤：**

**第一步：正常访问谷歌,到下面这个网站上面去注册：**

> https://beta.openai.com/signup

**第二步：邮箱认证。收到激活账号的邮件后，点击激活链接继续。**

**第三步：输入昵称、用途，点击下一步。**

**第四步：接下来需要验证手机号，但是尝试后发现使用我国的手机号无法注册，很多小伙伴也是卡在了这一步。**

**解决方法：找到一个可以\*提供国外手机号码验证服务的产品，从而实现接码（接收验证码）\***

> https://sms-activate.org/cn/getNumber

**打开一个新页面，输入上方接码网站，注册一个账号，右上角点击充值，选择支付宝，充值0.2美元，大约13卢比，1.4元人民币。**

![image-20221215232231019](https://gitee.com/fcjun/image/raw/master/img/202212152322516.png)

**在接码网站左侧服务搜索OpenAI，选择的是印度。选择之后，点击购物车 🛒 图标进行租用。**

别问为什么是印度，物美价廉

![image-20221215232714201](https://gitee.com/fcjun/image/raw/master/img/202212152327222.png)

**返回注册ChatGPT网页，输入刚刚购买的手机号，点击继续后，出现输入验证码页面。**

**切换到接码网站，等待手机验证码到来。之后复制，粘贴到注册ChatGPT网页，完成验证，注册成功。**

![image-20221215233042900](https://gitee.com/fcjun/image/raw/master/img/202212152330920.png)

**提醒：每个号码能用20分钟，没收到验证码可以点击右边 X 号退掉重租手机号接码。没有收到验证码，记得退掉，余额会返还！**



**第五步：注册完成，随便选择一个即可。**

![image-20221215233207853](https://gitee.com/fcjun/image/raw/master/img/202212152332873.png)

![image-20221215233247714](https://gitee.com/fcjun/image/raw/master/img/202212152332734.png)

**第六步：登录**

**到这里就注册完成了，我们再重新登录，即可进入！**

![image-20221215235102657](https://gitee.com/fcjun/image/raw/master/img/202212152351684.png)