# ChatGPT 在教学场景下的使用

## 注册chatgpt免费版

参考链接: [一元钱注册ChatGPT账号](https://mp.weixin.qq.com/s?__biz=MzA3ODc0NzExNw==&mid=2718052057&idx=1&sn=cbcfa20aa755fe37c55bc97ffdf14e0c&chksm=b8488a5b8f3f034d060f8c2c9c143925670269a9edac245ba79550d21ff225f0abc25022bab1&scene=132#wechat_redirect)

## 基本使用

### 需要科学上网(使用任意翻墙工具，如果没有可以使用极光)


   科学软件有很多种，但是大多需要付费 极少数提供免费试用，当然免费试用在就没法要求网速和稳定性了

在可以随意使用科学软件达成科学上网的目的，我们仅介绍一款 方便操作，并且提供一定免费时长的

1. 下载“科学”软件

   https://arr007.network/download/

   根据自己的设备情况下载对应的版本

   ![image-20230101202404071](https://gitee.com/fcjun/image/raw/master/img/202301012024457.png)

2. 下载完成之后进行安装，如果安装过程中提示病毒请关闭防火墙 忽略

3. 下载完毕后就可以使用，不要注册

   ![image-20230101202608596](https://gitee.com/fcjun/image/raw/master/img/202301012026614.png)

4. 因为是免费版 所以稳定性和速度 懂的都懂
5. 通过某些网站进行验证

     https://www.youtube.com/

### 登录

在科学上网的环境下登录

官网 [https://chat.openai.com/chat]https://chat.openai.com/chat)

![image-20230219182254175](https://gitee.com/fcjun/image/raw/master/img/202302191825087.png)

## 讲师使用

1. 在前一天查询上课所用知识点内容
2. 课堂查阅某些常规错误与api

![image-20230219183406282](https://gitee.com/fcjun/image/raw/master/img/202302191834308.png)

## 学员使用

1. 提供代码示例

ChatGPT 可以根据学生的问题提供相关的代码示例，帮助学生更好地理解和解决问题。这可以极大地提高学生的学习效率和编程能力。

![image-20230219183528678](https://gitee.com/fcjun/image/raw/master/img/202302191835704.png)

2. 提供语法和错误提示

ChatGPT 可以帮助学生检查语法错误并提供有用的提示，以帮助他们更快地找到问题所在。此外，ChatGPT 还可以根据代码上下文和学生的问题提供有用的错误提示，帮助他们理解和解决问题。

提供一个vscode错误提示：

![image-20230219184721715](https://gitee.com/fcjun/image/raw/master/img/202302191847739.png)

将错误信息复制到chatGPT 获的解决方案

![image-20230219184835838](https://gitee.com/fcjun/image/raw/master/img/202302191848864.png)

3. 提供代码建议

ChatGPT 可以根据学生的问题和代码上下文提供有用的代码建议，帮助他们提高代码质量和效率。这可以使学习者更好地掌握编程技能并提高工作效率。

比如优化以下代码: 

```vue
<template>
  <div v-for="item in data" :index="item.path" :key="item.path">
    <!-- item 有children 渲染2级-->
    <el-sub-menu
      :index="item.path"
      v-if="item.children && item.children.length > 0"
    >
      <template #title>{{ item.nameCn }}</template>
      <!-- 组件调用自己 实现递归 -->
      <custom-nav :data="item.children"></custom-nav>
    </el-sub-menu>
    <!-- item 没有children 渲染1级-->
    <el-menu-item v-else :index="item.path" @click="jump">
      {{ item.nameCn }}
    </el-menu-item>
  </div>
</template>
<script>
export default {
  props: ["data"],
  methods: {
    jump(e) {
      console.log(e);
      this.$router.push(e.index);
    },
  },
};
</script>

```

优化建议

```vue
<template>
  <div v-for="item in data" :index="item.path" :key="item.path">
    <!-- item 有children 渲染2级-->
    <el-sub-menu :index="item.path" v-if="hasChildren(item)">
      <template #title>{{ item.nameCn }}</template>
      <!-- 组件调用自己 实现递归 -->
      <custom-nav :data="item.children"></custom-nav>
    </el-sub-menu>
    <!-- item 没有children 渲染1级-->
    <el-menu-item v-else :index="item.path" @click="jump(item)">
      {{ item.nameCn }}
    </el-menu-item>
  </div>
</template>
<script>
export default {
  props: ["data"],
  methods: {
    hasChildren(item) {
      return item.children && item.children.length > 0;
    },
    jump(item) {
      console.log(item);
      this.$router.push(item.path);
    },
  },
};
</script>
上述代码中，我们将判断是否有子节点的逻辑提取为 hasChildren 方法，使得模板代码更加简洁。在 jump 方法中，我们传入了 item 对象，直接获取其 path 属性，而不是通过事件对象获取 index 属性，这样也使得逻辑更加清晰。
```



4. 提供面试编程知识点思路

ChatGPT 可以根据学生的问题提供相关的编程知识点，帮助他们更好地理解编程概念和技术。这可以提高学生的理论知识和实践能力，使他们成为更好的编程人员。

![image-20230219183332110](https://gitee.com/fcjun/image/raw/master/img/202302191833136.png)

5. 帮助阅读源码,和其他同学代码

​          复制一段pina源代码使用chatgpt进行解析

![image-20230219183900587](https://gitee.com/fcjun/image/raw/master/img/202302191839611.png)

6. 对一些常见的业务功能进行提示

提问: 使用react函数组件 + antd 实现一个登录页面

回答 ：

```jsx
import React, { useState } from 'react';
import { Form, Input, Button } from 'antd';

const LoginForm = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleEmailChange = e => setEmail(e.target.value);
  const handlePasswordChange = e => setPassword(e.target.value);

  const handleSubmit = e => {
    e.preventDefault();
    // 在此处处理登录逻辑
  };

  return (
    <Form layout="vertical" onFinish={handleSubmit}>
      <Form.Item label="邮箱">
        <Input type="email" value={email} onChange={handleEmailChange} />
      </Form.Item>
      <Form.Item label="密码">
        <Input type="password" value={password} onChange={handlePasswordChange} />
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit">
          登录
        </Button>
      </Form.Item>
    </Form>
  );
};

export default LoginForm;
该组件使用了 useState hook 来管理表单字段 email 和 password 的状态。当这两个字段的值发生变化时，会触发对应的 handleEmailChange 和 handlePasswordChange 回调函数。在表单提交时，会触发 handleSubmit 回调函数，在此函数中可以处理登录逻辑。

组件中使用了 Ant Design 的 Form、Input 和 Button 组件来构建表单。Form.Item 用于包裹每个表单项，label 属性用于设置表单项的标签。在 Button 组件中，type="primary" 设置按钮为主色调，htmlType="submit" 使按钮成为提交按钮。

```

## 总结

ChatGpt的使用到此为止，其实大家也可以看出ChatGpt对一些高频的，固定的问题回答还是比较精准的，对于一些业务逻辑复杂化的代码功能处理起来还是有一定问题滴！

对学习和开发效率的提升也是非常明显滴，真香！！