// 链接数据库
// http://www.mongoosejs.net/docs/api.html#Model
// https://note.youdao.com/s/bAvSd7xH

const mongoose = require("mongoose");
mongoose.connect("mongodb://47.122.24.223:27017/2216", { useNewUrlParser: true });

let db = mongoose.connection;
db.on("error", function (error) {
  console.log("Database connection failure：" + error);
});

db.on("open", function () {
  console.log("数据库连接成功");
  // initData();
});

db.on("disconnected", function () {
  console.log("数据库连接断开");
});

// function initData() {
//   // 1.创建一个Schema对象
//   const Schema = mongoose.Schema;
//   // 2.数据的表头信息
//   let userSchema = new Schema({
//     //  _id:{type:String,required:true},
//     user: { type: String, default: null },
//     pass: { type: String, default: null },
//     ctime: { type: Date, default: Date.now },
//   });
// //   // 3.将Schema和数据模型做关联 第一个参数是数据库的表名， 第二个参数就是表头数据对象
//   let userModel = mongoose.model("hehe", userSchema);

//   // userModel.insertMany({ user: "王二", pass: "hehe" }).then((db) => {
//   //   console.log(db);
//   //   console.log("添加 ok");
//   // });
//   userModel.deleteOne({ user: "王二" }).then((db) => {
//     console.log(db);
//     console.log("添加 ok");
//   });
// }
