// 创建和用户表相关的数据模型
const mongoose = require("mongoose");
  const Schema = mongoose.Schema;
// 2.数据的表头信息
let goodsSchema = new Schema({
  name: { type: String, default: null },
  desc: { type: String, default: null },
  img: { type: String, default: null },
  price: { type: Number, default: null },
  stock: { type: Number, default: null },
  ctime: { type: Date, default: Date.now },
});
//Schema  和数据表关联 
const goodsModel = mongoose.model('goods', goodsSchema);