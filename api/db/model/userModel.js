// 创建和用户表相关的数据模型
const mongoose = require("mongoose");
  const Schema = mongoose.Schema;
// 2.数据的表头信息
let userSchema = new Schema({
  user: { type: String, default: null },
  pass: { type: String, default: null },
  token: { type: String, default: null },
  ctime: { type: Date, default: Date.now },
});
//Schema  和数据表关联 
const userModel = mongoose.model('user', userSchema);

module.exports = userModel