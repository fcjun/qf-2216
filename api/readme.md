node  express mongodb mongoose 

创建一个node express 项目
接口的流程 
  1. 接口要能接受前端的数据
  2. 根据前端的数据做处理
     操作数据库
  3. 将结果返回给前端

# 项目上线
目的： 网页项目能够在互联网范围内访问到
ip 在互联网环境内找到服务器地址   10.9.75.111 
dns服务器 域名解析服务器  实现域名和ip的绑定关系
域名 一个代号
服务器 
   一台能被外部访问的电脑 
   有服务器软件 端口号
   node 
   iis 
   nginx 
   ftp

局域网 小型的互联网
互联网 将一台台的电脑 通过网线进行连接
端口号: 任何一个软件都运行都有一个端口号
node: 3000  
vue-cli 8080
mongodb 27017
ftp: 22
## https://note.youdao.com/s/bAvSd7xH
## 前端上线 -> 网页被访问
准备的服务器 
   是一台电脑 系统 win（8,9,10,11）  macos  linux（centos unbutn）
   要在互联网环境内访问 公网ip   私网（局域网ip）ip
   安装服务器软件 node静态资源 nginx  iis tomcat apache
代码如何上传到服务器？

## 后端上线 -> 接口能被访问
1. 安装nvm 
2. 通过nvm安装node
3. 通过git下载接口代码 没有git 使用apt-get安装
4. 安装接口需要的node_modules
5. 在对应的目录下运行接口代码 
6. 运行node接口后 没有办法做其他事情了 开启子进程 
7. 使用进程守卫 维护node接口的运行 screen

## 数据库
1. 安装完全参照 文档
2. 安装完毕后启动
3. 使用可视化工具尝试连接 失败
   失败原因
   1. 阿里云的安全组限制 -> 需要在安全组开启27017端口
   2. mongodb 本身限制 -> 需要配置文件将bindip 从 127.0.0.1 -》 0.0.0.0 -> 重启mongodb
4. 使用可视化工具尝试连接 成功
5. 尝试用node的接口代码测试

## 上线的代理配置
接口服务器 http://www.lrfc.vip:3001 
线上服务器 http://47.122.24.223
登录接口   http://www.lrfc.vip:3001/admin/login
线上登录接口 http://47.122.24.223/admin/login


## 项目优化


## token
1. 产生token
2. 验证token合法
3. 验证token是否有效
4. token拦截