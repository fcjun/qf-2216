const express = require('express');

// const db = require('./db/connect');
const cors = require("cors");
const app = express();
const bodyParser = require('body-parser');
// 使用 body-parser 中间件解析请求体
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// 跨域
app.use(cors())
// 静态服务
// 10.9.75.111:3000/index.html
// 10.9.75.111:3000/1.png
// app.use(express.static('hehe'));
// 10.9.75.111:3000/xixi/1.png
app.use("/hehe",express.static('hehe'));
// 引入路由
const userRouter = require("./router/user");
const bannerRouter = require("./router/banner");

// 使用路由
app.use("/user",userRouter)
app.use("/banner",bannerRouter)
// 启动服务器
app.listen(3000, () => {
  console.log('Server started on port 3000');
});