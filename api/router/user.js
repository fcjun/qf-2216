const express = require("express");
const router = express.Router();
const UserModel = require('../db/model/userModel');
const jwt = require("jsonwebtoken");
// 定义注册接口
router.post('/register', async  (req, res) => {
  // 接受数据
  const { user, pass } = req.body;
  // 判断用户名是否重复
  const count= await UserModel.find({ user }).count()
  if(count) {
    return res.send({ codo:-1, msg: "用户名重复"})
  }
  const insertData = await UserModel.insertMany({user,pass})
  if(insertData && insertData.length > 0 ) {
    return  res.send({ codo:0, msg: "注册成功"})
  }
  // 返回数据
  res.send({ codo:-2, msg: "注册失败"})
  
});

router.get('/login', async (req, res) => {
  // 接受数据
  const { user, pass } = req.query;
  const userInfo = await UserModel.find({ user, pass })
  if(userInfo && userInfo.length ) {
    console.log(userInfo)
    // 从用户信息里取出 除密码外的其他参数
    const { user, _id} = userInfo[0];
    const token = jwt.sign({user,_id, ctime:(new Date()).getTime()},"2216", { expiresIn: 60*60*24 })
    const updateData = await UserModel.findByIdAndUpdate(_id,{token})
    console.log(updateData)
    return res.send({ codo:0, msg: "登录ok", token, user,_id })
  }
  // 返回数据
  res.send({ codo:-2, msg: "登录失败"})
  
});

router.post('/deluser', async (req, res) => {
  // 接受数据
  const { id } = req.body;
  const delInfo = await UserModel.findByIdAndDelete(id)
  if(delInfo) {
    return res.send({ codo:0, msg: "删除ok"})
  }
  return res.send({ codo:-1, msg: "删除失败"})

  
});
/**
 * @api {post} /updateUser 更新用户信息
 * @apiName updateUser
 * @apiGroup User
 *
 * @apiParam {String} id 用户id
 * @apiParam {String} user 用户name
  * @apiParam {String} pass 用户pass
 *
 * @apiSuccess {Number} code code码 0  成功 -1 失败.
 * @apiSuccess {String} msg  返回信息.
  * @apiSuccess {Arrary} list  用户列表.
 */
router.post('/updateUser', async (req, res) => {
  // 接受数据
  const { id, user, pass } = req.body;
  const updateInfo = await UserModel.findByIdAndUpdate(id,{ user,pass})
console.log(updateInfo)
  if(updateInfo) {
    return res.send({ codo:0, msg: "修改ok"})
  }
  return res.send({ codo:-1, msg: "修改失败"})

  
});
/**
 * @api {get} /getUsers 查询用户列表
 * @apiName getUsers
 * @apiGroup User
 *
 * @apiParam {Number} page 页码数.
 * @apiParam {Number} pageSize 每页条数.
 *
 * @apiSuccess {Number} code code码 0  成功 -1 失败.
 * @apiSuccess {String} msg  返回信息.
  * @apiSuccess {Arrary} list  用户列表.
 */
// 查询所有的用户信息
router.get('/getUsers',async (req,res) => {
  const { page =1,pageSize =2} = req.query
  const list =await userModel.find().limit(pageSize).skip((page-1)*pageSize);
  if(list) {
    return res.send({ code:0,msg:"查询ok",list})
  }
  return res.send({ code:-1,msg:"查询nook"})
})
module.exports = router;