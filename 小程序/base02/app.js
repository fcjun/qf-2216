import storage from "./utils/storage";
storage.setItem("token", 6677)
// console.log(storage.getItem("token"))
App({
  hehe: {
    us:123,
    ps:456
  },
  xixi:"你好",
  onLaunch() {
    console.log("小程序启动")
    this.updateCheck();
  },
  onHide() {
    console.log("小程序隐藏")
  },
  onShow() {
    console.log("小程序显示")
  },
 updateCheck(params) {
    //  调用后端接口 isForce: true false 
    // 小程序内置的更新管理器  
    const updateManager = wx.getUpdateManager()

    updateManager.onCheckForUpdate(function (res) {
      // 请求完新版本信息的回调
      console.log(res)
    })
    
    updateManager.onUpdateReady(function () {
      // 代码下载完毕
      wx.showModal({
        title: '更新提示',
        content: '新版本已经准备好，是否重启应用？',
        success(res) {
          console.log(res)
          if (res.confirm) {
            // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
            updateManager.applyUpdate()
          }
        }
      })
    })
  }
})