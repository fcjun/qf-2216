/*
体验版 和 正式版小程序使用同一个套缓存 在某些场景下有bug出现
1. 用环境变量区分体验版还是正式版 
2. 让localstorage有过期时间
  
*/ 
// const env ="online"  
const env = "test"
class Storage {
  // 保存缓存
  setItem(key,data,exp = 0) {
    // 保存数据多保存一个创建时间 和过期时长
    const payload = {
      data,
      cTime: (new Date()).getTime(),
      exp
    }
    wx.setStorageSync(`${env}_${key}`, payload)
  }
  // 获取缓存
  getItem(key) {
    // 取值的时候用取值时间 - 过期时间 判断是否大于过期时长
    const {data,cTime,exp} = wx.getStorageSync(`${env}_${key}`);
    if(!exp) return data; // exp 0 没有过期时间
    const nowTime = (new Date()).getTime();
    if(nowTime - cTime > exp) {
      // 如果过期 返回null 将混存清除
      wx.removeStorageSync(`${env}_${key}`)
      return null
    }
    return data;
  }
  removeItem(key) {
    wx.removeStorageSync(`${env}_${key}`)
  }
  clear() {
    wx.clearStorageSync();
  }
}

export default new Storage();