/*
将wx内置的api进行二次处理
*/ 

function chooseImg() {
   return new Promise((resolve,reject) => {
     wx.chooseMedia({
      count: 1,
      mediaType: ['image'],
      sourceType: ['album', 'camera'],
      camera: 'back',
      success(res) {
        const {
          tempFilePath
        } = res.tempFiles[0]
        resolve(tempFilePath)
      }
     })
   })
}

// 文件上传
function uploadFile(filePath) {
  return new Promise((resolve,reject) => {
    wx.uploadFile({
      url: 'http://www.lrfc.vip:3002/upload', //仅为示例，非真实的接口地址
      filePath,
      name: 'hehe',
    
      success (res){
        const data = res.data
        console.log(6666,data)
        resolve(JSON.parse(data));
      }
    })
  })
}

export default {
  chooseImg,
  uploadFile
}
