import axios from "../../utils/axios.js";
import wxApi from "../../utils/wxApi";

Page({
  data: {
    tempFilePath: "http://tmp/qbSXJJX8XtkCe745ffc4d25384692f6f051a613defcb.png",
  },
  onLoad() {
    this.getData();
  },
  // 微信登录
  login() {
    wx.login({
      success: async (res) => {
        const data = await axios.post("/user/wxlogin",{code:res.code})
        console.log(data)
      }
    })
  },
  async getData() {
    // wx.request({
    //   url: 'http://www.lrfc.vip:3002/version', //仅为示例，并非真实的接口地址
    //   method: "GET",
    //   data: {},
    //   header: {
    //     'content-type': 'application/x-www-form-urlencoded' // 默认值
    //   },
    //   success (res) {
    //     console.log(res) 
    //   }
    // })
    const url = "/version"
    const res = await axios.get(url, {}, true)
    console.log(res)
  },
  // 选择图片或者视频
  async selFile() {
    const file = await wxApi.chooseImg();
    const {code, msg, data} = await wxApi.uploadFile(file);
    console.log(1111, data)
    this.setData({tempFilePath: data.url })
  }
  // selFile() {
  //   wx.chooseMedia({
  //     count: 1,
  //     mediaType: ['image'],
  //     sourceType: ['album', 'camera'],
  //     camera: 'back',
  //     success: (res) => {
  //       console.log("选择图片", res)
  //       const {
  //         tempFilePath
  //       } = res.tempFiles[0]
  //       // this.setData({ tempFilePath })
  //       // 将临时文件调用接口上传
  //       wx.uploadFile({
  //         url: 'http://www.lrfc.vip:3002/upload', //仅为示例，非真实的接口地址
  //         filePath: tempFilePath,
  //         name: 'hehe',
  //         success(res) {
  //           console.log(res);
  //         },
  //         fail(err) {
  //           console.log(err)
  //         }
  //       })
  //     }
  //   })
  // }

})