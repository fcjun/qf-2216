// 获取小程序的实例
const app = getApp();
import utils from "./util";
utils.sayHello();
// commonjs
// const utils  = require("./util");
// utils.sayHello();
Page({
  data: {
    value: 233
  },
  onLoad() {
    console.log("首页加载完成")
    // wx.setStorageSync('token', 12313)
    // wx.setStorageSync('hehe', 12313)
    // console.log(wx.getStorageSync('token'))
    // wx.removeStorageSync("hehe")
    // wx.clearStorageSync()

    // wx.showTabBarRedDot({
    //   index: 0,
    // })
    wx.setTabBarBadge({
      index: 0,
      text: '999+'
    })
  },
  onShow() {
    console.log("页面显示")
  },
  onHide() {
    console.log("页面隐藏")
  },
  // 跳转tabbar
  switchTab() {
    wx.switchTab({
      url: '/pages/logs/logs',
      // url: '/pages/detail/detail',
    })
  },
  // 跳转非tabbar
  navigate() {
    wx.navigateTo({
      url: '/pages/detail/detail?us=123&ps=456',
    })
  },
  // 跳转非tabbar页面
  redirect() {
    wx.redirectTo({
      url: '/pages/detail/detail',
    })
  },
  // 关闭所有页面跳转一个新的页面
  relunch() {
    wx.reLaunch({
      url: '/pages/logs/logs',
    })
  }


})
