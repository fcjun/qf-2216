/*
将wx内置的api进行二次处理
*/ 
import storage from "./storage";
function chooseImg(limit = false) {
   return new Promise((resolve,reject) => {
     wx.chooseMedia({
      count: 1,
      mediaType: ['image'],
      sourceType: ['album', 'camera'],
      camera: 'back',
      success(res) {
        const {
          tempFilePath
        } = res.tempFiles[0]
        if(limit) {
          // 如果要做限制 直接返回 文件对象
          resolve(res.tempFiles[0])
        } else {
          resolve(tempFilePath)
        }
      }
     })
   })
}

// 文件上传
function uploadFile(filePath) {
  const token = storage.getItem("token");
  if(!token) {
    // 判断token是否存在
    wx.redirectTo({
      url: '/pages/login/login',
    })
    return;
  }
  return new Promise((resolve,reject) => {
    wx.uploadFile({
      url: 'http://www.lrfc.vip:3002/upload', //仅为示例，非真实的接口地址
      filePath,
      name: 'hehe',
      header: {
        Authorization: "Bearer " + token,
      },
      success (res){
        const data = JSON.parse(res?.data|| "{}")
        if(!data.code) {
          resolve(data);
        } else if(data.code === 402) {
          wx.showModal({
            title: "登录状态丢失请重新登录!",
            cancelColor: 'cancelColor',
            success(res) {
              if(res.confirm) {
                wx.redirectTo({
                  url: '/pages/login/login',
                })
              }
            }
          })
        }
      }
    })
  })
}

// 获取登录code

function getLoginCode() {
  return new Promise((resolve,reject) => {
    wx.login({
      success(res) {
        resolve(res.code)
      }
    })
  })
}
// 获取地理位置信息
function getLocation() {
  return new Promise((resolve,reject) => {
    wx.getLocation({
      type: 'wgs84',
      success(res) {
         resolve(res)
      },
      fail(err){
        console.log(err)
      }
    })
  })
}


export default {
  chooseImg,
  uploadFile,
  getLoginCode,
  getLocation
}
