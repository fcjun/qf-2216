/*
调用接口的时候和axios一样 
1. 参数1 地址  
2. 参数2数据 
3.可以then 
4.也可以async
*/ 
import env from "../config/env.js";
console.log(env);
const { baseURL } =env;
import storage from "./storage"
class  Axios   { 
  // 请求的内置属性
  
  _request(url,method,data={},isLoading) {
    if(isLoading) {
      wx.showLoading({
        title: '数据加载ing',
      })
    }
    return new Promise((resolve,reject) => {
      const token = storage.getItem("token")
      const that = this;
      wx.request({
        url: baseURL + url, //仅为示例，并非真实的接口地址
        method,
        data,
        header: {
          // 'content-type': 'application/x-www-form-urlencoded' // 默认值
          Authorization: "Bearer " + token,
        },
        success(res) {
          const result = that.interceptor(res)
          resolve(result)
        },
        fail(err) {
          reject(err)
        },
        complete() {
          wx.hideLoading()
        }
      })

    })
  }
  get(url,data,isLoading ) {
    return this._request(url,"GET",data,isLoading)
  }
  post(url,data,isLoading) {
    return this._request(url,"POST",data,isLoading)
  }
  put() {}
  delete() {}
  interceptor(res) {
    // 处理响应数据
    return res.data
  }
}

export default new Axios();