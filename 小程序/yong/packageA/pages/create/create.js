import moment from "moment";
import service from "./service";
Page({
  data: {
    type: "63f7804539f0996868eff06e",
    topImg: "http://img.lrfc.vip/1678507131059.png", // 活动简介图
    title: "",
    desc: "",
    timeShow: false,
    activityStartTime: "",
    activityEndTime: "",
    applyStartTime: "",
    applyEndTime: "",
    activityStartTimeShow: "",
    activityEndTimeShow: "",
    applyStartTimeShow: "",
    applyEndTimeShow: "",
    activityImgs: [
      "http://img.lrfc.vip/1678507131059.png",
      "http://img.lrfc.vip/1678507135895.png"
    ],
    address:"",
    latitude:40.116261, 
    longitude:116.250127,
    map: true,
  },
  // 上传首图
  changeTopImg(e) {
    this.setData({
      topImg: e.detail[0]
    })
  },
  // 时间修改
  timeChange(e) {
    console.log("时间改变", this.data.timeShow, e)
    const obj = {}
    obj[this.data.timeShow] = e.detail
    obj[`${this.data.timeShow}Show`] = moment(e.detail).format("YYYY-MM-DD hh:mm:ss")
    obj['timeShow'] = false;
    console.log(obj)
    this.setData(obj)
  },
  // 选择时间
  selTime(e) {
    const {
      type
    } = e.currentTarget.dataset
    this.setData({
      timeShow: type
    })
    console.log("选择时间", type)
  },
  // 修改图片
  changeImgs(e) {
    this.setData({ activityImgs: e.detail})
  },
  // 选择地址
  chooseAddress() {
    console.log(1111)
    wx.chooseLocation({
      success: (result) => {
        const {address,latitude, longitude} = result
        this.setData({address,latitude, longitude})
      },
      fail(err) {
        console.log(err)
      }
    })
  },
  // 创建活动
  async create() {
    const res = await service.createActivity(this.data)
    console.log(res);
  }
})