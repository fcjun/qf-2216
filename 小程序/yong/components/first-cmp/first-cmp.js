import behavior from "./bahavior";
Component({
   data: {
     age: 16,
     name1: "自己的name1",
     info:{ us:123},
     arr:[1,2]
   },
   behaviors: [behavior],
  //  数据监听
   observers: {
    //  age(...arg) {
    //    console.log(arg)
    //  },
    //  name(nName) {
    //    console.log("name变了",nName)
    //  }
     "age,name"(...arg){
       console.log('数据变了',arg)
     }
   },
   options: {
     multipleSlots: true, // 开启多插槽
   },
  // properties 来接受组件上的自定义属性
   properties: {
     name: String,
   },
   methods: {
    //  调用父组件的方法
    callFa() {
      // 调用组件上的自定义事件
      // this.$emit("自定义事件名",参数)
      this.triggerEvent("hehe",{ us:123,ps:456})
    },
    //  add() {
    //    this.setData({ age: ++this.data.age, name: "123"})
    //  }
   },
  //  attached() {
  //    console.log("组件挂载")
  //  },
   lifetimes: {
     attached() {
       console.log("组件内部新组件挂载")
     }
   }
})