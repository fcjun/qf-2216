export default Behavior({
  data: {
    name1: "behavior name1 韩梅梅",
    info: {ps:456},
    arr:[3,4]
  },
  lifetimes: {
    attached() {
      console.log("behavior attacted")
    }
  },
  methods: {
    add() {
      console.log("behavior add")
    }
  }
})