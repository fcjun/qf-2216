import wxApi from "../../utils/wxApi"
Component({
  data: {
    fileList: [
     
    ],
  },
  properties: {
    max: Number,
    isCustom: false,
    list: Array,
  },
  methods: {
    async upload() {
      const {fileType, size, tempFilePath} = await wxApi.chooseImg(true);
      if(fileType!=="image" || size >= 1024*1024) {
        return wx.showToast({
          title: '文件类型或者大小限制',
        })
      } 
      const {code, data} = await wxApi.uploadFile(tempFilePath);
      if(code) return wx.showToast({
        title: '上传失败',
      })
      this.data.fileList.push(data.url)
      this.setData({fileList: this.data.fileList})
    },
    // 删除
    del(e) {
      const {idx} = e.currentTarget.dataset;
      this.data.fileList.splice(idx,1)
      this.setData({fileList: this.data.fileList})
    }
  },
  observers: {
    fileList(files) {
      console.log("文件数组改变",files)
      this.triggerEvent("finish",files)
    }
  },
  lifetimes: {
    attached() {
      this.setData({fileList: this.data.list})
    }
  }
})