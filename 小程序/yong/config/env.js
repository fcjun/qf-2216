const env = "test" 
// const env = "online" 
const obj = {
  test: {
    baseURL: "http://www.lrfc.vip:3002",
    env: "test"
  },
  online: {
    baseURL: "https://www.lrfc.vip",
    env: "online"
  }
}

export default obj[env]