import axios from "../../utils/axios";

// 获取轮播图
function getBanners() {
  const url = "/banner"
  return axios.get(url)
}
// 获取推荐列表
function getRecommend(page = 1,pageSize = 1000) {
  const url = `/activity?page=${page}&pageSize=${pageSize}`
  return axios.get(url)
}

export default  {
  getRecommend,
  getBanners
}