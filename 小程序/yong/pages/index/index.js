import service from "./service";
Page({
  data: {
    banners: [
      "https://img2.baidu.com/it/u=175449109,3788073609&fm=253&fmt=auto&app=138&f=JPEG?w=561&h=500",
      "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fc-ssl.duitang.com%2Fuploads%2Fblog%2F202106%2F09%2F20210609081952_51ef5.thumb.1000_0.jpg&refer=http%3A%2F%2Fc-ssl.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1681091630&t=ba451446082f60ca4aac1d162c54e13e",
      "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fc-ssl.duitang.com%2Fuploads%2Fblog%2F202106%2F22%2F20210622154903_3c36a.thumb.1000_0.jpeg&refer=http%3A%2F%2Fc-ssl.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1681091641&t=8bfa18057fea1f0b337032a3a362022e"
    ],
    list: [],
    nowPage:1, // 当前第几页
    pageSize: 6, // 每页多少条数据
    allPage: 1,// 总的页码数
  },
  // 方法
  jump(e) {
   const {path} = e.currentTarget.dataset
   wx.navigateTo({ url: path })
  },
  search() {
    wx.switchTab({
      url: '/pages/categroy/categroy',
    })
  },
  onLoad() {
    this.getBanner();
    this.getRecommend();
  },
  // 获取banner
  async getBanner() {
    const res = await service.getBanners();
    console.log(res)
  },
  // 获取推荐
  async getRecommend() {
    const { nowPage,pageSize} = this.data;
    const {code,count,list} = await service.getRecommend(nowPage,pageSize);
    const allPage = Math.ceil(count/pageSize);
    // 使用当前的页码数进行区分处理 如果是第一页 直接覆盖 如果不是第一页 拼接
    if(nowPage === 1) {
      this.setData({ list,allPage })
    } else  {
      this.setData({allPage,list: [...this.data.list,...list]})
    }
    // 接口调用成功停止下拉刷新
    wx.stopPullDownRefresh()
  },
  // 下拉刷新
  onPullDownRefresh() {
    // 下拉刷新把页码数重置
    this.setData({nowPage:1});
    this.getBanner();
    this.getRecommend();
  },
  // 上拉加载
  onReachBottom() {
    let { nowPage, allPage} = this.data
    console.log("到底了",nowPage,allPage)
    // 当前的页码数已经>=总的页码数 已经到底了
    if(nowPage >= allPage) {
      return wx.showToast({
        title: '已经到底了',
      })
    }
    // 页码数+
    this.setData({ nowPage: ++nowPage})
    // 调用接口
    this.getRecommend();
  },
  // 跳转详情页
  goDetail(e) {
    const {id} = e.currentTarget.dataset;
    wx.navigateTo({
      url: '/pages/detail/detail?id='+id,
    })
  },
  jumpList() {
    wx.navigateTo({
      url: '/pages/my-list/my-list',
    })
  }
})
/*
 实现上拉加载
 当前的页码数 每页数据条数 一共有多少页
 通过页码数调用接口获取数据
 刷新和加载用的是同一个接口 刷新是直接更新数据 加载数据的拼接

*/
