import axios from "../../utils/axios";

// 获取轮播图
function getBanners() {
  const url = "/banner"
  return axios.get(url)
}
// 获取推荐列表
function getRecommend() {
  const url = "/activity?page=1&pageSize=99999"
  return axios.get(url)
}

export default  {
  getRecommend,
  getBanners
}