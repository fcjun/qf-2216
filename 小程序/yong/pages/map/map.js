import wxApi from "../../utils/wxApi";
import service from "./service";
import moment from "moment"
Page({
  data: {
    selMarkerId: 0, // 当前点击的markerid
    longitude: 0,
    latitude: 0,
    markers: [],
  },
  async onLoad() {
    this.getData();
    const {
      longitude,
      latitude
    } = await wxApi.getLocation() || {};
    console.log(longitude, latitude)
    this.setData({
      longitude,
      latitude
    })
  },
  // 点击marker回调
  markertap(e) {
    const { markerId } = e.detail;
    this.setData({ selMarkerId: markerId })
  },
  close() {
    this.setData({ selMarkerId:0 })
  },
  // 跳转详情
  goDetail(e) {
    console.log(e);
    this.close();
    wx.navigateTo({
      url: `/pages/detail/detail?id=${e.currentTarget.dataset.id}`,
    })

  },
  async getData() {
    const {
      code,
      count,
      list
    } = await service.getRecommend();
    const markers = (list || []).filter(item => item.map).map((item, index) => {

      return {
        ...item,
        activityStartTime: moment(+item.activityStartTime).format("YYYY-MM-DD hh:mm:ss"),
        activityEndTime: moment(+item.activityEndTime).format("YYYY-MM-DD hh:mm:ss"),
        id: index + 1,
        latitude: item.latitude,
        longitude: item.longitude,
        iconPath: item.activityImgs[0],
        width: 40,
        height: 40,
        label: {
          content: item.title,
          anchorX: -20,
          anchorY: -60,
          bgColor: "#fff"
        },
   
      }
    })
    console.log(markers)
    this.setData({
      markers
    })
  }
})

/*
1. 显示一个地图 v
2. 以用户所在的地方为中心点 v
3. 在地图上展示活动的图标信息 v
4. 点击图标能看活动的简介
5. 点击简介进入活动详情页报名活动
*/