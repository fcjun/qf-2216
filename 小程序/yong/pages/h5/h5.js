import localhost from "../../utils/storage"
Page({
  data: {
    url: "",
    title: "",
    imageUrl: ""
  }, 
  onLoad() {
    const token = localhost.getItem("token")
    console.log(token)
    const url = `http://localhost:3000/hehe/luck.html?token=${token}`
    this.setData({ url })
  },
  // 分享当前页面的方法
  onShareAppMessage() {
    return {
      title: this.data.title,
      path:"/pages/h5/h5",
      imageUrl: this.data.imageUrl
    }
  },
  msgchange(e) {
    const data = e.detail.data[0]
    console.log("接受来自h5页面的消息",data)
    const { title, imageUrl } = data;
    this.setData({title, imageUrl})
  }
}
)