import axios from "../../utils/axios";


function wxlogin(code) {
  const url ="/user/wxlogin"
  return axios.post(url,{ code })
}

function wxGetPhone(code) {
  const url ="/user/wxGetPhone"
  return axios.post(url,{code})
}

function wxBindPhone(data) {
  const url ="/user/wxBindPhone"
  return axios.post(url,data)
}

export default {
  wxlogin,
  wxGetPhone,
  wxBindPhone
}
