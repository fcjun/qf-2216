import wxApi from "../../utils/wxApi";
import service from "./service";
import localStorage from "../../utils/storage";
Page({
  data: {
    isNew: false,
  },
  openid: null,
  // 微信登录
  async wxlogin() {
   const loginCode = await wxApi.getLoginCode();
   const data = await service.wxlogin(loginCode);
   // 新用户  
   const {code, isNew, openid, token } = data  || {} 
  if(isNew) {
    // 弹框绑定手机号
    this.openid = openid;
    this.setData({isNew})
  } else  {
    this.loginSuccess(token);
  }
  },
  // 获取手机号
  async getPhoneNumber(e) {
    const code = e.detail.code;
    // 不要调用获取手机号的接口 直接调用绑定手机号的接口
    // const res = await service.wxGetPhone(code);
    console.log(code, this.openid)
    if(!code || !this.openid) {
      return wx.showToast({
        title: '请先登录！',
      })
    }
    const payload = {
      code,
      openid: this.openid,
      nickName: "nickName",
      avatarUrl: "avatarUrl"
    }
    const data = await service.wxBindPhone(payload);
    if(data.code) {
      return wx.showToast({
        title: data.msg,
      })
    }
    // 关闭弹框
    this.setData({isNew: false })
    this.loginSuccess(data.token);
  },
  loginSuccess(token) {
    // 存token
    localStorage.setItem("token",token)
    // 跳转我的页面
    wx.redirectTo({
      url: '/pages/my/my',
    })
  },
  gowebview(e) {
    console.log(e)
    const  {id} = e.currentTarget.dataset
    wx.navigateTo({
      url: `/pages/webview/webiview?id=${id}`,
    })
  }
})