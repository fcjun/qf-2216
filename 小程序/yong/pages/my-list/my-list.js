import service from "./service";
Page({
  data: {
    applyList: [],
    createList: []
  },
  onChange(e) {
    // index 0 我参与的  1 我管理的
    const {index} = e.detail;
    if(index === 0) {
      this.myApply();
    } else {
      this.myCreat();
    }
  },
  async myApply() {
    const {code, result} = await service.getListMyApply();
    const list = result.map(item => item.activityId);
    this.setData({applyList: list})
  },
  async myCreat() {
    const {code,result} = await service.getListMyCreat();
    this.setData({createList: result})

  }
})