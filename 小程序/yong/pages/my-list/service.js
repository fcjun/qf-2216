import axios from "../../utils/axios";

// 我管理的
function getListMyCreat() {
  const url = `/activity/creator`
  return axios.get(url)
}
//我参加的 
function getListMyApply() {
  const url = `/activity/apply`
  return axios.get(url)
}



export default  {
  getListMyCreat,
  getListMyApply
}