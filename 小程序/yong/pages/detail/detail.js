import service from "./service";
Page({
  data: {
    info:{},
  },
  onLoad({id}) {
    console.log(id);
    this.id = id;
    this.getInfo(id);
  },
  async getInfo(id) {
    const {code,result} = await service.getInfoById(id);
    this.setData({info: result[0]})
  },
  async apply() {
    const { _id, creator } = this.data.info
    const {code,msg} = await service.apply(_id, creator);
    wx.showToast({
      title: msg,
    })
    if(code) return
    wx.redirectTo({
      url: '/pages/my-list/my-list',
    })
  },
  // 获取报名列表
  async getApplys() {
    const res = await service.getApplyList(this.id);
    console.log(res)
  },
  // 
  onShareAppMessage() {
    const {_id,title,topImg} = this.data.info;
    return {
      title,
      path: `/pages/detail/detail?id=${_id}`,
      imageUrl: topImg,
    }
  }
})