import axios from "../../utils/axios";

// 根据id获取活动详情
function getInfoById(id) {
  const url = `/activity/${id}`
  return axios.get(url)
}

function apply(activityId, creator) {
  const url ="/activity/apply"
  return axios.post(url,{ activityId, creator})
}

function getApplyList(activityId) {
  const url = `/activity/${activityId}/apply`
  return axios.get(url);
}

export default  {
  getInfoById,
  apply,
  getApplyList
}