# 真机调试
1. 设备和电脑处于同一局域网环境123123
2. 使用vcosole进行控制台查看
# git  Openshell
命令行工具 
## 名词
代码管理工具222： git/svn
远程仓库 远程服务器上的git仓库
本地仓库 个人开发者电脑上的git仓库
git管理平台 github gitee gitlab
.git  仓库 隐藏文件（win查看隐藏文件）
工作区 写代码的地方 通过 git add. 将工作区添加到暂存区
暂存区 暂时存储的地方 通过 git commit -m "本次提交的简介" 将暂存区的东西加入到本地仓库
本地仓库 .git
冲突： 当代码合并 同一处地方有不同的修改，git无法识别 就可能会产生冲突
分支： 
查看当前分支
新建分支
切换分支
删除分支
合并分支
查看提交记录
查看操作记录
版本回退
gitflow

## 基础指令
git status 查看本地仓库状态 
git clone  克隆远程仓库的代码（建立连接关系）
git init 在本地创建仓库
git remote add origin xxxx 本地仓库和远程仓库建立联系
git add .  工作区代码加入暂存区
git commit -m 'xxx'  暂存区加入本地仓库
git pull 本地仓库  更新远程仓库
git push 将本地仓库推送到远程
git push -f  强制推送 以本地为主进行覆盖
git branch  查看本地分支
git branch -a 查看所有分支包括远程
git chekcout 分支名 切换已有的分支(远程存在也可以直接进行切换)
git checkout -b 分支名  新建并且切换一个分支
git merge 分支名  以当前分支为基础合并分支 
git branch -d 分支名   删除本地分支
git reset --hard commitid 根据提交id回滚代码，强推
git log  查看提交记录 
git reflog 查看操作记录
git stash  将暂存区的代码 贮藏起来
git stash ls 显示所有的贮藏代码
git stash apply 放出最近的一条贮藏
git stash clear  清除贮藏
git rebase 变基

## 远程和本地仓库关联
1. 已经存在远程仓库
   gitee 勾选 初始化远程仓库
   本地用gitclone 命令
2. 本地没有存在远程仓库
   本地初始化仓库  git init 
   将本地仓库和gitee做关联 git remote add origin https://gitee.com/fcjun/2216-test1.git
1. git init 

## gitflow git的工作流
规范团队成员之间的git使用，减少冲突
开发环境（数据 后端）
   测试环境  数据可以随意的修改
   预发布环境 将正式环境的数据做一个备份 进行测试
   正式环境  数据不能修改

团队协作工具/项目管理工具
飞书 钉钉 企业微信 公司内部的管理工具 
tower jira 禅道
okr kpi

master分支 线上分支（main）
release 分支  预发布环境
dev分支   测试分支 
feature  功能分支 个人开发分支
    feature/login_上线时间
hotfix   bug分支
    hotfix/bug描述_时间

master 分支一般是保护分支 不允许直接本地push
CR（code review） MR（merge Request）gitlab  PR（PullRequest）gitee github 

## 按照规则填写提交注释 commit -m "doc"
feat:xxxx 新功能的添加
doc/md: xxxx  文档修改
style:样式变更
fix: bug修复
cofig: 配置的修改

## 团队文档建设
https://note.youdao.com/s/F5OwLMD
https://note.youdao.com/s/L6Jr0Jeo
https://note.youdao.com/s/OwfQczPK
https://note.youdao.com/s/Ms4kOage

## git可视化工具 
sourcetree
## node 工具
yarn 
cnpm 
npm 
nvm
## 项目如何（测试）上线
前端代码放到服务器上
npm run build ->  dist  html  -> 运维 后端
### CI/CD  devops
持续集成
持续交付
持续部署

1. 代码的管理 git 
   提交1个mrpr
   进行代码审核
2. 公司有一套cicd的系统， 上线的时间 9:00 上线的同学在cicd的系统上点击交付

## B/C(端)
设备: B浏览器  C：客户端
使用的人: boss 老板  C:用户
app：手机端客户端应用 安卓 ios
桌面端应用: 电脑上

产品大牛
前台 商城系统 给用户来用的
后台 管理系统 给内部工作人员来使用的

## 接口
1. 通过json文件在postman中导入
2. 接口关注点
   1. 地址
   2. 方法 post get   
       restfulapi 
   3. 入参
   4. 回参

xxxx/getBannerList get
xxxx/addBanner     post 
xxxx/delBanner     post 
xxxx/updateBanner  post
restful api 规范
xxxx/banner        get  获取数据 
xxxx/banner        post 增加
xxxx/banner        delete 删除
xxxx/banner        put  修改

## vue2 和 vue3 
1. 工程化
   <!-- grunt gulp -->
   webpack  vue-cli
   vite 
2. 插件
   router 4
      params router3 可以直接传递对象 
      params 必须和 动态路由一起使用 /home/:id  
   vuex   4
3. 原理的变更
   vue2 Object.definePropty
       1. 动态添加对象
       2. 操作数据的长度 下标
       vue.set() 
   vue3 proxy
3. api变更
   set api废弃
   生命周期变更
   增加组合式api
