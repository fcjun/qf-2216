// 函数声明
// function test(a,b) {
//   return c;
// } 

// 函数表达式
// const test2 = function(a,b) {
//   return c;
// }

// function sum(num1:number,num2:number):number{
//   const sum = num1 + num2;
//   return sum
// }

// sum(123,456,789)
//  函数表达式: (参数:参数类型，。。。。) => 返回值的类型
// let sum: (num1:number,num2:number) => number = function(num1:number,num2: number):number {
//   return num1 + num2
// }

// let decreace:(num1:number,num2:number) => number = function(num1:number,num2: number):number {
//   return num1 - num2
// }
// 通过接口修饰函数
interface NumFun {
  (num1:number,num2:number) : number
  // 参数列表: 返回值
}
// let decreace: NumFun = function(num1:number,num2: number):number {
//   return num1 - num2
// }

// decreace(678)
//  可选参数
// let sum: (num1:number,num2:number,num3?:number) => number = function(num1:number,num2: number,num3?: number):number {
//   return num1 + num2
// }
// sum(1,2)
function push(array:any[], ...items:any[]) {
  items.forEach(function(item) {
  array.push(item)
  })
}

let a: any[] = []
push(a, 1, 2, 3)