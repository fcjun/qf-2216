// const arr1 = [1,2,3];
// const arr2 = ["爱情","恐怖","动作"]
// const arr3 = [1,"动作"]
// const arr4 = [{us:123,ps:456}]
const arr1: number[] = [1,2,3];
const arr2: Array<string> = ["爱情","恐怖","动作"];
const arr3: Array<string|number> = [1,"动作"]
const arr4: { us: number,ps: number}[] = [{us:123,ps:456},{us:123,ps:456}]
const arr5: any[] = [{us:123,ps:456},{us:123,ps:456},1,2,3]
const arr6: Array<any> = [{us:123,ps:456},{us:123,ps:456},1,2,3]

