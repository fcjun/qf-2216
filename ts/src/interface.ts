// 不需要加,
interface Person {
  readonly name: string
  age: number
  sex?: number
  [propName: string]: any
}

// const wangyi: { name: string, age: number} = {
//   name: "网易",
//   age: 15
// }
// const wanger: { name: string, age: number} = {
//   name: "网易",
//   age: 15
// } 
const wangyi:Person = {
  name: "网易",
  age: 15
}
const wanger: Person = {
  name: "网易",
  age: 15,
  sex:1,
} 
// wanger.name = '7788'
console.log(wanger.name)