# 一、理解密码、私钥、keystore与助记词

> 在前面的教程中我们对以太坊钱包已经有了一定的认识，上一章也重点介绍了账号地址的生成过程，在以太坊钱包中一个重点就是账户系统，在这个模块中很多初学同学不是很清楚密码、keystore、助记词与私钥它们之间的关系。下面我们来看看它们之间到底有着怎样的爱恨情仇，让大家琢磨不透。

## 密码

密码不是私钥，它是在创建账户时候的密码（可以修改）

密码在以下情况下会使用到：

1. 作为转账的支付密码
2. 用keystore导入钱包的时候需要输入的密码，用来解锁keystore的

## 私钥 Private Key



私钥由64位长度的十六进制的字符组成，比如：`0xA4356E49C88C8B7AB370AF7D5C0C54F0261AAA006F6BDE09CD4745CF54E0115A`，一个账户只有一个私钥且不能修改。
通常一个钱包中私钥和公钥是成对出现的，有了私钥，我们就可以通过一定的算法生成公钥，再通过公钥经过一定的算法生成地址，这一过程都是不可逆的。私钥一定要妥善保管，若被泄漏别人可以通过私钥解锁账号转出你的该账号的数字货币。

## 公钥 Public Key

公钥(Public Key)是和私钥成对出现的，和私钥一起组成一个密钥对，保存在钱包中。公钥由私钥生成，但是无法通过公钥倒推得到私钥。公钥能够通过一系列算法运算得到钱包的地址，因此可以作为拥有这个钱包地址的凭证。

## Keystore

Keystore常见于以太坊钱包，它是将私钥以加密的方式保存为一份 JSON 文件，这份 JSON 文件就是 keystore，所以它就是加密后的私钥。Keystore必须配合钱包密码才能导入并使用该账号。当黑客盗取 Keystore 后，在没有密码情况下, 有可能通过暴力破解 Keystore 密码解开 Keystore，所以建议使用者在设置密码时稍微复杂些，比如带上特殊字符，至少 8 位以上，并安全存储。

## 助记词 Mnemonic

私钥是64位长度的十六进制的字符，不利于记录且容易记错，所以用算法将一串随机数转化为了一串12 ~ 24个容易记住的单词，方便保存记录。注意：

1. 助记词是私钥的另一种表现形式
2. 助记词=私钥，这是不正确的说法，通过助记词可以获取相关联的多个私钥，但是通过其中一个私钥是不能获取助记词的，因此**助记词≠私钥**。

#### BIP

要弄清楚助记词与私钥的关系，得清楚BIP协议，是`Bitcoin Improvement Proposals`的缩写，意思是Bitcoin 的改进建议，用于提出 Bitcoin 的新功能或改进措施。BIP协议衍生了很多的版本，主要有BIP32、BIP39、BIP44。

**BIP32**

BIP32是 HD钱包的核心提案，通过种子来生成主私钥，然后派生海量的子私钥和地址，种子是一串很长的随机数。

**BIP39**

由于种子是一串很长的随机数，不利于记录，所以我们用算法将种子转化为一串12 ~ 24个的单词，方便保存记录，这就是BIP39，它扩展了 HD钱包种子的生成算法。

**BIP44**

BIP44 是在 BIP32 和 BIP43 的基础上增加多币种，提出的层次结构非常全面，它允许处理多个币种，多个帐户，每个帐户有数百万个地址。

在BIP32路径中定义以下5个级别：

```
m/purpse'/coin_type'/account'/change/address_index
```

- purpose：在BIP43之后建议将常数设置为44'。表示根据BIP44规范使用该节点的子树。
- Coin_type：币种，代表一个主节点（种子）可用于无限数量的独立加密币，如比特币，Litecoin或Namecoin。此级别为每个加密币创建一个单独的子树，避免重用已经在其它链上存在的地址。开发人员可以为他们的项目注册未使用的号码。
- Account：账户，此级别为了设置独立的用户身份可以将所有币种放在一个的帐户中，从0开始按顺序递增。
- Change：常量0用于外部链，常量1用于内部链，外部链用于钱包在外部用于接收和付款。内部链用于在钱包外部不可见的地址，如返回交易变更。
- Address_index：地址索引，按顺序递增的方式从索引0开始编号。

BIP44的规则使得 HD钱包非常强大，用户只需要保存一个种子，就能控制所有币种，所有账户的钱包，因此由BIP39 生成的助记词非常重要，所以一定安全妥善保管，那么会不会被破解呢？如果一个 HD 钱包助记词是 12 个单词，一共有 2048 个单词可能性，那么随机的生成的助记词所有可能性大概是`5e+39`，因此几乎不可能被破解。

#### HD钱包

通过BIP协议生成账号的钱包叫做HD钱包。这个HD钱包，并不是Hardware Wallet硬件钱包，这里的 HD 是`Hierarchical Deterministic`的缩写，意思是分层确定性，所以HD钱包的全称为比特币分成确定性钱包 。

# 二、密码、私钥、keystore与助记词的关系

![img](https://cdn.nlark.com/yuque/0/2022/png/22208307/1668518245248-4c524198-7a6a-482e-802c-d36d31220b06.png)

# 三、钱包的核心：私钥

基于以上的分析，我们对以太坊钱包的账号系统有了一个很好的认识，那么我们在使用钱包的过程中，该如何保管自己的钱包呢？主要包含以下几种方式：

- 私钥（Private Key）
- Keystore+密码（Keystore+Password）
- 助记词（Mnemonic code）

通过以上三种中的一种方式都可以解锁账号，然后掌控它，所以对于每种方式中的数据都必须妥善包括，如有泄漏，请尽快转移数字资产。

我们可以得到以下总结：

- 通过私钥+密码可以生成keystore，即加密私钥；
- 通过keystore+密码可以获取私钥，即解密keystore。
- 通过助记词根据不同的路径获取不同的私钥，即使用HD钱包将助记词转化成种子来生成主私钥，然后派生海量的子私钥和地址。

可以看出这几种方式的核心其实都是为了获得私钥，然后去解锁账号，因此钱包的核心功能是私钥的创建、存储和使用。

**参考资料**

https://web3js.readthedocs.io/en/1.0/web3-eth-accounts.html
https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki
https://github.com/bitcoin/bips/blob/master/bip-0044.mediawiki
https://github.com/ethereum/EIPs/issues/84
https://github.com/ethereum/EIPs/issues/85

# 四、通过助记词Mnemonic 创建、解锁账户

## 一、通过助记词获取所有关联的账号：公钥、私钥、地址

需要使用`bip39`协议将助记词转换成种子，再通过`ethereumjs-wallet`库生成hd钱包，根据路径的不同从hd钱包中获取不同的keypair，keypair中就包含有公钥、私钥，再通过`ethereumjs-util`库将公钥生成地址，从而根据助记词获取所有关联的账号，能获取到公钥、私钥、地址等数据信息。

#### 1. 依赖库

需要用到三个库：bip39、ethereumjs-wallet/hdkey、ethereumjs-util。先安装依赖库，`cd`到项目跟路径运行命令`npm i bip39 ethereumjs-wallet ethereumjs-util`。

- [bip39](https://github.com/bitcoinjs/bip39)：随机产生新的 mnemonic code，并可以将其转成 binary 的 seed。
- [ethereumjs-wallet](https://github.com/ethereumjs/ethereumjs-wallet)：生成和管理公私钥，下面使用其中 hdkey 子套件来创建 HD 钱包。
- [ethereumjs-util](https://github.com/ethereumjs/ethereumjs-util)：Ethereum 的一个工具库。
- https://iancoleman.io/bip39/

#### 2. 通过助记词创建账号

* 创建助记词

  ```javascript
  // 引入bip39模块
  let bip39 = require("bip39");
  // 创建助记词 
  let mnemonic = bip39.generateMnemonic();
  console.log(mnemonic);
  // 结果 12位助记词
  // vote select solar shy embrace immense lizard stamp scrub vague negative forward
  ```

  

* 根据助记词生成密钥对 keypair

```javascript
      // 导入分层钱包模块
      const { hdkey } = require("ethereumjs-wallet");
      //1.将助记词转成seed
      let seed = await bip39.mnemonicToSeed("12位助记词");
      //3.通过hdkey将seed生成HD Wallet
      let hdWallet = hdkey.fromMasterSeed(seed);
      //4.生成钱包中在m/44'/60'/0'/0/i路径的keypair
      let keypair = hdWallet.derivePath("m/44'/60'/0'/0/0");
      console.log(keypair);
     
```



keypair 密钥对

![image-20221208222354836](https://gitee.com/fcjun/image/raw/master/img/image-20221208222354836.png)

#### 3. 由keypair 获取钱包地址和私钥

```javascript
// 获取钱包对象
let wallet = keyPair.getWallet();
// 获取钱包地址
let lowerCaseAddress = wallet.getAddressString();
// 获取钱包校验地址
let CheckSumAddress = wallet.getChecksumAddressString();
// 获取私钥
let prikey = wallet.getPrivateKey().toString("hex");
 console.log("lowerCaseAddress", lowerCaseAddress);
 console.log("CheckSumAddress", CheckSumAddress);
 console.log("prikey", prikey);
/*
lowerCaseAddress 0xd9fc0fd4412616c7075e68b151ab3a7bcb9a3f54
CheckSumAddress 0xd9fC0FD4412616c7075E68b151ab3A7bcB9A3f54
prikey 3fc11495517f1f015bbcb6c311da66e3b26b23e4c91c1285ccc4b69d9d274002
*/
```

#### 4. 通过`ethereumjs-util` 获取钱包和私钥

```javascript
let util = require("ethereumjs-util");
//从keypair中获取私钥
let privateKey = util.bufferToHex(key._hdkey._privateKey)
//从keypair中获取公钥
let publicKey = util.bufferToHex(key._hdkey._publicKey)
//使用keypair中的公钥生成地址
let address = util.pubToAddress(publicKey, true);
 console.log("私钥：" + privateKey);
 console.log("公钥：" +publicKey );
 console.log("地址：" + address.toString("hex"), "\n");  
 /*
	地址：d9fc0fd4412616c7075e68b151ab3a7bcb9a3f54 
	私钥：0x3fc11495517f1f015bbcb6c311da66e3b26b23e4c91c1285ccc4b69d9d274002
	公钥：0x03d68e75ebfeec000301e0030ddcbd17261feb81287e8221f658cc8ffc7360bbe1
 */
     
```



# 五、通过keystore + 密码 解锁账户

## 1.通过账户导出keystore

```javascript
let account = web3.eth.accounts.create(this.password);
let keystore = account.encrypt(this.password);
```



## 2.通过私钥获取keystore

```javascript
const res = await web3.eth.accounts.encrypt("账户私钥","密码");
```

## 3.通过助记词和密码获取 keystore

```javascript
let mnemonic=prompt("请输入助记词")
let pass=prompt("请输入密码")
let path=prompt("请输入path")
let seed = bip39.mnemonicToSeed(mnemonic)
let hdwallet = hdkey.fromMasterSeed(seed)
let wallet = hdwallet.derivePath(path).getWallet()
let keystore = wallet.toV3(pass)
```



##  4.通过keystore解锁账户

```javascript
// 模拟keystore数据
const keystoreJsonV3 = {
        version: 3,
        id: "dbb70fb2-52ad-4e1f-9c19-0b50329f89c3",
        address: "445b469888528dacd9b87246c5ce70407adaa411",
        crypto: {
          ciphertext:
            "1e53e7e775644422600188b8134907992db40d278064ea3a966da4dcdf80db64",
          cipherparams: { iv: "f9d2b047019674eee449b316f4a21491" },
          cipher: "aes-128-ctr",
          kdf: "scrypt",
          kdfparams: {
            dklen: 32,
            salt: "153e074d78d0ba36fae3e46e582c42e53f61653cb5d4f1a3a3f68094e6ca0160",
            n: 8192,
            r: 8,
            p: 1,
          },
          mac: "e91456c59b2505c16b80c2495ab7b4633273c2ae366cb6953f27de8cfebad629",
        },
      };
 const res = web3.eth.accounts.decrypt(keystoreJsonV3, "1235");
 console.log(res);
```

## 5. 通过keystore 校验密码是否合法

在做任何敏感操作前，应先验证密码是否正确 

```javascript
  checkPass(){
      let pass=prompt("请输入密码");
      let keystore=this.wallterData[0].keystore; // 获取本地保存的keystore
      try{
        let wallet = ethwallet.fromV3(keystore, pass)
        return wallet;
      }
      catch(e){
        alert("密码错误")
        return false
      }
    }
```

## 6.通过私钥解锁账户

```javascript
    let privatekey=new Buffer( prompt("请输入私钥"), 'hex' )
    let pass=prompt("请输入密码")
    let wallet = ethwallet.fromPrivateKey(privatekey)
    let keystore= wallet.toV3(pass)
```



# 六、转账记录

在这里和大多数钱包实现方式一样 只记录钱包创建后的转账记录，全量的转账记录都可以在区块链浏览器上进行查看

通过交易hash 查询本次转账数据

```javascript
web3.eth.getTransaction(tx).then((data)=>{
     // data本次教育的相关信息
    });
```

# 七、 交易监听

1. 转账交易监听

```javascript
 web3.eth.sendSignedTransaction(transationHx)
              .on('transactionHash', (txid)=>{
                console.log(txid)
                this.createOrderData(txid);
              })
              // .on('receipt', (ret)=>{console.log('receipt')})
              // .on('confirmation', (ret)=>{console.log('confirmation')})
              .on('error', (err)=>{
                console.log('error:'+err)
              })
```



# 八、区块链钱包项目流程

![image-20221209110335777](https://gitee.com/fcjun/image/raw/master/img/image-20221209110335777.png)

## 1.项目准备

1. 通过vue-cli 创建vue基本项目

2. 安装第三方包

   ```bash
    npm install web3 ethereumjs-tx@1.3.7 bip39 ethereumjs-wallet ethereumjs-util
    // 注意ethereumjs-tx 使用1.3.7版本
    // node版本为 v14.12.0
   ```

3. polyfill 处理

   ```javascript
   npm install node-polyfill-webpack-plugin -D 
   // vue.config.js
   const { defineConfig } = require("@vue/cli-service");
   const NodePolyfillWebpackPlugin = require("node-polyfill-webpack-plugin")
   module.exports = defineConfig({
     transpileDependencies: true,
     lintOnSave: false,
     configureWebpack: {
       plugins:[new NodePolyfillWebpackPlugin()]
     }
   });
   
   ```

4. vue文件引入

   ```javascript
   var Web3 = require("web3");
   
   var Tx = require("ethereumjs-tx");
   let bip39 = require("bip39");
   const { hdkey } = require("ethereumjs-wallet");
   let util = require("ethereumjs-util");
   
   const geerliWS =
     "wss://goerli.infura.io/ws/v3/e4f789009c9245eeaad1d93ce9d059bb";
   var web3 = new Web3(Web3.givenProvider || geerliWS);
   ```

5. 封装缓存函数 

   整个项目为了保证钱包的安全性，所有账户相关的操作，不经过中心化服务器，只在缓存使用

   在这里可以参考`webstorage`增加过期时间，cookie 等的封装

   ```javascript
   class Storage {
     setItem(key, val) {
       localStorage.setItem(key, JSON.stringify(val || ""));
     }
     getItem(key) {
       let val;
       try {
         val = JSON.parse(localStorage.getItem(key));
       } catch {
         val = null;
       }
       return val;
     }
   }
   export default new Storage();
   
   ```

   

## 2.通过Mnemonic助记词创建钱包

```javascript
async function createWallet() {
  const pass = prompt("请输入您的钱包密码");
  if (!pass) return false;
  let mnemonic = bip39.generateMnemonic();
  alert("您的助记词为:" + mnemonic);
  const checkMnemonic = prompt("请输入您的助记词");
  if (mnemonic === checkMnemonic) {
    //1.将助记词转成seed
    let seed = await bip39.mnemonicToSeed(mnemonic);
    //3.通过hdkey将seed生成HD Wallet
    let hdWallet = hdkey.fromMasterSeed(seed);
    //4.生成钱包中在m/44'/60'/0'/0/i路径的keypair
    let keyPair = hdWallet.derivePath("m/44'/60'/0'/0/0");
    // 获取钱包对象
    let wallet = keyPair.getWallet();
    // 获取钱包地址
    let lowerCaseAddress = wallet.getAddressString();
    // 获取钱包校验地址
    let CheckSumAddress = wallet.getChecksumAddressString();
    // 获取私钥
    let prikey = wallet.getPrivateKey().toString("hex");
    let keystore = await wallet.toV3(pass);
    console.log(keystore);
    // 保存钱包信息
    const walletInfo = {
      address: lowerCaseAddress,
      prikey,
      keystore,
      balance: 0,
      mnemonic, // 助记词不应该记录下来仅仅是为了便于演示
    };
    storage.setItem("wallet", walletInfo);
    wallet = walletInfo;
  } else {
    alert("助记词错误请重新输入");
  }
}
```

## 3.转账交易

1. 通过密码与keystore 获取私钥,安全起见不能直接使用私钥，混编app效率问题

   ```javascript
         let pass = prompt("请输入密码");
         let keystore = this.wallet.keystore;
         console.log(pass, keystore);
         let wallet;
         try {
           wallet = await ethwallet.fromV3(keystore, pass);
         } catch (error) {
           alert("密码错误");
           return false;
         }
         let prikey = wallet.getPrivateKey().toString("hex");
   ```

2. 构建交易hash

   ```javascript
     async createTransationHx(key, fromAddress, toAddress, money) {
         const nonce = await web3.eth.getTransactionCount(fromAddress);
         var privateKey = new Buffer(key, "hex");
         // 获取预计转账gas费
         let gasPrice = await web3.eth.getGasPrice();
         // 转账金额以wei为单位
         let balance = await web3.utils.toWei(money);
         // 转账的记录对象
         var rawTx = {
           from: fromAddress,
           nonce: nonce,
           gasPrice: gasPrice,
           to: toAddress,
           value: balance,
           data: "0x00", //转Token代币会用到的一个字段
         };
         console.log(rawTx);
         //需要将交易的数据进行预估gas计算，然后将gas值设置到数据参数中
         let gas = await web3.eth.estimateGas(rawTx);
         rawTx.gas = gas;
         // 通过tx实现交易对象的加密操作
         var tx = new Tx(rawTx);
         tx.sign(privateKey);
         var serializedTx = tx.serialize();
         var transationHx = "0x" + serializedTx.toString("hex");
         return transationHx;
       }
         
   ```

3. 发送交易，并进行监听

   ```javascript
     web3.eth
           .sendSignedTransaction(hx)
           .on("transactionHash", (txid) => {
             console.log("交易成功,请在区块链浏览器查看");
             console.log("交易id", txid);
             console.log(`https://goerli.etherscan.io/tx/${txid}`);
           })
            .on("receipt", (ret) => {
             console.log("receipt", ret);
             const { transactionHash } = ret;
             this.createOrderData(transactionHash);
           })
           // .on('confirmation', (ret)=>{console.log('confirmation')})
           .on("error", (err) => {
             console.log("error:" + err);
           });
   ```

4. 余额变化

```javascript
  async getBalance() {
      // 根据地址查询余额
      web3.eth.getBalance(this.wallet.lowerCaseAddress).then((ret) => {
        this.wallet.balance = web3.utils.fromWei(ret, "ether");
        console.log(this.wallet.balance);
      });
    },
```



## 4. 创建交易记录

在区块确认时创建订单详情

```javascript
  createOrderData(tx) {
      web3.eth.getTransaction(tx).then((data) => {
        let orderData = storage.getItem("orderData") || {};
        let price = web3.utils.fromWei(data.gasPrice, "ether");
        let gasSpend = (price * data.gas).toString();
        let tmpdata = {
          blockHash: data.blockHash,
          blockNumber: data.blockNumber,
          from: data.from,
          to: data.to,
          value: web3.utils.fromWei(data.value, "ether"),
          nonce: data.nonce,
          input: data.input,
          hash: data.hash,
          gasSpend: gasSpend,
        };
        if (!orderData[data.from]) {
          orderData[data.from] = {};
          orderData[data.from][data.hash] = tmpdata;
        } else {
          orderData[data.from][data.hash] = tmpdata;
        }
        storage.setItem("orderData", orderData);
        this.orderData = storage.getItem("orderData");
      });
    },
```



## 5.导出账户信息

1. 导出私钥

   ```javascript
         let pass = prompt("请输入密码");
         let keystore = this.wallet.keystore;
         let wallet;
         try {
           wallet = await ethwallet.fromV3(keystore, pass);
         } catch (error) {
           alert("密码错误");
           return false;
         }
         let prikey = wallet.getPrivateKey().toString("hex");
         console.log(prikey);
   ```

2. 导出keystore

   ```javascript
     let pass = prompt("请输入密码");
         let keystore = this.wallet.keystore;
         let wallet;
         try {
           wallet = await ethwallet.fromV3(keystore, pass);
           return keystore
         } catch (error) {
           alert("密码错误");
           return false;
         }
   ```

   

## 6.解锁账户信息



1. 助记词解锁
2. 私钥解锁
3. keystore + 密码解锁

## 7. 未来展望

1. 通过uniapp 、rn、electron 将项目变为app和桌面端应用
2. app添加扫码转账功能
3. 增加erc20代币转账功能
4. 增加nft数字藏品商城功能

