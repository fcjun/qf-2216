# 安装浏览器数字货币钱包 metamask 小狐狸
# 创建账户  记录助记词
# 网络变成测试网络 格尔丽
  1. web3 实例链接 wss://goerli.infura.io/ws/v3/e4f789009c9245eeaad1d93ce9d059bb
  2. 钱包的网络连接 https://goerli.infura.io/v3/
  3. 挖矿的页面：https://goerli-faucet.pk910.de/

# 智能合约 erc20代币
https://remix.ethereum.org/#optimize=false&runs=200&evmVersion=null&version=soljson-v0.8.7+commit.e28d00a7.js&lang=en

#  钱包项目功能点
1. 账户系统
   账户的创建
   账户的导出
   账户的导入
2. 余额的展示
   1. eth 余额
   2. erc20 代币展示
3. 转账操作
   1. eth 转账
   2. 代币转账
4. 转账记录
   1. eth 转账
   2. 代币转账
5. 打通区块链浏览器 查看信息