// var Web3 = require('web3');
/*
和账户相关的内容  私钥  助记词 keystore
keystore = 私钥 + 密码 
*/
import Web3 from "web3";
import { useEffect, useState } from "react";
import Tx from "ethereumjs-tx";
import { Buffer } from "buffer";
var web3 = new Web3(
  "wss://goerli.infura.io/ws/v3/e4f789009c9245eeaad1d93ce9d059bb"
);

function App() {
  const [balance, setBalance] = useState(0);
  const [toAddress, setToAddress] = useState("");
  const [money, setMoney] = useState(0);
  const address = "0xa72F6d7549fC056E3Fd0922727392A8CFFA95c49";
  // const address = "0xE251ddBe6191594922bfd3d338529EC9C613eB67"
  const privateKey =
    "0x7b9a22e0883a557899d6d9c21d2c1794a74fae3c04ec41896b537238e93b67db";
  function createAccount() {
    // 创建账号还有地址 私钥
    const { address, privateKey } = web3.eth.accounts.create("2216");
    console.log(address, privateKey)
  }
  function createKeystore() {
    // 创建keystore
    const keyStory = web3.eth.accounts.encrypt(privateKey, "123456");
    console.log(keyStory);
  }
  // 获取账户余额
  function getBanlace() {
    // 根据地址获取余额
    web3.eth.getBalance(address).then((res) => {
      // 将余额进行转化 从wei为单位变成 “ether”
      const balance = web3.utils.fromWei(res, "ether");
      setBalance(balance);
    });
  }
  // 转账
  async function sendToken() {
    const privateKey = prompt("请输入转账私钥")
    /********** 构建转账参数  ***********/
    // 获取账户交易次数
    let nonce = await web3.eth.getTransactionCount(address);
    // 获取预计转账gas费
    let gasPrice = await web3.eth.getGasPrice();
    console.log("转账的gas费用价格", gasPrice);
    // 转账金额以wei为单位
    let balance = await web3.utils.toWei(money);
    console.log("转账金额",balance);
    var rawTx = {
      from: address, // 转出的地
      nonce: nonce, //获取账户的交易次数
      gasPrice: gasPrice,
      to: toAddress, // 接受方的地址
      value: balance,
      data: "0x00", //转Token代币会用到的一个字段
    };
    /**********将转账参数进行加密处理***********/ 
      // 将私钥去除“ox”后进行hex转化

      var key = new Buffer(privateKey.slice(2), "hex");
      //需要将交易的数据进行预估gas计算，然后将gas值设置到数据参数中
      let gas = await web3.eth.estimateGas(rawTx);
      rawTx.gas = gas;
    //  // 通过 ethereumjs-tx 实现私钥加密
      var tx = new Tx(rawTx);
      //通过16进制的私钥尽心加密
      tx.sign(key);
      // 获取交易的hash
      var serializedTx = tx.serialize();
      console.log("交易hash",serializedTx)
      // 将交易hash 发送到区块链环境中
      web3.eth
      .sendSignedTransaction("0x" + serializedTx.toString("hex"))
      .on("transactionHash", (txid) => {
        // 将数据发送到区块链环境中
        console.log("交易成功,请在区块链浏览器查看");
        console.log("交易id", txid);
        console.log(`https://goerli.etherscan.io/tx/${txid}`);
      })
      .on('receipt', (ret)=>{console.log('receipt')})
      .on('confirmation', (ret)=>{
        // 区块确认
        console.log("confirmation")
        getBanlace();
      })
      .on("error", (err) => {
        console.log("error:" + err);
      });
      
  }

  useEffect(() => {
    getBanlace();
  }, []);
  return (
    <div className="App">
      <h1> 去中心化eth钱包项目</h1>
      <button></button>
      <button onClick={createAccount}>创建账号</button>
      <button onClick={createKeystore}>创建keyStore</button>
      <button onClick={sendToken}>转账</button>
      <hr />
      地址: {address}
      <hr />
      余额: {balance}
      <hr />
      私钥： {privateKey}
      <hr />
      <h3>转账</h3>
      接受方地址: <input type="text" onChange={(e) => {
        setToAddress(e.target.value)
      }}/>
      金额: <input type="text" onChange={(e) => {
         setMoney(e.target.value)
      } }/>
    </div>
  );
}

export default App;
