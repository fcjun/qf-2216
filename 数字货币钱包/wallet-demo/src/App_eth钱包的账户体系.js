// var Web3 = require('web3');
/*
和账户相关的内容  私钥  助记词 keystore
keystore = 私钥 + 密码 
*/
import Web3 from "web3";
// import { useEffect, useState } from "react";
// import Tx from "ethereumjs-tx";
import { Buffer } from "buffer";
import * as bip39 from "bip39";
import ethwallet, { hdkey } from "ethereumjs-wallet";
var web3 = new Web3(
  "wss://goerli.infura.io/ws/v3/e4f789009c9245eeaad1d93ce9d059bb"
);

function App() {
  const address = "0xb7d9f8ec2f604920fc85f60de48b6aab2b9b0af0";
  const privateKey = "8b75fc7e94f0b8829375b00d97a78fbbdf106be8ffc3a2db7fa15390148390a5"
  // 通过助记词创建钱包
  async function createWalletByMemonic() {
    // 创建助记词
    let mnemonic = bip39.generateMnemonic();
    console.log(mnemonic)
    // let mnemonic =
    //   "world average stock sound age desert laugh fashion ecology museum museum better";
    // 将助记词转化为 加密 种子
    let seed = await bip39.mnemonicToSeed(mnemonic);
    console.log("seed", seed);

    //3.通过hdkey将seed生成HD Wallet（分层钱包）
    let hdWallet = hdkey.fromMasterSeed(seed);
    //4.生成钱包中在m/44'/60'/0'/0/i路径的keypair
    let keypair = hdWallet.derivePath("m/44'/60'/0'/0/1");
    console.log(keypair);
    // 获取钱包对象
    let wallet = keypair.getWallet();
    // 获取钱包地址
    let lowerCaseAddress = wallet.getAddressString();
    // 获取钱包校验地址
    let CheckSumAddress = wallet.getChecksumAddressString();
    // 获取私钥
    let prikey = wallet.getPrivateKey().toString("hex");
    console.log("钱包地址",lowerCaseAddress)
    console.log("检验钱包地址",CheckSumAddress)
    console.log("私钥",prikey)

    // 用户输入密码 产生keystore
    createKeyStore(wallet)

  }
  // 通过助记词导入钱包
  async function importWalletByMemonic() {
    let mnemonic = prompt("请输入助记词")

    // let mnemonic =
    //   "world average stock sound age desert laugh fashion ecology museum museum better";
    // 将助记词转化为 加密 种子
    let seed = await bip39.mnemonicToSeed(mnemonic);
    console.log("seed", seed);

    //3.通过hdkey将seed生成HD Wallet（分层钱包）
    let hdWallet = hdkey.fromMasterSeed(seed);
    //4.生成钱包中在m/44'/60'/0'/0/i路径的keypair
    let keypair = hdWallet.derivePath("m/44'/60'/0'/0/1");
    console.log(keypair);
    // 获取钱包对象
    let wallet = keypair.getWallet();
    // 获取钱包地址
    let lowerCaseAddress = wallet.getAddressString();
    // 获取钱包校验地址
    let CheckSumAddress = wallet.getChecksumAddressString();
    // 获取私钥
    let prikey = wallet.getPrivateKey().toString("hex");
    console.log("钱包地址",lowerCaseAddress)
    console.log("检验钱包地址",CheckSumAddress)
    console.log("私钥",prikey)

    createKeyStore(wallet)
  }

  async function createKeyStore(wallet) {
    const pass = prompt("请输入密码")
    // 钱包产生keystore的方法
    const keystore = await wallet.toV3(pass);
    localStorage.setItem("keystore",JSON.stringify(keystore))
    console.log(keystore)

  }

  function importWalletByKeyStore() {
    // const  keyStore = localStorage.getItem("keystore")
    const  keyStore = prompt("请输入keystore")
    const pass = prompt("请输入密码")
    const res  = web3.eth.accounts.decrypt(keyStore, pass);
    console.log(res)
  }

  function exportPrivatekey() {
    const pass = prompt("请输入密码")
    const  keyStore = localStorage.getItem("keystore")
    const res  = web3.eth.accounts.decrypt(keyStore, pass);
    console.log(typeof res, res.privateKey)
    alert("您的私钥是:"+res.privateKey)
  }
  function importWalletByPrivateKey() {
    let privatekey=new Buffer( prompt("请输入私钥"), 'hex' )
    console.log(ethwallet)
    let wallet = ethwallet.fromPrivateKey(privatekey)
    const address = wallet.getAddressString();
    console.log(address)
  }
  

  return (
    <div className="App">
      <h1> 去中心化eth钱包项目</h1>
      <button onClick={createWalletByMemonic}>创建钱包 - 助记词</button>
      <button onClick={importWalletByMemonic}>导入钱包 - 助记词</button>
      <button onClick={importWalletByKeyStore}>通过keystore - 解锁账户</button>
      <button onClick={exportPrivatekey}>导出私钥</button>
      <button onClick={importWalletByPrivateKey}>通过私钥导入钱包</button>
      <hr />
      地址: {address}
      <hr />
      私钥: { privateKey }
      <hr />
      助记词：  "world average stock sound age desert laugh fashion ecology museum museum better"
    </div>
  );
}

export default App;

/*
钱包地址 0xb7d9f8ec2f604920fc85f60de48b6aab2b9b0af0
App.js:42 检验钱包地址 0xb7d9f8Ec2F604920fc85f60de48b6Aab2b9B0af0
App.js:43 私钥 8b75fc7e94f0b8829375b00d97a78fbbdf106be8ffc3a2db7fa15390148390a5

*/