// var Web3 = require('web3');
/*
和账户相关的内容  私钥  助记词 keystore
keystore = 私钥 + 密码 
*/
import Web3 from "web3";
import { useEffect, useState, useRef } from "react";
import Tx from "ethereumjs-tx";
import { Buffer } from "buffer";
import * as bip39 from "bip39";
import ethwallet, { hdkey } from "ethereumjs-wallet";
import dog from "./dog/info.json";
console.log(dog)
// 和格尔丽测试网进行关联
var web3 = new Web3(
  "wss://goerli.infura.io/ws/v3/e4f789009c9245eeaad1d93ce9d059bb"
);
// 创建智能合约对象 参数1 合约的abi 参数2 合约地址
const dogContract = new web3.eth.Contract(dog.abi,"0x7e9eB98374A11C5062056d86E5fe8e39B11E1641")
console.log(dogContract)

function App() {
  const address = "0xb7d9f8ec2f604920fc85f60de48b6aab2b9b0af0";
  const privateKey = "8b75fc7e94f0b8829375b00d97a78fbbdf106be8ffc3a2db7fa15390148390a5"
  const [dogInfo,setDogInfo] = useState({name:"",symbol:"",banlance:0});
  const [ethNum,setEthNum] = useState(0);
  const addressRef = useRef();
  const moneyRef = useRef();
  // 通过助记词创建钱包
  async function createWalletByMemonic() {
    // 创建助记词
    let mnemonic = bip39.generateMnemonic();
    console.log(mnemonic)
    // let mnemonic =
    //   "world average stock sound age desert laugh fashion ecology museum museum better";
    // 将助记词转化为 加密 种子
    let seed = await bip39.mnemonicToSeed(mnemonic);
    console.log("seed", seed);

    //3.通过hdkey将seed生成HD Wallet（分层钱包）
    let hdWallet = hdkey.fromMasterSeed(seed);
    //4.生成钱包中在m/44'/60'/0'/0/i路径的keypair
    let keypair = hdWallet.derivePath("m/44'/60'/0'/0/1");
    console.log(keypair);
    // 获取钱包对象
    let wallet = keypair.getWallet();
    // 获取钱包地址
    let lowerCaseAddress = wallet.getAddressString();
    // 获取钱包校验地址
    let CheckSumAddress = wallet.getChecksumAddressString();
    // 获取私钥
    let prikey = wallet.getPrivateKey().toString("hex");
    console.log("钱包地址",lowerCaseAddress)
    console.log("检验钱包地址",CheckSumAddress)
    console.log("私钥",prikey)

    // 用户输入密码 产生keystore
    createKeyStore(wallet)

  }
  // 通过助记词导入钱包
  async function importWalletByMemonic() {
    let mnemonic = prompt("请输入助记词")

    // let mnemonic =
    //   "world average stock sound age desert laugh fashion ecology museum museum better";
    // 将助记词转化为 加密 种子
    let seed = await bip39.mnemonicToSeed(mnemonic);
    console.log("seed", seed);

    //3.通过hdkey将seed生成HD Wallet（分层钱包）
    let hdWallet = hdkey.fromMasterSeed(seed);
    //4.生成钱包中在m/44'/60'/0'/0/i路径的keypair
    let keypair = hdWallet.derivePath("m/44'/60'/0'/0/1");
    console.log(keypair);
    // 获取钱包对象
    let wallet = keypair.getWallet();
    // 获取钱包地址
    let lowerCaseAddress = wallet.getAddressString();
    // 获取钱包校验地址
    let CheckSumAddress = wallet.getChecksumAddressString();
    // 获取私钥
    let prikey = wallet.getPrivateKey().toString("hex");
    console.log("钱包地址",lowerCaseAddress)
    console.log("检验钱包地址",CheckSumAddress)
    console.log("私钥",prikey)

    createKeyStore(wallet)
  }

  async function createKeyStore(wallet) {
    const pass = prompt("请输入密码")
    // 钱包产生keystore的方法
    const keystore = await wallet.toV3(pass);
    localStorage.setItem("keystore",JSON.stringify(keystore))
    console.log(keystore)

  }

  function importWalletByKeyStore() {
    // const  keyStore = localStorage.getItem("keystore")
    const  keyStore = prompt("请输入keystore")
    const pass = prompt("请输入密码")
    const res  = web3.eth.accounts.decrypt(keyStore, pass);
    console.log(res)
  }

  function exportPrivatekey() {
    const pass = prompt("请输入密码")
    const  keyStore = localStorage.getItem("keystore")
    const res  = web3.eth.accounts.decrypt(keyStore, pass);
    console.log(typeof res, res.privateKey)
    alert("您的私钥是:"+res.privateKey)
  }
  function importWalletByPrivateKey() {
    let privatekey=new Buffer( prompt("请输入私钥"), 'hex' )
    console.log(ethwallet)
    let wallet = ethwallet.fromPrivateKey(privatekey)
    const address = wallet.getAddressString();
    console.log(address)
  }

  //获取代币信息
  async function getCoinInfo() {
    // 智能合约对象.methods.[abi里的方法].call()
    const name = await dogContract.methods.name().call();
    const symbol = await dogContract.methods.symbol().call();
    const banlance = await dogContract.methods.balanceOf(address).call();
    setDogInfo({ name,symbol,banlance})

  }

  async function getEthInfo() {
    const num = await web3.eth.getBalance(address)
    setEthNum(num)
  }
// 狗币转账
  // function sendDog() {
  //   const toAddress = addressRef.current.value
  //   const money =web3.utils.toWei( moneyRef.current.value)
  //   dogContract.methods
  //   .transfer(toAddress, money)
  //   .send({
  //     from: address,
  //   })
  //   .on("receipt", function (receipt) {
  //     console.log("交易成功");
  //     console.log(receipt);
  //   });
  // }
  // 狗币转账1
  async function sendDog() {
    const toAddress = addressRef.current.value
    // const money =web3.utils.toWei( moneyRef.current.value)
    const money =moneyRef.current.value
    console.log(toAddress,money, typeof money)
    // 构建代币的转账数据
    const contractData = await dogContract.methods.transfer(toAddress,money).encodeABI()
    console.log(contractData)
    // return 
    const privateKey = prompt("请输入转账私钥")
    /********** 构建转账参数  ***********/
    // 获取账户交易次数
    let nonce = await web3.eth.getTransactionCount(address);
    // 获取预计转账gas费
    let gasPrice = await web3.eth.getGasPrice();
    console.log("转账的gas费用价格", gasPrice);
    // 转账金额以wei为单位
    var rawTx = {
      from: address, // 转出的地
      nonce: nonce, //获取账户的交易次数
      gasPrice: gasPrice,
      to:"0x7e9eB98374A11C5062056d86E5fe8e39B11E1641", // 接受方的地址 如果是erc20  应该智能合约的部署地址
      value: 0, // 这个value 指的是eth转账
      data: contractData, //转Token代币会用到的一个字段
    };
    /**********将转账参数进行加密处理***********/ 
      // 将私钥去除“ox”后进行hex转化

      var key = new Buffer(privateKey.slice(2), "hex");
      //需要将交易的数据进行预估gas计算，然后将gas值设置到数据参数中
      let gas = await web3.eth.estimateGas(rawTx);
      rawTx.gas = gas;
    //  // 通过 ethereumjs-tx 实现私钥加密
      var tx = new Tx(rawTx);
      //通过16进制的私钥尽心加密
      tx.sign(key);
      // 获取交易的hash
      var serializedTx = tx.serialize();
      console.log("交易hash",serializedTx)
      // 将交易hash 发送到区块链环境中
      web3.eth
      .sendSignedTransaction("0x" + serializedTx.toString("hex"))
      .on("transactionHash", (txid) => {
        // 将数据发送到区块链环境中
        console.log("交易成功,请在区块链浏览器查看");
        console.log("交易id", txid);
        console.log(`https://goerli.etherscan.io/tx/${txid}`);
      })
      .on('receipt', (ret)=>{console.log('receipt')})
      .on('confirmation', (ret)=>{
        // 区块确认
        console.log("confirmation")
        getCoinInfo();
      })
      .on("error", (err) => {
        console.log("error:" + err);
      });
  }

    // eth转账
    async function sendToken() {
      const toAddress = addressRef.current.value
      const money =web3.utils.toWei( moneyRef.current.value)
      const privateKey = prompt("请输入转账私钥")
      /********** 构建转账参数  ***********/
      // 获取账户交易次数
      let nonce = await web3.eth.getTransactionCount(address);
      // 获取预计转账gas费
      let gasPrice = await web3.eth.getGasPrice();
      console.log("转账的gas费用价格", gasPrice);
      // 转账金额以wei为单位

      var rawTx = {
        from: address, // 转出的地
        nonce: nonce, //获取账户的交易次数
        gasPrice: gasPrice,
        to: toAddress, // 接受方的地址
        value: money,
        data: "0x00", //转Token代币会用到的一个字段
      };
      /**********将转账参数进行加密处理***********/ 
        // 将私钥去除“ox”后进行hex转化
  
        var key = new Buffer(privateKey.slice(2), "hex");
        //需要将交易的数据进行预估gas计算，然后将gas值设置到数据参数中
        let gas = await web3.eth.estimateGas(rawTx);
        rawTx.gas = gas;
      //  // 通过 ethereumjs-tx 实现私钥加密
        var tx = new Tx(rawTx);
        //通过16进制的私钥尽心加密
        tx.sign(key);
        // 获取交易的hash
        var serializedTx = tx.serialize();
        console.log("交易hash",serializedTx)
        // 将交易hash 发送到区块链环境中
        web3.eth
        .sendSignedTransaction("0x" + serializedTx.toString("hex"))
        .on("transactionHash", (txid) => {
          // 将数据发送到区块链环境中
          console.log("交易成功,请在区块链浏览器查看");
          console.log("交易id", txid);
          console.log(`https://goerli.etherscan.io/tx/${txid}`);
        })
        .on('receipt', (ret)=>{console.log('receipt')})
        .on('confirmation', (ret)=>{
          // 区块确认
          console.log("confirmation")
        })
        .on("error", (err) => {
          console.log("error:" + err);
        });
        
    }
  
  useEffect(() => {
    getCoinInfo();
    getEthInfo();
  },[])

  return (
    <div className="App">
      <h1> 去中心化eth钱包项目</h1>
      <button onClick={createWalletByMemonic}>创建钱包 - 助记词</button>
      <button onClick={importWalletByMemonic}>导入钱包 - 助记词</button>
      <button onClick={importWalletByKeyStore}>通过keystore - 解锁账户</button>
      <button onClick={exportPrivatekey}>导出私钥</button>
      <button onClick={importWalletByPrivateKey}>通过私钥导入钱包</button>
      <hr />
      地址: {address}
      <hr />
      私钥: { privateKey }
      <hr />
      助记词：  "world average stock sound age desert laugh fashion ecology museum museum better"
      <h3>eth信息</h3>
       eth: {ethNum}
      <h3>代币信息</h3>
      <p>名称: {dogInfo.name}</p>
      <p>标识: {dogInfo.symbol}</p>
      <p>余额: {dogInfo.banlance}</p>
      <hr />
      收款地址: <input type="text" ref={addressRef} defaultValue="0xE251ddBe6191594922bfd3d338529EC9C613eB67" /><br/>
      转账金额: <input type="text" ref={moneyRef}  defaultValue="100"/>
      <button onClick={sendDog}>转账-dog</button>
      <button onClick={sendToken}>转账-eth</button>
    </div>
  );
}

export default App;

/*
钱包地址 0xb7d9f8ec2f604920fc85f60de48b6aab2b9b0af0
App.js:42 检验钱包地址 0xb7d9f8Ec2F604920fc85f60de48b6Aab2b9B0af0
App.js:43 私钥 8b75fc7e94f0b8829375b00d97a78fbbdf106be8ffc3a2db7fa15390148390a5

*/