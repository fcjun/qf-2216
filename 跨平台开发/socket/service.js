const WebSocket = require("ws");
// 创建websocket 服务器
const ws = new WebSocket.Server({ port: 8080 });
// 保存客户端
let clients = {};
let clientName = 0;
// 等待客户端链接
ws.on("connection", (client) => {
  console.log("客户端已连接");
  // client.name = ++clientName;
  // clients[client.name] = client;
  // client.send(11233)
  // 接受来自客户端的消息
  client.on('message', (msg) => {
    console.log("来自客户端的消息")
    const { type, value } = JSON.parse(msg.toString())
    switch (type) {
      case "login":
        // 登录
        client.name = value
        clients[client.name] = client;
        break;
      case "msg":
          // 登录
         console.log("来自",client.name,value)
         broadcast(client, value)
      default:
        break;
    }

  })

});

// let clients = {}
// let clientName = 0

// ws.on('connection', (client) => {
//   client.name = ++clientName
//   clients[client.name] = client

//   client.on('message', (msg) => {
//     broadcast(client, msg)
//   })

//   client.on('close', () => {
//     delete clients[client.name]
//     console.log(client.name + ' 离开了~')
//   })
// })

function broadcast(client, msg) {
  for (var key in clients) {
    if(key !== client.name) {
      clients[key].send(client.name + ' 说：' + msg)
    }
  }
}
