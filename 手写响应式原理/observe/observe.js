import Observer from './observer'
// 给数据添加响应式，不是递归添加 只添加最外面一层
// 给每一个级数据 通过__ob__属性添加observer的实例
export default function  observe (value) {
  if (typeof value != 'object') return;
  var ob;
  // console.log('observe',value.__ob__)
  //在每对象想保存Observer实例
  if(typeof value.__ob__ !== 'undefined') {
    ob = value.__ob__;
  } else {
    // 
    ob = new Observer(value)
  }
  return ob;
}