var uid = 0
// 给每一个dep创建一个uid 便于区分
// dep 类就是一个发布订阅模式 
// 每一个observer实例 都要保存一个dep
export default class Dep {
  constructor() {
    this.id = uid++; // 区分dep的实例对象
    // 用数组保存订阅者 subscribe
    this.subs = [];
  }
  addSub(sub) {
    // 添加订阅者
    this.subs.push(sub)
  }
  depend() {
    // 依赖收集
    // Dep.target 就是依赖 watcher 
    // 仅仅只是找了一个地方保存一个数据 
    // window.target
    if(Dep.target) {
      this.addSub(Dep.target)
    }
  }
  notify() {
    // 通知方法
    console.log('notify', this.subs)
    const subs = this.subs.slice();
    for (let index = 0; index < subs.length; index++) {
      // 执行watcher update执行一遍
      subs[index].update()
    }
  }
}