import defineReactive from './defineReactive'
import arrayMethods from './array';
import { def } from './utils';
import observe from './observe';
import Dep from './Dep'
export default class Observer {
  constructor(value) {
    // console.log("这里是observer的构造函数", value);
    // 每一个实例都要保存一个dep 
    this.dep = new Dep()
    // 给当前的数据添加 __ob__ 的属性 目的是保存observe实例
    def(value,"__ob__",this,false)
    // 判断value是否是一个对象

    if(Array.isArray(value)) {
       // 数组的处理  
      // [1,2,3,[4,5,6]]
      // [4,5,6]
      this.observeArray(value)
    } else if(typeof value === 'object') {
      this.walk(value)
    }
  }
  // 对对象属性做数据劫持
  walk(value) {
    for (const key in value) {
      defineReactive(value, key, value[key]);
    }
  }
  // 对数组做数据劫持
  observeArray(arr) { 
     // 看到数组强制的重写原型
     [1,2,3,[4,5,6]]
    Object.setPrototypeOf(arr,arrayMethods)
    // console.log('对数组的处理',arr)
    for (let index = 0; index < arr.length; index++) {
      observe(arr[index])
      
    }
  }
}
