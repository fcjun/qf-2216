import { def } from "./utils";
// 重写数组的方法 调用重写的方法 内部可以进行操作 在调用原先的方法
const arr = ["push", "pop", "shift", "unshift", "splice", "sort", "reverse"];

// 获取数组的原型
const arrayPrototype = Array.prototype;
// 创建一个新的对象原型和数组完全一致
const arrayMethods = Object.create(arrayPrototype)


arr.forEach((methodsName) => {
  // 保存数组的原方法
   const originMethod = arrayMethods[methodsName]
  //  console.log('originMethod',originMethod)
   def(arrayMethods,methodsName,function() {
    //  console.log('重写的方法',methodsName,arguments)
     // 在重写的方法里发送通知告知数据修改
    const ob = this.__ob__; // 获取保存过的observer实例
    console.log('数组改变了')
    ob.dep.notify();
     // 调用保存的原方法添加数据
     originMethod.apply(this,arguments)
     // 在数组里添加的数组也应该具有响应式
     const args = [...arguments]
    
     let inserted = []; // 数组添加的信息
     switch (methodsName) {
       case "push":
         inserted = args;
         break;
       case "unshift":
         inserted = args;
         break;
       case "splice":
         inserted = args.slice(2);
         break;
       default:
         break;
     }
     if (inserted) {
       // 处理通过push unshift 添加的数组
       ob.observeArray(inserted);
     }

    // 新添加的数据也需要响应式
   })
})
//重写的数组原型
// console.log(arrayMethods)
export default arrayMethods