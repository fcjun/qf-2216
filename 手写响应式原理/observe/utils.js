// 给一个对象添加属性
export const def = function(obj,key,value,enumerable) {
  Object.defineProperty(obj,key, {
    value, // 默认值
    enumerable, // 是否可枚举 -> 能不能允许遍历
    writable: true, // 是否允许可以改动
    configurable: true // 是否允许删除
  })
}