import observe from "./observe.js"
import Dep from './Dep'
export default function defineReactive(data,key,value) {
  // console.log('defineReactive',key,value)
  // value 值可以是数组 普通的值 对象
  // observe 在内部以及对对象做了判断 直接使用
  // 如果value是一个对象 对象里的属性也需要响应式 observe 处理
  const dep  = new Dep();
  observe(value)
  Object.defineProperty(data,key, {
    get() {
      // console.log('取值')
      // Dep.target 保存的是watcher 依赖 
      // 判断是否存在依赖 如果存在依赖才收集依赖
      console.log('getters')
      if(Dep.target) {
        dep.depend();
      }
      return value
    },
    set(setVal) {
      console.log('属性改变了发起notify') 
      value = setVal 
      // 属性改变之后 发起通知
      dep.notify();
    }
  })
} 
// 给属性添加getter 和 setter