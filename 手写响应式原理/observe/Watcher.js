var uid = 0;
import Dep from "./Dep";
export default class Watcher {
  constructor(target, expression, cb) {
    this.id = uid++;
    this.target = target;
    this.getter = parsePath(expression);
    // 传递一个表达式 返回一个获取数据的函数
    // 用来获取表达式对应的值
    // console.log(this.getter(this.target))
    this.value = this.get();
    this.cb = cb; //一个回调函数
  }
  update() {
    this.run();
  }
  run() {
    this.getAndInvoke(this.cb);
  }
  getAndInvoke(cb) {
    // 获取修改之前的数据和最新的数据做对比  通知页面更新
    // 获取最新的数据
    const value = this.get();
    if (value !== this.value || typeof value === "object") {
      // 老的数据
      const oldValue = this.value;
      // 将新数据保存起来
      this.value = value;
      // cb();
      cb.call(this.target, value, oldValue);
    }
  }
  get() {
    // 将wather的实例保存在dep.targer
    // 只有在wather实例化的时候 才有dep.target
    // 只有先实例watcher 才能收集依赖
    Dep.target = this;
    const data = this.target;
    var value;
    try {
      value = this.getter(data);
    } finally {
      Dep.target = null;
    }

    return value;
  }
}
// 通过表达式快速的从数据源获取数据
function parsePath(str) {
  var segments = str.split(".");
  return (obj) => {
    for (let index = 0; index < segments.length; index++) {
      if (!obj) return;
      obj = obj[segments[index]];
    }
    return obj;
  };
}
// [info, hehe, xixi]
// {
//   name: "韩梅梅",
//   age: 16,
//   info: { sex: 1, hehe: { xixi: 789 } },
//   arr: [1, 2, 3, 4],
// };
//  { sex: 1, hehe: { xixi: 789 } }
//  { xixi: 789 }
